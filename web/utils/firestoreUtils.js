export const toArrayWithIds = (querySnapshot) => [...querySnapshot.docs.map((doc) => ({ ...doc.data(), id: doc.id }))];

export const toObjectWithId = (querySnapshot) => ({ ...querySnapshot.data(), id: querySnapshot.id });

