import Papa from 'papaparse';
import moment from 'moment';
import { toast } from 'react-toastify';

export const toArrayWithIds = (querySnapshot, isGeo) => {
  if (isGeo) return [...querySnapshot.docs.map((doc) => ({ ...doc.data().d, id: doc.id }))];
  return [...querySnapshot.docs.map((doc) => ({ ...doc.data(), id: doc.id }))];
};

export const toObjectWithId = (querySnapshot, isGeo) => {
  if (isGeo) return ({ ...querySnapshot.data().d, id: querySnapshot.id });
  return ({ ...querySnapshot.data(), id: querySnapshot.id });
};

export const downloadFile = (name, content) => {
  const element = document.createElement('A');
  element.href = content;
  element.download = name;
  element.click();
};

export const callToast = (type, content) => {
  toast[type](content, { position: toast.POSITION.TOP_RIGHT });
};

export const csvFromJs = (content) => {
  return encodeURI('data:text/csv;charset=utf-8,' + Papa.unparse(content));
};

export function getStatusLabel(status) {
  let label = '';
  switch (status) {
    case 'waiting':
      label = 'Aguardando'
      break;
    case 'ready':
      label = 'Liberada';
      break;
      case 'ongoing':
      label = 'Em trânsito'
      break;
    case 'finished':
      label = 'Concluída'
      break;
    default:
      label = 'Aguardando'
      break;
  }
  return label;
}

export function getRecordTypeLabel(type) {
  let label = '';
  switch (type) {
    case 'note':
      label = 'Anotação';
      break
    case 'damage':
      label = 'Avaria';
      break
    case 'receipt':
      label = 'Comprovante';
      break
    case 'document':
      label = 'Documento';
      break
    case 'event':
      label = 'Registro';
      break
    case 'reference':
      label = 'Referência';
      break
    case 'conclusion':
      label = 'Registro';
      break
  }
  return label;
}

export const recordsCsvObject = (item) => {
  const object = {};
  object['Tipo'] = getRecordTypeLabel(item.type);
  object['Descrição'] = item.description;
  object['Criado por'] = item.createdBy.displayName || item.createdBy.email;
  object['Data'] = moment.unix(item.createdAt._seconds || item.createdAt.seconds).format('DD/MM/YYYY HH:mm');
  object['Vínculo'] = item.linkedTo;
  object['Anexo'] = item.attachmentUrl;
  return object;
};

export const billOfLadingsCsvObject = (item) => {
  const object = {};
  object['CTe'] = item.billOfLading;
  object['Embarcador'] = item.shipper.cnpj;
  object['Cliente'] = item.client.cnpj;
  object['Situação'] = getStatusLabel(item.status);
  object['Etiqueta'] = item.tag;
  object['Motorista']= (item.driver && item.driver.name) || 'Motorista não informado';
  object['Entrega'] = (
    (item.finishedAt && moment.unix(item.finishedAt._seconds)) ||
    (item.estimation && moment.unix(item.estimation._seconds)) ||
    'Sem data de entrega'
  );
  if (object['Entrega'] !== 'Sem data de entrega') object['Entrega'] = moment(object['Entrega']).format('DD/MM/YYYY');
  return object;
};

export const formatAddress = ({ cep, city, complement, neighborhood, number, description, uf }) => {
  return `${
    description || ''
  }, ${
    number || ''
  }${
    complement ? ' (' + complement + ')' : ''
  } - Bairro ${
    neighborhood || ''
  }, ${
    cep || ''
  } - ${
    city || ''
  } - ${
    uf || ''
  }`;
}
