import React from 'react';
import ReactDOM from 'react-dom';

import ReactGA from 'react-ga';

import 'shared-components/styles.scss';

import 'react-toastify/dist/ReactToastify.css';

import App from 'core/App';

ReactGA.initialize('UA-144833193-1');

window.newTabKeyIsPressed = false;

document.body.addEventListener('keydown', (e) => {
  window.newTabKeyIsPressed = e.ctrlKey || e.key === 'Meta';
});

document.body.addEventListener('keyup', (e) => {
  window.newTabKeyIsPressed = false;
});

ReactDOM.render(<App />, document.getElementById('root'));
