import { combineReducers } from 'redux';

// Reducers
import main from 'containers/MainContainer/reducer';
import delivery from 'containers/Delivery/reducer';
import deliveries from 'containers/Deliveries/reducer';
import drivers from 'containers/Drivers/reducer';
import shippers from 'containers/Shippers/reducer';
import clients from 'containers/Clients/reducer';

// Public reducers
import deliveryPublic from 'containers/DeliveryPublic/reducer';

const reducers = combineReducers({
  main,
  delivery,
  deliveries,
  drivers,
  shippers,
  clients,

  deliveryPublic
});

export default (state, action) => {
  // Reset all states when users logging out or token is missing
  if (action.type === 'APP_DESTROY_SESSION') state = undefined;
  return reducers(state, action);
};
