import { applyMiddleware, createStore, compose } from 'redux';
import 'regenerator-runtime/runtime';

import createSagaMiddleware from 'redux-saga';

import thunk from 'redux-thunk';

import reducers from './reducers';
import sagas from './sagas';

const sagaMiddleware = createSagaMiddleware();

const middleware = applyMiddleware(thunk, sagaMiddleware);
const store = createStore(reducers, compose(middleware));

sagaMiddleware.run(sagas);

export default store;
