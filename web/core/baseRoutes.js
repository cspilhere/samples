// Components
import { RouteWrapper } from 'utils/RouterUtils';

// Authentication routes
import authRoutes from 'containers/AuthUser/routes';

// Main routes with a private flow inside MainContainer
import MainContainer from 'containers/MainContainer';
import mainRoutes from 'containers/MainContainer/routes';
import DeliveryPublic from 'containers/DeliveryPublic';

const routes = [{
  component: RouteWrapper,
  routes: [{
    name: 'Auth',
    path: '/auth',
    component: RouteWrapper,
    routes: authRoutes
  }, {
    name: 'Public',
    path: '/public/:token',
    exact: true,
    component: DeliveryPublic
  }, {
    name: 'App',
    path: '/',
    component: MainContainer,
    routes: mainRoutes
  }]
}];

export default routes;
