import { all, fork } from 'redux-saga/effects';

import deliveries from 'containers/Deliveries/sagas';
import shippers from 'containers/Shippers/sagas';
import clients from 'containers/Clients/sagas';
import drivers from 'containers/Drivers/sagas';
import delivery from 'containers/Delivery/sagas';

// Public
import deliveryPublic from 'containers/DeliveryPublic/sagas';

export default function* root() {
  yield all([
    fork(deliveries),
    fork(shippers),
    fork(clients),
    fork(drivers),
    fork(delivery),

    fork(deliveryPublic)
  ])
};
