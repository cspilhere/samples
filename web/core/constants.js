export const FIREBASE_CONFIG = (process.env.NODE_ENV !== 'production') ? {
  apiKey: "AIzaSyA5KdJ-B9p1FlXEPZt_GlaTo6ALZAye3NQ",
  authDomain: "wac-teste123.firebaseapp.com",
  databaseURL: "https://wac-teste123.firebaseio.com",
  projectId: "wac-teste123",
  storageBucket: "gs://wac-teste123.appspot.com",
  messagingSenderId: "411712745510",
} : {
  apiKey: "AIzaSyDTjbfSzcUC1R2Ve_r0gpONWK3UKiiRfHA",
  authDomain: "levalog.firebaseapp.com",
  databaseURL: "https://levalog.firebaseio.com",
  projectId: "levalog",
  storageBucket: "levalog.appspot.com",
  messagingSenderId: "596092604591",
  appId: "1:596092604591:web:45079e23955772be"
};
