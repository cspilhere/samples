import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';

import Rollbar from "rollbar";

// import { hot } from 'react-hot-loader';

import AuthManager from 'containers/AuthManager';

import { ChildRoutes } from 'utils/RouterUtils';
import baseRoutes from 'core/baseRoutes';
import store from 'core/store';

import { createBrowserHistory } from 'history';
import ReactGA from 'react-ga';

const history = createBrowserHistory();

const unlisten = history.listen((location, action) => {
	ReactGA.set({ page: location.pathname });
  ReactGA.pageview(location.pathname);
  if (Intercom) Intercom('update');
});

const App = (props) => {
  useEffect(() => {
    new Rollbar({
      accessToken: '0fc5f287836546bdab2de75147b1f991',
      captureUncaught: true,
      captureUnhandledRejections: true,
      payload: {
        environment: process.env.NODE_ENV
      }
    });
    ReactGA.pageview(window.location.pathname);
    return unlisten;
  }, []);
  const basename = document.querySelector('base') ? document.querySelector('base').getAttribute('href') : null;
  return (
    <AuthManager>
      <Provider store={store}>
        <Router basename={basename || ''} history={history}>
          <ChildRoutes {...props} routes={baseRoutes} />
        </Router>
      </Provider>
    </AuthManager>
  );
};

let exportedApp = App;

// Use hot loader only in development
// if (process.env.NODE_ENV !== 'production') exportedApp = hot(module)(App);

export default exportedApp;
