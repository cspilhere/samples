import React from 'react';

import { FlexBar, Select } from 'shared-components';

import InlineFilter from 'components/custom/InlineFilter';

const Filter = ({ onFilterChange }) => {
  return (
    <InlineFilter
      name="shippers"
      placeholder="Nome, Documento, Telefone..."
      onChange={onFilterChange}
    />
  );
};

export default Filter;
