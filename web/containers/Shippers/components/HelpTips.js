import React from 'react';

import HelpTipsContainer from 'components/custom/HelpTipsContainer';

const HelpTips = ({ isVisible, onHide }) => {
  return (
    <HelpTipsContainer
      isVisible={isVisible}
      onHide={onHide}
      name="helpTips-Shippers"
      items={[
        'Cadastre e organize seus Embarcadores.',
        'Descubra quem faz mais entregas com a sua transportadora.',
        'Convide usuários para acompanhar as entregas do Embarcador.',
        'Agilize a captura e troca de informações.'
      ]}
      tourId={55825}
    />
  );
};

export default HelpTips;
