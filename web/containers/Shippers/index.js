import React from 'react';

import { connect } from 'react-redux';
import * as actions from './actions';

import { Text } from 'shared-components';
import ContentHeader from './components/ContentHeader';
import Table from './components/Table';
import HelpTips from './components/HelpTips';

import BaseLayout, { baseLayoutWrapper } from 'components/custom/baseLayout/BaseLayout';
import BaseHeader from 'components/custom/baseLayout/BaseHeader';

import { MainContext } from 'containers/MainContainer';

import CreateShipper from 'features/CreateShipper';
import UpdateShipper from 'features/UpdateShipper';

class Shippers extends React.Component {

  static contextType = MainContext;

  state = {
    showHelp: false
  }

  render() {

    const { reducer } = this.props;
    const { data, loading, isFirstLoading } = reducer;

    return (
      <BaseLayout
        baseHeader={(
          <BaseHeader
            breadcrumb={[{ name: 'Embarcadores', title: 'Embarcadores', pathname: '/' }]}
            onClickHelp={() => {
              this.setState({ showHelp: true });
            }}
          />
        )}
        contentHeader={(
          <ContentHeader
            onFilterChange={this.filter}
            openFeature={this.create}>
            <Text isSmall hasGray2Color>
              {loading && isFirstLoading && 'Carregando...'}
              {data && !isFirstLoading && `Mostrando ${data.length} embarcador${data.length == 1 ? '' : 'es'}`}
            </Text>
          </ContentHeader>
        )}>

        <HelpTips isVisible={this.state.showHelp} onHide={() => this.setState({ showHelp: false })} />

        <Table
          {...this.props}
          onRowClick={this.update}
          onRequestToRemove={this.remove}
        />
      </BaseLayout>
    );
  }

  remove = (shipper) => {
    this.props.removeShipper(shipper.id)
    .catch(() => this.props.callToast('error', 'Ocorreu um erro! Tente novamente.'));
  }

  update = (event, rowKey) => {
    this.props.openModal({
      title: 'Atualizar embarcador',
      component: (
        <UpdateShipper
          defaultValues={this.props.reducer.data[rowKey]}
          onClickToCancel={this.props.closeModal}
          onSuccess={this.props.closeModal}
        />
      )
    });
  }

  create = () => {
    this.props.openModal({
      title: 'Novo embarcador',
      disableBackgroundClick: true,
      component: (
        <CreateShipper
          onClickToCancel={this.props.closeModal}
          onSuccess={this.props.closeModal}
        />
      )
    });
  }

  filter = (filter) => {
    this.props.fetchShippers(filter);
  }

};

export default connect((store) => ({
  reducer: store.shippers
}), { ...actions })(baseLayoutWrapper(Shippers));
