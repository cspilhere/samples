import { eventChannel } from 'redux-saga';
import { takeLatest, put, take, call, all } from 'redux-saga/effects';

import { getAllStakeholders, removeStakeholder } from 'services/firestore';

function createChannel(collection, filter) {
  return eventChannel((emit) => {
    getAllStakeholders(collection, filter, emit)
    .catch((error) => emit(new Error(error)));
    return () => {};
  });
};

function* fetchShippers(action) {
  const channel = yield call(createChannel, 'shippers', action.filter);
  while (true) {
    try {
      const response = yield take(channel);
      yield put({ type: 'FETCH_SHIPPERS_SUCCESS', payload: response });
    } catch (error) {
      console.log(error);
    };
  };
};

function* removeShipper(action) {
  try {
    const response = yield call(removeStakeholder, 'shippers', action.id);
    yield put({ type: 'REMOVE_SHIPPER_SUCCESS', payload: response });
    action.resolve(response);
  } catch (error) {
    console.log(error);
    action.reject(error);
  }
};

export default function* root() {
  yield all([
    takeLatest('FETCH_SHIPPERS', fetchShippers),
    takeLatest('REMOVE_SHIPPER', removeShipper),
  ]);
};
