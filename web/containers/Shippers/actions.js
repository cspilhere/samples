export const fetchShippers = (filter) => ({ type: 'FETCH_SHIPPERS', filter });

export const removeShipper = (id) => (dispatch) => (
  new Promise((resolve, reject) => (
    dispatch({ type: 'REMOVE_SHIPPER', id, resolve, reject })
  ))
);
