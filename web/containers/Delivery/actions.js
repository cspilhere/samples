import moment from 'moment';

import { downloadFile, csvFromJs, recordsCsvObject } from 'utils/utils';

/**
 * Delivery
 */

export const fetchDelivery = (id) => ({ type: 'FETCH_DELIVERY', id });

export const fetchDeliveryDetails = (id) => ({ type: 'FETCH_DELIVERY_DETAILS', id });

export const updateDeliveryLabel = (id, value) => (
  { type: 'UPDATE_DELIVERY_LABEL', id, value }
);

export const removeDelivery = (id, success, error) => (
  { type: 'REMOVE_DELIVERY', id, success, error }
);

export const releaseDelivery = (id, success, error) => (
  { type: 'RELEASE_DELIVERY', id, success, error }
);

export const finishDelivery = (id, success, error) => (
  { type: 'FINISH_DELIVERY', id, success, error }
);

export const clearDeliveryData = () => (dispatch, store) => {
  dispatch({ type: 'CLEAR_DELIVERY_STATE' });
};

/**
 * Records
 */

export const fetchRecords = (deliveryId) => ({ type: 'FETCH_RECORDS', deliveryId });

export const createRecord = (deliveryId, body) => (
  { type: 'CREATE_RECORD', deliveryId, body }
);

export const removeRecord = (deliveryId, recordId) => (
  { type: 'REMOVE_RECORD', deliveryId, recordId }
);
