const INITIAL_STATE = {
  data: {},
  records: [],
  loading: false,
  error: false,

  loadingDetails: false,
  details: null
};

export default (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {

    case 'FETCH_DELIVERY':
      return state = {
        ...state,
        ...{ loading: true }
      };

    case 'FETCH_DELIVERY_SUCCESS':
      return state = {
        ...state,
        ...{ data: action.payload, loading: false, error: false }
      };

    case 'FETCH_DELIVERY_DETAILS':
      return state = {
        ...state,
        loadingDetails: true
      };

    case 'FETCH_DELIVERY_DETAILS_SUCCESS':
      return state = {
        ...state,
        loadingDetails: false,
        details: action.payload
      };

    case 'FETCH_RECORDS':
      return state = {
        ...state,
        ...{ loading: true }
      };
    case 'FETCH_RECORDS_SUCCESS':
      return state = {
        ...state,
        ...{ records: action.payload, loading: false, error: false }
      };

    case 'CLEAR_DELIVERY_STATE':
      return state = INITIAL_STATE;

    default:
      return state;
  }
};
