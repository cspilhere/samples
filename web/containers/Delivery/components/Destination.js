import React from 'react';

import { Button } from 'shared-components';

import { Box, Text, Icon } from 'components';

const Destination = (props) => {

  const { data, isFinished, onClickToUpdate, editDisabled } = props;

  let content = null;

  if (data && data.address) {
    content = (
      <>
        <div style={{ flex: 1, overflow: 'hidden', paddingRight: 12, marginBottom: -6, alignItems: 'center' }}>
          <Text truncated>
            <Icon name="fas fa-flag-checkered" /> <strong>Destino:</strong> {data.address}
          </Text>
        </div>
        {!editDisabled && (
          <Button
            id="delivery-update-destination"
            isSmall
            disabled={isFinished}
            onClick={onClickToUpdate}>
            <Icon name="far fa-map-marker-alt" />
          </Button>
        )}
      </>
    );
  }

  return (
    <Box fullWidth elevated inline>
      {content || (
        <Button
          id="delivery-add-destination"
          isSmall
          isFullWidth
          disabled={isFinished || editDisabled}
          isFlat
          onClick={onClickToUpdate}>
          Endereço de entrega
        </Button>
      )}
    </Box>
  );

};

export default Destination;
