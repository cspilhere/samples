import React from 'react';

import { Heading } from 'shared-components';

import { Box, Spacer } from 'components';

const EntityCard = (props) => {

  const {
    name = 'Company and Company CO',
    title
  } = props;

  const hasInfo = props.name ? '' : 'mock-text';

  return (
    <Box bordered>
      <div style={{ width: '100%', textAlign: 'center', overflow: 'hidden' }}>
        <Heading is8 hasGray1Color hasNoMargin isAlignedCenter>{title}</Heading>
        <Spacer horizontal size={2} />
        <Heading is7 hasNoMargin isTruncated title={name} isAlignedCenter>
          <span className={hasInfo}>{name}</span>
        </Heading>
      </div>
    </Box>
  );

};

export default EntityCard;
