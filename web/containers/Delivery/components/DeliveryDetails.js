import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { Box, ListItem, Title, Text, Icon, Spacer, RowWrapper, Wrapper, Fill } from 'components';

import EntityWithAddress from 'components/custom/cards/EntityWithAddress';
import GenericDate from 'components/custom/cards/GenericDate';
import AddressDescription from 'components/custom/cards/AddressDescription';
import DriverRelation from 'components/custom/cards/DriverRelation';
import PhoneNumber from 'components/custom/cards/PhoneNumber';
import DeliveriesCounter from 'components/custom/cards/DeliveriesCounter';

import { showTakerLabel } from 'services/transformKeysToString';
import { maskSchema } from 'services/masksAndValidators';

const DeliveryDetails = (props) => {

  useEffect(() => {
    if (props.detailsRequest) props.detailsRequest();
    return () => {}
  }, []);

  if (!props.data) return null;

  const {

    showDriverOnPublicDelivery,

    number,
    taker,
    client,
    shipper,
    destination,
    invoices,
    driver,
    createdAt,
    estimation,
    releasedAt,
    finishedAt,
    recordsByType,
    billOfLading
  } = props.data;

  return (
    <Box padding={20}>

      <ListItem>
        <RowWrapper>
          <Text regular><strong>CTe:</strong> {number}</Text>
          <Fill />
          <Text regular><strong>Tomador:</strong> {showTakerLabel(taker)}</Text>
        </RowWrapper>
      </ListItem>

      <hr />

      <ListItem>
        {billOfLading && (
          <>
            <RowWrapper>
              <Text small><strong>Chave:</strong> {billOfLading}</Text>
              <span
                className="inline-link"
                onClick={() => {
                  window.open(`http://www.cte.fazenda.gov.br/portal/consultaRecaptcha.aspx?tipoConsulta=completa&tipoConteudo=mCK/KoCqru0=`, '_blank');
                }}>
                <Icon name="fas fa-sm fa-share-square" />
              </span>
            </RowWrapper>
          </>
        )}
      </ListItem>

      {estimation && (
        <>
          <hr />
          <ListItem>
            <RowWrapper>
              <Icon name="far fa-calendar-day" small />
              <Spacer size={4}/>
              <Text small>Previsão de entrega</Text>
              <Fill />
              {estimation ? (
                <GenericDate content={estimation} datetime />
              ) : (
                <Text small>Não configurado</Text>
              )}
            </RowWrapper>
          </ListItem>
        </>
      )}

      {invoices && invoices.length > 0 && (
        <>
          <hr />
          <ListItem>
            <Title small color="#1A497B">Notas fiscais</Title>
            {invoices.map((item, index) => <Text small key={index}>#{index + 1}: {item}</Text>)}
          </ListItem>
        </>
      )}

      <hr />

      <ListItem>
        <Title small color="#1A497B">Embarcador</Title>
        <EntityWithAddress data={props.data} type="shipper" showDocument contentAsObject disabledTruncate />
        {shipper.users && shipper.users.length > 0 && (
          <>
            <Spacer size={6} />
            <Text small bold color="#1A497B"><Icon name="fas fa-users" size={12} /> Contatos</Text>
            {shipper.users.map((user, index) => {
              return (
                <Text small key={index}><strong>{user.name}</strong> - {user.email} - {maskSchema.phone.resolve(user.phone)}</Text>
              );
            })}
          </>
        )}
      </ListItem>

      <hr />

      <ListItem>
        <Title small color="#1A497B">Cliente</Title>
        <EntityWithAddress data={props.data} type="client" showDocument contentAsObject disabledTruncate />
        {client.users && client.users.length > 0 && (
          <>
            <Spacer size={6} />
            <Text small bold color="#1A497B"><Icon name="fas fa-users" size={12} /> Contatos</Text>
            {client.users.map((user, index) => {
              return (
                <Text small key={index}><strong>{user.name}</strong> - {user.email} - {maskSchema.phone.resolve(user.phone)}</Text>
              );
            })}
          </>
        )}
      </ListItem>

      {driver && showDriverOnPublicDelivery && (
        <>
          <hr />
          <ListItem>
            <Title small color="#1A497B">Motorista</Title>
            <RowWrapper align="flex-start">
              <Wrapper>
                <Text small><strong>{driver.name}</strong> ({maskSchema.cpf.resolve(driver.document)})</Text>
                <PhoneNumber content={driver.phone} />
              </Wrapper>
              <Fill />
              {driver.relation !== undefined && driver.relation !== null && (
                <Wrapper>
                  <DriverRelation content={driver.relation} />
                  <DeliveriesCounter content={driver.deliveries} align="flex-end" />
                </Wrapper>
              )}
            </RowWrapper>
          </ListItem>
        </>
      )}

      {destination && destination.address && (
        <>
          <hr />
          <ListItem>
            <Title small color="#1A497B">Endereço de entrega</Title>
            <AddressDescription content={destination.address} />
          </ListItem>
        </>
      )}

      {/* <hr />

      <ListItem>
        <Title small color="#1A497B">Registros</Title>
        <RowWrapper>
          {Object.keys(recordsByType || {}).map((key, index) => {
            return (
              <div key={index} style={{ width: '25%' }}>
                {key}
              </div>
            );
          })}
        </RowWrapper>
      </ListItem> */}

      <hr />

      <ListItem>
        <Title small color="#1A497B">Histórico</Title>
        {createdAt && (
          <RowWrapper>
            <Icon name="far fa-calendar-edit" small />
            <Spacer size={4}/>
            <Text small>Data de criação</Text>
            <Fill />
            <GenericDate content={createdAt} />
          </RowWrapper>
        )}
        {releasedAt && (
          <RowWrapper>
            <Icon name="far fa-calendar-check" small />
            <Spacer size={4}/>
            <Text small>Data de liberação</Text>
            <Fill />
            <GenericDate content={releasedAt} />
          </RowWrapper>
        )}
        {finishedAt && (
          <RowWrapper>
            <Icon name="far fa-calendar-star" small />
            <Spacer size={4}/>
            <Text small>Data de conclusão</Text>
            <Fill />
            <GenericDate content={finishedAt} />
          </RowWrapper>
        )}
      </ListItem>

    </Box>
  );
};

DeliveryDetails.propTypes = {};

export default DeliveryDetails;
