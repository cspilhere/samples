import React from 'react';

import { FlexBar, Text, Icon, Button } from 'shared-components';

import DeliverySituation from 'components/custom/cards/DeliverySituation';

import { LabelSelector } from 'components';

import { showTakerLabel } from 'services/transformKeysToString';

const DeliveryHeader = (props) => {

  const {
    number,
    taker,
    status,
    invoices = [],
    editDisabled,
    label,
    labels,
    onChangeLabel,
    onClickToOpenDetails,
    hideDetailsButton
  } = props;

  const hasInfo = props.code ? '' : 'mock-text';

  return (
    <div style={{ height: 24, flexShrink: 0 }}>
      <FlexBar specSelector="content-header">
        <FlexBar.Child grow>
          <Text>
            <span className={hasInfo}>
              <strong>CTe:</strong> {number}
            </span>
          </Text>
          <Text>
            <span className={hasInfo}><strong>Tomador:</strong> {showTakerLabel(taker)}</span>
          </Text>
          {invoices.length > 0 && (
            <Text>
              <span className={hasInfo}>
                <strong>NFs:</strong> ({invoices.length})
              </span>
            </Text>
          )}
        </FlexBar.Child>
        {!editDisabled && (
          <FlexBar.Child>
            <LabelSelector
              id="delivery-update-label-select"
              value={label}
              options={labels}
              placeholder="Sem etiqueta"
              color="#F2F2F2"
              onChange={onChangeLabel}
            />
          </FlexBar.Child>
        )}
        {status && (
          <FlexBar.Child>
            <DeliverySituation content={status} large />
          </FlexBar.Child>
        )}
        {!hideDetailsButton && (
          <FlexBar.Child>
            <Button id="delivery-open-details-button" isSmall onClick={onClickToOpenDetails}>
              <Icon name="fas fa-bars" />
            </Button>
          </FlexBar.Child>
        )}
      </FlexBar>
    </div>
  );

};

export default DeliveryHeader;
