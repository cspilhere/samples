import React from 'react';

import HelpTipsContainer from 'components/custom/HelpTipsContainer';

const HelpTips = ({ isVisible, onHide }) => {
  return (
    <HelpTipsContainer
      isVisible={isVisible}
      onHide={onHide}
      name="helpTips-Delivery"
      items={[
        'Acompanhamento completo da entrega!',
        'Crie registros com anexo contendo informações de acompanhamento, comprovantes, documentos, canhotos, avarias, coletas, entre outros.',
        'Localize a posição do Motorista no mapa.',
        'Seu Cliente e Embarcador podem acompanhar e interagir com a entrega em tempo real para obter informações importantes sem precisar ligar para você.'
      ]}
      tourId={55919}
    />
  );
};

export default HelpTips;
