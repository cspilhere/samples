import React from 'react';
import PropTypes from 'prop-types';

import {
  Button,
  Icon,
  FlexBar
} from 'shared-components';

const DeliveryActions = ({ delivery, onClickToCancel, onClickToFinish, onClickToRelease }) => {
  return (
    <FlexBar>
      <FlexBar.Child grow />
      <FlexBar.Child>

        {delivery.status === 'finished' ? (
          <div className="buttons">
            <Button
              id="delivery-remove-button"
              isSmall
              onClick={onClickToCancel}>
              Remover entrega
            </Button>
            <Button
              id="delivery-share-button"
              isSmall
              onClick={() => {
                window.open(`${location.origin}/public/${delivery.token}`, '_blank');
              }}>
              <Icon name="fas fa-share-square" />
            </Button>
          </div>
        ) : (
          <div className="buttons">
            <Button
              id="delivery-cancel-button"
              disabled={delivery.status === 'finished' || !delivery.id}
              isDanger
              isSmall
              onClick={onClickToCancel}>
              Cancelar entrega
            </Button>
            {delivery.isReady ? (
              <Button
                id="delivery-finish-button"
                disabled={delivery.status === 'finished' || !delivery.id}
                isPrimary
                isSmall
                onClick={onClickToFinish}>
                Concluir entrega
              </Button>
            ) : (
              <Button
                id="delivery-release-button"
                disabled={!delivery.id}
                isSuccess
                isSmall
                onClick={onClickToRelease}>
                Liberar entrega
              </Button>
            )}
            <Button
              id="delivery-share-button"
              isSmall
              onClick={() => {
                window.open(`${location.origin}/public/${delivery.token}`, '_blank');
              }}>
              <Icon name="fas fa-share-square" />
            </Button>
          </div>
        )}

      </FlexBar.Child>
    </FlexBar>
  );
};

DeliveryActions.propTypes = {};

export default DeliveryActions;
