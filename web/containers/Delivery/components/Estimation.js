import React from 'react';

import { Icon, Button } from 'shared-components';

import { Box, Spacer, RowWrapper } from 'components';
import EstimatedDate from 'components/custom/cards/EstimatedDate';

const Estimation = (props) => {

  const { content, isFinished, onClickToUpdate, editDisabled } = props;

  return (
    <Box elevated>
      <RowWrapper>
        {content ? (
          <>
            <EstimatedDate content={content} data={{ status: isFinished ? 'finished' : '' }} />
            {!editDisabled && (
              <>
                <Spacer size={15} />
                <Button
                  id="delivery-update-estimation"
                  isSmall
                  disabled={isFinished}
                  onClick={onClickToUpdate}>
                  <Icon name="far fa-cog" />
                </Button>
              </>
            )}
          </>
        ) : (
          <Button
            id="delivery-add-estimation"
            isSmall
            disabled={isFinished || editDisabled}
            isFlat
            onClick={onClickToUpdate}>
            Previsão de entrega
          </Button>
        )}
      </RowWrapper>
    </Box>
  );

};

export default Estimation;
