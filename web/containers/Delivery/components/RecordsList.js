import React from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';

import Scrollbar from 'react-scrollbars-custom';

import { Button } from 'shared-components';

import { Icon, Box } from 'components';

import styled from 'styled-components';

import { showRecordType } from 'services/transformKeysToString';

const Container = styled.div`display: flex; flex: 1; flex-direction: column; overflow: hidden;`;

const Card = styled.div`
  width: 100%;
`;

const CardType = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
`;

const CardTypeIcon = styled.div`
  color: #1A497B;
  text-align: right;
  display: flex;
  align-items: center;
  justify-content: center;
  div {
    cursor: pointer;
    margin-top: -2px;
  }
`;

const CardTypeLabel = styled.div`
  background-color: ${({ color }) => color};
  color: #FFF;
  font-size: 11px;
  border-radius: 10px;
  font-weight: 600;
  padding: 1px 8px;
`;

const CardCreator = styled.div`
  font-size: 11px;
  color: #738694;
  font-weight: normal;
`;

const CardDescription = styled.div`
  font-size: 13px;
  border-bottom: 1px solid #F2F2F2;
  padding-top: 10px;
  padding-bottom: 8px;
  padding-left: 2px;
  padding-right: 2px;
  margin-bottom: 8px;
  color: #5c7080;
  line-height: 1.3;
  font-weight: 600;
`;

const CardTimestamp = styled.div`
  color: #394b59;
  font-size: 12px;
  flex: 1;
  color: ${({ color }) => color};
  font-weight: bold;
  margin-left: 8px;
  margin-top: 2px;
`;

const CardFooter = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  padding-left: 1px;
`;

const RecordsList = ({ onClickToOpen, onClickToRemove, data, user, delivery, children }) => {

  const hasData = data.length > 0;

  return (
    <Container>
      {children}
      <Scrollbar
        trackYProps={{ style: { width: 8, top: 0, height: '100%' } }}
        thumbYProps={{ style: { backgroundColor: '#bfccd6' } }}
        style={{ width: '100%', marginTop: 20, marginBottom: 4 }}>

        {!hasData && (
          <div
            style={{
              display: 'flex',
              height: 320,
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'column',
              color: '#bfccd6',
              paddingTop: 170
            }}>
            <Icon name="fas fa-inbox" style={{ fontSize: 60, width: 60, height: 60 }} />
          </div>
        )}

        {data.map((item, index) => {

          const id = item.createdBy.id;
          const displayName = item.createdBy.displayName;
          const email = item.createdBy.email;
          const phoneNumber = item.createdBy.phoneNumber;

          const { color, label } = showRecordType(item.type);

          const timestamp = moment(item.createdAt.toDate()).format('DD/MM/YYYY HH:mm');

          const deliveryIsFinished = delivery.status === 'finished' && delivery.finishedAt;

          const cannotRemove = (
            // CreatedAt date is lower than finishedAt date
            // item is not automatic
            // Creator is not the same as the logged user
            deliveryIsFinished && (moment(delivery.finishedAt.toDate()).diff(moment(item.createdAt.toDate())) > 0) ||
            item.automatic ||
            id !== user.id
          );

          return (
            <Box bordered style={{ flex: 1, marginTop: index != 0 ? 10 : 0, marginRight: 0 }} key={item.id}>
              <Card>
                <CardType>
                  <CardTypeLabel color={color}>{label}</CardTypeLabel>
                  <CardTimestamp color={color}>{timestamp}</CardTimestamp>
                  <CardTypeIcon>
                    {item.attachment && (
                      <Button
                        isBare
                        isSmall
                        onClick={() => onClickToOpen(item)}>
                        <Icon name="fas fa-paperclip" color="#1A497B" />
                      </Button>
                    )}
                    {item.automatic && <Icon name="fas fa-robot" size={16} color="#8a9ba8" style={{ marginLeft: 8 }} />}
                  </CardTypeIcon>
                </CardType>
                <CardDescription>
                  {item.description}
                </CardDescription>
                <CardFooter>
                  <CardCreator>Criado por: {displayName || email || phoneNumber}</CardCreator>
                  <Button
                    isBare
                    isSmall
                    disabled={cannotRemove}
                    onClick={() => onClickToRemove(item)}>
                    <Icon size={12} name="fas fa-minus-circle" />
                  </Button>
                </CardFooter>
              </Card>
            </Box>
          );
        })}
      </Scrollbar>
    </Container>
  );
};

RecordsList.propTypes = {};

export default RecordsList;
