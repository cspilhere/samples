import React from 'react';

import { Icon, Button } from 'shared-components';

import { Box, Spacer, RowWrapper } from 'components';
import DriverSmallCard from 'components/custom/cards/DriverSmallCard';

const Driver = (props) => {

  const { content, isFinished, onClickToUpdate, editDisabled } = props;

  return (
    <Box elevated>
      <RowWrapper>
        {content ? (
          <>
            <DriverSmallCard content={content} />
            <Spacer size={15} />
            {!editDisabled && (
              <Button
                id="delivery-update-driver"
                isSmall
                disabled={isFinished}
                onClick={onClickToUpdate}>
                <Icon name="far fa-exchange" />
              </Button>
            )}
          </>
        ) : (
          <Button
            id="delivery-add-driver"
            isSmall
            disabled={isFinished || editDisabled}
            isFlat
            onClick={onClickToUpdate}>
            Motorista
          </Button>
        )}
      </RowWrapper>
    </Box>
  );

};

export default Driver;
