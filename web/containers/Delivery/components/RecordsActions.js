import React from 'react';
import PropTypes from 'prop-types';

import {
  Button,
  Text,
  Icon,
  FlexBar
} from 'shared-components';

const RecordsActions = ({ data, requestLogin, delivery, onClickToCreate, onClickToExport }) => {
  const hasData = data.length > 0;
  return (
    <FlexBar>
      <FlexBar.Child grow />
      <FlexBar.Child>
        <Text isSmall hasGray4Color>
          <em>
            {!hasData ? (
              ''
            ) : (
              `${data.length} registro${data.length > 1 ? 's' : ''} encontrado${data.length > 1 ? 's' : ''}`
            )}
          </em>
        </Text>
      </FlexBar.Child>
      <FlexBar.Child>
        {requestLogin ? (
          <div className="buttons">
            <Button
              id="delivery-login-button"
              isSecondary
              isSmall
              onClick={() => location.href = './auth?returnTo=' + location.href}>
              Entrar
            </Button>
          </div>
        ) : (
          <div className="buttons">
            <Button
              id="delivery-create-record-button"
              isPrimary
              isSmall
              disabled={!delivery.id}
              onClick={onClickToCreate}>
              Criar registro
            </Button>
            {hasData && (
              <Button
                id="delivery-download-records-button"
                isSmall
                onClick={onClickToExport}>
                <Icon name="fas fa-download" />
              </Button>
            )}
          </div>
        )}

      </FlexBar.Child>
    </FlexBar>
  );
};

RecordsActions.propTypes = {

};

export default RecordsActions;
