import React, { useEffect, useState } from 'react';

import { drivers } from 'services/firestore';

let stopListen;

const LocationMap = ({ driverPhone, releasedAt, finishedAt, render, hideDriverPosition }) => {

  const [locations, setLocations] = useState(null);

  if (hideDriverPosition) return render();

  useEffect(() => {
    drivers.location(driverPhone, releasedAt, finishedAt, (response, unsubscribe) => {
      setLocations(response.map((l) => ({
        lat: l.location.coords.latitude,
        lng: l.location.coords.longitude
      })));
      if (finishedAt && unsubscribe) unsubscribe()();
      stopListen = unsubscribe;
    });
    return () => {
      if (stopListen) stopListen()();
      stopListen = null;
    };
  }, [driverPhone, releasedAt, finishedAt]);

  return render(locations);

};

LocationMap.propTypes = {};

export default LocationMap;
