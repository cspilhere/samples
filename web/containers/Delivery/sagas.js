import { eventChannel } from 'redux-saga';
import { takeLatest, put, take, call, all } from 'redux-saga/effects';

import { delivery, drivers, records, fetchStakeholder } from 'services/firestore';

// Retrieve delivery data
function* fetchDelivery(action) {
  const createChannel = () => eventChannel((emit) => {
    delivery.fetch(action.id, emit).catch((error) => emit(new Error(error)));
    return () => {};
  });
  const channel = yield call(createChannel);
  while (true) {
    try {
      const response = yield take(channel);
      yield put({ type: 'FETCH_DELIVERY_SUCCESS', payload: response });
    } catch (error) {
      console.log(error);
    };
  };
};

function* fetchDeliveryDetails(action) {
  const createDeliveryChannel = () => eventChannel((emit) => {
    delivery.fetch(action.id, emit).catch((error) => emit(new Error(error)));
    return () => {};
  });
  const deliveryChannel = yield call(createDeliveryChannel);
  while (true) {
    try {
      let details = {};
      const delivery = yield take(deliveryChannel);
      details = { ...delivery };
      // Fetch client and shipper data
      const client = yield call(fetchStakeholder, 'clients', delivery.client.id);
      details.client = { ...details.client, ...client };
      const shipper = yield call(fetchStakeholder, 'shippers', delivery.shipper.id);
      details.shipper = { ...details.shipper, ...shipper };
      // if delivery has configured a driver, we try to fetch the driver data
      if (delivery.driver && delivery.driver.id) {
        const driver = yield call(drivers.fetch, delivery.driver.id);
        details.driver = { ...details.driver, ...driver };
      }
      yield put({
        type: 'FETCH_DELIVERY_DETAILS_SUCCESS',
        payload: details
      });
    } catch (error) {
      yield put({ type: 'FETCH_DELIVERY_DETAILS_ERROR', payload: error });
    };
  };
};

function* releaseDelivery(action) {
  try {
    yield call(delivery.release, action.id);
    yield put({ type: 'RELEASE_DELIVERY_SUCCESS' });
    if (action.success) action.success();
  } catch (error) {
    if (action.error) action.error(error);
  }
};

function* removeDelivery(action) {
  try {
    yield call(delivery.remove, action.id);
    yield put({ type: 'REMOVE_DELIVERY_SUCCESS' });
    if (action.success) action.success();
  } catch (error) {
    if (action.error) action.error(error);
  }
};

function* finishDelivery(action) {
  try {
    yield call(delivery.finish, action.id);
    yield put({ type: 'FINISH_DELIVERY_SUCCESS' });
    if (action.success) action.success();
  } catch (error) {
    if (action.error) action.error(error);
  }
};

function* updateDeliveryLabel(action) {
  try {
    yield call(delivery.updateLabel, action.id, action.value);
    yield put({ type: 'UPDATE_DELIVERY_LABEL_SUCCESS' });
  } catch (error) {}
};

// Retrieve all records
function* fetchRecords(action) {
  const createChannel = () => eventChannel((emit) => {
    records.fetch(action.deliveryId, emit).catch((error) => emit(new Error(error)));
    return () => {};
  });
  const channel = yield call(createChannel);
  while (true) {
    try {
      const response = yield take(channel);
      yield put({ type: 'FETCH_RECORDS_SUCCESS', payload: response });
    } catch (error) {
      console.log(error);
    };
  };
};

function* createRecord(action) {
  try {
    yield call(records.create, action.deliveryId, action.body);
    yield put({ type: 'CREATE_RECORD_SUCCESS' });
  } catch (error) {
    console.log(error);
  }
};

function* removeRecord(action) {
  try {
    yield call(records.remove, action.deliveryId, action.recordId);
    yield put({ type: 'REMOVE_RECORD_SUCCESS' });
  } catch (error) {
    console.log(error);
  }
};

export default function* root() {
  yield all([
    takeLatest('FETCH_DELIVERY', fetchDelivery),
    takeLatest('FETCH_DELIVERY_DETAILS', fetchDeliveryDetails),
    takeLatest('RELEASE_DELIVERY', releaseDelivery),
    takeLatest('REMOVE_DELIVERY', removeDelivery),
    takeLatest('FINISH_DELIVERY', finishDelivery),
    takeLatest('UPDATE_DELIVERY_LABEL', updateDeliveryLabel),
    takeLatest('FETCH_RECORDS', fetchRecords),
    takeLatest('CREATE_RECORD', createRecord),
    takeLatest('REMOVE_RECORD', removeRecord),
  ]);
};
