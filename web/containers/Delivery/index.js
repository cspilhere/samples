import React from 'react';

import { connect } from 'react-redux';
import * as actions from './actions';

import Viewer from 'viewerjs';
import 'viewerjs/dist/viewer.css';

import { Modal, Text, Icon, Grid } from 'shared-components';
import { Tag, Spacer } from 'components';

import CreateRecord from 'features/CreateRecord';
import UpdateDeliveryDriver from 'features/UpdateDeliveryDriver';
import UpdateDeliveryEstimation from 'features/UpdateDeliveryEstimation';
import UpdateDeliveryDestination from 'features/UpdateDeliveryDestination';

import Estimation from './components/Estimation';
import Driver from './components/Driver';
import Destination from './components/Destination';
import EntityCard from './components/EntityCard';
import DeliveryHeader from './components/DeliveryHeader';
import RecordsList from './components/RecordsList';
import RecordsActions from './components/RecordsActions';
import DeliveryActions from './components/DeliveryActions';
import DeliveryDetails from './components/DeliveryDetails';
import DriverLocation from './components/DriverLocation';
import HelpTips from './components/HelpTips';

import DeliverySituation from 'components/custom/cards/DeliverySituation';
import Map from 'components/custom/mapTracking/Map';
import Drawer from 'components/modules/Drawer';

import { MainContext } from 'containers/MainContainer';

import BaseLayout, { baseLayoutWrapper } from 'components/custom/baseLayout/BaseLayout';
import BaseHeader from 'components/custom/baseLayout/BaseHeader';

import { deliveryRecordsToCsv } from 'services/csvAndDownload';

class Delivery extends React.Component {

  static contextType = MainContext

  state = {
    showDrawer: false,
    showHelp: false
  }

  delivery = {}

  componentWillUnmount() {
    if (process.env.NODE_ENV === 'production') this.props.clearDeliveryData();
  }

  componentWillMount() {
    this.props.fetchDelivery(this.props.match.params.deliveryId);
    this.props.fetchRecords(this.props.match.params.deliveryId);
  }

  render() {

    const { company, currentUser } = this.context;
    const { data, records, loading, error, loadingDetails, details } = this.props.reducer;

    this.delivery = data;

    let driver = data.driver;

    if (driver && driver.phone) {
      driver.phone = driver.phone.replace('+55', '');
    }

    const isFinished = data.status === 'finished';

    let destinationCoordinates = null;

    const hasDestinationCoordinates = data && data.destination && data.destination.coordinates;

    if (hasDestinationCoordinates) {
      destinationCoordinates = {
        lat: data.destination.coordinates.latitude,
        lng: data.destination.coordinates.longitude
      };
    }

    return (
      <BaseLayout
        baseHeader={(
          <BaseHeader
            breadcrumb={[{
              name: 'Entregas', title: 'Minhas entregas', pathname: '/deliveries'
            }, {
              name: this.delivery.number || '', title: 'Detalhes da entrega'
            }]}
            onClickHelp={() => {
              this.setState({ showHelp: true });
            }}
          />
        )}
        contentHeader={(
          <DeliveryHeader
            {...data}
            labels={company.labels}
            onClickToOpenDetails={() => {
              this.setState({ showDrawer: true });
            }}
            onChangeLabel={this.updateLabel}
          />
        )}>

        <HelpTips isVisible={this.state.showHelp} onHide={() => this.setState({ showHelp: false })} />

        <Drawer
          show={this.state.showDrawer}
          hideRequest={() => this.setState({ showDrawer: false })}
          maxWidth={480}
          header={(
            <>
              <Tag color="#F2F2F2" small style={{ marginRight: 10 }}># {data.label || 'Sem etiqueta'}</Tag>
              <DeliverySituation content={data.status} />
            </>
          )}>
          {(drawerProps) => data.id && (
            <DeliveryDetails
              {...drawerProps}
              data={details}
              detailsRequest={() => this.props.fetchDeliveryDetails(data.id)}
            />
          )}
        </Drawer>

        <Grid isFlexShrink gapConfig={2}>
          <Grid.Col hasOverflowHidden>
            <EntityCard {...data.shipper} title={<><Icon name="fas fa-truck-loading" />&nbsp;Embarcador</>} />
          </Grid.Col>
          <Grid.Col hasOverflowHidden>
            <EntityCard {...data.client} title={<><Icon name="fas fa-box-check" />&nbsp;Cliente</>} />
          </Grid.Col>
        </Grid>

        <Grid style={{ flex: 1, minHeight: 620, overflow: 'hidden' }}>

          <Grid.Col is5 isFlex>
            <RecordsList
              data={records}
              user={currentUser}
              delivery={data}
              onClickToOpen={this.openRecordAttachment}
              onClickToRemove={this.removeRecord}>
              <RecordsActions
                data={records}
                delivery={data}
                onClickToExport={() => deliveryRecordsToCsv(data)}
                onClickToCreate={this.createRecord}
              />
            </RecordsList>
            <Spacer horizontal medium />
            <DeliveryActions
              delivery={data}
              onClickToCancel={this.cancelDelivery}
              onClickToFinish={this.finishDelivery}
              onClickToRelease={this.releaseDelivery}
            />
          </Grid.Col>

          <Grid.Col is7 isFlex>
            <DriverLocation
              driverPhone={driver && driver.phone}
              releasedAt={data.releasedAt}
              finishedAt={data.finishedAt}
              render={(locations) => {
                locations = locations || [];
                return (
                  <Map
                    origin={locations[0]}
                    destination={destinationCoordinates}
                    leftOverlay={(
                      <Driver
                        content={data.driver}
                        isFinished={isFinished}
                        onClickToUpdate={this.updateDriver}
                      />
                    )}
                    rightOverlay={(
                      <Estimation
                        content={data.estimation}
                        isFinished={isFinished}
                        onClickToUpdate={this.updateEstimation}
                      />
                    )}
                    bottomOverlay={(
                      <Destination
                        data={data.destination}
                        isFinished={isFinished}
                        onClickToUpdate={this.updateDestination}
                      />
                    )}
                  />
                );
              }}
            />
          </Grid.Col>

        </Grid>
      </BaseLayout>
    );
  }

  // Open record attachment
  openRecordAttachment = (content) => {

    if (content.attachment.url && content.attachment.type !== 'application/pdf') {
      const image = new Image();
      image.src = content.attachment.url;
      const viewer = new Viewer(image, {
        title: false,
        button: false,
        navbar: false,
        transition: false,
        loading: false,
        toolbar: {
          zoomIn: { show: true, size: 'large' },
          zoomOut: { show: true, size: 'large' },
          oneToOne: { show: true, size: 'large' },
          reset: { show: true, size: 'large' },
          rotateLeft: { show: true, size: 'large' },
          rotateRight: { show: true, size: 'large' },
          flipHorizontal: { show: true, size: 'large' },
          flipVertical: { show: true, size: 'large' },
        },
        hidden: () => viewer.destroy()
      });
      viewer.show();
      return;
    }

    if (content.attachment.url && content.attachment.type === 'application/pdf') {
      this.props.openModal({
        lightbox: true,
        component: (
          <iframe
            title={content.description}
            style={{ width: '100%', height: 'calc(100% - 12rem)', margin: '6rem' }}
            src={`${content.attachment.url}`}
          />
        )
      });
      return;
    }
    const { displayName = '', email = '' } = content.createdBy;

    this.props.openModal({
      title: `${displayName || email} - ${this.props.format.datetime(content.createdAt)}`,
      disableClose: true,
      component: (
        <Modal.Wrapper>
          <Modal.Content><Text>{content.description}</Text></Modal.Content>
        </Modal.Wrapper>
      )
    });

  }

  /**
   * Delivery settings
   */

  updateDestination = () => {
    const hasDestination = this.delivery.destination || {};
    this.props.openModal({
      title: !this.delivery.driver ? 'Configurar endereço de entrega' : 'Atualizar endereço de entrega',
      component: (
        <UpdateDeliveryDestination
          defaultValues={{ destination: this.delivery.destination, id: this.delivery.id }}
          onClickToCancel={this.props.closeModal}
          onSuccess={(response) => {
            if (hasDestination.address) {
              this.props.createRecord(
                this.delivery.id,
                this.buildRecordData(`Endereço de entrega atualizado para ${response.destination.address}`, null, true)
              );
            }
            this.props.closeModal();
          }}
          onError={(error) => {
            console.log(error);
          }}
        />
      )
    });
  }

  updateDriver = () => {
    const hasDriver = this.delivery.driver;
    this.props.openModal({
      title: !this.delivery.driver ? 'Configurar motorista' : 'Atualizar motorista',
      component: (
        <UpdateDeliveryDriver
          defaultValues={{ driver: this.delivery.driver, id: this.delivery.id }}
          onClickToCancel={this.props.closeModal}
          onSuccess={(response) => {
            if (hasDriver) {
              this.props.createRecord(
                this.delivery.id,
                this.buildRecordData(`Motorista atualizado para ${response.driver.name}`, null, true)
              );
            }
            this.props.closeModal();
          }}
          onError={(error) => {
            console.log(error);
          }}
        />
      )
    });
  }

  updateEstimation = () => {
    const oldDate = this.delivery.estimation && this.props.format.datetime(this.delivery.estimation);
    this.props.openModal({
      title: !this.delivery.estimation ? 'Configurar data de entrega' : 'Atualizar data de entrega',
      component: (
        <UpdateDeliveryEstimation
          defaultValues={{
            estimation: this.delivery.estimation && this.delivery.estimation.toDate(),
            id: this.delivery.id
          }}
          onClickToCancel={this.props.closeModal}
          onSuccess={(response) => {
            if (oldDate) {
              this.props.createRecord(
                this.delivery.id,
                this.buildRecordData(
                  `Data de entrega modificada de ${oldDate} para ${this.props.format.datetime(response.estimation)}`,
                  null,
                  true
                )
              );
            }
            this.props.closeModal();
          }}
          onError={() => {}}
        />
      )
    });
  }

  updateLabel = ({ target }) => {
    this.props.updateDeliveryLabel(this.delivery.id, target.value);
  }

  /**
   * Records actions
   */

  createRecord = () => {
    const links = [`CTe: ${this.delivery.billOfLading}`, ...(this.delivery.invoices || []).map((nf) => `NF: ${nf}`)];
    this.props.openModal({
      title: 'Criar registro',
      disableBackgroundClick: true,
      component: (
        <CreateRecord
          defaultValues={{ ...this.buildRecordData(), links }}
          onClickToCancel={this.props.closeModal}
          onSuccess={this.props.closeModal}
        />
      )
    });
  }

  removeRecord = (row) => {
    const confirm = this.props.alert({
      title: 'Tem certeza que deseja remover o registro?',
      description: row.description,
      onConfirm: () => {
        this.props.removeRecord(this.delivery.id, row.id);
        confirm.close();
      }
    });
  }

  buildRecordData = (description, type, automatic) => {
    const { currentUser } = this.context;
    return {
      companyId: this.delivery.companyId,
      deliveryId: this.delivery.id,
      createdBy: {
        id: currentUser.id,
        displayName: currentUser.displayName,
        email: currentUser.email,
        phoneNumber: currentUser.phoneNumber
      },
      clientId: this.delivery.client.id,
      shipperId: this.delivery.shipper.id,
      type: type || 'event',
      automatic: automatic || false,
      description: description || 'Sem descrição.'
    };
  }

  /**
   * Delivery actions
   */

  finishDelivery = () => {
    const confirm = this.props.alert({
      title: this.props.str('delivery.finish-alert-title'),
      description: this.props.str('delivery.finish-alert-confirmation'),
      enableConfirmActionAfter: 5000,
      onConfirm: () => {
        this.props.finishDelivery(this.delivery.id, () => {
          this.props.createRecord(this.delivery.id, this.buildRecordData('Entrega finalizada!', 'conclusion', true));
          this.props.callToast('success', this.props.str('delivery.finish-request-success'));
        });
        confirm.close();
      }
    });
  }

  cancelDelivery = () => {
    const confirm = this.props.alert({
      title: this.props.str('delivery.cancel-alert-title'),
      description: this.props.str('delivery.cancel-alert-confirmation'),
      enableConfirmActionAfter: 5000,
      onConfirm: () => {
        this.props.history.push('/'); // Return user to deliveries list
        confirm.close();
        this.props.removeDelivery(this.delivery.id, () => {
          this.props.callToast('success', this.props.str('delivery.cancel-request-success'));
        });
      }
    });
  }

  releaseDelivery = () => {
    const confirm = this.props.alert({
      title: this.props.str('delivery.release-alert-title'),
      confirmationButtonLabel: !this.delivery.driver ? 'Ok' : null,
      description: this.delivery.driver ? (
        this.props.str('delivery.release-alert-confirmation')
      ) : (
        this.props.str('delivery.release-alert-driver-notfound')
      ),
      onConfirm: () => {
        if (this.delivery.driver) {
          this.props.releaseDelivery(this.delivery.id, () => {
            this.props.createRecord(this.delivery.id, this.buildRecordData('Entrega liberada.', null, true));
            this.props.callToast('success', this.props.str('delivery.release-request-success'));
          });
        }
        confirm.close();
      }
    });
  }

};

export default connect((store) => ({
  reducer: store.delivery
}), { ...actions })(baseLayoutWrapper(Delivery));
