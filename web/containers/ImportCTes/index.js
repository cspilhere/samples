import React from 'react';

import { createStakeholder, delivery } from 'services/firestore';

import templateSelector from './components/templateSelector';

import {
  Button,
  Space,
  FlexBar,
  Icon,
  Text,
  FileReader,
} from 'shared-components';

import { MainContext } from 'containers/MainContainer';

import { arrayRemoveDuplicates } from 'shared-utils/utils';

import { alert } from 'shared-components/lib/Alert';

import Table from './components/Table';

import BaseLayout, { baseLayoutWrapper } from 'components/custom/baseLayout/BaseLayout';
import BaseHeader from 'components/custom/baseLayout/BaseHeader';

import Constants from 'services/Constants';
import HelpTips from './components/HelpTips';

class ImportCTes extends React.Component {

  static contextType = MainContext;

  state = {
    batch: [],
    isWaiting: true,
    message: '',
    showHelp: false
  };

  componentDidMount() {}

  render() {

    const batch = arrayRemoveDuplicates(this.state.batch, 'number');

    const stats = `${
      this.state.imported
    } importados (com erro: ${
      this.state.withError
    }, duplicados: ${
      (this.state.imported - this.state.withError) - batch.length
    }, prontos para enviar: ${batch.length})`;

    return (
      <BaseLayout
        baseHeader={(
          <BaseHeader
            breadcrumb={[{
              name: 'Entregas', title: 'Minhas entregas', pathname: '/deliveries'
            }, {
              name: 'Importação de CTes', title: 'Importação de CTes'
            }]}
            onClickHelp={() => {
              this.setState({ showHelp: true });
            }}
          />
        )}>

        <HelpTips isVisible={this.state.showHelp} onHide={() => this.setState({ showHelp: false })} />

        {this.state.batch.length < 1 || this.state.message == 'Concluído!' ? (
          <>
            <FileReader
              multiple
              readAsText
              onChange={this.readFiles} label="Importar CTes"
              useDropzone
              dropzoneStyles={{ height: 100 }}
              dropzoneContent={(
                <Text textAlign="center">
                  <Icon name="far fa-upload" /> Arraste seus CTes <strong>aqui</strong> ou clique para selecionar.
                </Text>
              )}
            />
            <Space size="medium" />
          </>
        ) : null}

        <Table
          data={batch}
          isDone={this.state.message === 'Concluído!'}
          onClickToRemove={(rowIndex) => {
            const batch = [...this.state.batch];
            batch.splice(rowIndex, 1);
            this.setState({ batch });
          }}
        />

        <Space />

        <FlexBar>
          <FlexBar.Child grow />
          {batch.length > 0 && (
            <FlexBar.Child>
              <Text isSmall hasGray2Color>
                {stats}
              </Text>
            </FlexBar.Child>
          )}
          <FlexBar.Child>
            <Text isSmall hasGray4Color>
              <em>
                {this.state.message}
              </em>
            </Text>
          </FlexBar.Child>
          <FlexBar.Child>
            <Button
              disabled={this.state.batch.length < 1 || this.state.isWaiting}
              isSmall
              onClick={() => {
                this.setState({ batch: [], isWaiting: false, message: '' });
              }}>
              Remover todos
            </Button>
          </FlexBar.Child>
          <FlexBar.Child>
            <Button
              isPrimary
              disabled={this.state.batch.length < 1 || this.state.isWaiting}
              isSmall
              onClick={() => {
                const confirmation = alert({
                  title: 'AVISO!',
                  description: 'Aguarde o processo de importação concluir antes de fechar a janela ou mudar de tela.',
                  onConfirm: () => {
                    confirmation.close();
                    this.submitBatch().then(() => {
                      this.setState({ message: 'Concluído!' });
                      this.props.callToast('success', 'Importação concluída com succeso!');
                    });
                  }
                });
              }}>
              Criar entregas
            </Button>
          </FlexBar.Child>
        </FlexBar>
      </BaseLayout>
    );
  }

  submitBatch = () => {

    this.setState({ isWaiting: true });

    return new Promise((resolve, reject) => {

      const filesBatch = arrayRemoveDuplicates(this.state.batch, 'number');

      filesBatch.forEach((file, index) => {

        if (index + 1 === filesBatch.length) this.setState({ message: 'Importando, aguarde...'});

        const client = {...file.client};
        const shipper = {...file.shipper};

        file.client.id = `${Constants.get('companyId')}_${client.document}`;
        file.shipper.id = `${Constants.get('companyId')}_${shipper.document}`;

        file.origin = 'xml-import';

        delivery.create(file)
        .then(() => (
          createStakeholder(client, 'clients', true)
          .then(() => createStakeholder(shipper, 'shippers', true))
        ))
        .then(() => {
          if (index + 1 === filesBatch.length) resolve();
        });

      });

    });

  }

  readFiles = (files) => {

    this.setState({
      batch: [],
      isWaiting: false,
      message: '',
      imported: files.length
    });

    files.forEach((file) => {
      file.preview().then((response) => {

        templateSelector(response)
        .then((billOfLadingObject) => {

          const billOfLadingsArray = this.state.batch;
          billOfLadingsArray.push(billOfLadingObject);

          const withError = files.length - billOfLadingsArray.length;
          const total = billOfLadingsArray.length;

          this.setState({
            batch: billOfLadingsArray,
            withError,
            duplicates: total - billOfLadingsArray.length
          });

        })
        .catch((error) => {
          console.log(error);
          this.props.callToast('warning', `O arquivo ${file.name} não pode ser importado.`);
        });

      });
    });

  }

};

export default baseLayoutWrapper(ImportCTes);
