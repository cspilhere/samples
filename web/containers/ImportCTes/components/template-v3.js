import { formatAddress, queryPredictions, latLngByAddress } from 'services/addressLocation';

function parseAddressFromXObs(value) {
  if (value.indexOf('End. Entrega:') !== -1) {
    value = value.slice(value.search('End. Entrega:'), value.length);
    value = value.replace('End. Entrega:', '');
    value = value.replace('Nr:', '');
    value = value.replace('Bairro:', '');
    value = value.replace('Cidade:', '');
    return value;
  } else {
    return '';
  }
}

function createStakeolderObject(stakeholder, addressPrefix) {
  return {
    document: stakeholder.CNPJ._text,
    documentType: 'cnpj',
    // email: (stakeholder.email ? stakeholder.email._text : null),
    // phone: (stakeholder.fone ? stakeholder.fone._text : null),
    name: stakeholder.xNome._text.toUpperCase(),
    address: {
      cep: stakeholder[`ender${addressPrefix}`].CEP._text,
      description: stakeholder[`ender${addressPrefix}`].xLgr._text,
      number: stakeholder[`ender${addressPrefix}`].nro._text,
      neighborhood: stakeholder[`ender${addressPrefix}`].xBairro._text,
      city: stakeholder[`ender${addressPrefix}`].xMun._text,
      uf: stakeholder[`ender${addressPrefix}`].UF._text,
    }
  };
};

export default (content) => {

  return new Promise((resolve, reject) => {

    try {

      let cteObject = {};

      const contentObs = content.cteProc.CTe.infCte.compl.xObs;
      const company = content.cteProc.CTe.infCte.emit;

      const number = content.cteProc.CTe.infCte.ide.nCT._text;
      const series = content.cteProc.CTe.infCte.ide.serie._text;
      const taker = content.cteProc.CTe.infCte.ide.toma3.toma._text;

      cteObject.number = parseInt(number);
      cteObject.series = parseInt(series);
      cteObject.taker = parseInt(taker);

      const destinationAddress = parseAddressFromXObs(contentObs && contentObs._text || '');

      const cteKey = content.cteProc.protCTe.infProt.chCTe._text;
      cteObject.billOfLading = cteKey;

      let invoiceKey = content.cteProc.CTe.infCte.rem.infNFe;
      if (!invoiceKey) invoiceKey = content.cteProc.CTe.infCte.infCTeNorm.infDoc.infNFe;

      let invoices = [];

      if (invoiceKey.map) invoices = invoiceKey.map((nf) => nf.chave._text);
      else invoices.push(invoiceKey.chave._text);

      cteObject.invoices = invoices;

      const shipper = content.cteProc.CTe.infCte.rem;
      cteObject.shipper = createStakeolderObject(shipper, 'Reme');

      const client = content.cteProc.CTe.infCte.dest;
      cteObject.client = createStakeolderObject(client, 'Dest');

      const addressToQuery = destinationAddress || formatAddress(cteObject.client.address, true);

      queryPredictions(addressToQuery)
      .then((predictedAddress) => {
        cteObject.destination = {};
        if (predictedAddress && predictedAddress.length > 0) {
          cteObject.destination.address = predictedAddress[0].description;
        } else {
          cteObject.destination.address = '';
        }
        return latLngByAddress(cteObject.destination.address);
      })
      .then((coordinates) => {
        cteObject.destination.coordinates = coordinates || null;
        resolve(cteObject);
      });

    } catch (error) {
      reject(error);
    }

  });

};
