import React from 'react';

import HelpTipsContainer from 'components/custom/HelpTipsContainer';

import { Text, Button } from 'components';

import { downloadFile } from 'services/csvAndDownload';

const HelpTips = ({ isVisible, onHide }) => {
  return (
    <HelpTipsContainer
      isVisible={isVisible}
      onHide={onHide}
      name="helpTips-ImportCTes"
      items={[
        'Você pode importar seus CTes utilizando os arquivos XML podendo enviar um ou mais arquivos ao mesmo tempo.',
        'Após concluir a importação retorne até a lista de Entregas para conferir seus CTes recém importados.',
        'Clientes e Embarcadores são salvos automaticamente no sistema.',
        <>
          <Text small light>
            Baixe um modelo de CTe para testar agora mesmo:&nbsp;
            <Button
              bare
              onClick={() => {
                downloadFile('ModeloCTe.xml', 'https://firebasestorage.googleapis.com/v0/b/levalog.appspot.com/o/ModeloCTe.xml?alt=media&token=4d11fcea-6f73-43a4-9e3a-b14827980962');
              }}>
              <Text small color="#ffc940">Modelo.xml</Text>
            </Button>
          </Text>
        </>
      ]}
      tourId={56514}
    />
  );
};

export default HelpTips;
