import convert from 'xml-js';

import templateV3 from './template-v3';

export default (file) => {

  const content = convert.xml2js(file, { compact: true, trim: true });

  try {

    const xmlVersion = parseFloat(content.cteProc._attributes.versao);

    if (xmlVersion >= 3 && xmlVersion < 4) {
      return templateV3(content);
    } else {
      return new Promise((r, reject) => reject('CTe version below 3 does not have support.'));
    }

  } catch (error) {

    return new Promise((r, reject) => reject(error));

  }

};
