import React from 'react';

import DataTable, { CellWrapper, Options } from 'components/custom/DataTable';

import EntityWithAddress from 'components/custom/cards/EntityWithAddress';
import GenericText from 'components/custom/cards/GenericText';
import AddressDescription from 'components/custom/cards/AddressDescription';

import { showTakerLabel } from 'services/transformKeysToString';

const Table = ({ data, done, onClickToRemove }) => (
  <DataTable
    data={data}
    columns={[{
      columnKey: 'number',
      header: 'CTe',
      alignHeader: 'center',
      cell: <CellWrapper center data={data} render={(renderProps) => <GenericText {...renderProps} bold />} />,
      width: 100
    }, {
      columnKey: 'taker',
      header: 'Tomador',
      alignHeader: 'center',
      cell: <CellWrapper center data={data} render={(renderProps) => <GenericText {...renderProps} parseWith={showTakerLabel} />} />,
      width: 120
    }, {
      columnKey: 'destination.address',
      header: 'Endereço de entrega',
      cell: <CellWrapper data={data} render={AddressDescription} />,
      flexGrow: 1,
      minWidth: 140,
      defaultWidth: 400,
      isResizable: true
    }, {
      columnKey: 'shipper',
      header: 'Embarcador',
      cell: <CellWrapper data={data} render={(renderProps) => <EntityWithAddress {...renderProps} type="shipper" contentAsObject />} />,
      flexGrow: 1,
      minWidth: 140,
      defaultWidth: 300,
      isResizable: true
    }, {
      columnKey: 'client',
      header: 'Cliente',
      cell: <CellWrapper data={data} render={(renderProps) => <EntityWithAddress {...renderProps} type="client" contentAsObject />} />,
      flexGrow: 1,
      minWidth: 160,
      defaultWidth: 300,
      isResizable: true
    }, {
      cell: (
        <Options
          data={data}
          disabled={done}
          onClickToRemove={onClickToRemove}
        />
      ),
      width: 60
    }]}
  />
);

export default Table;
