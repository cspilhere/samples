import React from 'react';

import logo from 'static/media/levalog-logo.svg';

import { connect } from 'react-redux';
import * as actions from './actions';

import Viewer from 'viewerjs';
import 'viewerjs/dist/viewer.css';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { Modal, Space, Text, Grid, Layout, FlexBar, Icon, Button, AlertContainer } from 'shared-components';

import CreateRecord from 'features/CreateRecord';
import CreateFeedback from 'features/CreateFeedback';

import Estimation from 'containers/Delivery/components/Estimation';
import Driver from 'containers/Delivery/components/Driver';
import Destination from 'containers/Delivery/components/Destination';
import EntityCard from 'containers/Delivery/components/EntityCard';
import DeliveryHeader from 'containers/Delivery/components/DeliveryHeader';
import RecordsList from 'containers/Delivery/components/RecordsList';
import RecordsActions from 'containers/Delivery/components/RecordsActions';
import DriverLocation from 'containers/Delivery/components/DriverLocation';
import DeliveryDetails from 'containers/Delivery/components/DeliveryDetails';

import DeliverySituation from 'components/custom/cards/DeliverySituation';
import Map from 'components/custom/mapTracking/Map';
import Drawer from 'components/modules/Drawer';

import BaseLayout, { baseLayoutWrapper } from 'components/custom/baseLayout/BaseLayout';

import { AuthContext } from 'containers/AuthManager';

import { deliveryRecordsToCsv } from 'services/csvAndDownload';

class DeliveryPublic extends React.Component {

  static contextType = AuthContext;

  state = {
    drawerState: false
  }

  delivery = {}

  componentDidMount() {
    this.context.onUserAuthChanged(({ isSignedIn }) => {
      if (isSignedIn) {
        this.props.fetchDelivery(this.props.match.params.token);
      }
    }, true);
  }

  render() {

    const { currentUser } = this.context;
    const { delivery, company, records, loading, error } = this.props.reducer;

    if (!currentUser || !company) return null;

    this.delivery = delivery

    const driver = delivery.driver;

    const isFinished = delivery.status === 'finished';

    const userIsAnonymous = currentUser.isAnonymous;

    let destinationCoordinates = null;

    const hasDestinationCoordinates = delivery && delivery.destination && delivery.destination.coordinates;

    if (hasDestinationCoordinates) {
      destinationCoordinates = {
        lat: delivery.destination.coordinates.latitude,
        lng: delivery.destination.coordinates.longitude
      };
    }

    const hasSettings = company.settings && company.settings.hasOwnProperty('showDriverOnPublicDelivery');
    const showDriverOnPublicDelivery = hasSettings ? company.settings.showDriverOnPublicDelivery : true;

    return (
      <Layout className="app-wrapper">

        <Layout.Header>
          <FlexBar specSelector="app-header" className="main-header-container" backgroundColor={company.color}>
            <FlexBar.Child grow>
              <img src={company.logo || logo} style={{ height: 22 }} />
            </FlexBar.Child>
            <FlexBar.Child>
              <span style={{ color: '#fff', fontSize: 18 }}><strong>Acompanhamento da entrega</strong></span>
            </FlexBar.Child>
          </FlexBar>
        </Layout.Header>
        <Layout.Main>

          <AlertContainer getRef={(alert) => alert ? this.alert = alert.setUpAlert : null} />
          <ToastContainer />

          <BaseLayout
            contentHeader={(
              <DeliveryHeader
                {...delivery}
                editDisabled={true}
                hideDetailsButton={userIsAnonymous}
                onClickToOpenDetails={() => {
                  this.setState({ drawerState: true });
                }}
              />
            )}>

            {!userIsAnonymous && (
              <Drawer
                show={this.state.drawerState}
                hideRequest={() => this.setState({ drawerState: false })}
                maxWidth={480}
                header={<DeliverySituation content={delivery.status} />}>
                {(drawerProps) => delivery.id && (
                  <DeliveryDetails
                    {...drawerProps}
                    showDriverOnPublicDelivery={showDriverOnPublicDelivery}
                    hideOwnerData={true}
                    data={delivery}
                  />
                )}
              </Drawer>
            )}

            <Grid isFlexShrink gapConfig={2}>
              <Grid.Col hasOverflowHidden>
                <EntityCard {...delivery.shipper} title={<><Icon name="fas fa-truck-loading" /> Embarcador</>} />
              </Grid.Col>
              <Grid.Col hasOverflowHidden>
                <EntityCard {...delivery.client} title={<><Icon name="fas fa-box-check" /> Cliente</>} />
              </Grid.Col>
            </Grid>

            <Grid style={{ flex: 1, minHeight: 620, overflow: 'hidden' }}>

              <Grid.Col is5 isFlex>
                <RecordsList
                  data={records}
                  user={currentUser}
                  delivery={delivery}
                  onClickToOpen={this.openRecordAttachment}
                  onClickToRemove={this.removeRecord}>
                  <RecordsActions
                    requestLogin={userIsAnonymous}
                    data={records}
                    delivery={delivery}
                    onClickToExport={() => deliveryRecordsToCsv(data)}
                    onClickToCreate={this.createRecord}
                  />
                </RecordsList>
                {isFinished && !userIsAnonymous && (
                  <div style={{ textAlign: 'right' }}>
                    <Space size="medium" />
                    <Button isSuccess onClick={this.createFeedback}>Avaliar entrega</Button>
                  </div>
                )}
              </Grid.Col>

              <Grid.Col is7 isFlex>
                <DriverLocation
                  hideDriverPosition={userIsAnonymous}
                  driverPhone={driver && driver.phone}
                  releasedAt={delivery.releasedAt}
                  finishedAt={delivery.finishedAt}
                  render={(locations) => {
                    locations = locations || [];
                    return (
                      <Map
                        origin={locations[0]}
                        destination={destinationCoordinates}
                        leftOverlay={showDriverOnPublicDelivery && (
                          <Driver
                            content={delivery.driver}
                            editDisabled={true}
                            isFinished={isFinished}
                            onClickToUpdate={this.updateDriver}
                          />
                        )}
                        rightOverlay={(
                          <Estimation
                            content={delivery.estimation}
                            editDisabled={true}
                            isFinished={isFinished}
                            onClickToUpdate={this.updateEstimation}
                          />
                        )}
                        bottomOverlay={(
                          <Destination
                            data={delivery.destination}
                            editDisabled={true}
                            isFinished={isFinished}
                            onClickToUpdate={this.updateDestination}
                          />
                        )}
                      />
                    );
                  }}
                />
              </Grid.Col>

            </Grid>

          </BaseLayout>

        </Layout.Main>
        <div style={{ height: 4, backgroundColor: company.color || '#1A497B' }} />
      </Layout>
    );
  }

  // Open record attachment
  openRecordAttachment = (content) => {
    if (content.attachment.url && content.attachment.type !== 'application/pdf') {
      const image = new Image();
      image.src = content.attachment.url;
      const viewer = new Viewer(image, {
        title: false,
        button: false,
        navbar: false,
        transition: false,
        loading: false,
        toolbar: {
          zoomIn: { show: true, size: 'large' },
          zoomOut: { show: true, size: 'large' },
          oneToOne: { show: true, size: 'large' },
          reset: { show: true, size: 'large' },
          rotateLeft: { show: true, size: 'large' },
          rotateRight: { show: true, size: 'large' },
          flipHorizontal: { show: true, size: 'large' },
          flipVertical: { show: true, size: 'large' },
        },
        hidden: () => viewer.destroy()
      });
      viewer.show();
      return;
    }

    if (content.attachment.url && content.attachment.type === 'application/pdf') {
      this.props.openModal({
        lightbox: true,
        component: (
          <iframe
            title={content.description}
            style={{ width: '100%', height: 'calc(100% - 12rem)', margin: '6rem' }}
            src={`${content.attachment.url}`}
          />
        )
      });
      return;
    }
    const { displayName = '', email = '' } = content.createdBy;

    this.props.openModal({
      title: `${displayName || email} - ${this.props.format.datetime(content.createdAt)}`,
      disableClose: true,
      component: (
        <Modal.Wrapper>
          <Modal.Content><Text>{content.description}</Text></Modal.Content>
        </Modal.Wrapper>
      )
    });

  }

  createFeedback = () => {
    const { currentUser } = this.context;
    this.props.openModal({
      title: 'Feedback',
      disableBackgroundClick: true,
      adaptiveWidth: true,
      component: (
        <CreateFeedback
          defaultValues={{
            deliveryId: this.delivery.id,
            createdBy: {
              id: currentUser.id,
              displayName: currentUser.displayName,
              email: currentUser.email,
              phoneNumber: currentUser.phoneNumber
            }
          }}
          onClickToCancel={this.props.closeModal}
          onSuccess={this.props.closeModal}
        />
      )
    });
  }

  /**
   * Records actions
   */

  createRecord = () => {
    const links = [`CTe: ${this.delivery.billOfLading}`, ...(this.delivery.invoices || []).map((nf) => `NF: ${nf}`)];
    this.props.openModal({
      title: 'Criar registro',
      disableBackgroundClick: true,
      component: (
        <CreateRecord
          defaultValues={{ ...this.buildRecordData(), links }}
          onClickToCancel={this.props.closeModal}
          onSuccess={this.props.closeModal}
        />
      )
    });
  }

  removeRecord = (row) => {
    const confirm = this.alert({
      title: 'Tem certeza que deseja remover o registro?',
      description: row.description,
      onConfirm: () => {
        this.props.removeRecord(this.delivery.id, row.id);
        confirm.close();
      }
    });
  }

  buildRecordData = (description, type, automatic) => {
    const { currentUser } = this.context;
    return {
      companyId: this.delivery.companyId,
      deliveryId: this.delivery.id,
      createdBy: {
        id: currentUser.id,
        displayName: currentUser.displayName,
        email: currentUser.email,
        phoneNumber: currentUser.phoneNumber
      },
      clientId: this.delivery.client.id,
      shipperId: this.delivery.shipper.id,
      type: type || 'event',
      automatic: automatic || false,
      description: description || 'Sem descrição.'
    };
  }

};

export default connect((store) => ({
  reducer: store.deliveryPublic
}), { ...actions })(baseLayoutWrapper(DeliveryPublic));
