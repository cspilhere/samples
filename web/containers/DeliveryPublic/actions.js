import moment from 'moment';

import { downloadFile, csvFromJs, recordsCsvObject } from 'utils/utils';

/**
 * Delivery
 */

export const fetchDelivery = (token) => ({ type: 'PUBLIC_FETCH_DELIVERY', token });

export const clearDeliveryData = () => (dispatch, store) => {
  dispatch({ type: 'PUBLIC_CLEAR_DELIVERY' });
};

/**
 * Records
 */

export const createRecord = (deliveryId, body) => (
  { type: 'PUBLIC_CREATE_RECORD', deliveryId, body }
);

export const removeRecord = (deliveryId, recordId) => (
  { type: 'PUBLIC_REMOVE_RECORD', deliveryId, recordId }
);
