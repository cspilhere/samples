import { eventChannel } from 'redux-saga';
import { takeEvery, takeLatest, select, put, take, call, all } from 'redux-saga/effects';

import { delivery, records, company } from 'services/firestore';

import Constants from 'services/Constants';

// Retrieve delivery data
function* fetchDelivery(action) {
  const createDeliveryChannel = () => eventChannel((emit) => {
    delivery.fetchByToken(action.token, emit).catch((error) => emit(new Error(error)));
    return () => {};
  });
  const createRecordsChannel = (deliveryId) => eventChannel((emit) => {
    records.fetch(deliveryId, emit).catch((error) => emit(new Error(error)));
    return () => {};
  });
  const deliveryChannel = yield call(createDeliveryChannel);
  while (true) {
    try {
      const delivery = yield take(deliveryChannel);
      Constants.set('companyId', delivery.companyId);
      const companyData = yield call(company.publicData, delivery.companyId);
      const recordsChannel = yield call(createRecordsChannel, delivery.id);
      const records = yield take(recordsChannel);
      yield put({
        type: 'PUBLIC_FETCH_DELIVERY_SUCCESS',
        payload: {
          delivery,
          company: companyData,
          records
        }
      });
    } catch (error) {
      console.log(error);
    };
  };
};

function* createRecord(action) {
  try {
    yield call(records.create, action.deliveryId, action.body);
    yield put({ type: 'PUBLIC_CREATE_RECORD_SUCCESS' });
  } catch (error) {
    console.log(error);
  }
};

function* removeRecord(action) {
  try {
    yield call(records.remove, action.deliveryId, action.recordId);
    yield put({ type: 'PUBLIC_REMOVE_RECORD_SUCCESS' });
  } catch (error) {
    console.log(error);
  }
};

export default function* root() {
  yield all([
    takeLatest('PUBLIC_FETCH_DELIVERY', fetchDelivery),
    takeLatest('PUBLIC_CREATE_RECORD', createRecord),
    takeLatest('PUBLIC_REMOVE_RECORD', removeRecord),
  ]);
};
