const INITIAL_STATE = {
  delivery: {},
  company: null,
  records: [],
  loading: false,
  error: false
};

export default (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {

    case 'PUBLIC_FETCH_DELIVERY':
      return state = {
        ...state,
        loading: true
      };

    case 'PUBLIC_FETCH_DELIVERY_SUCCESS':
      return state = {
        ...state,
        ...action.payload,
        loading: false,
        error: false
      };

    case 'PUBLIC_CLEAR_DELIVERY_STATE':
      return state = INITIAL_STATE;

    default:
      return state;
  }
};
