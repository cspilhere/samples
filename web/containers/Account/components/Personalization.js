import React, { useState } from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import { TwitterPicker } from 'react-color';

import { FileReader, Text } from 'shared-components';

import logo from 'static/media/levalog-logo.svg';

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  padding: 10px;
  border-radius: 2px;
  background-color: ${({ color }) => color};
`;

const ImgContainer = styled.div`
  height: 96px;
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  position: relative;
`;

const Img = styled.img`
  height: 22px;
  cursor: pointer;
`;

const Personalization = (props) => {

  const [imageSrc, setImageSrc] = useState(null);

  return (
    <Wrapper color={props.company.color || '#1A497B'}>

      <TwitterPicker
        color={props.company.color || '#1A497B'}
        colors={[
          '#FF6900',
          '#FCB900',
          '#00D084',
          '#1A497B',
          '#2670FF',
          '#0693E3',
          '#4A4A4A',
          '#EB144C',
          '#F78DA7',
          '#9900EF'
        ]}
        triangle="hide"
        onChange={props.onChangeColor}
      />
      <ImgContainer>
        <FileReader
          accept=".jpg, .png, .svg"
          onChange={(files) => {
            files[0].preview().then((base64) => {
              setImageSrc(base64);
            });
            props.onChangeLogo(files[0]);
          }}
          render={() => <Img src={props.company.logo || imageSrc || logo} />}
        />
        <Text
          isSmall
          hasWhiteColor
          style={{
            position: 'absolute',
            bottom: 0,
            right: 10
          }}>
          <em>Clique na imagem para atualizar</em>
        </Text>
      </ImgContainer>

    </Wrapper>
  );
};

Personalization.propTypes = {

};

export default Personalization;
