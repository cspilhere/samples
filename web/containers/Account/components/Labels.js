import React from 'react';
import PropTypes from 'prop-types';

import { company } from 'services/firestore';

import TagsInput from 'components/lib/TagsInput';

const Labels = (props) => {
  return (
    <TagsInput
      defaultValue={props.data || []}
      onChange={(options, newOption) => {
        company.updateLabels(options)
        .then((response) => {
          console.log(response);
        })
        .catch((error) => {
          console.log(error);
        })
      }}
    />
  );
};

Labels.propTypes = {

};

export default Labels;
