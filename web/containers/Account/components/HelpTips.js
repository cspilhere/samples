import React from 'react';
import PropTypes from 'prop-types';

import Tips from 'components/modules/Tips';

import { Title, Text, Icon, Button } from 'components';

function downloadFile(name, content) {
  const element = document.createElement('A');
  element.href = content;
  element.target = '_blank';
  element.download = name;
  element.click();
};

const HelpTips = ({ isVisible, onHide }) => {
  const isOpenByDefault = localStorage.getItem('helpTips-ImportCTes');
  return (
    <Tips
      isVisible={isVisible || !isOpenByDefault}
      onHide={() => {
        localStorage.setItem('helpTips-ImportCTes', false);
        if (onHide) onHide();
      }}
      cards={[{
        title: <Title light>Ajuda <Icon name="fas fa-life-ring" color="#1C99FE" /></Title>,
        items: [
          'Você pode importar seus CTes utilizando os arquivos XML, envie um ou mais arquivos ao mesmo tempo;',
          'Após concluir a importação retorne até a lista de Entregas para conferir seus CTes recém importados;',
          'Clientes e Embarcadores são salvos automaticamente no sistema;',
          <>
            <Text small light>
              Baixe um modelo de CTe para testar agora mesmo:&nbsp;
              <Button
                bare
                onClick={() => {
                  downloadFile('ModeloCTe.xml', 'https://firebasestorage.googleapis.com/v0/b/levalog.appspot.com/o/ModeloCTe.xml?alt=media&token=4d11fcea-6f73-43a4-9e3a-b14827980962');
                }}>
                <Text small color="#ffc940">Modelo.xml</Text>
              </Button>
            </Text>
          </>
        ]
      }, {
        title: 'Quer saber mais?',
        items: [
          <Button
            secondary
            small
            onClick={() => {
              localStorage.setItem('helpTips-ImportCTes', false);
              if (onHide) onHide();
              Intercom('startTour', 56514);
            }}>
            Iniciar Tour
          </Button>
        ]
      }]}
    />
  );
};

export default HelpTips;
