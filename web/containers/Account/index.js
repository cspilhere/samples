import React from 'react';

import {
  Button,
  Layout,
  Space,
  Datatable,
  Switch,
  Container,
  Heading,
  Text,
  FlexBar,
  AlertContainer,
  Icon,
  Grid,
  Spinner,
  FormField,
} from 'shared-components';

import { Formik, Form, FieldArray } from 'formik';

import { alert } from 'shared-components/lib/Alert';

import Box from 'components/lib/Box';
import Input from 'components/lib/Input';

import { company as companyMethods } from 'services/firestore';

import { callToast } from 'utils/utils';

import { checkObjectKeys, updateObjectAndClear } from 'shared-utils/utils';

import { MainContext } from 'containers/MainContainer';

import withModal from 'shared-components/HOCs/withModal';

import { saveData } from './actions';

import Labels from './components/Labels';
import Personalization from './components/Personalization';
import HelpTips from './components/HelpTips';

class Account extends React.Component {

  static contextType = MainContext;

  state = {
    fields: {},
    showHelp: false
  }

  componentDidMount() {}

  render() {

    const { company } = this.context;

    const { fields } = this.state;

    if (!company.settings) company.settings = {};

    const hasSettings = company.settings && company.settings.hasOwnProperty('showDriverOnPublicDelivery');
    const showDriverOnPublicDelivery = hasSettings ? company.settings.showDriverOnPublicDelivery : true;

    return (
      <Layout>

        {/* <LayoutHeader
          breadcrumbPath={[{
            name: <Icon name="fas fas-sm fa-home" style={{ marginRight: -3, marginLeft: -2 }} />, title: 'Home', pathname: '/'
          }, {
            name: 'Conta e configurações', title: 'Conta e configurações'
          }]}
          breadcrumbPathClick={(path) => this.props.history.push(path)}
        /> */}

        <Layout.Body>
          <Layout.Content grow padding={20}>

            <Space size="medium" />

            <div style={{ maxWidth: 1024, width: '100%', marginLeft: 'auto', marginRight: 'auto' }}>
              <Box clearFlexAlignment style={{ padding: 20 }}>

                <Heading is6 hasUnderline hasBottomSpace>Cadastro</Heading><Space />

                <Formik
                  initialValues={{ name: company.name }}
                  onSubmit={(values, actions) => {
                    if (values.name.length < 4) {
                      const confirm = alert({
                        title: 'Atenção!',
                        description: 'Para renomear sua empresa você precisa inserir um valor com no mínimo 4 letras.',
                        onConfirm: () => {
                          confirm.close();
                          actions.setFieldValue('name', company.name);
                          actions.setSubmitting(false);
                        }
                      });
                    } else {
                      companyMethods.updateName(values.name).then(() => {
                        actions.setSubmitting(false);
                      });
                    }
                  }}
                  render={({
                    values,
                    handleChange,
                    handleSubmit,
                    touched,
                    isSubmitting
                  }) => (
                    <Form>
                      <FormField label="Nome da empresa" labelInfo="Pressione ENTER para salvar">
                        <Input
                          name="name"
                          disabled={isSubmitting}
                          value={values.name}
                          loading={isSubmitting}
                          onChange={handleChange}
                        />
                        {touched && (
                          <>
                            &nbsp;
                            <Button type="submit" onClick={handleSubmit}><Icon name="fas fa-sm fa-pencil" /></Button>
                          </>
                        )}
                      </FormField>
                    </Form>
                  )}
                />

                {/*  */}
                <Space size="medium" />
                <Heading is7 hasUnderline hasBottomSpace>
                  Personalização&nbsp;{this.state.isUpdatingLogo && <Spinner size={16} />}
                </Heading>
                <Space />
                <Personalization
                  company={company}
                  onChangeColor={({ hex }) => {
                    companyMethods.updateColor(hex).then(() => {});
                  }}
                  onChangeLogo={(value) => {
                    this.setState({ isUpdatingLogo: true });
                    companyMethods.updateLogo(value).then((url) => {
                      this.setState({ isUpdatingLogo: false });
                    });
                  }}
                />

                {/*  */}
                <Space size="medium" /><Heading is7 hasUnderline hasBottomSpace>Etiquetas</Heading><Space />
                <Labels data={company.labels} />

                {/*  */}
                <Space size="medium" /><Heading is7 hasUnderline hasBottomSpace>Configurações</Heading><Space />
                <Switch
                  description={'Mostrar card do motorista na tela de acompanhamento? ' + (showDriverOnPublicDelivery ? 'Sim' : 'Não')}
                  defaultChecked={showDriverOnPublicDelivery}
                  onChange={({ target }) => {
                    companyMethods.updateSettings({ showDriverOnPublicDelivery: target.checked })
                    .then((response) => {
                      console.log(response);
                    })
                    .catch((error) => {
                      console.log(error);
                    });
                  }}
                />

                {/*  */}
                {/* <Space size="medium" /><Heading is7 hasUnderline hasBottomSpace>Contrato</Heading>
                <Text>
                  <strong><span style={{ fontSize: 22, color: '#1A497B' }}>Lançamento</span></strong>
                </Text>
                <Text hasGold5Color><span style={{ fontSize: 18 }}><em>Early adoption</em></span></Text> */}

              </Box>
            </div>
          </Layout.Content>

        </Layout.Body>
      </Layout>
    );
  }

  submitForm = () => {

    const { fields } = this.state;
    // const { defaultBody } = this.props;

    // if (!checkObjectKeys(fields, ['cnpj', 'name', 'email', 'phone'])) {
    //   callToast('warning', 'Preencha todos os campos obrigatórios!');
    //   return;
    // }

    // if (!checkObjectKeys(fields, ['address.location'])) {
    //   callToast('warning', 'Você precisa de um endereço válido para obter as coordenadas.');
    //   return;
    // }

    this.setState({ isLoading: true }, () => {
      saveData({ ...fields })
      .then((response) => {
        // console.log(response);
        callToast('success', 'Empresa atualizada com sucesso!');
        this.setState({ isLoading: false });
      })
      .catch((error) => {
        // console.log(error);
        callToast('error', 'Ocorreu um erro ao tentar atualizar a empresa!');
        this.setState({ isLoading: false });
      });
    });

  }

  handleInputEvent = ({ target }, isValid) => {
    if (target.value) this.updateFields(target.name, target.value);
  }

  updateFields = (name, value) => {
    if (value) this.setState({ fields: updateObjectAndClear(this.state.fields, name, value) });
  }

};

export default withModal(Account);
