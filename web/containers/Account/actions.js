import firebase from 'firebase';

const db = firebase.firestore();

import Constants from 'services/Constants';

export const saveData = (body) => {

  console.log(body);

  return new Promise((resolve, reject) => {

    if (body.address && body.address.location) {
      body.address.location = new firebase.firestore.GeoPoint(
        body.address.location.lat || body.address.location.latitude,
        body.address.location.lng || body.address.location.longitude
      );
    }

    db.collection('companies').doc(Constants.get('companyId'))
    .update(body)
    .then(resolve)
    .catch(reject);

  });

};
