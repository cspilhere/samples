import React from 'react';

import * as firebase from 'firebase/app';
import 'firebase/firestore';

import { FIREBASE_CONFIG } from 'core/constants';

const firebaseConfig = FIREBASE_CONFIG;

firebase.initializeApp(firebaseConfig);

export const AuthContext = React.createContext({});

class AuthManager extends React.PureComponent {

  state = {
    currentUser: null,
    isWaitingForUserData: true
  }

  onUserAuthChanged = (callback, isPublic) => (
    firebase.auth().onAuthStateChanged((currentUser) => {

      if (currentUser) {

        if (!this.state.currentUser) {

          if (isPublic) {
            this.setState({
              currentUser: { ...currentUser.toJSON(), id: currentUser.toJSON().uid },
              isWaitingForUserData: false
            }, () => {
              if (callback && typeof callback === 'function') callback({ isSignedIn: true });
            });
            return;
          }

          if (currentUser.toJSON().email) {

            this.setState({
              currentUser: { ...currentUser.toJSON(), id: currentUser.toJSON().uid },
              isWaitingForUserData: false
            }, () => {
              if (callback && typeof callback === 'function') callback({ isSignedIn: true });
            });

          } else {
            firebase.auth().signOut();
          }

        }

      } else {
        if (!currentUser) {
          if (callback && typeof callback === 'function') callback({ isSignedIn: false });
          console.log('User are logged out');
          if (isPublic) {
            firebase.auth()
            .signInAnonymously()
            .catch((error) => {
              console.log(error);
            });
            return;
          }
          if (location.pathname != '/auth') location.href = './auth';
        }
      }
    })
  )

  render() {
    return (
      <AuthContext.Provider
        value={{
          onUserAuthChanged: this.onUserAuthChanged,
          userIsReady: !this.state.isWaitingForUserData && this.state.currentUser ? true : false,
          ...this.state
        }}>
        {this.props.children}
      </AuthContext.Provider>
    );
  }

};

export default AuthManager;
