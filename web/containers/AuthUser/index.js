import React from 'react';

import ReactPasswordStrength from 'react-password-strength';

import {
  FormField,
  Button
} from 'shared-components';

import {
  Input,
  Title,
  Text
} from 'components';

import { FormHeader } from './components';

import { AuthContext } from 'containers/AuthManager';

import { signIn } from './actions';

import logo from 'static/media/levalog-logo.svg';
import illustration from 'static/media/illustration.png';

import FormWrapper from './components/FormWrapper';

import { validationSchema } from 'services/masksAndValidators';

const validator = validationSchema([
  'email',
  'password'
]);

export default class Auth extends React.Component {

  static contextType = AuthContext;

  state = {
    isSuccess: false,
    message: ''
  }

  render() {

    const { currentUser, isWaitingForUserData, loginProcessStarted } = this.context;

    const returnTo = location.search.replace('?returnTo=', '');

    return (

      <div className="auth-screen">
        <div className="auth-screen-form" style={{ position: 'relative' }}>

          <div className="auth-screen-form-wrapper">

            <FormHeader>
              <Title size={26} color="#1A497B">Acessar minha conta</Title>
              {/* <Text small color="#293742">Faça login para acessar sua conta no Levalog.</Text> */}
            </FormHeader>

            {this.state.message && (
              <div className={'auth-screen-form-output' + (this.state.isSuccess ? ' is-success' : '')} style={{
                position: 'absolute',
                top: 40,
                right: 40,
                width: 'calc(100% - 80px)'
              }}>
                <Text isSmall>{this.state.message}</Text>
              </div>
            )}

            <FormWrapper
              validationSchema={validator}
              onSubmit={(values, actions) => {

                signIn(values.email, values.password)
                .then(() => {
                  this.setState({
                    isSuccess: true,
                    message: 'Tudo certo! Redirecionando...'
                  });
                  const goto = returnTo ? returnTo : './';
                  setTimeout(() => location.href = goto, 800);
                })
                .catch(({ code }) => {
                  this.setState({ isSuccess: false });
                  if (code === 'auth/wrong-password') {
                    this.setState({ message: 'Email ou Senha incorretos. Porfavor confirme seus dados e tente novamente.' });
                    actions.setSubmitting(false);
                  }
                  if (code === 'auth/user-not-found') {
                    actions.setSubmitting(false);
                    this.setState({
                      message: (
                        <>
                          Cadastro não encontrado. Confirme seus dados ou&nbsp;
                          <span
                            id="login-output-register-link"
                            className="inline-link"
                            onClick={() => location.href = './auth/signup' + (returnTo ? '?returnTo=' + returnTo : '')}>
                            <strong>crie uma nova conta</strong>
                          </span>.
                        </>
                      )
                    });
                  }
                });

              }}
              render={({
                values,
                handleChange,
                handleSubmit,
                isValid,
                isSubmitting,
              }) => {

                const isReallyValid = values.email && values.password && isValid;

                return (
                  <div style={{ maxWidth: 300, margin: 'auto' }}>
                    <FormField label="Email">
                      <Input
                        id="login-email-field"
                        name="email"
                        disabled={isSubmitting}
                        value={values.email}
                        onFocus={() => setTimeout(() => this.setState({ message: '' }), 130)}
                        onChange={handleChange}
                      />
                    </FormField>

                    <FormField label="Senha">
                      <Input
                        id="login-password-field"
                        name="password"
                        type="password"
                        disabled={isSubmitting}
                        value={values.password}
                        onFocus={() => setTimeout(() => this.setState({ message: '' }), 130)}
                        onChange={handleChange}
                      />
                    </FormField>

                    <div className="auth-screen-form-helpers" style={{ marginTop: -14 }}>
                      <Text small>
                        <span
                          id="login-recovery-link"
                          className="inline-link"
                          onClick={() => location.href = './auth/recovery' + (returnTo ? '?returnTo=' + returnTo : '')}>
                          Perdeu sua senha?
                        </span>
                      </Text>
                    </div>

                    <div className="buttons">
                      <Button
                        id="login-signin-button"
                        isPrimary
                        isFullWidth
                        submit
                        disabled={isSubmitting || !isReallyValid}
                        onClick={handleSubmit}>
                        Entrar
                      </Button>
                    </div>

                  </div>
                );
              }}
            />

            <div className="auth-screen-form-footer" style={{ marginTop: 20 }}>
              <Text small>
                Ainda não tem uma conta?&nbsp;
                <span
                  id="login-register-link"
                  className="inline-link"
                  onClick={() => location.href = './auth/signup' + (returnTo ? '?returnTo=' + returnTo : '')}>
                  <strong>Cadastre-se!</strong>
                </span>
              </Text>
            </div>

          </div>

        </div>

        <div className="auth-screen-featured">
          <img src={logo} />
          {/* <img src={logo} style={{ position: 'absolute', top: 40, right: 40, width: 190 }} />
          <img src={illustration} style={{ width: '75%' }} /> */}
        </div>

      </div>
    );
  }

};
