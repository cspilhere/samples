import React from 'react';

import {
  Form,
  Text,
  TextInput,
  FormField,
  Button
} from 'shared-components';

import { AuthContext } from 'containers/AuthManager';

import { signIn } from '../actions';

import FormHeader from '../components/FormHeader';

import logo from 'static/media/levalog-logo.svg';

export default class Auth extends React.Component {

  static contextType = AuthContext;

  state = {
    isLoading: false,
    isSuccess: false,
    message: ''
  }

  render() {

    const { currentUser, isWaitingForUserData, loginProcessStarted } = this.context;

    const returnTo = location.search.replace('?returnTo=', '');

    return (

      <div className="auth-screen">
        <div className="auth-screen-form">

          <div className="auth-screen-form-wrapper">

            <FormHeader>Entrar</FormHeader>

            {this.state.message && (
              <div className={'auth-screen-form-output' + (this.state.isSuccess ? ' is-success' : '')}>
                <Text isSmall>{this.state.message}</Text>
              </div>
            )}

            <Form autoFocus>
              <FormField label="Email">
                <TextInput
                  id="login-email-field"
                  name="email"
                  disabled={this.state.isLoading}
                  value={this.state.email}
                  onFocus={() => setTimeout(() => this.setState({ message: '' }), 130)}
                  onChange={({ target }) => this.setState({ email: target.value })}
                />
              </FormField>
              <FormField label="Senha">
                <TextInput
                  id="login-password-field"
                  name="password"
                  type="password"
                  disabled={this.state.isLoading}
                  value={this.state.password}
                  onFocus={() => setTimeout(() => this.setState({ message: '' }), 130)}
                  onChange={({ target }) => this.setState({ password: target.value })}
                />
              </FormField>
            </Form>

            <div className="auth-screen-form-helpers">
              <Text isSmall>
                <span
                  id="login-recovery-link"
                  className="inline-link"
                  onClick={() => location.href = './auth/recovery' + (returnTo ? '?returnTo=' + returnTo : '')}>
                  Perdeu sua senha?
                </span>
              </Text>
            </div>

            <div className="buttons">
              <Button
                id="login-signin-button"
                isPrimary
                isFullWidth
                disabled={this.state.isLoading}
                onClick={() => {
                  this.setState({ isLoading: true });
                  signIn(this.state.email, this.state.password)
                  .then(() => {
                    this.setState({
                      isSuccess: true,
                      message: 'Tudo certo! Redirecionando...'
                    });
                    const goto = returnTo ? returnTo : './';
                    setTimeout(() => location.href = goto, 800);
                  })
                  .catch(({ code }) => {
                    this.setState({ isLoading: false, isSuccess: false });
                    if (code === 'auth/wrong-password') {
                      this.setState({ message: 'Email ou senha incorretos. Porfavor confirme seus dados novamente.' });
                    }
                    if (code === 'auth/user-not-found') {
                      this.setState({
                        message: (
                          <>
                            Cadastro não encontrado. Confirme seus dados ou&nbsp;
                            <span
                              id="login-output-register-link"
                              className="inline-link"
                              onClick={() => location.href = './auth/signup' + (returnTo ? '?returnTo=' + returnTo : '')}>
                              <strong>crie uma nova conta</strong>
                            </span>.
                          </>
                        )
                      });
                    }
                  });
                }}>
                Entrar
              </Button>
            </div>

            <div className="auth-screen-form-footer">
              <Text isSmall>
                Ainda não tem uma conta?&nbsp;
                <span
                  id="login-register-link"
                  className="inline-link"
                  onClick={() => location.href = './auth/signup' + (returnTo ? '?returnTo=' + returnTo : '')}>
                  <strong>Cadastre-se agora!</strong>
                </span>
              </Text>
            </div>

          </div>

        </div>

        <div className="auth-screen-featured">
          <img src={logo} />
        </div>

      </div>
    );
  }

};
