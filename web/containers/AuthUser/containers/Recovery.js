import React from 'react';

import {
  Form,
  Text,
  TextInput,
  FormField,
  Button
} from 'shared-components';

import { AuthContext } from 'containers/AuthManager';

import { passwordRecovery } from '../actions';

import FormHeader from '../components/FormHeader';

import logo from 'static/media/levalog-logo.svg';

export default class Recovery extends React.Component {

  static contextType = AuthContext;

  state = {
    isLoading: false,
    isSuccess: false,
    message: ''
  }

  render() {

    const { currentUser, isWaitingForUserData, loginProcessStarted } = this.context;

    const returnTo = location.search.replace('?returnTo=', '');

    return (

      <div className="auth-screen">
        <div className="auth-screen-form">

          <div className="auth-screen-form-wrapper">

            <FormHeader>Recuperação de Senha</FormHeader>

            {this.state.message && (
              <div className={'auth-screen-form-output' + (this.state.isSuccess ? ' is-success' : '')}>
                <Text isSmall>{this.state.message}</Text>
              </div>
            )}

            <Form autoFocus>
              <FormField label="Email" labelInfo="*">
                <TextInput
                  id="recovery-email-field"
                  name="email"
                  disabled={this.state.isLoading}
                  value={this.state.email}
                  onFocus={() => setTimeout(() => this.setState({ message: '' }), 130)}
                  onChange={({ target }) => this.setState({ email: target.value })}
                />
              </FormField>
            </Form>

            <div className="auth-screen-form-helpers"></div>

            <div className="buttons">
              <Button
                id="recovery-send-button"
                isPrimary
                isFullWidth
                disabled={this.state.isLoading}
                onClick={() => {
                  this.setState({ isLoading: true });
                  passwordRecovery(this.state.email, this.state.password)
                  .then((user) => {
                    this.setState({
                      isSuccess: true,
                      isLoading: false,
                      message: (
                        <>
                          Verifique seu email para definir uma nova senha e depois&nbsp;
                          <span
                            id="recovery-output-login-link"
                            className="inline-link"
                            onClick={() => location.href = './auth' + (returnTo ? '?returnTo=' + returnTo : '')}>
                            <strong>volte para a tela de login</strong>
                          </span>.
                        </>
                      )
                    });
                  })
                  .catch(({ code }) => {
                    console.log(code);
                    this.setState({ isLoading: false, isSuccess: false });
                    if (code === 'auth/email-already-in-use') {
                      this.setState({ message: 'Este email já existe. Tente fazer login novamente.' });
                      return;
                    }
                    this.setState({ message: 'Ocorreu um erro inesperado, tente novamente mais tarde. :(' });
                  });
                }}>
                Enviar
              </Button>
            </div>

            <div className="auth-screen-form-footer">
              <Text isSmall>
                Já possui cadastro?&nbsp;
                <span
                  id="recovery-login-link"
                  className="inline-link"
                  onClick={() => location.href = './auth' + (returnTo ? '?returnTo=' + returnTo : '')}>
                  <strong>Faça login para entrar.</strong>
                </span>
              </Text>
            </div>

          </div>

        </div>

        <div className="auth-screen-featured">
          <img src={logo} />
        </div>

      </div>
    );
  }

};
