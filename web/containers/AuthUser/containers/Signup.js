import React from 'react';

import ReactPasswordStrength from 'react-password-strength';

import {
  FormField,
  Button
} from 'shared-components';

import {
  Input,
  Title,
  Text
} from 'components';

import {
  FormHeader
} from '../components';

import { AuthContext } from 'containers/AuthManager';

import { createUser } from '../actions';

import logo from 'static/media/levalog-logo.svg';
import illustration from 'static/media/illustration.png';

import FormWrapper from '../components/FormWrapper';

import { validationSchema } from 'services/masksAndValidators';

const validator = validationSchema([
  'name',
  'email'
]);

export default class Signup extends React.Component {

  static contextType = AuthContext;

  state = {
    isSuccess: false,
    message: '',
    passwordIsValid: false
  }

  render() {

    const { currentUser, isWaitingForUserData, loginProcessStarted } = this.context;

    const returnTo = location.search.replace('?returnTo=', '');

    return (

      <div className="auth-screen">
        <div className="auth-screen-form" style={{ position: 'relative' }}>

          <div className="auth-screen-form-wrapper">

            <FormHeader>
              <Title size={26} color="#1A497B">Crie sua conta agora!</Title>
              <Text small color="#293742">E comece a usar o Levalog em 1 minuto.</Text>
            </FormHeader>

            {this.state.message && (
              <div className={'auth-screen-form-output' + (this.state.isSuccess ? ' is-success' : '')} style={{
                position: 'absolute',
                top: 40,
                right: 40,
                width: 'calc(100% - 80px)'
              }}>
                <Text isSmall>{this.state.message}</Text>
              </div>
            )}

            <FormWrapper
              validationSchema={validator}
              onSubmit={(values, actions) => {
                if (!(values.name && values.email && values.password)) {
                  this.setState({ message: 'Os campos Nome, Email e Senha são obrigatórios.' });
                  actions.setSubmitting(false);
                  return;
                }
                createUser(values.email, values.password, values.name)
                .then((user) => {
                  this.setState({
                    isSuccess: true,
                    message: 'Conta criada com sucesso! Redirecionando...'
                  });
                  const goto = returnTo ? returnTo : './';
                  setTimeout(() => location.href = goto, 800);
                })
                .catch(({ code }) => {
                  this.setState({ isSuccess: false });
                  if (code === 'auth/email-already-in-use') {
                    this.setState({ message: 'Este email já existe. Tente acessar sua conta.' });
                    actions.setSubmitting(false);
                    return;
                  }
                  this.setState({ message: 'Ocorreu um erro inesperado, tente novamente mais tarde. :(' });
                  actions.setSubmitting(false);
                });
              }}
              render={({
                values,
                errors,
                handleChange,
                setFieldValue,
                handleSubmit,
                isValid,
                isSubmitting,
              }) => {

                const isReallyValid = values.name && values.email && values.password && isValid && this.state.passwordIsValid;

                return (
                  <div style={{ maxWidth: 300, margin: 'auto' }}>
                    <FormField label="Qual é seu nome?" labelInfo={!errors.name && values.name ? '✔' : ''}>
                      <Input
                        id="register-name-field"
                        name="name"
                        disabled={isSubmitting}
                        value={values.name}
                        onFocus={() => setTimeout(() => this.setState({ message: '' }), 130)}
                        onChange={handleChange}
                      />
                    </FormField>
                    <FormField label="E seu email comercial?" labelInfo={!errors.email && values.email ? '✔' : ''}>
                      <Input
                        id="register-email-field"
                        name="email"
                        disabled={isSubmitting}
                        value={values.email}
                        onFocus={() => setTimeout(() => this.setState({ message: '' }), 130)}
                        onChange={handleChange}
                      />
                    </FormField>
                    <FormField label="Crie uma senha:" labelInfo={!errors.password && values.password ? '✔' : ''}>
                      <ReactPasswordStrength
                        style={{ width: '100%' }}
                        minLength={6}
                        minScore={1}
                        tooShortWord="Senha curta"
                        scoreWords={['Muito fraca', 'Fraca', 'Boa', 'Forte', 'Muito forte']}
                        changeCallback={({ isValid, password }) => {
                          setFieldValue('password', password);
                          this.setState({ passwordIsValid: isValid });
                        }}
                        inputProps={{
                          id: 'register-password-field',
                          name: 'password',
                          autoComplete: 'off',
                          onFocus: () => setTimeout(() => this.setState({ message: '' }), 130)
                        }}
                      />
                    </FormField>

                    <div className="buttons">
                      <Button
                        id="register-save-button"
                        isPrimary
                        isFullWidth
                        submit
                        disabled={isSubmitting || !isReallyValid}
                        onClick={handleSubmit}>
                        Começar!
                      </Button>
                    </div>

                    <Text size={12} color="#888" style={{ textAlign: 'center', width: '100%' }}>
                      Clicando em "Começar!" você concorda com nossos&nbsp;
                      <a href="https://levalog.com/terms-and-conditions/" target="_blank">termos de uso</a> e&nbsp;
                      <a href="https://levalog.com/privacy-policy/" target="_blank">política de privacidade.</a>
                    </Text>

                  </div>
                );
              }}
            />

            <div className="auth-screen-form-footer" style={{ marginTop: 20 }}>
              <Text small>
                Já possui cadastro?&nbsp;
                <span
                  id="register-login-link"
                  className="inline-link"
                  onClick={() => location.href = './auth' + (returnTo ? '?returnTo=' + returnTo : '')}>
                  <strong>Acesse sua conta.</strong>
                </span>
              </Text>
            </div>

          </div>

        </div>

        <div className="auth-screen-featured">
          <img src={logo} />
          {/* <img src={logo} style={{ position: 'absolute', top: 40, right: 40, width: 190 }} />
          <img src={illustration} style={{ width: '75%' }} /> */}
        </div>

      </div>
    );
  }

};
