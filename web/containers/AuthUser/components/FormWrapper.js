import React from 'react';

import { FormContainer } from 'components';

const FormWrapper = ({ render, autoFocus, renderFooterRight, footerRight, ...rest }) => (
  <FormContainer
    {...rest}
    autoFocus={autoFocus}
    render={(renderProps) => {
      return render(renderProps);
    }}
  />
);

FormWrapper.defaultProps = {
  render: () => null,
  autoFocus: true,
  renderFooterRight: () => null
};

export default FormWrapper;
