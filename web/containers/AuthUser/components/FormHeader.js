import React from 'react';

import { Text } from 'shared-components';

const FormHeader = ({ children }) => {
  return (
    <div className="auth-screen-form-header">
      <Text>
        <strong>{children}</strong>
      </Text>
      {/* <Text isSmall hasGray3Color>by PinMyCargo</Text> */}
    </div>
  );
};

export default FormHeader;
