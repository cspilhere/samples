import Main from './';
import Recovery from './containers/Recovery';
import Signup from './containers/Signup';
import NotFound from './containers/NotFound';

import { RouteWrapper } from 'utils/RouterUtils';

const routes = [{
  name: 'Authentication',
  path: '/auth',
  component: RouteWrapper,
  routes: [{
    name: 'Login',
    path: '/auth',
    exact: true,
    component: Main
  }, {
    name: 'Password Recovery',
    path: '/auth/recovery',
    exact: true,
    component: Recovery
  }, {
    name: 'Signup',
    path: '/auth/signup',
    exact: true,
    component: Signup
  }, {
    name: 'Auth 404',
    path: '/auth/**',
    component: NotFound
  }]
}];

export default routes;
