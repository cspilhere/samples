import * as firebase from 'firebase/app';
import 'firebase/auth';

export const signIn = (email, password) => {
  return new Promise((resolve, reject) => {
    firebase.auth().signInWithEmailAndPassword(email, password)
    .then(({ user }) => {
      resolve(user);
    })
    .catch((error) => {
      console.log('Error when trying to sign in: ', error);
      reject(error);
    });
  });
}

export const createUser = (email, password, name) => {
  return new Promise((resolve, reject) => {
    firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(({ user }) => {
      user.updateProfile({ displayName: name });
      resolve(user);
      // user.sendEmailVerification().then(() => {
      // }).catch((error) => {
      //   reject(error);
      //   console.log('Confirmation email not sent! ', error);
      // });
    })
    .catch((error) => {
      reject(error);
      console.log('Error when trying to create user: ', error);
    });
  });
}

export const passwordRecovery = (email) => {
  return new Promise((resolve, reject) => {
    firebase.auth().sendPasswordResetEmail(email)
    .then(() => {
      resolve({ success: true });
    })
    .catch((error) => {
      reject(error);
      console.log('Error when trying to reset password: ', error);
    });
  });
}

export const signOut = () => {
  return new Promise((resolve, reject) => {
    if (Intercom) Intercom('shutdown');
    firebase.auth().signOut().then(resolve).catch(reject);
  });
}
