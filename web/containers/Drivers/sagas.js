import { eventChannel } from 'redux-saga';
import { takeLatest, put, take, call, all } from 'redux-saga/effects';

import { drivers } from 'services/firestore';

function createChannel(filter) {
  return eventChannel((emit) => {
    drivers.fetchAll(filter, emit)
    .catch((error) => emit(new Error(error)));
    return () => {};
  });
};

function* fetch(action) {
  const channel = yield call(createChannel, action.filter);
  while (true) {
    try {
      const response = yield take(channel);
      yield put({ type: 'FETCH_DRIVERS_SUCCESS', payload: response });
    } catch (error) {
      console.log(error);
    };
  };
};

function* remove(action) {
  try {
    const response = yield call(drivers.remove, action.id);
    yield put({ type: 'REMOVE_DRIVER_SUCCESS', payload: response });
    action.resolve(response);
  } catch (error) {
    console.log(error);
    action.reject(error);
  }
};

export default function* root() {
  yield all([
    takeLatest('FETCH_DRIVERS', fetch),
    takeLatest('REMOVE_DRIVER', remove),
  ]);
};
