import React from 'react';

import { connect } from 'react-redux';
import * as actions from './actions';

import { Text } from 'shared-components';
import ContentHeader from './components/ContentHeader';
import Table from './components/Table';
import HelpTips from './components/HelpTips';

import BaseLayout, { baseLayoutWrapper } from 'components/custom/baseLayout/BaseLayout';
import BaseHeader from 'components/custom/baseLayout/BaseHeader';

import { MainContext } from 'containers/MainContainer';

import CreateDriver from 'features/CreateDriver';
import UpdateDriver from 'features/UpdateDriver';

class Drivers extends React.Component {

  static contextType = MainContext;

  state = {
    showHelp: false
  }

  render() {
    const { reducer } = this.props;
    const { data, loading, isFirstLoading } = reducer;
    return (
      <BaseLayout
        baseHeader={(
          <BaseHeader
            breadcrumb={[{ name: 'Motoristas', title: 'Motoristas', pathname: '/' }]}
            onClickHelp={() => {
              this.setState({ showHelp: true });
            }}
          />
        )}
        contentHeader={(
          <ContentHeader
            onFilterChange={this.filter}
            openFeature={this.create}>
            <Text isSmall hasGray2Color>
              {loading && isFirstLoading && 'Carregando...'}
              {data && !isFirstLoading && `Mostrando ${data.length} motorista${data.length == 1 ? '' : 's'}`}
            </Text>
          </ContentHeader>
        )}>

        <HelpTips isVisible={this.state.showHelp} onHide={() => this.setState({ showHelp: false })} />

        <Table
          {...this.props}
          onRowClick={this.update}
          onRequestToRemove={this.remove}
        />
      </BaseLayout>
    );
  }

  remove = (driver) => {
    this.props.removeDriver(driver.id)
    .catch(() => this.props.callToast('error', 'Ocorreu um erro! Tente novamente.'));
  }

  update = (event, rowKey) => {
    this.props.openModal({
      title: 'Atualizar motorista',
      component: (
        <UpdateDriver
          defaultValues={this.props.reducer.data[rowKey]}
          onClickToCancel={this.props.closeModal}
          onSuccess={this.props.closeModal}
        />
      )
    });
  }

  create = () => {
    this.props.openModal({
      title: 'Novo motorista',
      disableBackgroundClick: true,
      component: (
        <CreateDriver
          onClickToCancel={this.props.closeModal}
          onSuccess={this.props.closeModal}
        />
      )
    });
  }

  filter = (filter) => {
    this.props.fetchDrivers(filter);
  }

};

export default connect((store) => ({
  reducer: store.drivers
}), { ...actions })(baseLayoutWrapper(Drivers));
