import React from 'react';

import HelpTipsContainer from 'components/custom/HelpTipsContainer';

const HelpTips = ({ isVisible, onHide }) => {
  return (
    <HelpTipsContainer
      isVisible={isVisible}
      onHide={onHide}
      name="helpTips-Drivers"
      items={[
        'Gerencie todos os seus Motoristas facilmente.',
        'Avalie performance de forma individual e comparativa.',
        'Acompanhe a evolução do Motorista com a sua transportadora.'
      ]}
      tourId={55919}
    />
  );
};

export default HelpTips;
