import React from 'react';

import { FlexBar, Select } from 'shared-components';

import InlineFilter from 'components/custom/InlineFilter';

const Filter = ({ onFilterChange }) => {
  return (
    <InlineFilter
      name="drivers"
      placeholder="Nome, CPF, Telefone..."
      onChange={onFilterChange}
      render={(renderProps) => {
        return (
          <FlexBar hasSmallGap>
            <FlexBar.Child>
              <Select
                isSmall
                name="relation"
                value={renderProps.values.relation}
                hideBlankOption
                options={[
                  { label: 'Qualquer relação', value: '' },
                  { label: 'Contratado', value: '0' },
                  { label: 'Agregado', value: '1' },
                  { label: 'Terceirizado', value: '2' },
                  { label: 'Freelancer', value: '3' },
                ]}
                onChange={renderProps.handleChange}
              />
            </FlexBar.Child>
            <FlexBar.Child>
              <Select
                isSmall
                name="active"
                value={renderProps.values.active}
                hideBlankOption
                options={[
                  { label: 'Ativos e inativos', value: '' },
                  { label: 'Somente ativos', value: 'true' },
                  { label: 'Somente inativos', value: 'false' },
                ]}
                onChange={renderProps.handleChange}
              />
            </FlexBar.Child>
          </FlexBar>
        );
      }}
    />
  );
};

export default Filter;
