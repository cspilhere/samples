import React from 'react';

import DataTable, { CellWrapper, Options } from 'components/custom/DataTable';

import { alert } from 'shared-components/lib/Alert';

import DeliveriesCounter from 'components/custom/cards/DeliveriesCounter';
import ActiveStatus from 'components/custom/cards/ActiveStatus';
import DriverRelation from 'components/custom/cards/DriverRelation';
import PhoneNumber from 'components/custom/cards/PhoneNumber';
import EntityDocument from 'components/custom/cards/EntityDocument';
import GenericText from 'components/custom/cards/GenericText';
import GenericDate from 'components/custom/cards/GenericDate';

const Table = ({ reducer, onRowClick, onRequestToRemove }) => (
  <DataTable
    data={reducer.data}
    loading={reducer.loading}
    onRowClick={onRowClick}
    columns={[{
      columnKey: 'name',
      header: 'Motorista',
      cell: <CellWrapper {...reducer} render={(renderProps) => <GenericText {...renderProps} bold />} />,
      flexGrow: 1,
      width: 300
    }, {
      columnKey: 'document',
      header: 'Documento',
      cell: <CellWrapper {...reducer} render={(renderProps) => <EntityDocument {...renderProps} type="cpf" />} />,
      width: 240
    }, {
      columnKey: 'phone',
      header: 'Telefone',
      cell: <CellWrapper {...reducer} render={PhoneNumber} />,
      width: 160
    }, {
      columnKey: 'relation',
      header: 'Relação',
      cell: <CellWrapper {...reducer} render={DriverRelation} />,
      width: 140
    }, {
      columnKey: 'deliveries',
      header: 'Entregas',
      alignHeader: 'center',
      cell: <CellWrapper center {...reducer} render={DeliveriesCounter} />,
      width: 100
    }, {
      columnKey: 'active',
      header: 'Situação',
      alignHeader: 'center',
      cell: <CellWrapper center {...reducer} render={ActiveStatus} />,
      width: 100
    }, {
      columnKey: 'createdAt',
      header: 'Convidado em',
      alignHeader: 'center',
      cell: <CellWrapper center {...reducer} render={GenericDate} />,
      width: 140
    }, {
      cell: (
        <Options
          data={reducer.data}
          onClickToRemove={(rowIndex) => {
            const removeConfirmation = alert({
              title: 'Tem certeza que deseja remover o motorista?',
              description: `ATENÇÃO! Se você prosseguir o motorista ${
                reducer.data[rowIndex].name
              } será removido do sistema.`,
              enableConfirmActionAfter: 3000,
              onConfirm: () => {
                onRequestToRemove(reducer.data[rowIndex]);
                removeConfirmation.close();
              }
            });
          }}
        />
      ),
      width: 60
    }]}
  />
);

export default Table;
