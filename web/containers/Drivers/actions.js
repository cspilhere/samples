export const fetchDrivers = (filter) => ({ type: 'FETCH_DRIVERS', filter });

export const removeDriver = (id) => (dispatch) => (
  new Promise((resolve, reject) => (
    dispatch({ type: 'REMOVE_DRIVER', id, resolve, reject })
  ))
);
