import React from 'react';

import HelpTipsContainer from 'components/custom/HelpTipsContainer';

const HelpTips = ({ isVisible, onHide }) => {
  return (
    <HelpTipsContainer
      isVisible={isVisible}
      onHide={onHide}
      name="helpTips-Clients"
      items={[
        'Cadastre e organize seus Clientes.',
        'Descubra quem faz mais entregas com a sua transportadora.',
        'Convide usuários para acompanhar as entregas do Cliente.',
        'Agilize a captura e troca de informações.'
      ]}
      tourId={55916}
    />
  );
};

export default HelpTips;
