import React from 'react';

import {
  Button,
  FlexBar
} from 'shared-components';

import Filter from './Filter';

const ContentHeader = ({ onFilterChange, openFeature, children }) => {
  return (
    <FlexBar specSelector="content-header">
      <FlexBar.Child>
        <Filter onFilterChange={onFilterChange} />
      </FlexBar.Child>
      <FlexBar.Child isGrow>
        {children}
      </FlexBar.Child>
      <FlexBar.Child>
        <div className="buttons">
          <Button isPrimary isSmall onClick={openFeature}>Cadastrar cliente</Button>
        </div>
      </FlexBar.Child>
    </FlexBar>
  );
};

export default ContentHeader;
