import React from 'react';

import DataTable, { CellWrapper, Options } from 'components/custom/DataTable';

import { alert } from 'shared-components/lib/Alert';

import DeliveriesCounter from 'components/custom/cards/DeliveriesCounter';
import EntityDocument from 'components/custom/cards/EntityDocument';
import EntityWithAddress from 'components/custom/cards/EntityWithAddress';
import UsersCounter from 'components/custom/cards/UsersCounter';
import GenericDate from 'components/custom/cards/GenericDate';

const Table = ({ reducer, onRowClick, onRequestToRemove }) => (
  <DataTable
    data={reducer.data}
    loading={reducer.loading}
    onRowClick={onRowClick}
    columns={[{
      columnKey: 'name',
      header: 'Cliente',
      cell: <CellWrapper {...reducer} render={(renderProps) => <EntityWithAddress {...renderProps} type="client" />} />,
      flexGrow: 1,
      minWidth: 140,
      defaultWidth: 300,
      isResizable: true
    }, {
      columnKey: 'document',
      header: 'Documento',
      cell: <CellWrapper {...reducer} render={(renderProps) => <EntityDocument {...renderProps} />} />,
      width: 240
    }, {
      columnKey: 'deliveries',
      header: 'Entregas',
      alignHeader: 'center',
      cell: <CellWrapper center {...reducer} render={DeliveriesCounter} />,
      width: 100
    }, {
      columnKey: 'users',
      header: 'Usuários',
      alignHeader: 'center',
      cell: <CellWrapper center {...reducer} render={UsersCounter} />,
      width: 100
    }, {
      columnKey: 'createdAt',
      header: 'Cadastrado em',
      alignHeader: 'center',
      cell: <CellWrapper center {...reducer} render={GenericDate} />,
      width: 144
    }, {
      cell: (
        <Options
          data={reducer.data}
          onClickToRemove={(rowIndex) => {
            const removeConfirmation = alert({
              title: 'Remover cliente',
              description: `ATENÇÃO! Se você prosseguir o cliente ${
                reducer.data[rowIndex].name
              } será removido do sistema.`,
              enableConfirmActionAfter: 3000,
              onConfirm: () => {
                onRequestToRemove(reducer.data[rowIndex]);
                removeConfirmation.close();
              }
            });
          }}
        />
      ),
      width: 60
    }]}
  />
);

export default Table;
