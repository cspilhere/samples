const INITIAL_STATE = {
  data: [],
  isFirstLoading: true,
  loading: false,
  error: false
};

export default (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
  case 'FETCH_CLIENTS':
    return state = {
      ...state,
      loading: true
    };
  case 'FETCH_CLIENTS_SUCCESS':
    return state = {
      ...state,
      data: action.payload,
      loading: false,
      isFirstLoading: false,
      error: false
    };
  case 'FETCH_CLIENTS_ERROR':
    return state = {
      ...state,
      data: [],
      loading: false,
      error: true
    };
  default:
    return state;
  }
};
