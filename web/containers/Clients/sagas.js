import { eventChannel } from 'redux-saga';
import { takeLatest, put, take, call, all } from 'redux-saga/effects';

import { getAllStakeholders, removeStakeholder } from 'services/firestore';

function createChannel(collection, filter) {
  return eventChannel((emit) => {
    getAllStakeholders(collection, filter, emit)
    .catch((error) => emit(new Error(error)));
    return () => {};
  });
};

function* fetchClients(action) {
  const channel = yield call(createChannel, 'clients', action.filter);
  while (true) {
    try {
      const response = yield take(channel);
      yield put({ type: 'FETCH_CLIENTS_SUCCESS', payload: response });
    } catch (error) {
      console.log(error);
    };
  };
};

function* removeClient(action) {
  try {
    const response = yield call(removeStakeholder, 'clients', action.id);
    yield put({ type: 'REMOVE_CLIENT_SUCCESS', payload: response });
    action.resolve(response);
  } catch (error) {
    console.log(error);
    action.reject(error);
  }
};

export default function* root() {
  yield all([
    takeLatest('FETCH_CLIENTS', fetchClients),
    takeLatest('REMOVE_CLIENT', removeClient),
  ]);
};
