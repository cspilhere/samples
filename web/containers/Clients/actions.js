export const fetchClients = (filter) => ({ type: 'FETCH_CLIENTS', filter });

export const removeClient = (id) => (dispatch) => (
  new Promise((resolve, reject) => (
    dispatch({ type: 'REMOVE_CLIENT', id, resolve, reject })
  ))
);
