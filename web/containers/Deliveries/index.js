import React from 'react';

import { connect } from 'react-redux';
import * as actions from './actions';

import { Text } from 'shared-components';

import ContentHeader from './components/ContentHeader';
import Filter from './components/Filter';
import Table from './components/Table';
import HelpTips from './components/HelpTips';

import { deliveriesToCsv, recordsToCsv, billOfLadingsToCsv, invoicesToCsv } from 'services/csvAndDownload';

import CreateDelivery from 'features/CreateDelivery';

import BaseLayout, { baseLayoutWrapper } from 'components/custom/baseLayout/BaseLayout';
import BaseHeader from 'components/custom/baseLayout/BaseHeader';

import { MainContext } from 'containers/MainContainer';

class Deliveries extends React.Component {

  static contextType = MainContext;

  state = {
    filter: null,
    showHelp: false
  }

  render() {

    const { reducer } = this.props;
    const { company } = this.context;

    const { data, loading, isFirstLoading } = reducer;

    const { filter } = this.state;

    return (
      <BaseLayout
        baseHeader={(
          <BaseHeader
            breadcrumb={[{ name: 'Entregas', title: 'Minhas entregas', pathname: '/deliveries' }]}
            onClickHelp={() => {
              this.setState({ showHelp: true });
            }}
          />
        )}
        contentAside={(
          <Filter
            mergeIntoIndexes={{
              status: ['waiting', 'ready', 'ongoing', 'finished'],
              label: company.labels
            }}
            hasActiveFilter={filter}
            onFilterChange={(filter) => {
              this.setState({ filter });
              this.props.fetchDeliveries({ filter });
            }}
          />
        )}
        contentHeader={(
          <ContentHeader
            {...this.props}
            deliveriesToCsv={deliveriesToCsv}
            recordsToCsv={recordsToCsv}
            billOfLadingsToCsv={billOfLadingsToCsv}
            invoicesToCsv={invoicesToCsv}
            openFeature={this.openFeature}>
            <Text isSmall hasGray2Color>
              {loading && isFirstLoading && 'Carregando...'}
              {data && !isFirstLoading && `Mostrando ${data.length} entrega${data.length == 1 ? '' : 's'}`}
            </Text>
          </ContentHeader>
        )}>

        <HelpTips isVisible={this.state.showHelp} onHide={() => this.setState({ showHelp: false })} />

        <Table {...this.props} {...this.state} />

      </BaseLayout>
    );
  }

  openFeature = () => {
    this.props.openModal({
      title: 'Nova entrega',
      disableBackgroundClick: true,
      component: (
        <CreateDelivery
          onClickToCancel={this.props.closeModal}
          onSuccess={this.props.closeModal}
        />
      )
    });
  }

};

export default connect((store) => ({
  reducer: store.deliveries
}), { ...actions })(baseLayoutWrapper(Deliveries));
