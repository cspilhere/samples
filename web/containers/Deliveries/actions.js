export const fetchDeliveries = (filter) => {
  return { type: 'FETCH_DELIVERIES', filter };
};
