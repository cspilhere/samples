import { eventChannel } from 'redux-saga';
import { takeLatest, put, take, call, all } from 'redux-saga/effects';

import { deliveries } from 'services/firestore';

function* fetchDeliveries(action) {
  const createChannel = () => eventChannel((emit) => {
    deliveries.fetch(action.filter || {}, emit).catch((error) => emit(new Error(error)));
    return () => {};
  });
  const channel = yield call(createChannel);
  while (true) {
    try {
      const response = yield take(channel);
      yield put({ type: 'FETCH_DELIVERIES_SUCCESS', payload: response });
    } catch (error) {
      console.error('fetch-deliveries-error - ', error);
      yield put({ type: 'FETCH_DELIVERIES_ERROR', payload: error });
    };
  };
};

export default function* root() {
  yield all([
    takeLatest('FETCH_DELIVERIES', fetchDeliveries)
  ]);
};
