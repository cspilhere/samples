import React from 'react';

import HelpTipsContainer from 'components/custom/HelpTipsContainer';

const HelpTips = ({ isVisible, onHide }) => {
  return (
    <HelpTipsContainer
      isVisible={isVisible}
      onHide={onHide}
      name="helpTips-Deliveries"
      items={[
        'Aqui você gerencia facilmente suas entregas com foco nas informações mais importantes.',
        'Para criar entregas automaticamente utilize o importador de CTes.',
        'Organize suas entregas com o Filtro avançado que após configurado, mantém o estado da listagem mesmo depois de fechar o navegador.',
        'Produzir e exportar relatórios de suas entregas, CTes, NFs, Canhotos e Avarias.',
      ]}
      tourId={55688}
    />
  );
};

export default HelpTips;
