import React, { useState, useEffect } from 'react';

import remove from 'lodash/remove';

import localforage from 'localforage';

import styled, { css } from 'styled-components';

import { company } from 'services/firestore';

import moment from 'moment';

import {
  FormField,
  Icon,
  Button,
  Select
} from 'shared-components';

import { Input, CalendarDatePicker } from 'components';

import { getStatusLabel } from 'utils/utils';

const FilterGroupItem = styled.div`
  font-size: 13px;
  color: ${({ active }) => !active && '#5c7080'};
  cursor: pointer;
  line-height: 1.4;
  margin-bottom: 8px;
  position: relative;
  &:last-child {
    margin-bottom: 0;
  }
  &:hover {
    color: #5c7080;
  }
  ${({ active }) => active && css`
    &:before {
      content: "";
      position: absolute;
      width: 2px;
      height: 100%;
      background-color: #5c7080;
      left: -6px;
      font-size: 13px;
      font-weight: bold;
    }
  `}
`;

const FilterHeader = styled.header`
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  .icon {
    font-size: 14px;
    color: #5c7080;
  }
`;

const FilterGroupHeader = styled.header`
  font-size: 12px;
  font-weight: bold;
  color: #5c7080;
  margin-bottom: 4px;
`;

const FilterContainer = styled.div`
  padding-left: 6px;
  margin-left: -6px;
  max-height: 300px;
  overflow-y: auto;
`;

const FilterWrapper = ({ children }) => {
  return (<div>{children}</div>);
};

const StakeholdersFilterGroup = ({ filter, actives, name, label, onFilterChange }) => {
  const items = Object.keys(filter || {}).map((item) => {
    const isActive = actives.indexOf(item) !== -1;
    return (
      <FilterGroupItem
        key={item}
        active={isActive}
        onClick={() => {
          localforage.getItem('filter', (err, value) => {
            if (err) console.log('localforage error: ', err);
            let offlineFilter = value || {};
            if (!isActive) {
              const added = [...actives, ...[item]];
              offlineFilter[name] = added;
              onFilterChange(offlineFilter, name);
            } else {
              const removed = remove(actives, (i) => i != item);
              offlineFilter[name] = removed;
              onFilterChange(offlineFilter, name);
            }
            localforage.setItem('filter', offlineFilter);
          });
        }}>
        {filter[item]}
      </FilterGroupItem>
    );
  });
  if (items.length < 1) return null;
  return (
    <>
      <hr />
      <FilterWrapper>
        <FilterGroupHeader>{label}</FilterGroupHeader>
        <FilterContainer>{items}</FilterContainer>
      </FilterWrapper>
    </>
  );
};

StakeholdersFilterGroup.defaultProps = {
  filter: {},
  actives: [],
  onFilterChange: () => {},
}

const StringsFilterGroup = ({ filter, actives, name, label, formatter, onFilterChange }) => {
  const items = filter.map((item) => {
    const isActive = actives.indexOf(item) !== -1;
    return (
      <FilterGroupItem
        key={item}
        active={isActive}
        onClick={() => {
          localforage.getItem('filter', (err, value) => {
            if (err) console.log('localforage error: ', err);
            let offlineFilter = value || {};
            if (!isActive) {
              const added = [...actives, ...[item]];
              offlineFilter[name] = added;
              onFilterChange(offlineFilter, name);
            } else {
              const removed = remove(actives, (i) => i != item);
              offlineFilter[name] = removed;
              onFilterChange(offlineFilter, name);
            }
            localforage.setItem('filter', offlineFilter);
          });
        }}>
        {formatter(item)}
      </FilterGroupItem>
    );
  });
  if (items.length < 1) return null;
  return (
    <>
      <hr />
      <FilterWrapper>
        <FilterGroupHeader>{label}</FilterGroupHeader>
        <FilterContainer>{items}</FilterContainer>
      </FilterWrapper>
    </>
  );
};

StringsFilterGroup.defaultProps = {
  filter: [],
  actives: [],
  onFilterChange: () => {},
  formatter: (string) => string,
}

/**
 * Filter
 */

let _isMounted = false;

const Filter = (props) => {

  const [indexes, setIndexes] = useState({});

  const [dateRangeField, setDateRangeField] = useState({});
  const [dateRange, setDateRange] = useState([]);
  const [queryValue, setQueryValue] = useState('');

  const [actives, setActives] = useState({});

  const { onFilterChange, mergeIntoIndexes, hasActiveFilter } = props;

  const defaultDateRange = [moment().startOf('isoWeek')._d, moment().endOf('isoWeek')._d];
  const defaultDateRangeField = 'updatedAt';

  useEffect(() => {

    _isMounted = true;

    // Fetch indexes and set listener
    company.indexes((result) => {
      return _isMounted && setIndexes({ ...result, ...mergeIntoIndexes });
    });

    /**
     * Setup default filter
     */
    localforage.getItem('filter', (err, value) => {
      if (!value) {
        let offlineFilter = value || {};
        offlineFilter['dateRange'] = defaultDateRange;
        offlineFilter['dateRangeField'] = defaultDateRangeField;
        setDateRange(offlineFilter['dateRange']);
        setDateRangeField(offlineFilter['dateRangeField']);
        onFilterChange(offlineFilter);
        localforage.setItem('filter', offlineFilter);
      }
      if (value) {
        setDateRange(value['dateRange']);
        setDateRangeField(value['dateRangeField']);
        setQueryValue(value['query']);
        setActives(value);
        onFilterChange(value);
      }
    });

    return () => {
      _isMounted = false;
    }

  }, []);

  const correctDateRange = dateRange || defaultDateRange;
  const correctDateRangeField = dateRangeField || defaultDateRangeField;

  return (
    <>

      <FilterHeader>
        Filtro avançado
        <Button
          isSmall
          isBare
          onClick={() => {
            setDateRange(defaultDateRange);
            setDateRangeField(defaultDateRangeField);
            setActives({});
            setQueryValue('');
            onFilterChange(null);
            localforage.setItem('filter', null);
          }}>
          <Icon name="fas fa-undo" />
        </Button>
      </FilterHeader>

      <FormField
        label="Data"
        hasIconsLeft>
        <CalendarDatePicker
          isSmall
          keepOpened
          defaultValue={{ startDate: correctDateRange[0], endDate: correctDateRange[1] }}
          onChange={(range) => {
            if (!range) return;
            localforage.getItem('filter', (err, value) => {
              if (err) console.log('localforage error: ', err);
              let offlineFilter = value || {};
              offlineFilter['dateRange'] = [moment(range[0]).startOf('day').toDate(), moment(range[1]).endOf('day').toDate()];
              setDateRange(offlineFilter);
              onFilterChange(offlineFilter, 'dateRange');
              localforage.setItem('filter', offlineFilter);
            });
          }}
        />
        <Icon name="fas fa-sm fa-calendar-day" isLeft isSmall />
      </FormField>

      <FormField label="Ordenar data por:">
        <Select
          isSmall
          isFullWidth
          value={correctDateRangeField}
          hideBlankOption
          options={[
            { label: 'Data de criação', value: 'createdAt' },
            { label: 'Última atualização', value: 'updatedAt' },
            { label: 'Previsão de entrega', value: 'estimation' },
            { label: 'Data de liberação', value: 'releasedAt' },
            { label: 'Data de conclusão', value: 'finishedAt' },
          ]}
          onChange={({ target }) => {
            setDateRangeField(target.value);
            localforage.getItem('filter', (err, value) => {
              if (err) console.log('localforage error: ', err);
              let offlineFilter = value || {};
              offlineFilter['dateRangeField'] = target.value;
              onFilterChange(offlineFilter, 'dateRangeField');
              localforage.setItem('filter', offlineFilter);
            });
          }}
        />
      </FormField>

      <hr />

      <FormField label="Palavra-chave">
        <Input
          small
          placeholder="Nome, documento, endereço..."
          value={queryValue}
          onChange={({ target }) => {
            setQueryValue(target.value);
            localforage.getItem('filter', (err, value) => {
              if (err) console.log('localforage error: ', err);
              let offlineFilter = value || {};
              offlineFilter['query'] = target.value;
              onFilterChange(offlineFilter, 'query');
              localforage.setItem('filter', offlineFilter);
            });
          }}
        />
      </FormField>

      <StakeholdersFilterGroup
        filter={indexes.clients}
        actives={actives && actives['client.document']}
        onFilterChange={(filterState) => {
          onFilterChange(filterState);
          setActives(filterState);
        }}
        name="client.document"
        label="Clientes"
      />

      <StakeholdersFilterGroup
        filter={indexes.shippers}
        actives={actives && actives['shipper.document']}
        onFilterChange={(filterState) => {
          onFilterChange(filterState);
          setActives(filterState);
        }}
        name="shipper.document"
        label="Embarcadores"
      />

      <StakeholdersFilterGroup
        filter={indexes.drivers}
        actives={actives && actives['driver.document']}
        onFilterChange={(filterState) => {
          onFilterChange(filterState);
          setActives(filterState);
        }}
        name="driver.document"
        label="Motoristas"
      />

      <StringsFilterGroup
        filter={indexes.label}
        actives={actives && actives['label']}
        onFilterChange={(filterState) => {
          onFilterChange(filterState);
          setActives(filterState);
        }}
        name="label"
        label="Etiqueta"
      />

      <StringsFilterGroup
        filter={indexes.status}
        actives={actives && actives['status']}
        onFilterChange={(filterState) => {
          onFilterChange(filterState);
          setActives(filterState);
        }}
        formatter={getStatusLabel}
        name="status"
        label="Situação"
      />

    </>
  );
};

Filter.defaultProps = {
  mergeIntoIndexes: {}
};

export default Filter;
