import React from 'react';

import DataTable, { CellWrapper } from 'components/custom/DataTable';

import DeliveryRating from 'components/custom/cards/DeliveryRating';
import DeliveryLabel from 'components/custom/cards/DeliveryLabel';
import EntityWithAddress from 'components/custom/cards/EntityWithAddress';
import GenericText from 'components/custom/cards/GenericText';
import GenericDate from 'components/custom/cards/GenericDate';
import AddressDescription from 'components/custom/cards/AddressDescription';
import DriverSmallCard from 'components/custom/cards/DriverSmallCard';
import EstimatedDate from 'components/custom/cards/EstimatedDate';
import DeliverySituation from 'components/custom/cards/DeliverySituation';

import { showTakerLabel } from 'services/transformKeysToString';

const Table = ({ history, reducer, onRowClick }) => (
  <DataTable
    data={reducer.data}
    loading={reducer.loading}
    onRowClick={(event, rowIndex) => {
      if (!window.newTabKeyIsPressed) {
        history.push(`/deliveries/${reducer.data[rowIndex].id}`);
        return;
      }
      window.open(`${location.origin}/deliveries/${reducer.data[rowIndex].id}`, '_blank');
    }}
    columns={[{
      columnKey: 'status',
      header: 'Situação',
      cell: <CellWrapper {...reducer} render={DeliverySituation} />,
      flexGrow: 1,
      width: 140
    }, {
      columnKey: 'destination.address',
      header: 'Endereço de entrega',
      cell: <CellWrapper {...reducer} render={AddressDescription} />,
      flexGrow: 1,
      minWidth: 140,
      defaultWidth: 400,
      isResizable: true
    }, {
      columnKey: 'shipper',
      header: 'Embarcador',
      cell: <CellWrapper {...reducer} render={(renderProps) => <EntityWithAddress {...renderProps} type="shipper" contentAsObject />} />,
      flexGrow: 1,
      minWidth: 140,
      defaultWidth: 300,
      isResizable: true
    }, {
      columnKey: 'client',
      header: 'Cliente',
      cell: <CellWrapper {...reducer} render={(renderProps) => <EntityWithAddress {...renderProps} type="client" contentAsObject />} />,
      flexGrow: 1,
      minWidth: 160,
      defaultWidth: 300,
      isResizable: true
    }, {
      columnKey: 'number',
      header: 'CTe',
      alignHeader: 'center',
      cell: <CellWrapper center {...reducer} render={(renderProps) => <GenericText {...renderProps} bold />} />,
      width: 100
    }, {
      columnKey: 'taker',
      header: 'Tomador',
      alignHeader: 'center',
      cell: <CellWrapper center {...reducer} render={(renderProps) => <GenericText {...renderProps} parseWith={showTakerLabel} />} />,
      width: 120
    }, {
      columnKey: 'driver',
      header: 'Motorista',
      alignHeader: 'center',
      cell: <CellWrapper center {...reducer} render={DriverSmallCard} />,
      flexGrow: 1,
      width: 240
    }, {
      columnKey: 'estimation',
      header: 'Previsão de entrega',
      alignHeader: 'center',
      cell: <CellWrapper center {...reducer} render={EstimatedDate} />,
      flexGrow: 1,
      width: 190
    }, {
      columnKey: 'label',
      header: 'Etiqueta',
      alignHeader: 'center',
      cell: <CellWrapper center {...reducer} render={DeliveryLabel} />,
      flexGrow: 1,
      width: 160
    }, {
      columnKey: 'rating',
      header: 'Avaliação',
      alignHeader: 'center',
      cell: <CellWrapper center {...reducer} render={DeliveryRating} />,
      width: 140
    }, {
      columnKey: 'releasedAt',
      header: 'Data de liberação',
      alignHeader: 'center',
      cell: <CellWrapper center {...reducer} render={GenericDate} />,
      width: 160
    }, {
      columnKey: 'finishedAt',
      header: 'Data de conclusão',
      alignHeader: 'center',
      cell: <CellWrapper center {...reducer} render={GenericDate} />,
      width: 160
    }, {
      columnKey: 'createdAt',
      header: 'Data de criação',
      alignHeader: 'center',
      cell: <CellWrapper center {...reducer} render={GenericDate} />,
      width: 160
    }, {
      columnKey: 'updatedAt',
      header: 'Última atualização',
      alignHeader: 'center',
      cell: <CellWrapper center {...reducer} render={GenericDate} />,
      width: 160
    }]}
  />
);

export default Table;
