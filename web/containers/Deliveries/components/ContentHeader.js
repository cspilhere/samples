import React from 'react';

import { Button, FlexBar, Text, Icon, Dropdown } from 'shared-components';

const ContentHeader = (props) => {

  const hasData = (props.reducer.data || []).length > 0;

  const actions = [{
    content: <Text isSmall><Icon name="fas fa-file-csv" /> Exportar entregas (.csv)</Text>,
    onClick: () => props.deliveriesToCsv([...props.reducer.data])
  }, {
    content: <Text isSmall><Icon name="fas fa-file-csv" /> Exportar CTes (.csv)</Text>,
    onClick: () => props.billOfLadingsToCsv([...props.reducer.data])
  }, {
    content: <Text isSmall><Icon name="fas fa-file-csv" /> Exportar NFs (.csv)</Text>,
    onClick: () => props.invoicesToCsv([...props.reducer.data])
  }, {
    content: <Text isSmall><Icon name="fas fa-file-csv" /> Exportar registros (.csv)</Text>,
    onClick: () => props.recordsToCsv([...props.reducer.data])
  }];

  return (
    <FlexBar specSelector="content-header">
      <FlexBar.Child isGrow>
        {props.children}
      </FlexBar.Child>
      <FlexBar.Child>
        <div className="buttons" style={{ marginBottom: 0 }}>
          <span id="deliveries-main-actions" style={{ marginRight: 8 }}>
            <Button isPrimary isSmall onClick={() => props.history.push('/deliveries/import')} style={{ marginBottom: 0 }}>Importar CTes</Button>
            <Button isSmall onClick={props.openFeature} style={{ marginBottom: 0 }}>Criar entrega</Button>
          </span>
          <Dropdown
            isRight
            trigger={(
              <Button isSmall disabled={!hasData} style={{ marginBottom: 0 }}><Icon name="far fa-ellipsis-v" /></Button>
            )}>
            {actions.map(({ onClick, content }, index) => (
              <Dropdown.Item key={index} onClick={onClick} disabled={!hasData}>{content}</Dropdown.Item>
            ))}
          </Dropdown>
        </div>
      </FlexBar.Child>
    </FlexBar>
  );
};

export default ContentHeader;
