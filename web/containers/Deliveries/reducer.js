const INITIAL_STATE = {
  data: [],
  isFirstLoading: true,
  loading: false,
  error: false
};

export default (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {

    case 'FETCH_DELIVERIES':
      return state = {
        ...state,
        loading: true,
        error: false
      };

    case 'FETCH_DELIVERIES_SUCCESS':
      return state = {
        ...state,
        data: action.payload,
        loading: false,
        isFirstLoading: false,
        error: false
      };

    case 'FETCH_DELIVERIES_ERROR':
      return state = {
        ...state,
        loading: false,
        error: action.payload
      };

    default:
      return state;
  }
};
