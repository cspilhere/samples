import * as firebase from 'firebase/app';
import 'firebase/firestore';

const db = firebase.firestore();

import Constants from 'services/Constants';

// Get the transport company from current user by id
export const getCompany = (userId) => (dispatch) => {

  return new Promise((resolve, reject) => {
    // If userId was not passed, dispatch an empty object as Company reducer data
    if (!userId) {
      dispatch({ type: 'GET_COMPANY', payload: [] });
      return;
    }
    db.collection('companies')
    .where('ownerId', '==', userId)
    .onSnapshot((querySnapshot) => {

      const response = [...querySnapshot.docs.map((doc) => ({ ...doc.data(), id: doc.id }))];

      Constants.set('companyId', (response[0] || {}).id);

      dispatch({
        type: 'GET_COMPANY',
        payload: response[0] || {}
      });
    });
  });

};

// All users has to create a transport company to use the app, this action will call when a fresh new user signed in
export const createCompany = (userId, body) => (dispatch) => {

  body = { name: body.name, ownerId: userId };

  return new Promise((resolve, reject) => {
    db.collection('companies')
    .add(body)
    .then((docRef) => {
      docRef.get().then((doc) => {

        const company = { ...doc.data(), id: doc.id };

        Constants.set('companyId', doc.id);

        dispatch({
          type: 'GET_COMPANY',
          payload: company
        });

      });
    })
    .catch((error) => {
      console.error('Error adding companies: ', error);
    });
  });

};
