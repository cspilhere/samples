import React from 'react';

import { ToastContainer } from 'react-toastify';

import { connect } from 'react-redux';
import * as actions from './actions';

import {
  Layout,
  SplashScreen,
  Spinner,
  AlertContainer
} from 'shared-components';

import { AuthContext } from 'containers/AuthManager';
import { ChildRoutes } from 'utils/RouterUtils';

import { signOut } from 'containers/AuthUser/actions';

import InitialSetup from './components/InitialSetup';
import MainHeader from './components/MainHeader';

import colors from 'components/colors';

export const MainContext = React.createContext({});

class MainContainer extends React.Component {

  static contextType = AuthContext;

  state = {}

  componentDidMount() {
    this.context.onUserAuthChanged(({ isSignedIn }) => {
      if (isSignedIn) {

        /**
         * Fetch company data based on the owner id (for this moment, is the only user can access the company)
         */
        this.props.getCompany(this.context.currentUser.uid);

        /**
         * Intercom boot
         */
        Intercom('boot', {
          app_id: 'ev7rop39',
          email: this.context.currentUser.email,
          name: this.context.currentUser.displayName,
          user_id: this.context.currentUser.id,
          company: this.props.company,
          hide_default_launcher: true
        });

      }
    });
  }

  render() {

    const { currentUser, userIsReady } = this.context;

    const { company } = this.props;

    if (!userIsReady || !company) return <SplashScreen isOpen={true}><Spinner size={32} /></SplashScreen>;

    const isFirstAccess = Object.keys(company).length < 1;

    if (location.pathname !== '/' && isFirstAccess) this.props.history.push('/');

    return (
      <Layout className="app-wrapper">

        <AlertContainer />
        <ToastContainer />

        {/* Mensagem de plano expirando ou email não confirmado */}
        {/* <div style={{ height: 30, backgroundColor: colors.warning }} /> */}

        <Layout.Header>
          <MainHeader {...this.props} currentUser={currentUser} onClickLogout={signOut} isFirstAccess={isFirstAccess} />
        </Layout.Header>

        <Layout.Main>

          {isFirstAccess ? (
            <InitialSetup currentUser={currentUser} createCompany={this.props.createCompany.bind(null, currentUser.uid)} />
          ) : (
            company && (
              <MainContext.Provider value={{ currentUser, company }}>
                <ChildRoutes routes={this.props.routes} />
              </MainContext.Provider>
            )
          )}

        </Layout.Main>

        <div style={{ height: 4, backgroundColor: company.color || colors.primary }} />

      </Layout>
    );

  }

};

export default connect((store) => ({
  company: store.main.company
}), { ...actions })(MainContainer);
