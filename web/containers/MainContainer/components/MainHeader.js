import React, { useEffect, useState } from 'react';

import { Button, Dropdown, Menu } from 'shared-components';

import { Divider, RowWrapper, Fill, Icon, Badged, Text } from 'components';

import TrialTimeline from 'components/custom/TrialTimeline';

import logo from 'static/media/levalog-logo.svg';

import colors from 'components/colors';

const MainHeader = (props) => {

  const [showBadge, setBadge] = useState(0);

  useEffect(() => {
    Intercom('onUnreadCountChange', (unreadCount) => {
      setBadge(unreadCount != showBadge);
    });
  }, []);

  const { isFirstAccess, company, currentUser } = props;

  const modules = [{
    name: 'Entregas',
    path: '/',
    route: '/dashboard'
  // }, {
  //   name: 'Entregas',
  //   path: '/deliveries',
  }, {
    name: 'Embarcadores',
    path: '/shippers'
  }, {
    name: 'Clientes',
    path: '/clients'
  }, {
    name: 'Motoristas',
    path: '/drivers'
  }].map((item, index) => {

    const isActive = (
      item.path == props.location.pathname ||
      props.location.pathname.indexOf(item.path) != -1 && item.path != '/' ||
      props.location.pathname.indexOf(item.route) != -1
    );

    return (
      <Button
        isSmall
        isMainHeaderNavigation
        isActive={isActive}
        key={index}
        onClick={() => props.history.push(item.path)}>
        {item.name}
      </Button>
    );

  });

  return (
    <RowWrapper padding="12px 20px" useSpacer spacerSize={10} elevated color={company.color || colors.primary}>

      <img
        src={company.logo || logo}
        style={{ height: 22, cursor: 'pointer' }}
        onClick={() => { props.history.push('/'); }}
      />

      <Fill />

      {!isFirstAccess && modules}

      <Divider vertical color="#F2F2F2" />

      <Badged showBadge={showBadge}>
        <Button
          isSmall
          isMainHeaderActions
          onClick={() => Intercom('show')}>
          <Icon name="fas fa-comment-alt" style={{ marginTop: 2, fontSize: 14 }} />
        </Button>
      </Badged>

      <Dropdown isRight trigger={<Button isSmall isMainHeaderActions><Icon name="fas fa-user" /></Button>} style={{ paddingTop: 0 }}>

        <Dropdown.Item>
          <div
            style={{
              backgroundColor: '#f5f8fa',
              margin: '-6px -16px 4px',
              padding: 16,
              paddingTop: 18,
              borderTopLeftRadius: 2,
              borderTopRightRadius: 2,
              borderBottom: `2px solid ${company.color || '#1C99FE'}`
            }}>
            {!isFirstAccess && (
              <>
                <Text title={company.name || ''} style={{ color: '#293742', lineHeight: 1.1 }}>
                  <strong>{company && company.name}</strong>
                </Text>
              </>
            )}
          </div>
        </Dropdown.Item>

        <Dropdown.Item>
          <Text small truncated block style={{ marginBottom: -2 }}><strong>{currentUser.displayName}</strong></Text>
          <Text small truncated style={{ marginBottom: -2 }}>{currentUser.email}</Text>
        </Dropdown.Item>

        <Dropdown.Item hasDivider />

        {!isFirstAccess && (
          <>
            <Dropdown.Item>
              <Menu>
                <Menu.Item onClick={() => { props.history.push('/account'); }}>
                  <Text small><Icon name="fas fa-shield-alt" /> Conta e configurações</Text>
                </Menu.Item>
              </Menu>
            </Dropdown.Item>
          </>
        )}

        {!isFirstAccess && company.createdAt && (
          <>
            <Dropdown.Item hasDivider />
            <Dropdown.Item>
              <TrialTimeline
                accountCreatedAt={company.createdAt.toDate()}
                onClick={() => props.history.push('/account')}
              />
            </Dropdown.Item>
          </>
        )}
        <Dropdown.Item hasDivider />
        <Dropdown.Item>
          <div style={{ textAlign: 'right' }}>
            <Button isSmall onClick={props.onClickLogout}>
              <Icon name="fas fa-sign-out" />&nbsp;Sair
            </Button>
          </div>
        </Dropdown.Item>

      </Dropdown>

    </RowWrapper>
  );

};

export default MainHeader;
