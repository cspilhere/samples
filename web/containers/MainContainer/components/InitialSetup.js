import React from 'react';

// import Slider from 'react-slick';
// import 'slick-carousel/slick/slick.css';
// import 'slick-carousel/slick/slick-theme.css';

// import Lottie from 'react-lottie';
// import animationData from 'static/media/lottie/truck-running.json';
// import illustration from 'static/media/illustrations/levalog-illustration.svg';

import {
  Button,
  FlexBar,
  Icon,
  Heading,
  Text,
  Form,
  Space,
  FormField,
  TextInput
} from 'shared-components';

const lottieOptions = {
  loop: true,
  autoplay: true,
  // animationData: animationData
};

export default class InitialSetup extends React.Component {

  state = {
    slideIndex: 0,
    updateCount: 0,
    hasFocus: false,
    fields: {
      name: ''
    }
  };

  render() {
    const { fields } = this.state;

    const slickSettings = {
      dots: false,
      infinite: true,
      speed: 400,
      slidesToShow: 1,
      slidesToScroll: 1,
      swipeToSlide: false,
      swipe: false,
      nextArrow: <></>,
      prevArrow: <></>,
      afterChange: () => {},
      beforeChange: (current, next) => {
        this.setState({ slideIndex: next });
      }
    };

    return (
      <div className="initial-setup">
        <div className="initial-setup-slider">
          <div style={{ textAlign: 'center' }} className="initial-setup-slide">
            {/* <img
              src={illustration}
              style={{ height: 280 }}
            />
            <Space size="medium" /> */}
            <Heading is4><span style={{ color: '#1A497B' }}>
              Olá {this.props.currentUser.displayName.split(' ')[0]}, bem vindo ao Levalog!</span>
            </Heading>
            <Text style={{ marginTop: -8, fontSize: 14 }}>Um jeito simples de gerenciar suas entregas.</Text>
            <Space />
            <Form
              onSubmit={() => {
                this.setState({ isLoading: true });
                this.props.createCompany({ name: fields.name });
              }}>
              <div

                style={{
                  display: 'flex',

                }}>
                <TextInput
                  placeholder="Qual é o nome da sua empresa?"
                  disabled={this.state.isLoading}
                  value={fields.name}
                  disableAutoComplete
                  onChange={({ target }) => {
                    this.setState({
                      fields: {
                        name: target.value
                      }
                    });
                  }}
                />

                <Button
                  id="initial-setup-save-button"
                  style={{

                    borderRadius: 100,
                    marginTop: 12,
                    height: 33,
                    width: 33,
                    marginLeft: -48,
                    color: 'white',
                    backgroundColor: '#1C99FE'

                  }}
                  isSuccess
                  disabled={this.state.isLoading || fields.name.length < 4}
                  onClick={() => {
                    this.setState({ isLoading: true });
                    this.props.createCompany({ name: fields.name });
                  }}>
                  <Icon name="fas fa-check" />
                </Button>
              </div>
            </Form>
            <Space size="large" />

          </div>
        </div>
      </div>
    );
  }

  submitForm = () => {

    const { fields } = this.state;
    const { defaultBody } = this.props;

    if (!checkObjectKeys(fields, ['cnpj', 'name', 'email', 'phone'])) {
      toast.warning('Preencha todos os campos obrigatórios!', {
        position: toast.POSITION.TOP_RIGHT
      });
      return;
    }

    if (!checkObjectKeys(fields, ['address.location'])) {
      toast.warning('Você precisa de um endereço válido para obter as coordenadas.', {
        position: toast.POSITION.TOP_RIGHT
      });
      return;
    }

    if (fields.users) {
      const checkUsers = [...fields.users].filter((user, index) => {
        if (!checkObjectKeys(user, ['name', 'email'])) {
          toast.warning(`Preencha todas os campos do usuário #${index + 1} ou remova ele da lista.`, {
            position: toast.POSITION.TOP_RIGHT
          });
        } else {
          return user;
        }
      });
      if (checkUsers.length !== fields.users.length) return;
    }

    this.setState({ isLoading: true }, () => {
      saveData({ ...fields, ...defaultBody })
      .then(this.props.onSuccess).catch(() => {
        this.setState({ isLoading: false }, this.props.onError);
      });
    });

  }

  handleInputEvent = ({ target }, isValid) => {
    if (target.value) this.updateFields(target.name, target.value);
  }

  updateFields = (name, value) => {
    if (value) this.setState({ fields: updateObjectAndClear(this.state.fields, name, value) });
  }


};
