import Dashboard from 'containers/Dashboard';
import Deliveries from 'containers/Deliveries';
import Clients from 'containers/Clients';
import Shippers from 'containers/Shippers';
import Drivers from 'containers/Drivers';
import Delivery from 'containers/Delivery';
import ImportCTes from 'containers/ImportCTes';

import Account from 'containers/Account';

const routes = [{

  name: 'Configurações da conta',
  path: '/account',
  component: Account
}, {

  name: 'Motoristas',
  path: '/drivers',
  component: Drivers
}, {
  name: 'Embarcadores',
  path: '/shippers',
  component: Shippers
}, {
  name: 'Clientes',
  path: '/clients',
  component: Clients
}, {
  name: 'Importação de CTes',
  path: '/deliveries/import',
  exact: true,
  component: ImportCTes
}, {
  name: 'Dashboard da entrega',
  path: '/deliveries/:deliveryId',
  exact: true,
  component: Delivery
}, {
  name: 'Lista de entregas',
  path: '/deliveries',
  component: Deliveries
}, {

  // Home
  name: 'Dashboard',
  path: '/',
  component: Deliveries || Dashboard
}];

export default routes;
