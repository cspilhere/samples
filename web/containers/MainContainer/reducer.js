const initialState = {
  company: null
};

export default (state = initialState, action = {}) => {

  switch (action.type) {

    case 'GET_COMPANY':
      state = {
        ...state,
        ...{ company: action.payload }
      };
    break;

    default:
      break;
  }

  return state;
};
