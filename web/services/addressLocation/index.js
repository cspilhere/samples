import request from 'axios';

const placesAutocompleteService = new google.maps.places.AutocompleteService(null, {
  types: ['geocode']
});

export const queryPredictions = (inputValue) => {
  if (!inputValue) {
    console.log('queryPredictions error: inputValue not found - ', inputValue);
    return;
  }
  return new Promise((resolve, reject) => {
    placesAutocompleteService.getQueryPredictions({ input: inputValue }, (predictions, status) => {
      resolve(predictions);
    });
  });
};

export const latLngByAddress = (address) => {
  if (!address) {
    console.log('latLngByAddress error: address not found - ', address);
    return;
  }
  return new Promise((resolve, reject) => {

    if (address === '') resolve();

    let parsedValue = address.replace(new RegExp(' ', 'g'), '+').replace(new RegExp('-', 'g'), '');
    request.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${parsedValue}&key=AIzaSyBLVH65RWntYT7QCKbWEg-hKMdvQMCrFrw`)
    .then((response) => {
      try {
        const location = response.data.results[0].geometry.location;
        resolve({ latitude: location.lat, longitude: location.lng });
      } catch (error) {
        console.log('Error on SearchGeocoding: ' + error);
        reject();
      }
    })
    .catch((error) => {
      console.log('error', error);
      reject();
    });
  });
};

export const formatAddress = ({ cep, city, complement, neighborhood, number, description, uf }, clean) => {
  return `${
    description || ''
  }, ${
    number || ''
  }${
    complement ? ' (' + complement + ')' : ''
  } ${!clean ? '- Bairro' : ''} ${
    neighborhood || ''
  }, ${
    cep || ''
  } - ${
    city || ''
  } - ${
    uf || ''
  }`;
}
