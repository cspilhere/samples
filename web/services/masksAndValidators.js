import * as Yup from 'yup';
import IMask from 'imask';

export const maskSchema = {
  cnpj: IMask.createMask({ mask: '00.000.000/0000-00' }),
  cpf: IMask.createMask({ mask: '000.000.000-00' }),
  phone: IMask.createMask({ mask: '(00) 0000-00000' }),
  number: IMask.createMask({ mask: Number })
};

export const validationSchema = (fields) => {

  const fieldsMap = {};

  fields.forEach((field) => {
    const name = field.name || field;
    const required = field.required || false;
    if (schemas[name]) {
      fieldsMap[name] = schemas[name];
      if (required) fieldsMap[name].required(`Field ${name} is required!`);
    }
  });

  return Yup.object().shape(fieldsMap);
};

const schemas = {

  document: Yup.string()
  .matches(
    /(^\d{3}\.\d{3}\.\d{3}\-\d{2}$)|(^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$)/,
    'Documento incorreto'
  ),

  name: Yup.string()
  .min(6, 'O nome precisa ter no mínimo 6 caractéres'),

  email: Yup.string().email('Este não é um email válido'),

  password: Yup.string()
  .min(6, 'A senha precisa ter no mínimo 6 caractéres')

};

