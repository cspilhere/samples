import Papa from 'papaparse';
import moment from 'moment';

import { showRecordTypeLabel, showStatusLabel, showTaker } from 'services/transformKeysToString';

export const deliveriesToCsv = (data) => {
  const arrayToConvert = [];
  data.forEach((item) => arrayToConvert.push(parseDeliveries({ ...item })));
  downloadFile('Levalog-Entregas-' + moment().format('DDMMYYYYhhmmss') + '.csv', csvFromJs(arrayToConvert));
};

export const billOfLadingsToCsv = (data) => {
  const arrayToConvert = [];
  data.forEach((item) => arrayToConvert.push(parseBillOfLading({ ...item })));
  downloadFile('Levalog-CTes-' + moment().format('DDMMYYYYhhmmss') + '.csv', csvFromJs(arrayToConvert));
};

export const invoicesToCsv = (data) => {
  const arrayToConvert = [];
  data.forEach((item) => {
    if (item.invoices) {
      item.invoices.forEach((nf) => {
        if (!nf) return;
        arrayToConvert.push({ 'NF': nf, ...parseBillOfLading({ ...item }) });
      });
    }
  });
  downloadFile('Levalog-NFs-' + moment().format('DDMMYYYYhhmmss') + '.csv', csvFromJs(arrayToConvert));
};

export const recordsToCsv = (data) => {
  const deliveries = data;
  let records = [];
  deliveries.forEach((delivery) => {
    if (!delivery.recordsList) return;
    Object.keys(delivery.recordsList).forEach((recordKey) => {
      delivery.recordsList[recordKey].billOfLading = delivery.number;
      records = [...records, delivery.recordsList[recordKey]];
    });
  });
  const dataToExport = records.map((item) => parseRecord({ ...item }));
  downloadFile('Levalog-Registros-da-Entrega-' + moment().format('DDMMYYYYhhmmss') + '.csv', csvFromJs(dataToExport));
};

export const deliveryRecordsToCsv = (data) => {
  const delivery = data;
  let records = [];
  if (!delivery.recordsList) return;
  Object.keys(delivery.recordsList).forEach((recordKey) => {
    delivery.recordsList[recordKey].billOfLading = delivery.number;
    records = [...records, delivery.recordsList[recordKey]];
  });
  const dataToExport = records.map((item) => parseRecord(item));
  downloadFile('Levalog-Registros-da-Entrega-' + moment().format('DDMMYYYYhhmmss') + '.csv', csvFromJs(dataToExport));
};

function parseRecord(item) {
  const object = {};
  object['Tipo'] = showRecordTypeLabel(item.type);
  object['Descrição'] = item.description;
  object['CTe'] = item.number;
  object['Criado por'] = item.createdBy.displayName || item.createdBy.email || item.createdBy.phoneNumber;
  object['Data de criação'] = moment(item.createdAt.toDate()).format('DD/MM/YYYY HH:mm');
  object['Vínculo'] = item.linkedTo;
  object['Anexo'] = (item.attachment || {}).url || 'Sem anexo';
  return object;
};

function parseDeliveries(item) {
  const object = {};
  object['CTe'] = item.number;
  object['Chave'] = item.billOfLading;
  object['Tomador'] = showTaker(item.taker).label;
  object['Embarcador'] = item.shipper.document;
  object['Cliente'] = item.client.document;
  object['Situação'] = showStatusLabel(item.status);
  object['Etiqueta'] = item.label || 'Sem etiqueta';
  object['Etiqueta'] = item.label;
  let rating = null;
  if (item.rating) {
    let values = item.rating.map((value) => value.value);
    let sum = values.reduce((previous, current) => current += previous);
    item.rating = sum / values.length;
    item.rating = parseFloat(item.rating).toFixed(1);
    rating = item.rating * 1 + '/5'
  }
  object['Previsão de entrega'] = (
    (item.finishedAt && moment(item.finishedAt.toDate())) ||
    (item.estimation && moment(item.estimation.toDate())) ||
    'Previsão de entrega não configurado'
  );
  if (object['Previsão de entrega'] !== 'Previsão de entrega não configurado') {
    object['Previsão de entrega'] = moment(object['Previsão de entrega']).format('DD/MM/YYYY');
  }
  object['Endereço de entrega'] = (item.destination || {}).address || 'Sem endereço de entrega';
  object['Avaliação'] = rating || 'Sem avaliação';
  object['Motorista']= (item.driver && item.driver.name) || 'Motorista não informado';
  object['Data de criação'] = moment(item.createdAt.toDate()).format('DD/MM/YYYY');
  object['Data de conclusão'] = (
    (item.finishedAt && moment(item.finishedAt.toDate())) ||
    (item.estimation && moment(item.estimation.toDate())) ||
    'Em andamento'
  );
  if (object['Data de conclusão'] !== 'Em andamento') {
    object['Data de conclusão'] = moment(object['Data de conclusão']).format('DD/MM/YYYY');
  }
  return object;
};

function parseBillOfLading(item) {
  const object = {};
  object['CTe'] = item.number;
  object['Embarcador'] = item.shipper.document;
  object['Cliente'] = item.client.document;
  object['Situação'] = showStatusLabel(item.status);
  object['Etiqueta'] = item.label || 'Sem etiqueta';
  object['Tomador'] = showTaker(item.taker).label;
  object['Motorista']= (item.driver && item.driver.name) || 'Motorista não informado';
  object['Previsão de entrega'] = (
    (item.finishedAt && moment(item.finishedAt.toDate())) ||
    (item.estimation && moment(item.estimation.toDate())) ||
    'Previsão de entrega não configurado'
  );
  if (object['Previsão de entrega'] !== 'Previsão de entrega não configurado') {
    object['Previsão de entrega'] = moment(object['Previsão de entrega']).format('DD/MM/YYYY');
  }
  return object;
};

export function downloadFile(name, content) {
  const element = document.createElement('A');
  element.href = content;
  element.download = name;
  element.click();
};

function csvFromJs(content) {
  return encodeURI('data:text/csv;charset=utf-8,' + Papa.unparse(content));
};
