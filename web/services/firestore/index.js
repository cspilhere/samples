import * as firebase from 'firebase/app';
import 'firebase/firestore';

import Fuse from 'fuse.js';
import moment from 'moment';

import Constants from 'services/Constants';
import { toArrayWithIds, toObjectWithId, documentExists } from './utilities';

const db = firebase.firestore();

export const fetchStakeholder = (collection, documentId, eventCallback) => (
  new Promise((resolve, reject) => {
    db.collection(collection)
    .doc(documentId)
    .onSnapshot((snapshot) => {
      let data = toObjectWithId(snapshot);
      if (eventCallback && typeof eventCallback === 'function') {
        eventCallback(data);
      } else {
        resolve(data);
      }
    }, reject);
  })
);

export const getAllStakeholders = (collection, filter, eventCallback) => (
  new Promise((resolve, reject) => {

    const { query } = filter || {};

    db.collection(collection)
    .where('companyId', '==', Constants.get('companyId'))
    .orderBy('createdAt', 'desc')
    .onSnapshot((snapshot) => {

      let data = toArrayWithIds(snapshot);

      if (query) {
        const fuse = new Fuse(data, {
          tokenize: true,
          matchAllTokens: true,
          threshold: 0.3,
          shouldSort: true,
          keys: [
            'document',
            'name',
            'address.description',
            'address.city',
            'address.uf',
          ]
        });
        data = fuse.search(query);
      }

      if (eventCallback && typeof eventCallback === 'function') {
        eventCallback(data);
      } else {
        resolve(data);
      }

    }, reject);
  })
);

export const createStakeholder = (body, collection, avoidChecking) => (
  new Promise((resolve, reject) => {
    body.createdAt = moment(moment()).toDate();
    body.companyId = Constants.get('companyId');
    body.document = body.document.replace(/\D/g, '');
    if (body.users) {
      body.usersEmails = [...body.users].map((user) => user.email);
      body.users = [...body.users].map((user) => {
        user.phone = user.phone.replace(/\D/g, '');
        return user;
      });
    }

    const documentId = `${body.companyId}_${body.document}`;

    if (avoidChecking) {
      db.collection(collection).doc(documentId).set(body, { merge: true })
      .then(resolve)
      .catch(reject);
    } else {
      documentExists(collection, documentId)
      .then((reference) => reference.set(body, { merge: true }).then(resolve).catch(reject))
      .catch(reject);
    }

  })
);

export const updateStakeholder = (body, objectId, collection) => (
  new Promise((resolve, reject) => {
    body.updatedAt = moment(moment()).toDate();
    if (body.users) {
      body.usersEmails = [...body.users].map((user) => user.email);
      body.users = [...body.users].map((user) => {
        user.phone = user.phone.replace(/\D/g, '');
        return user;
      });
    }
    db.collection(collection).doc(objectId).update(body).then(resolve).catch(reject);
  })
);

export const removeStakeholder = (collection, objectId) => (
  new Promise((resolve, reject) => {
    db.collection(collection).doc(objectId).delete().then(resolve).catch(reject);
  })
);

export { default as deliveries } from './deliveries';
export { default as drivers } from './drivers';
export { default as delivery } from './delivery';
export { default as records } from './records';
export { default as company } from './company';
export { default as storage } from './storage';
