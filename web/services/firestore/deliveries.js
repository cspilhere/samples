import * as firebase from 'firebase/app';
import 'firebase/firestore';

const searchjs = require('searchjs');
import Fuse from 'fuse.js';
import moment from 'moment';

import Constants from 'services/Constants';
import { toArrayWithIds } from './utilities';

const db = firebase.firestore();

export default {

  fetch: ({ filter }, eventCallback) => {

    const { dateRangeField, dateRange, query } = filter || {};

    return new Promise((resolve, reject) => {
      db.collection('deliveries')
      .where('d.companyId', '==', Constants.get('companyId'))
      .where('d.' + (dateRangeField || 'updatedAt'), '>=', dateRange && dateRange[0] || moment().startOf('isoWeek')._d)
      .where('d.' + (dateRangeField || 'updatedAt'), '<=', dateRange && dateRange[1] || moment().endOf('isoWeek')._d)
      .orderBy('d.' + (dateRangeField || 'updatedAt'), 'desc')
      .onSnapshot((snapshot) => {

        let data = toArrayWithIds(snapshot);

        const queryConfig = [];

        Object.keys(filter || {}).forEach((key) => {
          if ('client.document' === key && filter[key].length > 0) {
            queryConfig.push({ _join: 'OR', 'client.document': filter[key] });
          }
          if ('shipper.document' === key && filter[key].length > 0) {
            queryConfig.push({ _join: 'OR', 'shipper.document': filter[key] });
          }
          if ('driver.document' === key && filter[key].length > 0) {
            queryConfig.push({ _join: 'OR', 'driver.document': filter[key] });
          }
          if ('label' === key && filter[key].length > 0) queryConfig.push({ 'label': filter[key] });
          if ('status' === key && filter[key].length > 0) queryConfig.push({ 'status': filter[key] });
        });

        if (queryConfig.length > 0) {
          data = searchjs.matchArray(data, {
            terms: queryConfig
          });
        }

        if (query) {
          const fuse = new Fuse(data, {
            tokenize: true,
            matchAllTokens: true,
            threshold: 0.3,
            shouldSort: true,
            keys: [
              'client.document',
              'client.name',
              'client.address.description',
              'client.address.city',
              'client.address.uf',
              'shipper.document',
              'shipper.name',
              'shipper.address.description',
              'shipper.address.city',
              'shipper.address.uf',
              'driver.name',
              'driver.phone',
              'driver.document',
              'number',
              'destination.address',
            ]
          });
          data = fuse.search(query);
        }

        if (eventCallback && typeof eventCallback === 'function') {
          eventCallback(data);
        } else {
          resolve(data);
        }

      }, reject);
    });

  },

};
