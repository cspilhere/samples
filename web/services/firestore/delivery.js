import * as firebase from 'firebase/app';
import 'firebase/firestore';

import moment from 'moment';

import Constants from 'services/Constants';

import { toObjectWithId, createToken } from './utilities';

import { filterObject, uuid } from 'shared-utils/utils';

import { GeoFirestore } from 'geofirestore';

const db = firebase.firestore();

const geoFirestore = new GeoFirestore(db);
const geoCollection = geoFirestore.collection('deliveries');

const delivery = {

  fetch: (id, eventCallback) => (
    new Promise((resolve, reject) => {
      db.collection('deliveries')
      .doc(id)
      .onSnapshot((snapshot) => {
        if (eventCallback && typeof eventCallback === 'function') {
          eventCallback(toObjectWithId(snapshot));
        } else {
          resolve(toObjectWithId(snapshot));
        }
      }, reject);
    })
  ),

  fetchByToken: (token, eventCallback) => (
    new Promise((resolve, reject) => {
      db.collection('deliveries')
      .where('d.token', '==', token)
      .onSnapshot((snapshot) => {
        if (eventCallback && typeof eventCallback === 'function') {
          eventCallback(toObjectWithId(snapshot.docs[0]));
        } else {
          resolve(toObjectWithId(snapshot.docs[0]));
        }
      }, reject);
    })
  ),

  release: (id) => delivery.update(id, { isReady: true, status: 'ready', releasedAt: moment(moment()).toDate() }),

  remove: (id) => (
    new Promise((resolve, reject) => {
      db.collection('deliveries')
      .doc(id)
      .delete()
      .then(resolve)
      .catch(reject);
    })
  ),

  finish: (id) => delivery.update(id, { finishedAt: moment(moment()).toDate(), status: 'finished' }),

  updateLabel: (id, value) => delivery.update(id, { label: value }),

  saveRating: (id, body) => delivery.update(id, {
    rating: firebase.firestore.FieldValue.arrayUnion({
      timestamp: moment(moment()).toDate(),
      value: body.rating,
      createdBy: body.createdBy
    })
  }),

  create: (body) => (
    new Promise((resolve, reject) => {
      if (body.destination && body.destination.coordinates) {
        body.coordinates = new firebase.firestore.GeoPoint(
          body.destination.coordinates.latitude,
          body.destination.coordinates.longitude
        );
      }
      body.companyId = Constants.get('companyId');
      body.createdAt = moment(moment()).toDate();
      body.updatedAt = moment(moment()).toDate();
      body.status = !body.isReady ? 'waiting' : 'ready';
      body.code = uuid(true).toUpperCase();
      if (body.isReady) delete body.isReady;
      if (body.driver) body.driver = filterObject(body.driver, ['name', 'id', 'document', 'phone']);
      if (body.client) body.client = filterObject(body.client, ['address', 'document', 'email', 'id', 'name', 'phone']);
      if (body.shipper) body.shipper = filterObject(body.shipper, ['address', 'document', 'email', 'id', 'name', 'phone']);
      if (body.invoices && typeof body.invoices === 'string') {
        body.invoices = body.invoices.replace(/ /g, '').replace(/;/g, ',').replace(/[-|.]/g, '').split(',');
      }
      body.token = createToken();
      geoCollection
      // .doc(`${body.companyId}_${body.number}${body.series}`)
      .doc()
      .set(body, { merge: true })
      .then(resolve)
      .catch(reject);
    })
  ),

  update: (id, body) => (
    new Promise((resolve, reject) => {
      console.log(id, body);
      db.collection('deliveries')
      .doc(id)
      .set({
        d: {
          ...body,
          updatedAt: moment(moment()).toDate(),
        }
      }, { merge: true })
      .then(resolve)
      .catch(reject);
    })
  ),

  updateWithNewCoordinates: (id, body) => (
    new Promise((resolve, reject) => {
      if (body.destination && body.destination.coordinates) {
        body.coordinates = new firebase.firestore.GeoPoint(
          body.destination.coordinates.lat || body.destination.coordinates.latitude,
          body.destination.coordinates.lng || body.destination.coordinates.longitude
        );
      }
      body.updatedAt = moment(moment()).toDate();
      geoCollection
      .doc(id)
      .set(body, { merge: true })
      .then(resolve)
      .catch(reject);
    })
  )

};

export default delivery;
