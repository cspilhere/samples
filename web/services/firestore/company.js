import * as firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';

import moment from 'moment';

import Constants from 'services/Constants';

import { uuid } from 'shared-utils/utils';

import { toObjectWithId } from './utilities';

const db = firebase.firestore();

const storageRef = firebase.storage().ref();

const company = {

  updateLabels: (items) => company.update({ labels: items }),

  updateName: (value) => company.update({ name: value }),

  updateColor: (value) => company.update({ color: value }),

  updateSettings: (value) => company.update({ settings: value }),

  updateLogo: (file) => {
    return new Promise((resolve, reject) => {
      const fileRef = storageRef.child(`${Constants.get('companyId')}/${uuid()}.${file.ext}`);
      const upload = fileRef.put(file.raw, { name: file.name });
      upload.on('state_changed', (snapshot) => {
        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log('Upload is ' + progress + '% done');
      }, (error) => {
        reject();
      }, () => {
        upload.snapshot.ref.getDownloadURL().then((fileUrl) => {
          company.update({ logo: fileUrl }).then(resolve);
        });
      });
    });
  },

  publicData: (companyId, eventCallback) => (
    new Promise((resolve, reject) => {
      db.collection('companiesPublicData')
      .doc(Constants.get('companyId') || companyId)
      .onSnapshot((snapshot) => {
        if (eventCallback && typeof eventCallback === 'function') {
          eventCallback(toObjectWithId(snapshot));
        } else {
          resolve(toObjectWithId(snapshot));
        }
      }, reject);
    })
  ),

  indexes: (eventCallback) => (
    new Promise((resolve, reject) => {
      db.collection('companiesIndexes')
      .doc(Constants.get('companyId'))
      .onSnapshot((snapshot) => {

        const data = toObjectWithId(snapshot);

        if (eventCallback && typeof eventCallback === 'function') {
          eventCallback(data);
        } else {
          resolve(data);
        }

      }, reject);
    })
  ),

  update: (body) => (
    new Promise((resolve, reject) => {
      db.collection('companies')
      .doc(Constants.get('companyId'))
      .set({
        ...body,
        updatedAt: moment(moment()).toDate(),
      }, { merge: true })
      .then(resolve)
      .catch(reject);
    })
  ),

};

export default company;
