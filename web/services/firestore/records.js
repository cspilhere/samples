import * as firebase from 'firebase/app';
import 'firebase/firestore';

import moment from 'moment';

import { toArrayWithIds } from './utilities';

const db = firebase.firestore();

export default {

  fetch: (deliveryId, eventCallback) => (
    new Promise((resolve, reject) => {
      db.collection('deliveries')
      .doc(deliveryId)
      .collection('records')
      .orderBy('createdAt', 'desc')
      .onSnapshot((snapshot) => {
        if (eventCallback && typeof eventCallback === 'function') {
          eventCallback(toArrayWithIds(snapshot));
        } else {
          resolve(toArrayWithIds(snapshot));
        }
      }, reject);
    })
  ),

  remove: (deliveryId, recordId) => (
    new Promise((resolve, reject) => {
      db.collection('deliveries')
      .doc(deliveryId)
      .collection('records')
      .doc(recordId)
      .delete()
      .then(resolve)
      .catch(reject);
    })
  ),

  create: (deliveryId, body) => (
    new Promise((resolve, reject) => {
      db.collection('deliveries')
      .doc(deliveryId)
      .collection('records')
      .add({
        ...body,
        createdAt: moment(moment()).toDate(),
      })
      .then(resolve)
      .catch(reject);
    })
  )

};
