import * as firebase from 'firebase/app';
import 'firebase/storage';

const storageRef = firebase.storage().ref();

const delivery = {

  upload: (body) => (
    new Promise((resolve, reject) => {
      const fileRef = storageRef.child(body.fileRef);
      const upload = fileRef.put(body.file, { name: body.name });
      upload.on('state_changed', (snapshot) => {
        const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log('Upload is ' + progress + '% done');
        switch (snapshot.state) {
          case firebase.storage.TaskState.PAUSED: // or 'paused'
            console.log('Upload is paused');
            break;
          case firebase.storage.TaskState.RUNNING: // or 'running'
            console.log('Upload is running');
            break;
        }
      }, (error) => {
        reject(error);
      }, () => {
        resolve(upload.snapshot.ref);
      });
    })
  )

};

export default delivery;
