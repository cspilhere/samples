import * as firebase from 'firebase/app';
import 'firebase/firestore';

import CryptoJS from 'crypto-js';

const db = firebase.firestore();

export const toArrayWithIds = (snapshot) => {
  return [...snapshot.docs.map((doc) => {
    let data = doc.data();
    if (!data) return false;
    if (data.hasOwnProperty('d')) {
      data = data.d;
    }
    return { ...data, id: doc.id };
  })];
};

export const toObjectWithId = (snapshot) => {
  let data = snapshot.data();
  if (!data) return false;
  if (data.hasOwnProperty('d')) {
    data = data.d;
  }
  return {
    ...data,
    id: snapshot.id
  };
};

export function documentExists(collection, docId) {
  return new Promise((resolve, reject) => {
    const reference = db.collection(collection).doc(docId);
    reference.get().then((snapshot) => {
      if (!snapshot.exists) {
        resolve(reference);
      } else {
        reject({ code: 'document-already-exists', data: toObjectWithId(snapshot) });
      }
    });
  });
};

export function checkIfDocAlreadyExists(collection, docId) {
  return new Promise((resolve, reject) => {
    const ref = db.collection(collection).doc(docId);
    ref.get().then((doc) => {
      if (!doc.exists) {
        resolve(ref);
      } else {
        reject({ code: 'doc-already-exists', data: doc.data() });
      }
    });
  });
};

export const createToken = () => {
  let time = () => new Date();
  let uuid = () => Math.floor((1 + Math.random()) * 0x10000).toString(36).substring(1) + time().getTime().toString(36);
  const hash = CryptoJS.SHA512(uuid());
  return hash.toString();
};
