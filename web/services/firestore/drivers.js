import * as firebase from 'firebase/app';
import 'firebase/firestore';

const searchjs = require('searchjs');
import Fuse from 'fuse.js';
import moment from 'moment';

import Constants from 'services/Constants';
import { toArrayWithIds, toObjectWithId, documentExists } from './utilities';

const db = firebase.firestore();

export default {

  fetch: (documentId, eventCallback) => (
    new Promise((resolve, reject) => {
      db.collection('drivers')
      .doc(documentId)
      .onSnapshot((snapshot) => {
        let data = toObjectWithId(snapshot);

        data.phone = data.phone.replace('+55', '');

        if (eventCallback && typeof eventCallback === 'function') {
          eventCallback(data);
        } else {
          resolve(data);
        }
      }, reject);
    })
  ),

  fetchAll: (filter, eventCallback) => (
    new Promise((resolve, reject) => {

      const { query } = filter || {};

      db.collection('drivers')
      .where('companyId', '==', Constants.get('companyId'))
      .orderBy('createdAt', 'desc')
      .onSnapshot((snapshot) => {

        let data = toArrayWithIds(snapshot);

        const queryConfig = [];

        Object.keys(filter || {}).forEach((key) => {
          if ('relation' === key && filter[key] !== '') {
            queryConfig.push({ _join: 'OR', 'relation': filter[key] * 1 });
          }
          if ('active' === key && filter[key] !== '') {
            queryConfig.push({ _join: 'OR', 'active': JSON.parse(filter[key]) });
          }
        });

        if (queryConfig.length > 0) {
          data = searchjs.matchArray(data, {
            terms: queryConfig
          });
        }

        if (query) {
          const fuse = new Fuse(data, {
            tokenize: true,
            matchAllTokens: true,
            threshold: 0.3,
            shouldSort: true,
            keys: [
              'document',
              'name',
              'phone'
            ]
          });
          data = fuse.search(query);
        }

        data = data.map((driver) => {
          driver.phone = driver.phone.replace('+55', '');
          return driver;
        });

        if (eventCallback && typeof eventCallback === 'function') {
          eventCallback(data);
        } else {
          resolve(data);
        }

      }, reject);
    })
  ),

  location: (phone, releasedAt, finishedAt, eventCallback) => (
    new Promise((resolve, reject) => {


      if (!phone || !releasedAt) reject('Phone number or releasedAt date not found');

      console.log(phone, releasedAt, finishedAt, Constants.get('companyId'));

      const unsubscribe = db.collection('locations')
      .where('location.extras.companyId', '==', Constants.get('companyId'))
      .where('location.extras.driver.phone', '==', '+55' + phone)
      .where('location.extras.createdAt', '>=', releasedAt)
      .where('location.extras.createdAt', '<=', finishedAt || moment(moment().add(1, 'day')).toDate())
      .orderBy('location.extras.createdAt', 'desc')
      .onSnapshot((snapshot) => {
        const data = toArrayWithIds(snapshot);
        console.log(data);
        if (eventCallback && typeof eventCallback === 'function') {
          eventCallback(data, () => unsubscribe);
        } else {
          resolve(data, () => unsubscribe);
        }
      }, reject);
    })
  ),

  create: (body) => (
    new Promise((resolve, reject) => {
      body.createdAt = moment(moment()).toDate();
      body.companyId = Constants.get('companyId');
      body.document = body.document.replace(/\D/g, '');
      body.phone = body.phone.replace(/\D/g, '');
      body.phone = '+55' + body.phone;
      const documentId = `${body.companyId}_${body.document}_${body.phone}`;
      documentExists('drivers', documentId)
      .then((reference) => reference.set(body).then(resolve).catch(reject))
      .catch(reject);
    })
  ),

  update: (body, id) => (
    new Promise((resolve, reject) => {
      body.updatedAt = moment(moment()).toDate();
      body.document = body.document.replace(/\D/g, '');
      body.phone = body.phone.replace(/\D/g, '');
      body.phone = '+55' + body.phone;
      db.collection('drivers').doc(id).update(body).then(resolve).catch(reject);
    })
  ),

  remove: (id) => (
    new Promise((resolve, reject) => {
      db.collection('drivers').doc(id).delete().then(resolve).catch(reject);
    })
  )

};
