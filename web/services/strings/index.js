import { deepKey } from 'shared-utils/utils';

const dict = {
  'delivery': {
    'release-alert-title': 'Liberar entrega para viagem',
    'release-alert-confirmation': 'Ao confirmar a entrega será liberada para viagem e irá aparecer no APP do Motorista.',
    'release-alert-driver-notfound': 'ATENÇÃO! Você precisa escolher um motorista antes de liberar a entrega para viagem.',
    'release-request-error': '',
    'release-request-success': 'Entrega liberada com sucesso!',

    'finish-alert-title': 'Tem certeza que deseja concluir a entrega?',
    'finish-alert-confirmation': 'ATENÇÃO! Se você prosseguir a entrega será concluída e todos as partes serão notificadas.',
    'finish-request-success': 'Entrega concluída com sucesso!',

    'cancel-alert-title': 'Tem certeza que deseja cancelar a entrega?',
    'cancel-alert-confirmation': 'ATENÇÃO! Se você prosseguir a entrega será cancelada e removida do banco de dados.',
    'cancel-request-success': 'Entrega cancelada com sucesso!',

    'action-release-delivery': 'Liberar entrega',
  },
  'global': {
    'alert-title-attention': 'Atenção'
  }
};

export default (path) => {
  return deepKey(dict, path);
};
