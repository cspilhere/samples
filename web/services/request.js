import axios from 'axios';
import { baseURL } from 'core/constants';
import { buildQuery } from 'shared-utils/utils';

const request = axios.create({
  baseURL: baseURL
});

request.interceptors.request.use((config) => {
  return config;
}, (error) => {
  return Promise.reject(error);
});

request.interceptors.response.use((response) => {
  return response;
}, (error) => {
  return Promise.reject(error.response);
});

export default request;
