export function showStatusLabel(value) {
  switch (value) {
    case 'waiting':
      return 'Aguardando';
    case 'ready':
      return 'Liberada';
    case 'ongoing':
      return 'Em trânsito';
    case 'finished':
      return 'Concluída';
    default:
      return 'Aguardando';
  }
}

export function showStatus(value) {
  let situation = {};
  switch (value) {
    case 'waiting':
      situation.label = 'Aguardando';
      situation.color = '#8a9ba8';
      situation.secondaryColor = '#8a9ba8';
      break;
    case 'ready':
      situation.label = 'Liberada';
      situation.color = '#3dcc91';
      situation.secondaryColor = '#3dcc91';
      break;
    case 'ongoing':
      situation.label = 'Em trânsito';
      situation.color = '#1A497B';
      situation.secondaryColor = '#1A497B';
      break;
    case 'finished':
      situation.label = 'Concluída';
      situation.color = '#ffc940';
      situation.secondaryColor = '#ffc940';
      break;
  }
  return situation;
}

export function showRecordTypeLabel(type) {
  switch (type) {
    case 'note':
      return 'Anotação';
    case 'damage':
      return 'Avaria';
    case 'receipt':
      return 'Comprovante';
    case 'document':
      return 'Documento';
    case 'gathering':
      return 'Coleta';
    case 'event':
      return 'Registro';
    case 'reference':
      return 'Referência';
    case 'conclusion':
      return 'Registro';
  }
}

export const showRecordType = (type) => {
  let labelAndIcon = {};
  switch (type) {
    case 'note':
      labelAndIcon.label = 'Anotação';
      labelAndIcon.icon = 'fa-sticky-note';
      labelAndIcon.color = '#8a9ba8';
      break
    case 'damage':
      labelAndIcon.label = 'Avaria';
      labelAndIcon.icon = 'fa-fragile';
      labelAndIcon.color = '#ff7373';
      break
    case 'receipt':
      labelAndIcon.label = 'Comprovante';
      labelAndIcon.icon = 'fa-receipt';
      labelAndIcon.color = '#3dcc91';
      break
    case 'document':
      labelAndIcon.label = 'Documento';
      labelAndIcon.icon = 'fa-file-alt';
      labelAndIcon.color = '#669eff';
      break
    case 'gathering':
      labelAndIcon.label = 'Coleta';
      labelAndIcon.icon = 'fa-calendar-star';
      labelAndIcon.color = '#1A497B';
      break
    case 'event':
      labelAndIcon.label = 'Registro';
      labelAndIcon.icon = 'fa-calendar-star';
      labelAndIcon.color = '#48aff0';
      break
    case 'reference':
      labelAndIcon.label = 'Referência';
      labelAndIcon.icon = 'fa-asterisk';
      labelAndIcon.color = '#ad99ff';
      break
    case 'conclusion':
      labelAndIcon.label = 'Conclusão';
      labelAndIcon.icon = 'fa-glass-cheers';
      labelAndIcon.color = '#ffc940';
      break
  }
  return labelAndIcon;
}

export const showDriverRelationLabel = (value) => {
  switch (value) {
    case '0':
      return 'Contratado'
    case '1':
      return 'Agregado';
    case '2':
      return 'Terceirizado'
    case '3':
      return 'Freelancer'
    case 0:
      return 'Contratado'
    case 1:
      return 'Agregado';
    case 2:
      return 'Terceirizado'
    case 3:
      return 'Freelancer'
  }
};

export const showTaker = (value) => {
  let taker = {};
  switch (value) {
    case 0:
      taker.description = 'Remetente';
      taker.label = 'CIF';
      break
    case '0':
      taker.description = 'Remetente';
      taker.label = 'CIF';
      break
    case 1:
      taker.description = 'Expedidor';
      taker.label = 'Embarcadora';
      break
    case '1':
      taker.description = 'Expedidor';
      taker.label = 'Embarcadora';
      break
    case 2:
      taker.description = 'Recebedor';
      taker.label = 'Cliente';
      break
    case '2':
      taker.description = 'Recebedor';
      taker.label = 'Cliente';
      break
    case 3:
      taker.description = 'Destinatário';
      taker.label = 'FOB';
      break
    case '3':
      taker.description = 'Destinatário';
      taker.label = 'FOB';
      break
    default:
      taker.description = '';
      taker.label = '';
      break
  }
  return taker;
}

export const showTakerLabel = (value) => {
  switch (value) {
    case 0:
      return 'CIF';
    case '0':
      return 'CIF';
    case 1:
      return 'Embarcadora';
    case '1':
      return 'Embarcadora';
    case 2:
      return 'Cliente';
    case '2':
      return 'Cliente';
    case 3:
      return 'FOB';
    case '3':
      return 'FOB';
    default:
      return 'Não informado';
  }
}
