import React from 'react';
import PropTypes from 'prop-types';

import styled, { css, keyframes } from 'styled-components';

import { fadeInRight, fadeOutRight } from 'react-animations';

import { Icon, Fill } from 'components';
import settings from 'components/settings';

const enterRightAnimation = keyframes`${fadeInRight}`;
const leaveRightAnimation = keyframes`${fadeOutRight}`;

const Container = styled.div`${({ maxWidth }) => css`
  position: absolute;
  z-index: 9;
  top: 0;
  right: 0;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  max-width: ${maxWidth || 320}px;
  background-color: white;
  animation: .4s ${enterRightAnimation};
  box-shadow: -1px 0px 4px rgba(0, 0, 0, .3);
`}`;

// fromRight
// zIndex

const HideButton = styled.button`
  ${settings.buttonReset}
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 20px;
`;

const Content = styled.div`
  overflow: auto;
  flex: 1;
`;

const Drawer = (props) => {
  const {
    children,
    hideRequest,
    show,
    header,
    maxWidth
  } = props;
  if (!show) return null;
  return (
    <Container {...{ maxWidth }}>
      <Header>
        <HideButton id="drawer-close-button" onClick={hideRequest}>
          <Icon name="far fa-times-circle" size={24} color="#8a9ba8" />
        </HideButton>
        <Fill />
        {header}
      </Header>
      <Content>{children({ show, hideRequest })}</Content>
    </Container>
  );
};

Drawer.propTypes = {};

export default Drawer;
