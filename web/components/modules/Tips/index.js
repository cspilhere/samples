import React, { Fragment } from 'react';
import PropTypes, { string } from 'prop-types';

import styled, { css } from 'styled-components';

import { Title, Text, Box, Icon, Button, Space } from 'components';

const Container = styled.div`
  width: 340px;
  padding: 25px;
  position: absolute;
  z-index: 100;
  bottom: 0;
  ${({ side }) => css`
    ${side || 'right'}: 0;
  `}
`;

const CloseTips = styled.div`
  position: absolute;
  top: -8px;
  right: 25px;
`;

const Tips = ({ cards, isVisible, onHide }) => {

  if (!isVisible) return null;

  const cardsList = cards.map((card, index) => {
    const titleIsString = typeof card.title === 'string';
    return (
      <Box elevated borderRadius={6} color="#1C2541" padding={15} style={{ marginBottom: 10 }} key={index}>
        {titleIsString ? <Title light small>{card.title}</Title> : card.title}
        {(card.items || []).map((item, index) => {
          const itemIsString = typeof item === 'string';
          return (
            <Fragment key={index}>
              <Space size={8} />
              {itemIsString ? <Text light small>{item}</Text> : item}
            </Fragment>
          );
        })}
      </Box>
    );
  });

  return (
    <Container>
      <CloseTips>
        <Button onClick={onHide} bare style={{ marginRight: 1 }}>
          <Text small color="#293742">Esconder</Text>&nbsp;<Icon name="far fa-times" color="#293742" />
        </Button>
      </CloseTips>
      {cardsList}
    </Container>
  );

};

Tips.propTypes = {};

export default Tips;
