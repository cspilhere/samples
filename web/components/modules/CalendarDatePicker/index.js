import React, { useEffect, useState, useRef } from 'react';

import { Manager, Reference, Popper } from 'react-popper';

import moment from 'moment';
import 'moment/locale/pt-br';

import DayPicker, { DateUtils } from 'react-day-picker';
import MomentLocaleUtils from 'react-day-picker/moment';
import 'react-day-picker/lib/style.css';
import './styles.scss';

import { Button } from 'shared-components';

const CalendarDatePicker = (props) => {

  const [isOpen, setOpenState] = useState(false);

  const [isDefaultValueUpdated, setDefaultValueStatus] = useState(false);

  const [isLastMonth, setIfIsLastMonth] = useState(false);

  const [from, setFrom] = useState(null);
  const [to, setTo] = useState(null);
  const [enteredTo, setEnteredTo] = useState(null);

  const { value, onChange, disabled, keepOpened, isSmall, defaultValue = {}, callOnChangeAfterMount } = props;

  const containerRef = useRef(null);

  useEffect(() => {

    if (defaultValue && defaultValue.startDate && defaultValue.endDate) {
      setDefaultValueStatus(true);
      setFrom(defaultValue.startDate);
      setTo(defaultValue.endDate);
      setEnteredTo(defaultValue.endDate);
      if (onChange && callOnChangeAfterMount) onChange([defaultValue.startDate, defaultValue.endDate]);
    }

    function handleClickOutside(event) {
      if (containerRef.current && !containerRef.current.contains(event.target)) {
        if (!from) {
          setFrom(defaultValue.startDate);
          setTo(defaultValue.endDate);
          setEnteredTo(defaultValue.endDate);
          if (onChange) onChange([defaultValue.startDate, defaultValue.endDate]);
        }
        setOpenState(false);
      }
    }

    document.addEventListener('click', handleClickOutside, true);

    return function cleanup() {
      document.removeEventListener('click', handleClickOutside, true);
    };

  }, [defaultValue]);

  function handleChanges(range) {
    if (!keepOpened) setOpenState(false);
    if (range) {
      if (moment(range[0]).isSame(moment(range[1]), 'month')) {
        setIfIsLastMonth(false);
      } else {
        setIfIsLastMonth(true);
      }
      setFrom(range[0]);
      setTo(range[1]);
      setEnteredTo(range[1]);
    }
    if (onChange) onChange(range);
  }

  function isSelectingFirstDay(from, to, day) {
    const isBeforeFirstDay = from && DateUtils.isDayBefore(day, from);
    const isRangeSelected = from && to;
    return !from || isBeforeFirstDay || isRangeSelected;
  }

  function handleDayClick(day) {
    setIfIsLastMonth(false);
    if (from && to && day >= from && day <= to) {
      handleResetClick();
      return;
    }
    if (isSelectingFirstDay(from, to, day)) {
      setFrom(day);
      setTo(null);
      setEnteredTo(null);
    } else {
      setTo(day);
      setEnteredTo(day);
      handleChanges([from, enteredTo]);
    }
  }

  function handleDayMouseEnter(day) {
    if (!isSelectingFirstDay(from, to, day)) setEnteredTo(day);
  }

  function handleResetClick() {
    setIfIsLastMonth(false);
    setFrom(null);
    setTo(null);
    setEnteredTo(null);
    handleChanges(null);
  }

  function selectToday() {
    handleChanges([moment().startOf('day')._d, moment().endOf('day')._d]);
  }

  function selectYesterday() {
    handleChanges([moment().subtract(1, 'day').startOf('day')._d, moment().subtract(1, 'day').endOf('day')._d]);
  }

  function selectThisWeek() {
    handleChanges([moment().startOf('isoWeek')._d, moment().endOf('isoWeek')._d]);
  }

  function selectLastSevenDays() {
    handleChanges([moment().subtract(7, 'days').startOf('day')._d, moment().subtract(1, 'day').endOf('day')._d]);
  }

  function selectThisMonth() {
    handleChanges([moment().startOf('month')._d, moment().endOf('month')._d]);
  }

  function selectLastMonth() {
    handleChanges([moment().startOf('month').subtract(1, 'month')._d, moment().endOf('month').subtract(1, 'month')._d]);
  }

  const modifiers = { start: from, end: enteredTo };
  const selectedDays = [from, { from, to: enteredTo }];

  const startDate = moment(from).format('DD/MM/YYYY');
  const endDate = moment(enteredTo).format('DD/MM/YYYY');

  const hasStartDateSelected = startDate != 'Invalid date';

  let dateLabel = `${startDate != 'Invalid date' ? startDate : null} - ${endDate != 'Invalid date' ? endDate : '??/??/????'}`

  return (
    <div ref={containerRef} style={{ width: '100%' }}>
      <Manager>
        <Reference>
          {({ ref }) => (
            <div ref={ref} className="calendar-picker-trigger-wrapper">
              <button
                type="button"
                className={`calendar-picker-trigger${isSmall ? ' is-small' : ''}`}
                onClick={() => setOpenState(!isOpen)}>
                {hasStartDateSelected ? dateLabel : ''}
              </button>
              {/* {hasStartDateSelected && (
                <button
                  type="button"
                  className="calendar-picker-trigger-reset"
                  onClick={handleResetClick}>
                  <svg height="20" width="20" viewBox="0 0 20 20" aria-hidden="true" focusable="false" className="css-19bqh2r"><path d="M14.348 14.849c-0.469 0.469-1.229 0.469-1.697 0l-2.651-3.030-2.651 3.029c-0.469 0.469-1.229 0.469-1.697 0-0.469-0.469-0.469-1.229 0-1.697l2.758-3.15-2.759-3.152c-0.469-0.469-0.469-1.228 0-1.697s1.228-0.469 1.697 0l2.652 3.031 2.651-3.031c0.469-0.469 1.228-0.469 1.697 0s0.469 1.229 0 1.697l-2.758 3.152 2.758 3.15c0.469 0.469 0.469 1.229 0 1.698z"></path></svg>
                </button>
              )} */}
            </div>
          )}
        </Reference>
        <Popper
          placement="bottom-end"
          positionFixed={true}
          modifiers={{ preventOverflow: { enabled: true, escapeWithReference: true, boundariesElement: 'viewport' } }}>
          {({ ref, style, placement, arrowProps }) => isOpen && (
            <div ref={ref} style={{ ...style, zIndex: 99 }} data-placement={placement}>

              <div className="calendar-picker-wrapper">
                <div className="calendar-picker">
                  <div className="calendar-picker-presets">
                    <Button isSmall onClick={selectToday}>Hoje</Button>
                    <Button isSmall onClick={selectYesterday}>Ontem</Button>
                    <Button isSmall onClick={selectThisWeek}>Semana atual</Button>
                    <Button isSmall onClick={selectLastSevenDays}>Últimos 7 dias</Button>
                    <Button isSmall onClick={selectThisMonth}>Mês atual</Button>
                    <Button isSmall onClick={selectLastMonth}>Mês anterior</Button>
                    {/* <Button isSmall onClick={handleResetClick}>Limpar</Button> */}
                  </div>
                  <div className="calendar-picker-calendar">
                    <DayPicker
                      className="Range"
                      fixedWeeks
                      numberOfMonths={2}
                      fromMonth={from}
                      month={from}

                      fromMonth={from}
                      toMonth={moment(from).add(1, 'month')._d}

                      selectedDays={selectedDays}
                      modifiers={modifiers}
                      onDayClick={handleDayClick}
                      onDayMouseEnter={handleDayMouseEnter}
                      localeUtils={MomentLocaleUtils}
                      locale={'pt-BR'}
                    />
                  </div>
                </div>

                <div ref={arrowProps.ref} style={arrowProps.style} />

              </div>
            </div>
          )}
        </Popper>
      </Manager>
    </div>
  );
};

export default CalendarDatePicker;
