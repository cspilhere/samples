import { css } from 'styled-components';

import colors, { convertHexToRGB } from './colors';

export const baseFontSize = 14;
export const smallFontSize = 12;
export const largeFontSize = 18;
export const regularFontSize = 16;

export const smallLineHeight = 1.2;
export const baseLineHeight = 1.5;

export const borderWidth = 1;
export const borderRadius = 2;

export const focus = css`&:focus {
  outline: none;
  border: 1px solid ${convertHexToRGB(colors.primary, .8)};
  box-shadow: 0 0 3px 2px ${convertHexToRGB(colors.secondary, .6)};
}`;

export const hover = css`&:hover {
  border: 1px solid ${convertHexToRGB(colors.primary, .8)};
}`;

export const elevation = css`box-shadow: 1px 1px 4px ${convertHexToRGB(colors.black, .3)};`;

export const inputReset = css`
  ${elementReset}
  font-size: ${baseFontSize}px;
`;

export const buttonReset = css`
  ${elementReset}
  cursor: pointer;
`;

export const elementReset = css`
  appearance: none;
  background: transparent;
  border: 0;
  display: inline;
  line-height: 1.2;
  margin: 0;
  outline: none;
  padding: 0;
`;

export const inputDisabled = css`
  &:disabled {
    background-color: ${colors.lightGray};
    border-color: ${colors.lightGray1};
  }
`;

export const absoluteFill = css`
  flex: 1;
  height: 100%;
  left: 0;
  position: absolute;
  top: 0;
  width: 100%;
`;

export const truncate = css`
  max-width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

export const truncateReverse = css`
  direction: rtl;
  max-width: 100%;
  overflow: hidden;
  text-align: left;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

export const placeholder = css`
  &::-moz-placeholder {
    color: rgba(54, 54, 54, 0.3);
  }
  &::-webkit-input-placeholder {
    color: rgba(54, 54, 54, 0.3);
  }
  &:-moz-placeholder {
    color: rgba(54, 54, 54, 0.3);
  }
  &:-ms-input-placeholder {
    color: rgba(54, 54, 54, 0.3);
  }
`;

export default {
  absoluteFill,
  baseFontSize,
  baseLineHeight,
  borderRadius,
  borderWidth,
  buttonReset,
  focus,
  hover,
  inputDisabled,
  inputReset,
  largeFontSize,
  placeholder,
  regularFontSize,
  smallFontSize,
  smallLineHeight
};
