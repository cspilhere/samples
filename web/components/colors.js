import _invertColor from 'invert-color';

export default {
  primary: '#1A497B',
  secondary: '#1C99FE',
  primaryDark: '#1C2541',
  secondaryDark: '#1C2541',

  success: '#3dcc91',
  successDark: '#0a6640',

  warning: '#ffc940',
  warningDark: '#a67908',

  danger: '#ff7373',
  dangerDark: '#a82a2a',

  disabled: '#8a9ba8',

  black: '#202b33',
  white: '#ffffff',

  darkGray1: '#202b33',
  darkGray2: '#293742',

  gray1: '#8a9ba8',
  gray2: '#8a9ba8',

  lightGray1: '#FAFAFA',
  lightGray2: '#ced9e0',
};

const invertColorConfig = {
  black: '#202b33',
  white: '#FFFFFF',
  threshold: .8
};

export const invertColor = (color, configs) => _invertColor(color, configs || invertColorConfig);

export const convertHexToRGB = (hex, opacity, returnAsArray) => {
  hex = hex.replace('#', '');
  let r = parseInt(hex.substring(0, 2), 16);
  let g = parseInt(hex.substring(2, 4), 16);
  let b = parseInt(hex.substring(4, 6), 16);
  let result = `rgb(${r}, ${g}, ${b})`;
  if (typeof opacity === 'number') result = `rgba(${r}, ${g}, ${b}, ${opacity})`;
  if (returnAsArray) result = [r, g, b];
  return result;
};

export const convertRGBToHSL = (rgb) => {
  const r = rgb[0] / 255;
  const g = rgb[1] / 255;
  const b = rgb[2] / 255;
  const min = Math.min(r, g, b);
  const max = Math.max(r, g, b);
  const delta = max - min;
  let h;
  let s;
  if (max === min) {
    h = 0;
  } else if (r === max) {
    h = (g - b) / delta;
  } else if (g === max) {
    h = 2 + (b - r) / delta;
  } else if (b === max) {
    h = 4 + (r - g) / delta;
  }
  h = Math.min(h * 60, 360);
  if (h < 0) {
    h += 360;
  }
  const l = (min + max) / 2;
  if (max === min) {
    s = 0;
  } else if (l <= 0.5) {
    s = delta / (max + min);
  } else {
    s = delta / (2 - max - min);
  }
  return [h, s * 100, l * 100];
};

export const darken = (color, ratio) => {
  let hsl = convertRGBToHSL(convertHexToRGB(color, null, true));
  hsl[2] -= hsl[2] * ratio;
  return `hsl(${hsl[0]}, ${hsl[1]}%, ${hsl[2]}%)`;
};

export const lighten = (color, ratio) => {
  let hsl = convertRGBToHSL(convertHexToRGB(color, null, true));
  hsl[2] += hsl[2] * ratio;
  return `hsl(${hsl[0]}, ${hsl[1]}%, ${hsl[2]}%)`;
};
