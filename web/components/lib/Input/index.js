import React from 'react';

import styled from 'styled-components';

import settings, { inputDisabled } from 'components/settings';
import colors from 'components/colors';

import { Spinner } from 'shared-components';

const Wrapper = styled.span`
  display: block;
  position: relative;
  width: 100%;
`;

const Loading = styled.span`
  position: absolute;
  top: 7px;
  right: 7px;
`;

const InputComponent = styled.input`
  border-radius: 2px;
  font-size: ${({ small }) => small ? 13 : 15}px;
  color: ${colors.darkGray2};
  min-height: ${({ small }) => small ? 28 : 32}px;
  border: 1px solid #ccc;
  padding-bottom: calc(.375em - 1px);
  padding-left: calc(.625em - 1px);
  padding-right: calc(.625em - 1px);
  padding-top: calc(.375em - 1px);
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-start;
  ${settings.focus}
  ${settings.hover}
  ${settings.placeholder}
  ${inputDisabled}
`;

const TextareaComponent = styled.textarea`
  border-radius: 2px;
  font-size: ${({ small }) => small ? 13 : 15}px;
  color: ${colors.darkGray2};
  min-height: ${({ small }) => small ? 50 : 70}px;
  border: 1px solid #ccc;
  padding-bottom: calc(.375em - 1px);
  padding-left: calc(.625em - 1px);
  padding-right: calc(.625em - 1px);
  padding-top: calc(.375em - 1px);
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-start;
  ${settings.focus}
  ${settings.hover}
  ${settings.placeholder}
  ${inputDisabled}
`;

const Input = ({ loading, style, textarea, disableAutoComplete, value, ...rest }) => {

  const defaultProps = {
    ...rest,
    value: value || '',
    autoComplete: disableAutoComplete ? 'new-password' : null,
    style: { width: '100%', ...style }
  };

  return (
    <Wrapper>
      {loading && <Loading><Spinner size={18} /></Loading>}
      {textarea ? <TextareaComponent {...defaultProps} /> : <InputComponent {...defaultProps} />}
    </Wrapper>
  );

};

Input.defaultProps = {
  onChange: () => {}
};

export default Input;
