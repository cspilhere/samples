import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

import colors from 'components/colors';
import settings from 'components/settings';

const MULTIPLY = 1.2;

const Title = styled.h1`${({
  color,
  light,

  noMargin,

  size,
  small,
  large,
  regular
}) => css`
  color: ${light ? colors.lightGray : color};
  line-height: 1;
  font-weight: bold;
  margin-bottom: ${noMargin ? 0 : 6 }px;
  font-size: ${
    small ? settings.smallFontSize * MULTIPLY : false ||
    large ? settings.largeFontSize * MULTIPLY : false ||
    regular ? settings.regularFontSize * MULTIPLY : false ||
    typeof size === 'string' ? settings[`${size}FontSize`] * MULTIPLY : size * MULTIPLY
  }px;
`}`;

Title.defaultProps = {
  color: colors.primaryDark,
  size: settings.baseFontSize
};

Title.propTypes = {};

export default Title;
