/** @jsx jsx */
import React from 'react';

import { jsx } from '@emotion/core';
import styles from './styles';

const Spinner = ({ isLight, size, color }) => {
  if (isLight) color = '#bfccd6';
  return <div css={styles.spinner(size, color)} />;
};

export default Spinner;
