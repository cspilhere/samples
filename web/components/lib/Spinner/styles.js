import { keyframes } from '@emotion/core'

import { convertHex } from 'shared-utils/utils';

const spin = keyframes`
  0% { transform: rotate(0); }
  100% { transform: rotate(180deg); }
`;

export default {
  spinner: (size = 24, color = '#5c7080') => ({
    position: 'relative',
    width: size,
    height: size,
    '&:before': {
      content: '""',
      width: size,
      height: size,
      position: 'absolute',
      zIndex: 1,
      top: 0,
      left: 0,
      display: 'inline-block',
      borderRadius: '100%',
      borderTop: `${size / 10}px solid ${convertHex(color, 90)}`,
      borderBottom: `${size / 10}px solid ${convertHex(color, 90)}`,
      borderLeft: `${size / 10}px solid ${convertHex('#FFFFFF', 1)}`,
      borderRight: `${size / 10}px solid ${convertHex('#FFFFFF', 1)}`,
      animation: `${spin} .25s linear infinite`,
    },
    '&:after': {
      content: '""',
      width: size - ((size / 10) * 4),
      height: size - ((size / 10) * 4),
      position: 'absolute',
      zIndex: 0,
      opacity: 1,
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      backgroundColor: `${convertHex(color, 10)}`,
      borderRadius: '100%',
    }
  })
};
