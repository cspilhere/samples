import React from 'react';
import { shallow } from 'enzyme';

import Spacer from './';

describe('Spacer', () => {

  it('should render correctly', () => {
    shallow(<Spacer />);
    shallow(<Spacer><span /></Spacer>);
  });

});
