import React from 'react';
import PropTypes from 'prop-types';

import styled, { css } from 'styled-components';

import colors, { convertHexToRGB } from 'components/colors';
import { elevation } from 'components/settings';

const Box = styled.div`${({
  bordered,
  borderRadius,
  color,
  elevated,
  fullWidth,
  inline,
  isBordered,
  isElevated,
  isFullWidth,
  isInline,
  margin,
  opacity,
  padding
}) => css`
  display: flex;
  flex-direction: ${isInline || inline ? 'row' : 'column'};
  align-items: ${isInline || inline ? 'center' : 'initial'};
  justify-content: flex-start;
  background-color: ${color ? convertHexToRGB(color, opacity) : convertHexToRGB(colors.white, opacity)};
  width: ${isFullWidth || fullWidth ? '100%' : 'auto'};
  padding: ${padding || 10}px;
  margin: ${margin ? margin : '0'}px;
  border: ${isBordered || bordered ? `1px solid ${colors.gray1}` : 'none'};
  border-radius: ${borderRadius ? borderRadius : 2}px;
  ${isElevated || elevated && elevation};
`}`;

Box.propTypes = {
  bordered: PropTypes.bool,
  borderRadius: PropTypes.number,
  color: PropTypes.number,
  elevated: PropTypes.bool,
  fullWidth: PropTypes.bool,
  inline: PropTypes.bool,
  isBordered: PropTypes.bool,
  isElevated: PropTypes.bool,
  isFullWidth: PropTypes.bool,
  isInline: PropTypes.bool,
  margin: PropTypes.number,
  opacity: PropTypes.number,
  padding: PropTypes.number
};

export default Box;
