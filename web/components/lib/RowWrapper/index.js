import React, { Children, Fragment } from 'react';
import PropTypes from 'prop-types';

import styled, { css } from 'styled-components';

import { Spacer } from 'components';

export const Fill = styled.div`${({ width }) => css`
  ${width && `width: ${width};`}
  flex-grow: ${width ? 0 : 1};
  flex-shrink: 0;
`}`;

export const Wrapper = styled.div`${() => css``}`;

const Container = styled.div`${({ align, padding, color, elevated }) => css`
  padding: ${typeof padding === 'string' ? padding : padding + 'px'};
  display: flex;
  flex-direction: row;
  background-color: ${color || 'transparent'};
  align-items: ${align || 'center'};
  justify-content: space-between;
  width: 100%;
  box-shadow: ${elevated ? `1px 1px 4px rgba(0, 0, 0, .3)` : 'none'};
`}`;

const RowWrapper = (props) => {
  return (
    <Container {...props}>
      {Children.toArray(props.children).map((element, index) => {
        return (
          <Fragment key={index}>
            {index !== 0 && props.useSpacer && <Spacer size={props.spacerSize} />}
            {element}
          </Fragment>
        );
      })}
    </Container>
  );
};

RowWrapper.propTypes = {};

export default RowWrapper;
