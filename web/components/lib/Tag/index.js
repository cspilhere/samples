import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import colors, { invertColor } from 'components/colors';
import settings from 'components/settings';

const DIFF = 1;

const Wrapper = styled.span`display: inline-flex;`;

const Container = styled.span`
  font-size: ${({
    size,
    small,
    large,
    regular
  }) => (
    small ? settings.smallFontSize - DIFF : false ||
    large ? settings.largeFontSize - DIFF : false ||
    regular ? settings.regularFontSize - DIFF : false ||
    typeof size === 'string' ? settings[`${size}FontSize`] - DIFF : size - DIFF
  )}px;
  background-color: ${({ color }) => color || colors.primary};
  color: ${({ color }) => invertColor(color || colors.primary)};
  padding: 6px 12px;
  border-radius: 80px;
  font-weight: bold;
  line-height: 1.1;
`;

const Tag = ({ children, style, color, size, small, large, regular }) => {
  return (
    <Wrapper>
      <Container {...{ color, size, small, large, regular }} style={style}>
        {children}
      </Container>
    </Wrapper>
  );
};

Tag.defaultProps = {
  color: colors.primaryDark,
  size: settings.baseFontSize
};

Tag.propTypes = {};

export default Tag;
