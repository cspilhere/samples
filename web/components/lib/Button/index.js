import React from 'react';
import PropTypes from 'prop-types';

import styled, { css } from 'styled-components';

import colors, { invertColor, darken } from 'components/colors';
import { buttonReset } from 'components/settings';

const Wrapper = styled.button`${({
  isBare,
  isFullFill,
  isFullWidth,
  offsetH,
  offsetV,
  size,
  themeColor
}) => isBare ? buttonReset : css`
  ${buttonReset}
  align-items: center;
  border-radius: 2px;
  display: flex;
  height: ${size}px;
  justify-content: center;
  margin-bottom: ${offsetV || 0}px;
  margin-left: ${offsetH || 0}px;
  margin-right: ${offsetH || 0}px;
  margin-top: ${offsetV || 0}px;
  padding-left: ${size / 1.8}px;
  padding-right: ${size / 1.8}px;
  padding: 0;
  transition: all .15s ease-in-out;
  ${isFullFill && 'flex: 1;'}
  ${isFullWidth && 'width: 100%;'}
  ${themeColor && css`background-color: ${themeColor};`}
  &:hover {
    background-color: ${darken(themeColor, .3)};
  }
`}`;

const Content = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const Label = styled.span`${({ size, color }) => css`
  color: ${color};
  font-size: ${size / 2}px;
`}`;

const Button = (props) => {

  const { onClick, children, ...rest } = props;

  let themeColor = colors.primary;
  let size = 32;

  const {
    isSmall,
    isMedium,
    isLarge,
    isPrimary,
    isSecondary,
    isSuccess,
    isWarning,
    isDanger,
    isDisabled
  } = rest;

  if (isSmall) size = 24;
  if (isMedium) size = 28;
  if (isLarge) size = 36;

  if (isPrimary) themeColor = colors.primary;
  if (isSecondary) themeColor = colors.secondary;
  if (isSuccess) themeColor = colors.success;
  if (isWarning) themeColor = colors.warning;
  if (isDanger) themeColor = colors.danger;
  if (isDisabled || rest.disabled) themeColor = colors.disabled;

  return (
    <Wrapper
      {...rest}
      size={size}
      themeColor={themeColor}
      onClick={!rest.disabled ? onClick : null}>
      <Content>
        {typeof children === 'string' ? (
          <Label size={size} color={invertColor(themeColor)}>
            {children}
          </Label>
        ) : (
          typeof children === 'function' ? (
            children({
              ...rest,
              labelColorSuggestion: invertColor(themeColor)
            })
          ) : (
            children
          )
        )}
      </Content>
    </Wrapper>
  );

};

Button.defaultProps = {
  children: null,
  isBare: false,
  isDanger: false,
  isDisabled: false,
  isFullFill: false,
  isFullWidth: false,
  isLarge: false,
  isMedium: false,
  isPrimary: false,
  isSecondary: false,
  isSmall: false,
  isSuccess: false,
  isWarning: false,
  offsetH: null,
  offsetV: null,
  onClick: () => {}
};

Button.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.func,
    PropTypes.element
  ]),
  isBare: PropTypes.bool,
  isDanger: PropTypes.bool,
  isDisabled: PropTypes.bool,
  isFullFill: PropTypes.bool,
  isFullWidth: PropTypes.bool,
  isLarge: PropTypes.bool,
  isMedium: PropTypes.bool,
  isPrimary: PropTypes.bool,
  isSecondary: PropTypes.bool,
  isSmall: PropTypes.bool,
  isSuccess: PropTypes.bool,
  isWarning: PropTypes.bool,
  offsetH: PropTypes.number,
  offsetV: PropTypes.number,
  onClick: PropTypes.func,
};

export default Button;
