import React from 'react';
import { shallow } from 'enzyme';

import renderer from 'react-test-renderer';
import 'jest-enzyme';
import 'jest-styled-components';

import colors from 'components/colors';
import Button from './';

describe('Button', () => {

  it('should render correctly', () => {
    shallow(<Button>Click me!</Button>);
    shallow(<Button>{() => 'Click me!'}</Button>);
    shallow(<Button><span>Click me!</span></Button>);
  });

  it('should change color background to primary', () => {
    const wrapper = renderer.create(<Button isPrimary>Click me!</Button>).toJSON();
    expect(wrapper).toHaveStyleRule('background-color', colors.primary);
  });

  it('should change color to disabled', () => {
    const wrapper = renderer.create(<Button disabled>Click me!</Button>).toJSON();
    expect(wrapper).toHaveStyleRule('background-color', colors.disabled);
  });

  it('should disabled onClick callback', () => {
    const wrapper = shallow(<Button disabled onClick={() => {}}>Click me!</Button>);
    expect(wrapper.prop('onClick')).toEqual(null);
  });

});
