import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import colors from 'components/colors';

const Wrapper = styled.div`position: relative;`;

export const Badge = styled.span`
  align-items: center;
  background-color: ${({ isDark }) => isDark ? colors.darkGray1 : colors.success};
  border-radius: 100%;
  bottom: 0;
  color: white;
  display: flex;
  font-size: 10px;
  justify-content: center;
  line-height: 1;
  min-height: 10px;
  min-width: 10px;
  padding: 1px 2px 2px;
  position: absolute;
  right: -2px;
  text-align: center;
  vertical-align: middle;
`;

Badge.displayName = 'Badge';

const Badged = ({ isDark, children, hasBadge, badgeValue }) => (
  <Wrapper>
    {children}
    {hasBadge && <Badge isDark={isDark}>{badgeValue}</Badge>}
  </Wrapper>
);

Badged.defaultProps = {
  badgeValue: null,
  hasBadge: false,
  isDark: false
};

Badged.propTypes = {
  badgeValue: PropTypes.number,
  children: PropTypes.element.isRequired,
  hasBadge: PropTypes.bool,
  isDark: PropTypes.bool
};

export default Badged;
