import React from 'react';
import { shallow } from 'enzyme';

import renderer from 'react-test-renderer';
import 'jest-enzyme';
import 'jest-styled-components';

import colors from 'components/colors';
import Badged, { Badge } from './';

describe('Badged', () => {

  it('should render correctly', () => {
    shallow(<Badged><span /></Badged>);
  });

  it('should render with Badge inside', () => {
    const wrapper = shallow(<Badged hasBadge><span /></Badged>);
    expect(wrapper.find('Badge')).toHaveLength(1);
  });

  it('should render Badge component with dark background color', () => {
    const wrapper = renderer.create(<Badge isDark />).toJSON();
    expect(wrapper).toHaveStyleRule('background-color', colors.darkGray1);
  });

});
