import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import { Icon } from 'shared-components';

import { Container, Tag, RemoveButton, Input, Ruler } from './components';

const TagsInput = (props) => {

  const [inputValue, setInputValue] = useState('');
  const [items, setItems] = useState([]);
  const [rulerWidth, setRulerWidth] = useState(2);
  const [focus, setFocus] = useState(false);

  const container = useRef();
  const ruler = useRef();
  const input = useRef();

  useEffect(() => {
    setItems(props.defaultValue);
  }, [props.defaultValue]);

  const updateItems = (updatedItems, lastValue) => {
    setItems(updatedItems);
    props.onChange(updatedItems, lastValue);
  };

  const handleInputChange = (event) => {
    setInputValue(event.target.value);
    ruler.current.innerText = event.target.value.replace(/\s/g, '-');
    setRulerWidth(ruler.current.offsetWidth + 1);
  };

  const handleInputFocus = () => {
    setFocus(true);
  };

  const handleInputBlur = () => {
    setFocus(false);
  };

  const handleInputKeyDown = (event) => {
    const { value } = event.target;
    if (event.keyCode === 13 && value.length > 0) {
      setInputValue('');
      setRulerWidth(2);
      updateItems([...items, value], value);
    }
    if (items.length && event.keyCode === 8 && !inputValue.length) {
      updateItems(items.slice(0, items.length - 1));
    }
  };

  const handleRemoveItem = (index) => {
    return () => {
      updateItems(items.filter((item, i) => i !== index));
    }
  };

  return (
    <Container
      id={props.name}
      ref={container}
      onClick={() => { input.current.focus(); }}
      focus={focus}>
      {items.map((item, i) =>
        <Tag key={i}>
          {item}
          <RemoveButton onClick={handleRemoveItem(i)}><Icon name="fas fa-sm fa-times" /></RemoveButton>
        </Tag>
      )}
      <Input
        ref={input}
        value={inputValue}
        htmlFor={props.name}
        style={{ width: rulerWidth }}
        onChange={handleInputChange}
        onFocus={handleInputFocus}
        onBlur={handleInputBlur}
        onKeyDown={handleInputKeyDown}
      />
      <Ruler ref={ruler} />
    </Container>
  );

};

TagsInput.defaultProps = {
  name: 'tags-input'
};

export default TagsInput;
