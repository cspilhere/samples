import React from 'react';
import styled from 'styled-components';

import colors, { convertHexToRGB } from 'components/colors';
import vars from 'components/settings';

export const Container = styled.label`
  border-radius: 2px;
  min-height: 32px;
  border: 1px solid #ccc;
  padding: 0;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-start;
  padding: 0 1px;
  ${({ focus }) => focus && (`
    outline: none;
    border: 1px solid ${convertHexToRGB(colors.primary, .8)};
    box-shadow: 0 0 3px 2px ${convertHexToRGB(colors.secondary, .6)};
  `)};
  ${vars.hover}
`;

export const Tag = styled.span`
  display: inline-block;
  padding: 2px;
  background-color: ${colors.gray3};
  color: ${colors.white};
  border-radius: 2px;
  margin-right: 2px;
  cursor: pointer;
  font-size: 14px;
  height: 28px;
  margin-top: 1px;
  padding: 3px 8px;
`;

export const RemoveButton = styled.span`
  margin-left: 2px;
  margin-right: -3px;
`;

export const Input = styled.input`
  height: 28px;
  font-size: 14px;
  padding: 0;
  appearance: none;
  outline: none;
  border: none;
`;

export const Ruler = styled.span`
  height: 30px;
  font-size: 14px;
  visibility: hidden;
  position: fixed;
  top: -99999;
  left: -99999;
  white-space: nowrap;
`;
