import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

import colors from 'components/colors';
import settings, { truncate, truncateReverse } from 'components/settings';

const Text = styled.span`${({
  color,
  block,
  light,
  bold,
  truncated,
  truncatedReverse,
  size,
  small,
  large,
  regular
}) => css`
  color: ${light ? colors.lightGray : color};
  line-height: ${settings.baseLineHeight};
  font-weight: ${bold ? 'bold' : 'initial'};
  display: ${block ? 'block' : 'inline-block'};
  font-size: ${
    small ? settings.smallFontSize : false ||
    large ? settings.largeFontSize : false ||
    regular ? settings.regularFontSize : false ||
    typeof size === 'string' ? settings[`${size}FontSize`] : size
  }px;
  ${truncated && truncate}
  ${truncatedReverse && truncateReverse}
  > ${Text} { display: inline-block; }
`}`;

Text.defaultProps = {
  color: colors.primaryDark,
  size: settings.baseFontSize
};

Text.propTypes = {};

export default Text;
