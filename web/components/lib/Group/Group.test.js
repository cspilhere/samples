import React from 'react';
import { shallow } from 'enzyme';

import Group from './';

describe('Group', () => {

  it('should render correctly', () => {
    shallow(<Group>{/* Empty */}</Group>);
    shallow(<Group><span /><span /><span /></Group>);
  });

});
