import React, { Fragment, Children } from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

import { Spacer } from 'components/lib/Spacer';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  ${({ isStacked }) => isStacked && css`
    flex-direction: column;
  `}
`;

const Group = ({
  children,
  isStacked,
  size,
  ...rest
}) => (
  <Wrapper {...rest}>
    {Children.toArray(children).map((element, index) => (
      // eslint-disable-next-line react/no-array-index-key
      <Fragment key={index}>
        {index !== 0 && <Spacer isVertical={isStacked} size={size} />}
        {element}
      </Fragment>
    ))}
  </Wrapper>
);

Group.defaultProps = {
  children: null,
  isStacked: false,
  size: null
};

Group.propTypes = {
  children: PropTypes.array,
  isStacked: PropTypes.bool,
  size: PropTypes.number
};

export default Group;
