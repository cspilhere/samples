import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from 'components/colors';
import vars from 'components/settings';

const Item = styled.div`
  font-size: 14px;
  padding: 6px 8px;
  color: ${colors.darkGray2};
  background-color: ${({ active }) => active && colors.lightGray1};
  border-radius: ${vars.borderRadius};
  margin-top: 2px;
  &:first-child {
    margin-top: 0;
  }
`;

const ListItem = (props) => {

  return <Item {...props} />;
};

ListItem.defaultProps = {};

ListItem.propTypes = {
  active: PropTypes.bool
};

export default ListItem;
