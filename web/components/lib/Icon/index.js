import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import settings from 'components/settings';

const Wrapper = styled.span`
  align-items: center;
  display: inline-flex;
  justify-content: center;
  line-height: 1;
  height: ${({ size }) => size || settings.baseFontSize}px;
  font-size: ${({ size }) => size + 'px' || 'inherit'};
  color: ${({ color }) => color || 'inherit'};
`;

const Icon = ({
  color,
  name,
  size,
  style
}) => (
  <Wrapper {...{ color, size }} style={style}>
    <i className={name} />
  </Wrapper>
);

Icon.defaultProps = {
  color: null,
  size: null,
  style: null
};

Icon.propTypes = {
  color: PropTypes.string,
  name: PropTypes.string.isRequired,
  size: PropTypes.number,
  style: PropTypes.object
};

export default Icon;
