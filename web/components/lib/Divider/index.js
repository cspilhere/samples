import React from 'react';
import PropTypes from 'prop-types';

import styled, { css } from 'styled-components';

import colors from 'components/colors';
import { Spacer } from 'components/lib/Spacer';

const Line = styled.div`
  background-color: ${({ color }) => color || colors.white};
  border-radius: 10px;
  opacity: .4;
  ${({ isVertical }) => isVertical ? css`
    height: 100%;
    width: 2px;
  ` : css`
    height: 2px;
    width: 100%;
  `}
`;

const Divider = ({
  color,
  isLarge,
  isMedium,
  isSmall,
  isVertical,
  size
}) => {
  if (isSmall) size = 10;
  if (isMedium) size = 20;
  if (isLarge) size = 30;
  if (typeof size === 'number') size = size;
  return (
    <Spacer isVertical={isVertical} size={size}>
      <Line isVertical={isVertical} color={color}>&nbsp;</Line>
    </Spacer>
  );
};

Divider.defaultProps = {
  color: null,
  isLarge: false,
  isMedium: false,
  isSmall: false,
  isVertical: false,
  size: 6
};

Divider.propTypes = {
  color: PropTypes.string,
  isLarge: PropTypes.bool,
  isMedium: PropTypes.bool,
  isSmall: PropTypes.bool,
  isVertical: PropTypes.bool,
  size: PropTypes.number
};

export default Divider;
