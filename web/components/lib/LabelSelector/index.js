import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import colors, { invertColor, convertHexToRGB } from 'components/colors';
import { inputReset } from 'components/settings';

import { Icon } from 'components';

const Wrapper = styled.span`display: inline-flex;`;

const Container = styled.span`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${({ color }) => color || colors.disabled};
  color: ${({ color }) => invertColor(color || colors.disabled)};
  border-radius: 4px;
  &:after {
    content: "";
    border: 3px solid ${({ color }) => convertHexToRGB(invertColor(color || colors.disabled), .6)};
    border-radius: 2px;
    border-right: 0;
    border-top: 0;
    display: block;
    height: 0.625em;
    margin-top: -0.4375em;
    pointer-events: none;
    position: absolute;
    top: 50%;
    right: 10px;
    transform: rotate(-45deg);
    transform-origin: center;
    width: 0.625em;
  }
`;

const Select = styled.select`
  ${inputReset}
  position: relative;
  padding: 6px 10px;
  padding-right: 26px;
  color: ${({ color }) => invertColor(color || colors.disabled)};
`;

const LabelSelector = ({ onChange, options, value, placeholder, color, id, name }) => {
  return (
    <Wrapper>
      <Container {...{ color }}>
        <Icon name="fas fa-hashtag" style={{ marginLeft: 8, marginRight: -5 }} />
        <Select
          {...{
            name,
            value,
            onChange,
            color,
            id
          }}>
          <option value="">{placeholder}</option>
          {options.map((option, index) => {
            return <option value={option} key={index}>{option}</option>
          })}
        </Select>
      </Container>
    </Wrapper>
  );
};

LabelSelector.defaultProps = {
  id: 'LabelSelector',
  options: []
};

LabelSelector.propTypes = {
  options: PropTypes.array.isRequired
};

export default LabelSelector;
