import { keyframes } from '@emotion/core'

const fadeIn = keyframes`
  0% { opacity: 0; }
  90% { opacity: 1; }
`;

const fadeOut = keyframes`
  0% { opacity: 1; }
  90% { opacity: 0; }
`;

export default {
  container: (easeTime = .2, backgroundColor = 'rgba(255, 255, 255, 1)') => ({
    position: 'absolute',
    zIndex: 999999,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    opacity: 1,
    transition: `opacity ${easeTime}s ease-in-out`,
    backgroundColor: backgroundColor,
  }),

  isHidden: (easeTime = .2) => ({
    width: 0,
    height: 0,
    top: '-10000px',
    left: '-10000px',
    transition: `all 0s ease-in-out ${easeTime}s`,
  }),

  entering: { opacity: 1 },
  entered:  { opacity: 1 },
  exiting:  { opacity: 0 },
  exited:  { opacity: 0 }

};
