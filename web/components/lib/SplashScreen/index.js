/** @jsx jsx */
import React from 'react';
import ReactDOM from 'react-dom';

import { Transition } from 'react-transition-group';

import { jsx } from '@emotion/core';
import styles from './styles';

const SplashScreen = ({ isOpen, easeTime = .4, children, backgroundColor }) => {
  if (typeof isOpen !== 'boolean') return null;
  return (
    <Transition
      in={isOpen}
      appear={true}
      mountOnEnter={true}
      timeout={easeTime}>
      {state => (
        <div
          css={{
            ...styles.container(easeTime, backgroundColor),
            ...styles[state],
            ...(state === 'exited' ? styles.isHidden(easeTime) : {})
          }}>
          {children || 'Waiting...'}
        </div>
      )}
    </Transition>
  );
};

const body = document.body;

class Portal extends React.Component {
  constructor(props) {
    super(props);
    this.element = document.createElement('div');
  }

  componentDidMount() {
    body.appendChild(this.element);
  }

  componentWillUnmount() {
    body.removeChild(this.element);
  }

  render() {
    return ReactDOM.createPortal(
      this.props.children,
      this.element
    );
  }
}

const SplashScreenComponent = (props) => <Portal><SplashScreen {...props} /></Portal>;

export default SplashScreenComponent;
