import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import moment from 'moment';

import { Text, Icon, Space, RowWrapper } from 'components';

import colors from 'components/colors';

const Wrapper = styled.div`
  cursor: pointer;
`;

const Description = styled.div`
  margin-top: 2px;
`;

const TimelineContainer = styled.div`
  width: 100%;
  height: 6px;
  background-color: ${colors.secondary};
  border-radius: 3px;
`;

const TimelineBar = styled.div`
  width: ${({ value }) => value || 0}%;
  height: 6px;
  background-color: ${colors.primary};
  border-radius: 3px;
`;

const TrialTimeline = ({ accountCreatedAt, onClick }) => {

  const future = moment(accountCreatedAt).add(30, 'days').endOf('day');
  const present = moment().startOf('day');
  const diff = moment(future).diff(present, 'days');

  const expirationProgress = (diff * 100) / 30;

  return (
    <Wrapper onClick={onClick}>
      <RowWrapper>
        <Text color={colors.primary}>
          Plano atual: <Text bold color={colors.warningDark}>Lançamento</Text>
        </Text>
        <Icon name="fas fa-rocket" size={13} color={colors.primary} />
      </RowWrapper>
      <Space size={6} />
      <TimelineContainer>
        <TimelineBar value={expirationProgress} />
      </TimelineContainer>
      <Description>
        <Text small color={colors.gray3}><em>Seu período de testes acaba em {diff} dias</em></Text>
      </Description>
    </Wrapper>
  );
};

TrialTimeline.propTypes = {

};

export default TrialTimeline;
