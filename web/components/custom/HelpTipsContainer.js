import React from 'react';

import Tips from 'components/modules/Tips';

import { Title, Icon, Button, Group } from 'components';

const HelpTipsContainer = ({ isVisible, onHide, name, items, tourId }) => {

  let isOpenByDefault = JSON.parse(localStorage.getItem(name));

  if (isOpenByDefault == null) isOpenByDefault = true;

  const close = () => {
    localStorage.setItem(name, false);
    isOpenByDefault = false;
    if (onHide) onHide();
  };

  console.log(isOpenByDefault, isVisible);

  return (
    <Tips
      isVisible={isOpenByDefault || isVisible}
      onHide={close}
      cards={[{
        title: <Title light>Dicas e recursos <Icon name="fas fa-life-ring" color="#1C99FE" /></Title>,
        items: items
      }, {
        title: 'Quer saber mais?',
        items: [
          <Group>
            <Button
              secondary
              small
              fullFill
              onClick={() => {
                close();
                if (tourId && Intercom) Intercom('startTour', tourId);
              }}>
              Fazer um tour
            </Button>
            <Button
              secondary
              small
              fullFill
              onClick={() => Intercom && Intercom('show')}>
              Fale com a gente
            </Button>
          </Group>
        ]
      }]}
    />
  );
};

export default HelpTipsContainer;
