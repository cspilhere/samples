import React from 'react';
import PropTypes from 'prop-types';

import {
  Icon
} from 'shared-components';

const TablePlaceholder = (props) => {

  return (
    <div className="table-placeholder">
      <Icon name="far fa-inbox" />
    </div>
  );

};

TablePlaceholder.propTypes = {};

export default TablePlaceholder;
