import React, { useState } from 'react';

import sizeMe from 'react-sizeme'

const { Table, Column } = require('fixed-data-table-2');

import 'fixed-data-table-2/dist/fixed-data-table.css';
import 'fixed-data-table-2/dist/fixed-data-table-base.css';
import 'fixed-data-table-2/dist/fixed-data-table-style.css';

import TablePlaceholder from './TablePlaceholder';

import { Icon, Button } from 'shared-components';

import { deepKey } from 'shared-utils/utils';

export const CellWrapper = ({ data, rowIndex, columnKey, inline, center, render }) => {
  let content = null;
  if (data && data[rowIndex] && columnKey) content = deepKey(data[rowIndex], columnKey);
  return (
    <div
      style={{
        padding: 18,
        height: 80,
        display: 'flex',
        flexDirection: !inline ? 'column' : 'row',
        justifyContent: 'center',
        alignItems: center ? 'center' : null
      }}>
      {render({ content, data: data && data[rowIndex] || null })}
    </div>
  );
};

CellWrapper.defaultProps = { render: () => {} };

export const Options = (props) => {
  return (
    <CellWrapper
      render={() => (
        <div className="buttons is-flex-aligned-center">
          {props.onClickToRemove && (
            <Button
              isSmall
              disabled={props.disabled}
              onClickCapture={(event) => {
                event.stopPropagation();
                props.onClickToRemove(props.rowIndex);
              }}>
              <Icon name="fas fa-minus-circle" />
            </Button>
          )}
        </div>
      )}
    />
  );
};

const DataTable = (props) => {

  const [columnWidths, setWidth] = useState({});

  const { onRowClick, data, columns, loading, align } = props;

  const { width, height } = props.size;

  const calculatedHeight = height > ((data.length) * 80) + 58 ? (
    ((data.length) * 80) + 58
  ) : (
    height
  );

  const rowsData = data || [];

  return (
    <div className="table-container">
      {width ? (
        <>

          {rowsData.length < 1 && <TablePlaceholder loading={loading || data === null } />}

          <Table
            onRowClick={onRowClick}
            rowHeight={80}
            rowsCount={rowsData.length}
            onColumnResizeEndCallback={(newColumnWidth, columnKey) => {
              setWidth({ ...columnWidths, [columnKey]: newColumnWidth });
            }}
            isColumnResizing={false}
            headerHeight={40}
            pureRendering={false}
            allowCellsRecycling={true}
            width={width}
            height={calculatedHeight}>

            {columns.map((item, index) => {
              const { width, alignHeader, isResizable, defaultWidth, header, ...rest } = item;
              if (isResizable && !columnWidths[rest.columnKey]) {
                setWidth({
                  ...columnWidths,
                  [rest.columnKey]: defaultWidth
                });
              }
              return (
                <Column
                  key={rest.columnKey || index}
                  header={() => <span style={{ textAlign: alignHeader }}>{header}</span>}
                  width={width || columnWidths[rest.columnKey] || defaultWidth}
                  isResizable={isResizable}
                  {...rest}
                />
              );
            })}

          </Table>
        </>
      ) : null}
    </div>
  );
};

export default sizeMe({ monitorHeight: true, noPlaceholder: false })(DataTable);
