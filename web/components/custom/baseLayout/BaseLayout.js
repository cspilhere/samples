import React from 'react';

import moment from 'moment';

import localforage from 'localforage';

import { callToast } from 'utils/utils';

import Scrollbar from 'react-scrollbars-custom';

// import SimpleBar from 'simplebar-react';
// import 'simplebar/dist/simplebar.min.css';

import { Layout } from 'shared-components';
import { alert } from 'shared-components/lib/Alert';
import withModal from 'shared-components/HOCs/withModal';

import { Spacer } from 'components';

import str from 'services/strings';

let sideFilterToggleState = true;

localforage.getItem('sideFilterToggleState', (err, value) => {
  if (err) console.log('localforage error: ', err);
  if (!value) localforage.setItem('sideFilterToggleState', true);
  sideFilterToggleState = value;
});

const datetime = (value) => {
  if (value.toDate) value = value.toDate();
  return moment(value).format('DD/MM/YYYY HH:mm');
};

const date = (value) => {
  if (value.toDate) value = value.toDate();
  return moment(value).format('DD/MM/YYYY');
};

const BaseLayout = (props) => (
  <Layout>

    <Layout.Header>
      {props.baseHeader}
    </Layout.Header>

    <Layout.Body>

      {props.contentAside && (
        <Layout.Aside
          // backgroundColor="rgb(250, 250, 250)"
          hasToggle
          onToggleEnd={(toggleState) => {
            sideFilterToggleState = toggleState;
            localforage.setItem('sideFilterToggleState', toggleState);
          }}
          isOpenByDefault={sideFilterToggleState}>
          <div
            style={{
              width: 270,
              height: '100%',
              backgroundColor: 'rgb(250, 250, 250)',
              boxShadow: `0 0 4px rgba(16, 22, 26, .1)`
            }}>

            {/* <SimpleBar style={{ height: '100%' }}>
              <div style={{ padding: '22px 20px 30px', }}>
                {props.contentAside}
              </div>
            </SimpleBar> */}

            <Scrollbar
              trackYProps={{ style: { width: 8, top: 0, height: '100%' } }}
              thumbYProps={{ style: { backgroundColor: '#bfccd6' } }}
              style={{ width: '100%' }}>
              <div style={{ padding: '22px 20px 30px', }}>
                {props.contentAside}
              </div>
            </Scrollbar>

          </div>
        </Layout.Aside>
      )}

      <Layout.Content grow padding={20}>

        <Spacer horizontal medium />

        {props.contentHeader}

        <Spacer horizontal large />

        {props.render ? props.render({
          str,
          alert,
          callToast,
          format: {
            date,
            datetime
          },
          openModal: props.openModal,
          closeModal: props.closeModal
        }) : props.children}

      </Layout.Content>
    </Layout.Body>
  </Layout>
);

export function baseLayoutWrapper(Component) {

  const BaseLayout = (props) => {

    const passedProps = {
      ...props,
      str,
      alert,
      callToast,
      format: {
        date,
        datetime
      }
    };

    return <Component {...passedProps} />;

  };

  BaseLayout.displayName = `BaseLayout(${getDisplayName(Component)})`;

  return withModal(BaseLayout);

};

function getDisplayName(Component) {
  return Component.displayName || Component.name || 'Component';
};

export default BaseLayout;
