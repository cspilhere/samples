import React from 'react';
import { withRouter } from "react-router";

import {
  FlexBar,
  Breadcrumb
} from 'shared-components';

import {
  Text,
  Button
} from 'components';

const BaseHeader = ({ breadcrumb, history, onClickHelp }) => {
  console.log(history);
  return (
    <FlexBar className="module-header-container">
      <FlexBar.Child grow>
        <Breadcrumb
          path={[{
            name: 'Início', title: 'Home', pathname: '/'
          }, ...breadcrumb]}
          onClick={history.push}
        />
      </FlexBar.Child>
      <FlexBar.Child>

        {onClickHelp && <Button bare onClick={onClickHelp}><Text light small>Ajuda?</Text></Button>}

      </FlexBar.Child>
    </FlexBar>
  );
};

export default withRouter(BaseHeader);
