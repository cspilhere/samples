import React, { useState, useEffect } from 'react';

import { toast } from 'react-toastify';

import firebase from 'firebase';

import withModal from 'shared-components/HOCs/withModal';

import { Button, Grid, Icon } from 'shared-components';

import CreateShipper from 'features/CreateShipper';

// import CreatableSelect from 'react-select/lib/Creatable';
// import AsyncSelect from 'react-select/lib/Async';
import Select from 'react-select';

import Constants from 'services/Constants';

const db = firebase.firestore();

function getShippers(companyId, inputValue) {
  return new Promise((resolve, reject) => {
    db.collection('shippers')
    .where('companyId', '==', companyId)
    .onSnapshot((querySnapshot) => {
      const response = [...querySnapshot.docs.map((doc) => {
        delete doc.data().companyId;
        return ({...doc.data(), id: doc.id });
      })];
      resolve(response);
    }, (error) => {
      console.log(error);
      reject();
    });
  });
};

let cachedList = null;

const FindShipper = (props) => {

  const [alreadyUsedDefaultValue, setAlreadyUsedDefaultValue] = useState(false);

  const [shippers, updateList] = useState([]);
  const [selectedClient, selectShipper] = useState(null);
  const [isLoading, setLoading] = useState(false);

  const { onChange, disabled, defaultBody, onInputChange, hideAddButton, defaultValue = {} } = props;

  useEffect(() => {

    setLoading(true);

    if (cachedList) {
      updateList(cachedList);
      setLoading(false);
      if (!alreadyUsedDefaultValue && !selectedClient && defaultValue.id) {
        selectShipper([...cachedList].filter((item) => item.id === defaultValue.id));
        setAlreadyUsedDefaultValue(true);
      }
      return;
    }

    getShippers(Constants.get('companyId'))
    .then((data) => {
      cachedList = data;
      updateList(data);
      setLoading(false);
      if (!alreadyUsedDefaultValue && !selectedClient && defaultValue.id) {
        selectShipper([...data].filter((item) => item.id === defaultValue.id));
        setAlreadyUsedDefaultValue(true);
      }
    });

  }, []);

  const openFeature = () => {
    props.openModal({
      title: 'Novo embarcador',
      disableBackgroundClick: true,
      component: (
        <CreateShipper
          onClickToCancel={props.closeModal}
          onSuccess={(response) => {
            delete response.companyId;
            updateList([...shippers, ...[{...response}]]);
            selectShipper(response);
            if (onChange) onChange(response);
            props.closeModal();
          }}
        />
      )
    });
  };

  return (
    <Grid gapConfig={1}>
      <Grid.Col>
        <Select
          cacheOptions
          defaultOptions
          menuPosition="fixed"
          className="react-select-input has-action-button"
          classNamePrefix="custom"
          menuShouldBlockScroll={true}
          isClearable
          isDisabled={isLoading || disabled}
          isLoading={isLoading}
          placeholder={props.placeholder || ''}
          onChange={(option) => {
            selectShipper(option);
            if (onChange) onChange(option);
          }}
          onInputChange={onInputChange}
          getOptionLabel={(item) => item.name}
          getOptionValue={(item) => item.id}
          value={selectedClient}
          loadingMessage={(inputValue) => 'Pesquisando...'}
          noOptionsMessage={(inputValue) => (<>Nenhum embarcador encontrado.</>)}
          // loadOptions={promiseOptions}
          options={shippers}
        />
      </Grid.Col>
      {/* {!hideAddButton && (
        <Grid.Col isAlignedBottom isNarrow>
          <Button
            isLink
            isOutlined
            avoidTab
            style={{ marginRight: 3, marginLeft: -11, borderBottomLeftRadius: 0, borderTopLeftRadius: 0 }}
            onClick={openFeature}>
            <Icon name="far fa-plus" />
          </Button>
        </Grid.Col>
      )} */}
    </Grid>
  );

};

export default withModal(FindShipper);
