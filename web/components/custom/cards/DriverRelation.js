import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import { Icon, Text } from 'shared-components';

import colors from 'components/colors';

import { showDriverRelationLabel } from 'services/transformKeysToString';

const DriverRelation = ({ content }) => {
  return (
    <div style={{ display: 'flex', height: 18 }}>
      <Icon name="far fa-sm fa-id-card-alt" color={colors.darkGray2} style={{ marginTop: 1 }} />&nbsp;
      <Text isSmall>{showDriverRelationLabel(content)}</Text>
    </div>
  );
};

DriverRelation.propTypes = {

};

export default DriverRelation;
