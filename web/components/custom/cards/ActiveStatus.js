import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import { Icon } from 'shared-components';

import colors from 'components/colors';

const ActiveStatus = ({ content }) => {

  const icon = content ? 'circle' : 'pause-circle';
  const color = content ? colors.success : colors.gray3;

  return (
    <Icon name={`fas fa-sm fa-${icon}`} color={color} />
  );
};

ActiveStatus.propTypes = {

};

export default ActiveStatus;
