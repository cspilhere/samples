import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import { Icon, Text } from 'shared-components';

import colors from 'components/colors';

const DeliveriesCounter = ({ content, align }) => {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: align || 'center',
        alignItems: align || 'center',
      }}>
      <Text style={{ marginTop: -2 }}>{content || 0}</Text>&nbsp;
      <Icon name="fas fa-sm fa-shipping-fast" color={colors.primary} />
    </div>
  );
};

DeliveriesCounter.propTypes = {};

export default DeliveriesCounter;
