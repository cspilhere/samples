import React from 'react';
import PropTypes from 'prop-types';

import {
  Icon,
  Text
} from 'shared-components';

import { maskSchema } from 'services/masksAndValidators';

const DriverSmallCard = ({ content }) => {

  if (!content || !content.name) return (
    <div className="driver-card">
      <div className="driver-card-title">
        <div className="driver-card-avatar">
          <Icon name="fas fa-lg fa-steering-wheel" hasGray3Color />
        </div>
        <div style={{ marginTop: 0, flex: 1, maxWidth: '100%' }}>
          <Text isSmall isTruncated isMockText>
            <strong className="has-gray-3-color">Senhor do Mar</strong>
            <span className="has-gray-5-color" style={{ display: 'block', marginTop: -4 }}>48 9999 9999</span>
          </Text>
        </div>
      </div>
    </div>
  );

  return (
    <div className="driver-card">
      <div className="driver-card-title">
        <div className="driver-card-avatar">
          <Icon name="fas fa-lg fa-steering-wheel" />
        </div>
        <div style={{ marginTop: -1, flex: 1, maxWidth: '100%', overflow: 'hidden' }}>
          <Text isSmall isTruncated>
            <strong>{content.name}</strong>
            <span className="has-gray-1-color" style={{ display: 'block', marginTop: -4 }}>
              {maskSchema.phone.resolve(content.phone)}
            </span>
          </Text>
        </div>
      </div>
    </div>
  );

};

DriverSmallCard.propTypes = {};

export default DriverSmallCard;
