import React from 'react';
import PropTypes from 'prop-types';

import { Text } from 'shared-components';

import { maskSchema } from 'services/masksAndValidators';

function typeLabel(value) {
  switch (value) {
    case 'cpf':
      return 'CPF';
    case 'cnpj':
      return 'CNPJ';
  }
  return '#';
};

const EntityDocument = ({ content, type, data }) => {
  if (!content) return null;
  if (!type && data) type = data.documentType;
  return maskSchema[type] ? <Text>{typeLabel(type)}: {maskSchema[type].resolve(content)}</Text> : null;
};

EntityDocument.propTypes = {};

export default EntityDocument;
