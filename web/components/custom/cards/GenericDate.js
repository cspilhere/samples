import React from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';

import { Text } from 'components';

const GenericDate = ({ content, datetime }) => {
  const hasDate = content && content.toDate;
  return (
    <Text small bold>
      {hasDate ? (
        <>{moment(content.toDate()).format(`DD/MM/YYYY${datetime ? ' HH:mm' : ''}`)}</>
      ) : (
        <span className="is-mock-text has-light-gray-2-color">{`DD/MM/YYYY${datetime ? ' HH:mm' : ''}`}</span>
      )}
    </Text>
  )
};

GenericDate.propTypes = {};

export default GenericDate;
