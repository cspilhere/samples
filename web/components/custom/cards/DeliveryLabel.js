import React from 'react';
import PropTypes from 'prop-types';

import { Tag } from 'components';

const DeliveryLabel = ({ content }) => {
  return (
    <Tag color="#F2F2F2" small style={{ marginRight: 10 }}>
      # {content || 'Sem etiqueta'}
    </Tag>
  );
};

DeliveryLabel.propTypes = {};

export default DeliveryLabel;
