import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import { Icon, Text } from 'shared-components';

import colors from 'components/colors';

const UsersCounter = ({ content }) => {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text style={{ marginTop: -2 }}>{content && content.length || 0}</Text>&nbsp;
      <Icon name="fas fa-sm fa-users" color={colors.primary} />
    </div>
  );
};

UsersCounter.propTypes = {};

export default UsersCounter;
