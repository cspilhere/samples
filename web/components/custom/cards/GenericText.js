import React from 'react';
import PropTypes from 'prop-types';

import { Text } from 'shared-components';

const GenericText = ({ content, bold, parseWith }) => {
  return <Text style={{ fontWeight: bold ? 'bold' : null }}>{parseWith(content)}</Text>;
};

GenericText.defaultProps = {
  parseWith: (value) => value
};

export default GenericText;
