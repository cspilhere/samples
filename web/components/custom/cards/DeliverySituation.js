import React from 'react';
import PropTypes from 'prop-types';

import { Box, Tag, ListItem, Title, Text, Icon, Space, RowWrapper, Fill } from 'components';

import { showStatus } from 'services/transformKeysToString';

const DeliverySituation = ({ content, large }) => {

  const values = showStatus(content);

  return (
    <Tag color={values.color} small={!large}>
      {values.label}
    </Tag>
  );

};

DeliverySituation.propTypes = {};

export default DeliverySituation;
