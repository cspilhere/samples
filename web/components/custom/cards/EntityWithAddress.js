import React from 'react';
import PropTypes from 'prop-types';

import { Text, Icon } from 'shared-components';

import { maskSchema } from 'services/masksAndValidators';
import { formatAddress } from 'services/addressLocation';

function typeIcon(value) {
  switch (value) {
    case 'client':
      return 'box-check';
    case 'shipper':
      return 'truck-loading';
  }
  return '#';
};

const EntityWithAddress = ({ content, type, data, showDocument, contentAsObject, disabledTruncate }) => {
  if (contentAsObject && data) {
    data = data[type];
    content = data.name;
  }
  if (!data || !content) return null;
  return (
    <>
      <Text isSmall isTruncated={!disabledTruncate}>
        <Icon name={`fas fa-${typeIcon(type)}`} /> {content}
        {showDocument && data.documentType && ` (${maskSchema[data.documentType].resolve(data.document)})`}
      </Text>
      {data.address ? (
        <Text isSmall isTruncated={!disabledTruncate} hasGray2Color>
          <Icon name="fas fa-map-marked-alt" /> {formatAddress(data.address)}
        </Text>
      ) : null}
    </>
  );
};

EntityWithAddress.propTypes = {};

export default EntityWithAddress;
