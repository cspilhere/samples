import React from 'react';
import PropTypes from 'prop-types';

import { Text } from 'shared-components';

import { maskSchema } from 'services/masksAndValidators';

const PhoneNumber = ({ content }) => {
  if (!content) return null;
  return <Text isSmall><strong>{maskSchema.phone.resolve(content)}</strong></Text>;
};

PhoneNumber.propTypes = {};

export default PhoneNumber;
