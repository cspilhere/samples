import React from 'react';
import PropTypes from 'prop-types';

import { Text, Icon } from 'shared-components';

import { formatAddress } from 'services/addressLocation';

const AddressDescription = ({ content, data }) => {
  const hasAddress = content;
  const isString = typeof content == 'string';
  return (
    <Text isSmall isTruncated hasGray1Color>
      {hasAddress ? (
        <><Icon name="fas fa-map-marked-alt" /> {isString ? content : formatAddress(content)}</>
      ) : (
        <span className="is-mock-text has-light-gray-2-color">Lorena kalipsum dolores simeticona</span>
      )}
    </Text>
  );
};

AddressDescription.propTypes = {};

export default AddressDescription;
