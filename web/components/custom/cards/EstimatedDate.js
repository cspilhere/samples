import React from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';

import { Text } from 'shared-components';

const EstimatedDate = ({ content, data }) => {

  if (!content) return (
    <div className="estimation-card" style={{ padding: 0, textAlign: 'center' }}>
      <Text isMockText hasGray3Color>
        <strong>XX dias</strong>
      </Text>
      <div className="estimation-card-description is-mock-text has-gray-5-color" style={{ marginTop: -3 }}>
        01/01/2001 00:00
      </div>
    </div>
  );

  const isFinished = data.status === 'finished';

  const current = moment().startOf('day');
  const future = moment(content.toDate()).endOf('day');
  const diff = future.diff(current, 'days');

  let description = content && moment(content.toDate()).format('DD/MM/YYYY HH:mm');
  let title = `${diff > 0 ? diff : 0} ${diff > 1 ? 'dias' : (diff === 0 ? 'dias' : 'dia')}` + ' para entregar';

  if (diff < 0) {
    title = `${-diff} dias de atraso`;
  }

  if (isFinished) title = 'Concluída';

  return (
    <div className="estimation-card" style={{ padding: 0, textAlign: 'center' }}>
      <Text hasRed2Color={diff < 1 && !isFinished}  hasGreen3Color={diff > 0 || isFinished}>
        <strong>{title}</strong>
      </Text>
      {!isFinished && (
        <div className="estimation-card-description" style={{ marginTop: -3 }}>
          {description}
        </div>
      )}
    </div>
  );

};

EstimatedDate.propTypes = {};

export default EstimatedDate;
