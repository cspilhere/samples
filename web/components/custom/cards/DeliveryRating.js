import React from 'react';
import PropTypes from 'prop-types';

const DeliveryRating = ({ content }) => {

  if (content) {
    let values = content.map(v => v.value);
    let sum = values.reduce((previous, current) => current += previous);
    content = sum / values.length;
    content = parseFloat(content).toFixed(1);
  }

  return (
    content ? (
      <div style={{ textAlign: 'center', width: '100%', fontSize: 14, fontWeight: 'bold', color: '#293742' }}>
        {content * 1}/5
      </div>
    ) : (
      <div style={{ textAlign: 'center', width: '100%', fontSize: 13, color: '#293742' }}>Sem avaliação</div>
    )
  );

};

DeliveryRating.propTypes = {};

export default DeliveryRating;
