import React, { useState, useEffect } from 'react';

import firebase from 'firebase';

import withModal from 'shared-components/HOCs/withModal';

import { Button, Grid, Icon } from 'shared-components';

import CreateDriver from 'features/CreateDriver';

import { callToast } from 'utils/utils';

import Constants from 'services/Constants';

import { toArrayWithIds } from 'utils/utils';

import Select from 'react-select';

const db = firebase.firestore();

function getCollection() {
  return new Promise((resolve, reject) => {
    db.collection('drivers')
    .where('companyId', '==', Constants.get('companyId'))
    .orderBy('createdAt', 'desc')
    .onSnapshot((querySnapshot) => {
      resolve(toArrayWithIds(querySnapshot));
    }, reject);
  });
};

const FindDriver = (props) => {

  const [alreadyUsedDefaultValue, setAlreadyUsedDefaultValue] = useState(false);

  const [options, updateOptions] = useState([]);
  const [selectedOption, selectOption] = useState();
  const [isLoading, setLoading] = useState(false);

  const { onChange, disabled, onInputChange, hideAddButton, defaultValue = {} } = props;

  useEffect(() => {
    setLoading(true);
    getCollection()
    .then((data) => {
      updateOptions(data);
      setLoading(false);
      if (!alreadyUsedDefaultValue && !selectedOption && defaultValue && defaultValue.id) {
        selectOption([...data].filter((item) => item.id === defaultValue.id));
        setAlreadyUsedDefaultValue(true);
      }
    });
  }, []);

  const openFeature = () => {
    props.openModal({
      title: 'Novo motorista',
      disableBackgroundClick: true,
      component: (
        <CreateDriver
          onClickToCancel={props.closeModal}
          onSuccess={(response) => {
            delete response.companyId;
            updateOptions([...options, ...[{...response}]]);
            selectOption(response);
            if (onChange) onChange(response);
            props.closeModal();
          }}
        />
      )
    });
  };

  return (
    <Grid gapConfig={1}>
      <Grid.Col>
        <Select
          cacheOptions
          defaultOptions
          menuPosition="fixed"
          className="react-select-input has-action-button"
          classNamePrefix="custom"
          menuShouldBlockScroll={true}
          isClearable
          isDisabled={isLoading || disabled}
          isLoading={isLoading}
          placeholder={props.placeholder || ''}
          onChange={(option) => {
            selectOption(option);
            if (onChange) onChange(option);
          }}
          onInputChange={onInputChange}
          getOptionLabel={(item) => item.name}
          getOptionValue={(item) => item.id}
          value={selectedOption}
          loadingMessage={(inputValue) => 'Pesquisando...'}
          noOptionsMessage={(inputValue) => (<>Nenhum motorista encontrado.</>)}
          options={options}
        />
      </Grid.Col>
      {/* {!hideAddButton && (
        <Grid.Col isAlignedBottom isNarrow>
          <Button
            isLink
            isOutlined
            avoidTab
            style={{ marginRight: 3, marginLeft: -11, borderBottomLeftRadius: 0, borderTopLeftRadius: 0 }}
            onClick={openFeature}>
            <Icon name="far fa-plus" />
          </Button>
        </Grid.Col>
      )} */}
    </Grid>
  );

};

export default withModal(FindDriver);
