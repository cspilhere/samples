import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';

import { Manager, Reference, Popper } from 'react-popper';

import ListItem from 'components/lib/ListItem';
import Box from 'components/lib/Box';
import Input from 'components/lib/Input';

import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'components/custom/PlacesAutocomplete';

const FindCoordinates = (props) => {

  const [address, setAddress] = useState('');
  const [isActive, setIsActive] = useState(false);

  const wrapper = useRef();

  useEffect(() => {
    if (props.defaultValue) setAddress(props.defaultValue);
  }, [props.defaultValue]);

  const wrapperWidth = wrapper.current && wrapper.current.offsetWidth ? wrapper.current.offsetWidth : 0;

  const handleChange = (address) => {
    setIsActive(true);
    setAddress(address);
  };

  const handleSelect = (address) => {
    if (!address) return;
    setIsActive(false);
    setAddress(address);
    geocodeByAddress(address)
    .then((results) => getLatLng(results[0]))
    .then((coordinates) => {
      props.onChange({
        address,
        coordinates
      });
    })
    .catch((error) => {
      console.error('Error', error);
    });
  };

  const handleInputBlur = () => {
    setIsActive(false);
  };

  return (
    <div ref={wrapper} style={{ width: '100%' }}>
      <PlacesAutocomplete
        value={address}
        onChange={handleChange}
        onSelect={handleSelect}>
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
          <Manager>
            <Reference>
              {({ ref }) => (
                <div ref={ref}>
                  <Input
                    {...getInputProps({})}
                    onBlur={handleInputBlur}
                    loading={loading}
                  />
                </div>
              )}
            </Reference>
            <Popper
              placement="bottom-start"
              positionFixed={true}
              modifiers={{ preventOverflow: { enabled: true, escapeWithReference: true, boundariesElement: 'viewport' } }}>
              {({ ref, style, placement, arrowProps }) => (isActive && suggestions.length > 0) && (
                <div ref={ref} style={{ ...style, zIndex: 110, marginTop: 2 }} data-placement={placement}>
                  <Box
                    elevated
                    style={{ width: wrapperWidth, overflow: 'hidden' }}>
                    {suggestions.map((suggestion) => {
                      return (
                        <ListItem {...getSuggestionItemProps(suggestion, { active: suggestion.active })}>
                          <span>{suggestion.description}</span>
                        </ListItem>
                      );
                    })}
                  </Box>
                  <div ref={arrowProps.ref} style={arrowProps.style} />
                </div>
              )}
            </Popper>
          </Manager>
        )}
      </PlacesAutocomplete>
    </div>
  );
};

FindCoordinates.defaultProps = {
  onChange: () => {}
};

FindCoordinates.propTypes = {
  onChange: PropTypes.func
};

export default FindCoordinates;
