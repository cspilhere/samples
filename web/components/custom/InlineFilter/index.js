import React, { useEffect, useState } from 'react';

import localforage from 'localforage';

import {
  Icon,
  Button,
  FormField,
  FlexBar
} from 'shared-components';

import {
  Input
} from 'components';

const InlineFilter = ({ name, placeholder, onChange, render }) => {

  const [values, setValues] = useState({});

  useEffect(() => {
    localforage.getItem(name, (err, value) => {
      if (!value) {
        onChange({}, name);
        localforage.setItem(name, {});
      }
      if (value) {
        setValues(value);
        onChange(value, name);
        localforage.setItem(name, value);
      }
    });
    return () => {};
  }, []);

  return (
    <FlexBar hasSmallGap>
      <FlexBar.Child>
        <div
          style={{
            width: '100%',
            minWidth: 280,
            display: 'flex'
          }}>
          <FormField hasIconsRight>
            <Input
              small
              name="query"
              placeholder={placeholder}
              value={values.query}
              onChange={(event) => {
                const fieldName = event.target.name;
                const fieldValue =  event.target.value;
                const updatedValues =  { ...values, [fieldName]: fieldValue };
                setValues(updatedValues);
                localforage.getItem(name, (err, value) => {
                  if (err) console.log('localforage error: ', err);
                  onChange(updatedValues, name);
                  localforage.setItem(name, updatedValues);
                });
              }}
            />
            <Icon name="fas fa-search" isRight style={{ marginTop: -1 }} />
          </FormField>
        </div>
      </FlexBar.Child>

      {render && (
        <FlexBar.Child>
          {render({
            values,
            handleChange: (event) => {
              const fieldName = event.target.name;
              const fieldValue =  event.target.value;
              const updatedValues =  { ...values, [fieldName]: fieldValue };
              setValues(updatedValues);
              localforage.getItem(name, (err, value) => {
                if (err) console.log('localforage error: ', err);
                onChange(updatedValues, name);
                localforage.setItem(name, updatedValues);
              });
            }
          })}
        </FlexBar.Child>
      )}

      {Object.keys(values).length > 0 && (
        <>
          <FlexBar.Child />
          <FlexBar.Child>
            <Button
              isBare
              onClick={() => {
                setValues({});
                onChange({}, name);
                localforage.setItem(name, null);
              }}>
              <Icon name="fas fa-undo" />
            </Button>
          </FlexBar.Child>
        </>
      )}

    </FlexBar>
  );
};

InlineFilter.defaultProps = {
  render: null
};

export default InlineFilter;
