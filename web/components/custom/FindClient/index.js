import React, { useState, useEffect } from 'react';

import { toast } from 'react-toastify';

import firebase from 'firebase';

import withModal from 'shared-components/HOCs/withModal';

import { Button, Grid, Icon } from 'shared-components';

import CreateClient from 'features/CreateClient';

// import CreatableSelect from 'react-select/lib/Creatable';
// import AsyncSelect from 'react-select/lib/Async';
import Select from 'react-select';

import Constants from 'services/Constants';

const db = firebase.firestore();

function getClients(companyId, inputValue) {
  return new Promise((resolve, reject) => {
    db.collection('clients')
    .where('companyId', '==', companyId)
    .onSnapshot((querySnapshot) => {
      const response = [...querySnapshot.docs.map((doc) => {
        delete doc.data().companyId;
        return ({...doc.data(), id: doc.id });
      })];
      resolve(response);
    }, (error) => {
      console.log(error);
      reject();
    });
  });
};

let cachedList = null;

const FindClient = (props) => {

  const [alreadyUsedDefaultValue, setAlreadyUsedDefaultValue] = useState(false);

  const [clients, updateList] = useState([]);
  const [selectedClient, selectClient] = useState(null);
  const [isLoading, setLoading] = useState(false);

  const { onChange, disabled, defaultBody, onInputChange, hideAddButton, defaultValue = {} } = props;

  useEffect(() => {

    setLoading(true);

    if (cachedList) {
      updateList(cachedList);
      setLoading(false);
      if (!alreadyUsedDefaultValue && !selectedClient && defaultValue.id) {
        selectClient([...cachedList].filter((item) => item.id === defaultValue.id));
        setAlreadyUsedDefaultValue(true);
      }
      return;
    }

    getClients(Constants.get('companyId'))
    .then((data) => {
      cachedList = data;
      updateList(data);
      setLoading(false);
      if (!alreadyUsedDefaultValue && !selectedClient && defaultValue.id) {
        selectClient([...data].filter((item) => item.id === defaultValue.id));
        setAlreadyUsedDefaultValue(true);
      }
    });

  }, []);

  const openFeature = () => {
    props.openModal({
      title: 'Novo cliente',
      disableBackgroundClick: true,
      component: (
        <CreateClient
          onClickToCancel={props.closeModal}
          onSuccess={(response) => {
            delete response.companyId;
            updateList([...clients, ...[{...response}]]);
            selectClient(response);
            if (onChange) onChange(response);
            props.closeModal();
          }}
        />
      )
    });
  };

  return (
    <Grid gapConfig={1}>
      <Grid.Col>
        <Select
          cacheOptions
          defaultOptions
          menuPosition="fixed"
          className="react-select-input has-action-button"
          classNamePrefix="custom"
          menuShouldBlockScroll={true}
          isClearable

          isDisabled={isLoading || disabled}
          isLoading={isLoading}
          placeholder={props.placeholder || ''}
          onChange={(option) => {
            selectClient(option);
            if (onChange) onChange(option);
          }}
          onInputChange={onInputChange}
          getOptionLabel={(item) => item.name}
          getOptionValue={(item) => item.id}
          value={selectedClient}
          loadingMessage={(inputValue) => 'Pesquisando...'}
          noOptionsMessage={(inputValue) => (<>Nenhum cliente encontrado.</>)}
          // loadOptions={promiseOptions}
          options={clients}
        />
      </Grid.Col>
      {/* {!hideAddButton && (
        <Grid.Col isAlignedBottom isNarrow>
          <Button
            isLink
            isOutlined
            avoidTab
            style={{ marginRight: 3, marginLeft: -11, borderBottomLeftRadius: 0, borderTopLeftRadius: 0 }}
            onClick={openFeature}>
            <Icon name="far fa-plus" />
          </Button>
        </Grid.Col>
      )} */}
    </Grid>
  );

};

export default withModal(FindClient);
