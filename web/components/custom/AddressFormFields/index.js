import React, { useState, useEffect } from 'react';

import { Grid, TextInput, FormField } from 'shared-components';

import FullAddressByCep from 'components/custom/FullAddressByCep';

const AddressFormFields = (props) => {

  const [fields, setFields] = useState(props.defaultValues || {});

  const { onAddressChange, disabled, value, defaultValues = {} } = props;

  useEffect(() => {
    if (JSON.stringify(value) && JSON.stringify(value) !== '{}') {
      let parsedValue = {...value};
      const keys = ['description', 'neighborhood', 'cep', 'number', 'complement', 'city', 'uf', 'location'];
      Object.keys(parsedValue).forEach((key) => {
        if (keys.indexOf(key) == -1) delete parsedValue[key];
      });
      if (JSON.stringify(parsedValue) !== JSON.stringify(fields)) updateFields(parsedValue);
    }
  }, [value]);

  const updateFields = (address) => {
    setFields(address);
    if (onAddressChange) onAddressChange(address);
  }

  const handleInputEvent = ({ target }, isValid) => {
    updateFields({ ...fields, [target.name]: target.value });
  }

  return (
    <Grid gapConfig={1} isMultiline>
      <Grid.Col is3>
        <FullAddressByCep
          value={fields.cep}
          defaultValue={defaultValues.cep}
          disabled={disabled}
          onChange={(value) => updateFields({ ...fields, 'cep': value })}
          onGetResponse={(response) => {
            if (response.error) return;
            updateFields({
              ...fields,
              description: response.logradouro,
              neighborhood: response.bairro,
              cep: response.cep,
              city: response.localidade,
              uf: response.uf
            });
          }}
        />
      </Grid.Col>
      <Grid.Col is9>
        <FormField label="Endereço">
          <TextInput
            name="description"
            defaultValue={defaultValues.description}
            disabled={disabled}
            value={fields.description}
            onChange={handleInputEvent}
          />
        </FormField>
      </Grid.Col>
      <Grid.Col is2>
        <FormField label="Número">
          <TextInput
            mask={['onlyNumbers']}
            name="number"
            defaultValue={defaultValues.number}
            disabled={disabled}
            value={fields.number}
            onChange={handleInputEvent}
          />
        </FormField>
      </Grid.Col>
      <Grid.Col is6>
        <FormField label="Cidade">
          <TextInput
            name="city"
            defaultValue={defaultValues.city}
            disabled={disabled}
            value={fields.city}
            onChange={handleInputEvent}
          />
        </FormField>
      </Grid.Col>
      <Grid.Col is4>
        <FormField label="UF">
          <TextInput
            name="uf"
            defaultValue={defaultValues.uf}
            disabled={disabled}
            value={fields.uf}
            onChange={handleInputEvent}
          />
        </FormField>
      </Grid.Col>
      <Grid.Col is5>
        <FormField label="Bairro">
          <TextInput
            name="neighborhood"
            defaultValue={defaultValues.neighborhood}
            disabled={disabled}
            value={fields.neighborhood}
            onChange={handleInputEvent}
          />
        </FormField>
      </Grid.Col>
      <Grid.Col>
        <FormField label="Complemento" labelInfo="(Opcional)">
          <TextInput
            name="complement"
            defaultValue={defaultValues.complement}
            disabled={disabled}
            value={fields.complement}
            onChange={handleInputEvent}
          />
        </FormField>
      </Grid.Col>
    </Grid>
  );

};

export default AddressFormFields;
