import React, { useState, useEffect } from 'react';

import request from 'axios';
import jsonpAdapter from 'axios-jsonp'

import { Grid, Icon, FormField, TextInput, Button } from 'shared-components';

const FullAddressByCep = (props) => {

  const [isLoading, setLoadingStatus] = useState(false);
  const [fieldValue, setValue] = useState('');
  const [fieldIsValid, setValidation] = useState(null);

  const { onGetResponse, value, onChange, defaultValue, disabled } = props;

  useEffect(() => {
    if (fieldValue !== value) setValue(value);
  }, [value]);

  function onInputCep(value, isValid) {
    if (!isValid || !value) return;
    setLoadingStatus(true);
    request.get('https://viacep.com.br/ws/' + value.replace('-', '').replace('.', '') + '/json', { adapter: jsonpAdapter })
    .then((response) => {
      if (onGetResponse) onGetResponse(response.data);
      setLoadingStatus(false);
    })
    .catch((error) => {
      console.log('error', error);
    });
  };

  function onChangeInput({ target }, isValid) {
    setValue(target.value);
    setValidation(isValid);
    if (onChange) onChange(target.value);
  };

  function onSearch() {
    onInputCep(fieldValue || defaultValue, fieldIsValid);
  };

  return (
    <Grid gapConfig={1}>
      <Grid.Col>
        <FormField label="CEP">
          <TextInput
            mask={['cep']}
            validator={['cep']}
            value={fieldValue}
            defaultValue={defaultValue}
            disabled={isLoading || disabled}
            onChange={onChangeInput}
          />
        </FormField>
      </Grid.Col>
      <Grid.Col isAlignedBottom isNarrow>
        <Button
          isLink
          avoidTab
          disabled={!fieldIsValid || disabled}
          isLoading={isLoading}
          style={{ marginRight: 3, marginLeft: -9, borderBottomLeftRadius: 0, borderTopLeftRadius: 0 }}
          onClick={onSearch}>
          <Icon name="far fa-search" />
        </Button>
      </Grid.Col>
    </Grid>
  );

};

export default FullAddressByCep;
