import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';

import { Icon, Button } from 'shared-components';

import GoogleMapReact from 'google-map-react';

import mapTheme from './mapTheme';

import Marker from './Marker';

import styled from 'styled-components';

const Container = styled.div`
  width: 100%;
  flex: 1;
  min-height: 580px;
  overflow: hidden;
  position: relative;
  border-radius: 2px;
  border: 1px solid #d8e1e8;
`;

const LeftOverlay = styled.div`
  position: absolute;
  z-index: 1;
  padding: 7px;
  top: 0;
  left: 0;
`;

const RightOverlay = styled.div`
  position: absolute;
  z-index: 1;
  padding: 7px;
  top: 0;
  right: 0;
`;

const BottomOverlay = styled.div`
  width: 100%;
  position: absolute;
  z-index: 1;
  padding: 7px 7px 30px;
  bottom: 0;
  left: 0;
`;

const TrackingMap = (props) => {

  const [mapInstance, setMapInstance] = useState(null);

  const mapContainer = useRef();

  const { origin, destination, leftOverlay, rightOverlay, bottomOverlay } = props;

  let from = origin;
  let to = destination || TrackingMap.defaultProps.destination;

  if (mapInstance && from) {
    // getBounds(mapInstance, from, to);
  }

  const handleApiLoaded = (map, maps) => !mapInstance && setMapInstance(map);

  const focusOnMarkers = () => mapInstance && getBounds(mapInstance, from, to);

  const focusOnDriver = () => {
    if (!mapInstance) return;
    mapInstance.setCenter(from);
    mapInstance.setZoom(15);
  };

  function getBounds(mapInstance, from, to) {
    if (!window.google) {
      throw new Error('[Map]: Google Maps JavaScript API library must be loaded.');
    }
    if (!mapInstance) return;
    const bounds = new google.maps.LatLngBounds();
    if (from.lat) bounds.extend(from);
    if (to.lat) bounds.extend(to);
    mapInstance.fitBounds(bounds, { top: 80, bottom: 80, right: 20, left: 20 });
  };

  return (
    <Container ref={mapContainer}>

      <LeftOverlay>{leftOverlay}</LeftOverlay>
      <RightOverlay>{rightOverlay}</RightOverlay>
      <BottomOverlay>{bottomOverlay}</BottomOverlay>

      <div className="buttons" style={{ position: 'absolute', zIndex: 1, bottom: 70, right: 6 }}>
        <Button isRounded isPrimary isLarge onClick={focusOnMarkers}><Icon name="fas fa-expand" /></Button>
        <Button isRounded isPrimary isLarge onClick={focusOnDriver}><Icon name="fas fa-location" /></Button>
      </div>

      <GoogleMapReact
        bootstrapURLKeys={{ key: GOOGLE_MAPS_KEY }}
        center={to}
        defaultZoom={TrackingMap.defaultProps.zoom}
        layerTypes={['TrafficLayer', 'TransitLayer']}
        options={{ disableDefaultUI: true }}
        yesIWantToUseGoogleMapApiInternals
        onGoogleApiLoaded={({ map, maps }) => handleApiLoaded(map, maps)}>

        {from && <Marker {...from} iconName="fas fa-truck" />}
        {!to.default && <Marker {...to} iconName="fas fa-flag-checkered" />}

      </GoogleMapReact>

    </Container>
  );

};

TrackingMap.protoTypes = {
  origin: PropTypes.object,
  destination: PropTypes.object
};

TrackingMap.defaultProps = {
  origin: null,
  destination: { lat: -9.9856871, lng: -53.9164012, default: true },
  zoom: 5
};

export default TrackingMap;
