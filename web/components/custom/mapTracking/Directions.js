if (mapInstance && googleMaps && drawDirections && drawDirections.length > 0) {
  const polyline = new googleMaps.Polyline({
    path: drawDirections,
    // geodesic: true,
    options: {
      strokeColor: '#1C99FE',
      strokeOpacity: 1,
      strokeWeight: 3
    }
  });
  polyline.setMap(mapInstance);
};

