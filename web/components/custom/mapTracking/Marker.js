import React from 'react';
import PropTypes from 'prop-types';

import { Icon } from 'shared-components';

import styled from 'styled-components';

const Container = styled.div`
  width: 30px;
  height: 30px;
  margin-top: -15px;
  margin-left: -15px;
  border-radius: 30px;
  background-color: #293742;
  box-shadow: 1px 1px 4px rgba(0, 0, 0, .3);
  display: flex;
  justify-content: center;
  align-items: center;
  color: #FFF;
`;

const Marker = ({ iconName }) => {
  return (
    <Container>
      <Icon name={iconName} />
    </Container>
  );
};

Marker.propTypes = {

};

export default Marker;
