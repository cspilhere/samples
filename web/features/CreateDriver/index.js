import React from 'react';

import { Grid, FormField, Select, Switch } from 'shared-components';
import { Input } from 'components';
import FormWrapper from '../components/FormWrapper';

import { callToast } from 'utils/utils';
import { checkObjectKeys } from 'shared-utils/utils';

import { drivers } from 'services/firestore';
import { maskSchema } from 'services/masksAndValidators';

class CreateDriver extends React.Component {

  render() {

    const { onClickToCancel } = this.props;

    return (
      <FormWrapper
        initialValues={{ relation: 0, active: true }}
        onSubmit={(values, actions) => {
          this.setState({ isSubmitting: true });
          this.submitForm(values, () => {
            this.setState({ isSubmitting: false });
            actions.setSubmitting(false);
          });
        }}
        onClickToCancel={onClickToCancel}
        renderFooterRight={({ values, setFieldValue, isSubmitting }) => (
          <Switch
            id="update-driver-active-switch"
            isSmall
            disabled={isSubmitting}
            description={(values.active ? 'Ativo' : 'Inativo')}
            defaultChecked={values.active}
            onChange={({ target }) => {
              setFieldValue('active', target.checked);
            }}
          />
        )}
        render={({
          values,
          handleChange,
          isSubmitting
        }) => {
          return (
            <Grid gapConfig={1} isMultiline>
              <Grid.Col is8>
                <FormField label="Nome" labelInfo="*">
                  <Input
                    id="create-driver-name-input"
                    name="name"
                    disabled={isSubmitting}
                    value={values.name}
                    onChange={handleChange}
                  />
                </FormField>
              </Grid.Col>
              <Grid.Col is4>
                <FormField label="CPF" labelInfo="*">
                  <Input
                    id="create-driver-document-input"
                    name="document"
                    disabled={isSubmitting}
                    value={values.document}
                    onChange={(event) => {
                      event.target.value = maskSchema.cpf.resolve(event.target.value);
                      handleChange(event);
                    }}
                  />
                </FormField>
              </Grid.Col>
              <Grid.Col is9>
                <FormField label="Relação">
                  <Select
                    id="create-driver-relation-select"
                    name="relation"
                    isFullWidth
                    hideBlankOption
                    options={[
                      { label: 'Contratado', value: 0 },
                      { label: 'Agregado', value: 1 },
                      { label: 'Terceirizado', value: 2 },
                      { label: 'Freelancer', value: 3 }
                    ]}
                    disabled={isSubmitting}
                    value={values.relation}
                    onChange={handleChange}
                  />
                </FormField>
              </Grid.Col>
              <Grid.Col>
                <FormField label="Telefone" labelInfo="*">
                  <Input
                    id="create-driver-phone-input"
                    name="phone"
                    disabled={isSubmitting}
                    value={values.phone}
                    onChange={(event) => {
                      event.target.value = maskSchema.phone.resolve(event.target.value);
                      handleChange(event);
                    }}
                  />
                </FormField>
              </Grid.Col>
            </Grid>
          );
        }}
      />
    );
  }

  submitForm = (values, errorPostAction) => {

    const { onSuccess, onError } = this.props;

    if (!checkObjectKeys(values, ['name', 'phone', 'document'])) {
      callToast('warning', 'Os campos Nome, CPF e Telefone são obrigatórios.');
      errorPostAction();
      return;
    }

    drivers.create({ ...values })
    .then((response) => {
      callToast('success', 'Motorista convidado com sucesso!');
      onSuccess(response);
    })
    .catch((error) => {
      if (error.code == 'document-already-exists') {
        callToast('error', 'Você está tentando convidar um motorista que já existe.');
      } else {
        callToast('error', 'Ocorreu um erro ao tentar convidar o motorista. :(');
      }
      onError(error);
      errorPostAction();
    });

  }

}

CreateDriver.defaultProps = {
  onSuccess: () => {},
  onError: () => {}
};

export default CreateDriver;
