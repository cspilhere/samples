import React from 'react';

import {
  FormField,
  Grid
} from 'shared-components';

import FindDriver from 'components/custom/FindDriver';

import { checkObjectKeys, filterObject } from 'shared-utils/utils';

import { delivery } from 'services/firestore';

import { callToast } from 'utils/utils';

import FormWrapper from '../components/FormWrapper';

const UpdateDeliveryDriver = (props) => {

  const { onSuccess, onError, onClickToCancel, defaultValues } = props;

  const submitForm = (values, errorPostAction) => {

    if (!checkObjectKeys(values, ['driver'])) {
      callToast('warning', 'O campo Motorista é obrigatório.');
      errorPostAction();
      return;
    }

    const request = values;

    values.driver = filterObject(values.driver, ['name', 'id', 'document', 'phone']);

    delivery.update(defaultValues.id, request)
    .then(() => {
      callToast('success', 'Motorista atualizado com sucesso!');
      onSuccess(request);
    })
    .catch((error) => {
      callToast('error', 'Ocorreu um erro ao tentar atualizar o motorista. :(');
      onError(error);
      errorPostAction();
    });

  };

  return (
    <FormWrapper
      initialValues={filterObject(defaultValues, ['driver'])}
      isInitialValid={true}
      onSubmit={(values, actions) => {
        submitForm(values, () => {
          actions.setSubmitting(false);
        });
      }}
      onClickToCancel={onClickToCancel}
      render={({
        values,
        setFieldValue,
        isSubmitting
      }) => {
        return (
          <Grid gapConfig={1}>
            <Grid.Col>
              <FormField label="Motorista">
                <FindDriver
                  defaultValue={values.driver}
                  disabled={isSubmitting}
                  onChange={(driver) => setFieldValue('driver', driver)}
                />
              </FormField>
            </Grid.Col>
          </Grid>
        );
      }}
    />
  );

};

UpdateDeliveryDriver.defaultProps = {
  onSuccess: () => {},
  onError: () => {},
  defaultValues: {}
};

export default UpdateDeliveryDriver;
