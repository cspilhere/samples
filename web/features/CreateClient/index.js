import React from 'react';

import {
  Grid,
  Heading,
  FormField,
  Button,
  Icon
} from 'shared-components';

import AddressFormFields from 'components/custom/AddressFormFields';

import { Input, FieldRepeater } from 'components';
import FormWrapper from '../components/FormWrapper';

import { callToast } from 'utils/utils';
import { checkObjectKeys } from 'shared-utils/utils';

import { createStakeholder } from 'services/firestore';
import { maskSchema, validationSchema } from 'services/masksAndValidators';

const validator = validationSchema([
  'name',
  'document'
]);

class CreateStakeholder extends React.Component {

  state = {
    documentType: '',
    address: ''
  }

  render() {

    const { onClickToCancel } = this.props;
    const { documentType, address } = this.state;

    return (
      <FormWrapper
        initialValues={{ document: '', users: [] }}
        validationSchema={validator}
        onSubmit={(values, actions) => {
          this.submitForm(values, () => {
            actions.setSubmitting(false);
          });
        }}
        onClickToCancel={onClickToCancel}
        render={({
          values,
          isValid,
          handleChange,
          isSubmitting
        }) => {
          return (
            <>
              <Grid gapConfig={1} isMultiline>
                <Grid.Col is4>
                  <FormField label="Documento" labelInfo={`* ${(documentType || '').toUpperCase()}`}>
                    <Input
                      id="create-client-document-input"
                      name="document"
                      disabled={isSubmitting}
                      value={values.document}
                      onChange={(event) => {
                        let type = 'cpf';
                        let typeDescription = '';
                        if (event.target.value.length > 14) {
                          type = 'cnpj';
                          typeDescription = 'cnpj';
                        }
                        if (event.target.value.length === 14) {
                          typeDescription = 'cpf';
                        }
                        maskSchema[type].resolve(event.target.value);
                        event.target.value = maskSchema[type].value;
                        this.setState({ documentType: typeDescription });
                        handleChange(event);
                      }}
                    />
                  </FormField>
                </Grid.Col>
                <Grid.Col>
                  <FormField label="Nome" labelInfo="*">
                    <Input
                      id="create-client-name-input"
                      name="name"
                      disabled={isSubmitting}
                      value={values.name}
                      onChange={handleChange}
                    />
                  </FormField>
                </Grid.Col>
              </Grid>

              <AddressFormFields
                value={address}
                onAddressChange={(addressObject) => {
                  this.setState({ address: addressObject });
                }}
                disabled={isSubmitting}
              />

              <Heading is6 hasUnderline hasBottomSpace>Contatos</Heading>

              <FieldRepeater
                name="users"
                render={(arrayHelpers) => (
                  <>
                    {values.users.map((user, index) => {
                      return (
                        <Grid gapConfig={1} key={index}>
                          <Grid.Col is4>
                            <FormField label="Nome" labelInfo="*">
                              <Input
                                disabled={isSubmitting}
                                name={`users.${index}.name`}
                                value={user.name}
                                onChange={handleChange}
                                small
                              />
                            </FormField>
                          </Grid.Col>
                          <Grid.Col is4>
                            <FormField label="Email" labelInfo="*">
                              <Input
                                disabled={isSubmitting}
                                name={`users.${index}.email`}
                                value={user.email}
                                onChange={handleChange}
                                small
                              />
                            </FormField>
                          </Grid.Col>
                          <Grid.Col>
                            <FormField label="Telefone" labelInfo="*">
                              <Input
                                disabled={isSubmitting}
                                name={`users.${index}.phone`}
                                value={user.phone}
                                onChange={(event) => {
                                  maskSchema.phone.resolve(event.target.value);
                                  event.target.value = maskSchema.phone.value;
                                  handleChange(event);
                                }}
                                small
                              />
                            </FormField>
                          </Grid.Col>
                          <Grid.Col isNarrow isAlignedBottom>
                            <Button avoidTab isSmall onClick={() => arrayHelpers.remove(index)}>
                              <Icon name="fas fa-user-times" />
                            </Button>
                          </Grid.Col>
                        </Grid>
                      );
                    })}
                    <Button
                      isSmall
                      isLink
                      avoidTab
                      disabled={isSubmitting || !isValid}
                      onClick={() => arrayHelpers.push({ name: '', email: '', phone: '' })}>
                      <Icon name="fas fa-user-plus" /> Convidar usuário
                    </Button>
                  </>
                )}
              />
            </>
          );
        }}
      />
    );
  }

  submitForm = (values, errorPostAction) => {

    const { documentType, address } = this.state;
    const { onSuccess, onError } = this.props;

    if (!checkObjectKeys(values, ['name', 'document'])) {
      callToast('warning', 'Os campos Nome e Documento são obrigatórios.');
      errorPostAction();
      return;
    }

    if (values.users) {
      const checkUsers = [...values.users].filter((user, index) => {
        if (!checkObjectKeys(user, ['name', 'email', 'phone'])) {
          callToast('warning', `Os campos Nome, Email e Telefone são obrigatórios para convidar um usuário.`);
          errorPostAction();
        } else {
          return user;
        }
      });
      if (checkUsers.length !== values.users.length) return;
    }

    createStakeholder({
      ...values,
      documentType,
      address
    }, 'clients')
    .then((response) => {
      callToast('success', 'Cliente cadastrado com sucesso!');
      onSuccess(response);
    })
    .catch((error = {}) => {
      if (error.code == 'document-already-exists') {
        callToast('error', 'Você está tentando cadastrar um cliente que já existe.');
      } else {
        callToast('error', 'Ocorreu um erro ao tentar cadastrar o cliente. :(');
      }
      onError(error);
      errorPostAction();
    });

  }

}

CreateStakeholder.defaultProps = {
  onSuccess: () => {},
  onError: () => {}
};

export default CreateStakeholder;
