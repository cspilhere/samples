import React, { useState } from 'react';

import {
  FormField,
  Grid
} from 'shared-components';

import FindCoordinates from 'components/custom/FindCoordinates';

import { checkObjectKeys, filterObject } from 'shared-utils/utils';

import { delivery } from 'services/firestore';

import { callToast } from 'utils/utils';

import FormWrapper from '../components/FormWrapper';

const UpdateDeliveryDestination = (props) => {

  const [coordinates, setCoordinates] = useState({});

  const { onSuccess, onError, onClickToCancel, defaultValues } = props;

  const submitForm = (values, errorPostAction) => {

    if (!checkObjectKeys(values, ['destination'])) {
      callToast('warning', 'O campo Endereço de entrega é obrigatório.');
      errorPostAction();
      return;
    }

    const request = values;

    delivery.updateWithNewCoordinates(defaultValues.id, request)
    .then(() => {
      callToast('success', 'Endereço de entrega atualizado com sucesso!');
      onSuccess(request);
    })
    .catch((error) => {
      callToast('error', 'Ocorreu um erro ao tentar atualizar o endereço de entrega. :(');
      onError(error);
      errorPostAction();
    });

  };

  const hasCoordinates = coordinates && coordinates.latitude && coordinates.longitude;
  const { latitude, longitude } = coordinates;

  return (
    <FormWrapper
      initialValues={filterObject(defaultValues, ['destination'])}
      isInitialValid={true}
      onSubmit={(values, actions) => {
        submitForm(values, () => {
          actions.setSubmitting(false);
        });
      }}
      onClickToCancel={onClickToCancel}
      render={({
        values,
        setFieldValue,
        isSubmitting
      }) => {
        if (!values.destination) values.destination = {};
        return (
          <Grid gapConfig={1}>
            <Grid.Col>
              <FormField
                label="Endereço de entrega"
                labelInfo={hasCoordinates && `(${latitude}, ${longitude})`}>
                <FindCoordinates
                  disabled={isSubmitting}
                  defaultValue={values.destination.address}
                  onChange={(destination) => {
                    setFieldValue('destination', destination);
                    setCoordinates(destination.coordinates)
                  }}
                />
              </FormField>
            </Grid.Col>
          </Grid>
        );
      }}
    />
  );

};

UpdateDeliveryDestination.defaultProps = {
  onSuccess: () => {},
  onError: () => {},
  defaultValues: {}
};

export default UpdateDeliveryDestination;
