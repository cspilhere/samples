import React from 'react';

import {
  Modal,
  Button,
  FlexBar
} from 'shared-components';

import { FormContainer } from 'components';

const FormWrapper = ({ render, autoFocus, renderFooterRight, footerRight, ...rest }) => (
  <FormContainer
    {...rest}
    autoFocus={autoFocus}
    render={(renderProps) => {
      return (
        <Modal.Wrapper>
          <Modal.Content withOverflow>
            {render(renderProps)}
          </Modal.Content>
          <Modal.Footer>
            <FlexBar>
              <FlexBar.Child isGrow>
                {footerRight || renderFooterRight(renderProps)}
              </FlexBar.Child>
              <FlexBar.Child>
                <div className="buttons">
                  <Button
                    id="feature-cancel-button"
                    avoidTab
                    disabled={renderProps.isSubmitting}
                    isSmall
                    onClick={rest.onClickToCancel}>
                    Cancelar
                  </Button>
                  <Button
                    id="feature-save-button"
                    isPrimary
                    submit
                    disabled={renderProps.isSubmitting || !renderProps.isValid}
                    isSmall
                    onClick={renderProps.handleSubmit}>
                    Salvar
                  </Button>
                </div>
              </FlexBar.Child>
            </FlexBar>
          </Modal.Footer>
        </Modal.Wrapper>
      );
    }}
  />
);

FormWrapper.defaultProps = {
  render: () => null,
  autoFocus: true,
  renderFooterRight: () => null
};

export default FormWrapper;
