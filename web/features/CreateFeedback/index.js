import React from 'react';

import {
  FormField,
  Text,
  Icon
} from 'shared-components';

import Rating from 'react-rating';

import { callToast } from 'utils/utils';

import { delivery } from 'services/firestore';

import FormWrapper from '../components/FormWrapper';

const CreateRecord = (props) => {

  const { onSuccess, onError, onClickToCancel, defaultValues } = props;

  const submitForm = (values, errorPostAction) => {
    delivery.saveRating(defaultValues.deliveryId, { ...values, createdBy: defaultValues.createdBy })
    .then(() => {
      callToast('success', 'Feedback registrado com sucesso!');
      onSuccess();
    })
    .catch((error) => {
      onError(error);
      errorPostAction();
    });
  };

  return (
    <FormWrapper
      onSubmit={(values, actions) => {
        submitForm(values, () => {
          actions.setSubmitting(false);
        });
      }}
      onClickToCancel={onClickToCancel}
      render={({
        values,
        setFieldValue
      }) => {
        return (
          <>
            <div style={{ width: '100%', padding: '0 45px 10px', textAlign: 'center' }}>
              <Text><strong>Avalie esta entrega</strong></Text>
            </div>
            <FormField>
              <div style={{ width: 300, textAlign: 'center' }}>
                <Rating
                  id="create-feedback-rating"
                  initialRating={values.rating}
                  onChange={(value) => {
                    setFieldValue('rating', value);
                  }}
                  emptySymbol={<Icon name="fal fa-2x fa-star" style={{ margin: '14px', color: '#f2b824' }} />}
                  fullSymbol={<Icon name="fas fa-2x fa-star" style={{ margin: '14px', color: '#f2b824' }} />}
                />
              </div>
            </FormField>
            <div
              style={{
                width: '100%',
                padding: '0 45px',
                marginTop: -14,
                textAlign: 'center',
                display: 'flex',
                justifyContent: 'space-between'
              }}>
              <Text isSmall>Muito ruim</Text>
              <Text isSmall>Muito bom</Text>
            </div>
          </>
        );
      }}
    />
  );

};

CreateRecord.defaultProps = {
  onSuccess: () => {},
  onError: () => {},
  defaultValues: {}
};

export default CreateRecord;
