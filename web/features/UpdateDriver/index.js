import React from 'react';

import { Grid, FormField, Select, Switch } from 'shared-components';
import { Input } from 'components';
import FormWrapper from '../components/FormWrapper';

import { callToast } from 'utils/utils';
import { checkObjectKeys, filterObject } from 'shared-utils/utils';

import { drivers } from 'services/firestore';
import { maskSchema } from 'services/masksAndValidators';

class UpdateDriver extends React.Component {

  render() {

    const { onClickToCancel, defaultValues } = this.props;

    return (
      <FormWrapper
        initialValues={filterObject(defaultValues, ['name', 'document', 'relation', 'phone', 'active']) }
        isInitialValid={true}
        onSubmit={(values, actions) => {
          this.setState({ isSubmitting: true });
          this.submitForm(values, () => {
            this.setState({ isSubmitting: false });
            actions.setSubmitting(false);
          });
        }}
        onClickToCancel={onClickToCancel}
        renderFooterRight={({ values, setFieldValue, isSubmitting }) => (
          <Switch
            id="update-driver-active-switch"
            isSmall
            disabled={isSubmitting}
            description={(values.active ? 'Ativo' : 'Inativo')}
            defaultChecked={values.active}
            onChange={({ target }) => {
              setFieldValue('active', target.checked);
            }}
          />
        )}
        render={({
          values,
          handleChange,
          isSubmitting
        }) => {

          const document = maskSchema.cpf.resolve(values.document);
          const phone = maskSchema.phone.resolve(values.phone);

          return (
            <Grid gapConfig={1} isMultiline>
              <Grid.Col is8>
                <FormField label="Nome" labelInfo="*">
                  <Input
                    id="update-driver-name-input"
                    name="name"
                    disabled={isSubmitting}
                    value={values.name}
                    onChange={handleChange}
                  />
                </FormField>
              </Grid.Col>
              <Grid.Col is4>
                <FormField label="CPF" labelInfo="*">
                  <Input
                    id="update-driver-document-input"
                    name="document"
                    disabled={true}
                    value={document}
                  />
                </FormField>
              </Grid.Col>
              <Grid.Col is9>
                <FormField label="Relação">
                  <Select
                    id="update-driver-relation-select"
                    name="relation"
                    isFullWidth
                    hideBlankOption
                    options={[
                      { label: 'Contratado', value: 0 },
                      { label: 'Agregado', value: 1 },
                      { label: 'Terceirizado', value: 2 },
                      { label: 'Freelancer', value: 3 }
                    ]}
                    disabled={isSubmitting}
                    value={values.relation}
                    onChange={handleChange}
                  />
                </FormField>
              </Grid.Col>
              <Grid.Col>
                <FormField label="Telefone" labelInfo="*">
                  <Input
                    id="update-driver-phone-input"
                    name="phone"
                    disabled={true}
                    value={phone}
                  />
                </FormField>
              </Grid.Col>
            </Grid>
          );
        }}
      />
    );
  }

  submitForm = (values, errorPostAction) => {

    const { onSuccess, onError, defaultValues } = this.props;

    if (!checkObjectKeys(values, ['name', 'phone', 'document'])) {
      callToast('warning', 'Os campos Nome, CPF e Telefone são obrigatórios.');
      errorPostAction();
      return;
    }

    const request = { ...values };

    drivers.update(request, defaultValues.id)
    .then(() => {
      callToast('success', `Motorista atualizado com sucesso!`);
      onSuccess(request);
    })
    .catch((error) => {
      callToast('error', 'Ocorreu um erro ao tentar atualizar o motorista. :(');
      onError(error);
      errorPostAction();
    });

  }

}

UpdateDriver.defaultProps = {
  onSuccess: () => {},
  onError: () => {},
  defaultValues: {}
};

export default UpdateDriver;
