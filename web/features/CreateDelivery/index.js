import React, { useState } from 'react';

import {
  FormField,
  Select,
  Switch,
  DateTimePicker,
  Grid,
  Icon
} from 'shared-components';

import FindDriver from 'components/custom/FindDriver';
import FindClient from 'components/custom/FindClient';
import FindShipper from 'components/custom/FindShipper';
import FindCoordinates from 'components/custom/FindCoordinates';

import { checkObjectKeys } from 'shared-utils/utils';

import { delivery } from 'services/firestore';

import { callToast } from 'utils/utils';

import { Input } from 'components';
import FormWrapper from '../components/FormWrapper';

import { maskSchema, validationSchema } from 'services/masksAndValidators';

const validator = validationSchema([
  'number',
  'taker',
  'client',
  'shipper'
]);

const CreateDelivery = (props) => {

  const [isReady, setReady] = useState(false);
  const [coordinates, setCoordinates] = useState({});

  const { onSuccess, onError, onClickToCancel } = props;

  const submitForm = (values, errorPostAction) => {

    if (!checkObjectKeys(values, ['number', 'billOfLading', 'taker', 'client', 'shipper'])) {
      callToast('warning', 'Os campos CTe, Chave de acesso, Tomador, Cliente e Embarcador são obrigatórios.');
      errorPostAction();
      return;
    }

    delivery.create({ ...values, isReady })
    .then((response) => {
      callToast('success', 'Entrega criada com sucesso!');
      onSuccess(response);
    })
    .catch((error) => {
      callToast('error', 'Ocorreu um erro ao tentar criar a entrega. :(');
      onError(error);
      errorPostAction();
    });

  };

  const hasCoordinates = coordinates && coordinates.latitude && coordinates.longitude;
  const { latitude, longitude } = coordinates;

  return (
    <FormWrapper
      initialValues={{ series: 1 }}
      validationSchema={validator}
      onSubmit={(values, actions) => {
        submitForm(values, () => {
          actions.setSubmitting(false);
        });
      }}
      onClickToCancel={onClickToCancel}
      renderFooterRight={({ values, isSubmitting }) => (
        <Switch
          id="create-delivery-release-switch"
          isSmall
          disabled={!values.driver || isSubmitting}
          description={'Liberar viagem?' + (isReady ? ' Sim.' : '')}
          defaultChecked={false}
          onChange={({ target }) => setReady(target.checked)}
        />
      )}
      render={({
        values,
        handleChange,
        setFieldValue,
        isSubmitting
      }) => {
        return (
          <>
            <Grid gapConfig={1} isMultiline>
              <Grid.Col>
                <FormField label="Número do CTe">
                  <Input
                    id="create-delivery-number-input"
                    name="number"
                    disabled={isSubmitting}
                    value={values.number}
                    onChange={(event) => {
                      event.target.value = maskSchema.number.resolve(event.target.value);
                      handleChange(event);
                    }}
                  />
                </FormField>
              </Grid.Col>
              <Grid.Col isNarrow>
                <FormField label="Série">
                  <Select
                    id="create-delivery-series-select"
                    name="series"
                    value={values.series}
                    isFullWidth
                    hideBlankOption
                    options={[
                      { label: '1', value: '1' },
                      { label: '2', value: '2' },
                      { label: '3', value: '3' },
                      { label: '4', value: '4' }
                    ]}
                    disabled={isSubmitting}
                    onChange={handleChange}
                  />
                </FormField>
              </Grid.Col>
              <Grid.Col is5>
                <FormField label="Tomador" labelInfo="*">
                  <Select
                    id="create-delivery-taker-select"
                    name="taker"
                    value={values.taker}
                    isFullWidth
                    options={[
                      { label: 'CIF', value: '0' },
                      { label: 'Embarcadora', value: '1' },
                      { label: 'Cliente', value: '2' },
                      { label: 'FOB', value: '3' },
                      { label: 'Não informar', value: '4' }
                    ]}
                    disabled={isSubmitting}
                    onChange={handleChange}
                  />
                </FormField>
              </Grid.Col>

              <Grid.Col is12>
                <FormField label="Chave de acesso">
                  <Input
                    id="create-delivery-billoflading-input"
                    name="billOfLading"
                    disabled={isSubmitting}
                    value={values.billOfLading}
                    onChange={(event) => {
                      event.target.value = maskSchema.number.resolve(event.target.value);
                      handleChange(event);
                    }}
                  />
                </FormField>
              </Grid.Col>

            </Grid>

            <FormField label="NFs" labelInfo={'Informe os números das NFs separados por ","'}>
              <Input
                id="create-delivery-invoices-input"
                name="invoices"
                value={values.invoices}
                textarea
                disabled={isSubmitting}
                onChange={handleChange}
              />
            </FormField>

            <Grid gapConfig={1}>
              <Grid.Col is6>
                <FormField label="Embarcador" labelInfo="*">
                  <FindShipper
                    disabled={isSubmitting}
                    onChange={(shipper) => setFieldValue('shipper', shipper)}
                  />
                </FormField>
              </Grid.Col>
              <Grid.Col is6>
                <FormField label="Cliente" labelInfo="*">
                  <FindClient
                    disabled={isSubmitting}
                    onChange={(client) => setFieldValue('client', client)}
                  />
                </FormField>
              </Grid.Col>
            </Grid>

            <Grid gapConfig={1}>
              <Grid.Col is4>
                <FormField label="Previsão de entrega" hasIconsLeft>
                  <DateTimePicker
                    showTimeSelect
                    disabled={isSubmitting}
                    onChange={(estimation) => setFieldValue('estimation', estimation)}
                  />
                  <Icon name="fas fa-calendar-day" isLeft isSmall />
                </FormField>
              </Grid.Col>
              <Grid.Col is8>
                <FormField label="Motorista">
                  <FindDriver
                    disabled={isSubmitting}
                    onChange={(driver) => setFieldValue('driver', driver)}
                  />
                </FormField>
              </Grid.Col>
            </Grid>

            <Grid gapConfig={1}>
              <Grid.Col>
                <FormField
                  label="Endereço de entrega"
                  labelInfo={hasCoordinates && `(${latitude}, ${longitude})`}>
                  <FindCoordinates
                    onChange={(destination) => {
                      setFieldValue('destination', destination);
                      setCoordinates(destination.coordinates)
                    }}
                  />
                </FormField>
              </Grid.Col>
            </Grid>
          </>
        );
      }}
    />
  );

};

export default CreateDelivery;
