import React, { useState } from 'react';

import {
  FormField,
  Text,
  Select,
  FileReader
} from 'shared-components';

import { uuid, checkObjectKeys } from 'shared-utils/utils';

import { callToast } from 'utils/utils';

import { storage, records } from 'services/firestore';

import { Input } from 'components';
import FormWrapper from '../components/FormWrapper';

const CreateRecord = (props) => {

  const [fileToUpload, setFile] = useState(null);

  const { onSuccess, onError, onClickToCancel, defaultValues } = props;

  const submitForm = (values, errorPostAction) => {

    if (!checkObjectKeys(values, ['type', 'description'])) {
      callToast('warning', 'Os campos Tipo e Descrição são obrigatórios.');
      errorPostAction();
      return;
    }

    delete defaultValues.links;

    if (fileToUpload && checkObjectKeys(fileToUpload, ['name', 'type', 'file', 'fileRef'])) {
      storage.upload(fileToUpload)
      .then((fileRef) => {
        fileRef.getDownloadURL().then((url) => {
          url = url.replace(fileRef.fullPath, encodeURIComponent(fileRef.fullPath));
          records.create(defaultValues.deliveryId, {
            ...defaultValues,
            ...values,
            attachment: {
              url,
              type: fileToUpload.type
            }
          })
          .then(onSuccess)
          .catch(onError);
        });
      })
      .catch((error) => {
        callToast('error', 'Ocorreu um erro ao tentar enviar o arquivo e o registro não pode ser criado. :(');
        errorPostAction();
      });
    } else {
      records.create(defaultValues.deliveryId, { ...defaultValues, ...values })
      .then(onSuccess)
      .catch((error) => {
        callToast('error', 'Ocorreu um erro ao tentar criar o registro. :(');
        onError(error);
        errorPostAction();
      });
    }

  };

  function setupFileToUpload(files) {
    if (!files && !files[0]) return;
    const fileRef = `${defaultValues.companyId}/${defaultValues.deliveryId}/${uuid()}.${files[0].ext}`;
    setFile({
      name: files[0].name,
      type: files[0].raw.type,
      file: files[0].raw,
      fileRef
     });
  };

  return (
    <FormWrapper
      initialValues={{ linkedTo: defaultValues.links[0] }}
      onSubmit={(values, actions) => {
        submitForm(values, () => {
          actions.setSubmitting(false);
        });
      }}
      onClickToCancel={onClickToCancel}
      renderFooterRight={({ isSubmitting }) => (
        <Text>{isSubmitting ? 'Salvando, aguarde...' : ''}</Text>
      )}
      render={({
        values,
        handleChange,
        isSubmitting,
        setFieldValue
      }) => {
        return (
          <>

            <FormField label="Tipo" labelInfo="*">
              <Select
                id="create-record-type-select"
                value={values.type}
                isFullWidth
                options={[
                  { label: 'Anotação', value: 'note' },
                  { label: 'Avaria', value: 'damage' },
                  { label: 'Comprovante', value: 'receipt' },
                  { label: 'Coleta', value: 'gathering' },
                  { label: 'Documento', value: 'document' },
                  { label: 'Registro', value: 'event' },
                  { label: 'Referência', value: 'reference' }
                ]}
                disabled={isSubmitting}
                onChange={({ target }) => setFieldValue('type', target.value)}
              />
            </FormField>

            <FormField label="Descrição">
              <Input
                id="create-record-description-input"
                name="description"
                value={values.description}
                textarea
                disabled={isSubmitting}
                onChange={handleChange}
              />
            </FormField>

            <FormField label="Vínculo">
              {defaultValues.links.length == 1 ? (
                <Input
                  value={defaultValues.links[0]}
                  readOnly
                />
              ) : (
                <Select
                  id="create-record-linkedTo-select"
                  value={values.linkedTo}
                  isFullWidth
                  hideBlankOption
                  options={defaultValues.links}
                  disabled={isSubmitting}
                  onChange={({ target }) => setFieldValue('linkedTo', target.value)}
                />
              )}
            </FormField>

            <FormField label="Anexo" labelInfo="Somente imagem ou pdf (até 3MB)">
              <FileReader
                id="create-record-file-input"
                disabled={isSubmitting}
                onChange={(files) => {
                  if (files[0].size > 3000000) {
                    callToast('warning', 'O arquivo excedeu o tamanho máximo permitido, reduza o tamanho do arquivo e tente novamente!');
                  } else {
                    setupFileToUpload(files);
                  }
                }}
                accept="image/png, image/jpeg, image/jpg, .pdf"
              />
            </FormField>

          </>
        );
      }}
    />
  );
};

CreateRecord.defaultProps = {
  onSuccess: () => {},
  onError: () => {},
  defaultValues: { links: [] }
};

export default CreateRecord;
