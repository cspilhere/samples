import React from 'react';

import {
  Grid,
  Heading,
  FormField,
  Button,
  Icon
} from 'shared-components';

import AddressFormFields from 'components/custom/AddressFormFields';

import { Input, FieldRepeater } from 'components';
import FormWrapper from '../components/FormWrapper';

import { callToast } from 'utils/utils';
import { checkObjectKeys, filterObject } from 'shared-utils/utils';

import { updateStakeholder } from 'services/firestore';
import { maskSchema, validationSchema } from 'services/masksAndValidators';

const validator = validationSchema([
  'name'
]);

class UpdateStakeholder extends React.Component {

  state = {
    address: ''
  }

  render() {

    const { onClickToCancel, defaultValues } = this.props;
    const { address } = this.state;

    return (
      <FormWrapper
        initialValues={filterObject(defaultValues, ['name', 'document', 'documentType', 'users'])}
        isInitialValid={true}
        validationSchema={validator}
        onSubmit={(values, actions) => {
          this.submitForm(values, () => {
            actions.setSubmitting(false);
          });
        }}
        onClickToCancel={onClickToCancel}
        render={({
          values,
          handleChange,
          isSubmitting
        }) => {

          const documentType = values.documentType;
          maskSchema[documentType].resolve(values.document);

          return (
            <>
              <Grid gapConfig={1} isMultiline>
                <Grid.Col is4>
                  <FormField label="Documento" labelInfo={`${(documentType || '').toUpperCase()}`}>
                    <Input
                      id="update-shipper-document-input"
                      name="document"
                      disabled={isSubmitting}
                      value={values.document}
                      disabled={true}
                      value={maskSchema[documentType].value}
                    />
                  </FormField>
                </Grid.Col>
                <Grid.Col>
                  <FormField label="Nome" labelInfo="*">
                    <Input
                      id="update-shipper-name-input"
                      name="name"
                      disabled={isSubmitting}
                      value={values.name}
                      onChange={handleChange}
                    />
                  </FormField>
                </Grid.Col>
              </Grid>

              <AddressFormFields
                value={address || defaultValues.address}
                onAddressChange={(addressObject) => {
                  this.setState({ address: addressObject });
                }}
                disabled={isSubmitting}
              />

              <Heading is6 hasUnderline hasBottomSpace>Contatos</Heading>

              <FieldRepeater
                name="users"
                render={(arrayHelpers) => (
                  <>
                    {(values.users || []).map((user, index) => {
                      return (
                        <Grid gapConfig={1} key={index}>
                          <Grid.Col is4>
                            <FormField label="Nome" labelInfo="*">
                              <Input
                                disabled={isSubmitting}
                                name={`users.${index}.name`}
                                value={user.name}
                                onChange={handleChange}
                                small
                              />
                            </FormField>
                          </Grid.Col>
                          <Grid.Col is4>
                            <FormField label="Email" labelInfo="*">
                              <Input
                                disabled={isSubmitting}
                                name={`users.${index}.email`}
                                value={user.email}
                                onChange={handleChange}
                                small
                              />
                            </FormField>
                          </Grid.Col>
                          <Grid.Col>
                            <FormField label="Telefone" labelInfo="*">
                              <Input
                                disabled={isSubmitting}
                                name={`users.${index}.phone`}
                                value={user.phone}
                                onChange={(event) => {
                                  maskSchema.phone.resolve(event.target.value);
                                  event.target.value = maskSchema.phone.value;
                                  handleChange(event);
                                }}
                                small
                              />
                            </FormField>
                          </Grid.Col>
                          <Grid.Col isNarrow isAlignedBottom>
                            <Button avoidTab isSmall onClick={() => arrayHelpers.remove(index)}>
                              <Icon name="fas fa-user-times" />
                            </Button>
                          </Grid.Col>
                        </Grid>
                      );
                    })}
                    <Button
                      isSmall
                      isLink
                      avoidTab
                      disabled={isSubmitting}
                      onClick={() => arrayHelpers.push({ name: '', email: '', phone: '' })}>
                      <Icon name="fas fa-user-plus" /> Convidar usuário
                    </Button>
                  </>
                )}
              />
            </>
          );
        }}
      />
    );
  }

  submitForm = (values, errorPostAction) => {

    const { address } = this.state;
    const { onSuccess, onError, defaultValues } = this.props;

    if (!checkObjectKeys(values, ['name', 'document'])) {
      callToast('warning', 'Os campos Nome e Documento são obrigatórios.');
      errorPostAction();
      return;
    }

    if (values.users) {
      const checkUsers = [...values.users].filter((user, index) => {
        if (!checkObjectKeys(user, ['name', 'email', 'phone'])) {
          callToast('warning', `Os campos Nome, Email e Telefone são obrigatórios para convidar um usuário.`);
          errorPostAction();
        } else {
          return user;
        }
      });
      if (checkUsers.length !== values.users.length) return;
    }

    const request = { ...values, address };

    updateStakeholder(request, defaultValues.id, 'shippers')
    .then(() => {
      callToast('success', 'Embarcador cadastrado com sucesso!');
      onSuccess(request);
    })
    .catch((error = {}) => {
      if (error.code == 'document-already-exists') {
        callToast('error', 'Você está tentando cadastrar um embarcador que já existe.');
      } else {
        callToast('error', 'Ocorreu um erro ao tentar cadastrar o embarcador. :(');
      }
      onError(error);
      errorPostAction();
    });

  }

}

UpdateStakeholder.defaultProps = {
  onSuccess: () => {},
  onError: () => {},
  defaultValues: {}
};

export default UpdateStakeholder;
