import React from 'react';

import {
  Grid,
  FormField,
  Icon,
  DateTimePicker
} from 'shared-components';

import { checkObjectKeys, filterObject } from 'shared-utils/utils';

import { delivery } from 'services/firestore';

import { callToast } from 'utils/utils';

import FormWrapper from '../components/FormWrapper';

const UpdateDeliveryEstimation = (props) => {

  const { onSuccess, onError, onClickToCancel, defaultValues } = props;

  const submitForm = (values, errorPostAction) => {

    if (!checkObjectKeys(values, ['estimation'])) {
      callToast('warning', 'O campo Previsão de entrega é obrigatório.');
      errorPostAction();
      return;
    }

    const request = values;

    delivery.update(defaultValues.id, request)
    .then(() => {
      callToast('success', 'Previsão de entrega atualizada com sucesso!');
      onSuccess(request);
    })
    .catch((error) => {
      callToast('error', 'Ocorreu um erro ao tentar atualizar a previsão de entrega. :(');
      onError(error);
      errorPostAction();
    });

  };

  return (
    <FormWrapper
      initialValues={filterObject(defaultValues, ['estimation'])}
      isInitialValid={true}
      onSubmit={(values, actions) => {
        submitForm(values, () => {
          actions.setSubmitting(false);
        });
      }}
      onClickToCancel={onClickToCancel}
      render={({
        values,
        setFieldValue,
        isSubmitting
      }) => {
        return (
          <Grid gapConfig={1}>
            <Grid.Col>
              <FormField label="Previsão de entrega" hasIconsLeft>
                <DateTimePicker
                  showTimeSelect
                  defaultValue={values.estimation}
                  disabled={isSubmitting}
                  onChange={(value) => setFieldValue('estimation', value)}
                />
                <Icon name="fas fa-calendar-day" isLeft isSmall />
              </FormField>
            </Grid.Col>
          </Grid>
        );
      }}
    />
  );

};

UpdateDeliveryEstimation.defaultProps = {
  onSuccess: () => {},
  onError: () => {},
  defaultValues: {}
};

export default UpdateDeliveryEstimation;
