import * as Yup from 'yup';
import IMask from 'imask';

import moment from 'moment';
import momentDuration from 'moment-duration-format';
momentDuration(moment);

export const mask = {
  phone: IMask.createMask({ mask: '(00) 00000-0000' }),
  duration: (value, type, format) => moment.duration(value, type).format(format)
};

const yupValidators = {
  phone: Yup.string()
  .matches(/^[0-9]{2}\s[0-9]{1}\s[0-9]{4}\s[0-9]{4}$/, {
    message: 'Telefone incorreto'
  })
  .required('Informe o número de telefone')
};

export const validator = {
  schema: Yup.object().shape(yupValidators),
  fields: (fields = []) => {
    let selected = {};
    fields.forEach((field) => {
      if (yupValidators[field]) {
        selected[field] = yupValidators[field];
      }
    });
    return Yup.object().shape(selected);
  }
};


