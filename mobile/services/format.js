import moment from 'moment';
import momentDuration from 'moment-duration-format';
momentDuration(moment);

export default {
  duration: (value, type) => moment.duration(value, type)
};
