import isEqual from 'lodash/isEqual';

export default function fetchAndRenderRoute({
  origin,
  destination,
  waypoints,
  apikey,
  onStart,
  onReady,
  onError,
  mode = 'DRIVING',
  language = 'en',
  optimizeWaypoints,
  directionsServiceBaseUrl = 'https://maps.googleapis.com/maps/api/directions/json',
  region,
}) {

  if (!origin || !destination) {
    return;
  }

  if (origin.latitude && origin.longitude) {
    origin = `${origin.latitude},${origin.longitude}`;
  }

  if (destination.latitude && destination.longitude) {
    destination = `${destination.latitude},${destination.longitude}`;
  }

  if (!waypoints || !waypoints.length) {
    waypoints = '';
  } else {
    waypoints = waypoints
      .map(waypoint => (waypoint.latitude && waypoint.longitude) ? `${waypoint.latitude},${waypoint.longitude}` : waypoint)
      .join('|');
  }

  if (optimizeWaypoints) {
    waypoints = `optimize:true|${waypoints}`;
  }

  onStart && onStart({
    origin,
    destination,
    waypoints: waypoints ? waypoints.split('|') : [],
  });

  fetchRoute(directionsServiceBaseUrl, origin, waypoints, destination, apikey, mode, language, region)
    .then(result => {
      // console.log(result);
      onReady && onReady(result);
    })
    .catch(errorMessage => {
      console.warn(`MapViewDirections Error: ${errorMessage}`); // eslint-disable-line no-console
      onError && onError(errorMessage);
    });


};


function decode(t, e) {
  for (var n, o, u = 0, l = 0, r = 0, d = [], h = 0, i = 0, a = null, c = Math.pow(10, e || 5); u < t.length;) {
    a = null, h = 0, i = 0;
    do a = t.charCodeAt(u++) - 63, i |= (31 & a) << h, h += 5; while (a >= 32);
    n = 1 & i ? ~(i >> 1) : i >> 1, h = i = 0;
    do a = t.charCodeAt(u++) - 63, i |= (31 & a) << h, h += 5; while (a >= 32);
    o = 1 & i ? ~(i >> 1) : i >> 1, l += n, r += o, d.push([l / c, r / c]);
  }

  return d = d.map(function(t) {
    return {
      latitude: t[0],
      longitude: t[1],
    };
  });
};


function fetchRoute(directionsServiceBaseUrl, origin, waypoints, destination, apikey, mode, language, region) {

  // Define the URL to call. Only add default parameters to the URL if it's a string.
  let url = directionsServiceBaseUrl;
  if (typeof (directionsServiceBaseUrl) === 'string') {
    url += `?origin=${origin}&waypoints=${waypoints}&destination=${destination}&key=${apikey}&mode=${mode.toLowerCase()}&language=${language}&region=${region}`;
  }

  return fetch(url)
    .then(response => response.json())
    .then(json => {

      if (json.status !== 'OK') {
        const errorMessage = json.error_message || 'Unknown error';
        return Promise.reject(errorMessage);
      }

      if (json.routes.length) {

        const route = json.routes[0];

        return Promise.resolve({
          distance: route.legs.reduce((carry, curr) => {
            return carry + curr.distance.value;
          }, 0) / 1000,
          duration: route.legs.reduce((carry, curr) => {
            return carry + curr.duration.value;
          }, 0) / 60,
          coordinates: decode(route.overview_polyline.points),
          fare: route.fare,
        });

      } else {
        return Promise.reject();
      }
    })
    .catch(err => {
      console.warn(
        'react-native-maps-directions Error on GMAPS route request',
        err
      );
    });
};
