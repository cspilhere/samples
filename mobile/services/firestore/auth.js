import { firebase } from '@react-native-firebase/auth';

const auth = {

  signIn: (phoneNumber) => {
    return new Promise((resolve, reject) => {
      phoneNumber = phoneNumber.replace(/\s/g,'');
      firebase.auth().signInWithPhoneNumber(phoneNumber)
      .then((confirmResult) => {
        if ((
          phoneNumber === '+5555555555555' ||
          phoneNumber === '+5544444444444'
        ) && __DEV__) {
          confirmResult.confirm('123456').then(resolve).catch(reject);
        } else {
          resolve();
        }
      })
      .catch((error) => {
        console.log(error);
        reject(error);
      });
    })
  },

  signOut: () => {
    return new Promise((resolve, reject) => {
      firebase.auth().signOut().then(resolve).catch(reject);
    });
  }

};

export default auth;
