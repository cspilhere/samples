import firebaseUtils from '@react-native-firebase/utils';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';

import moment from 'moment';
import groupBy from 'lodash/groupBy';

import { toArrayWithIds, toObjectWithId, formatDeliveryData } from './utilities';

import { GeoFirestore } from 'geofirestore';

const db = firestore();

const geoFirestore = new GeoFirestore(db);
const geoCollection = geoFirestore.collection('deliveries');

const delivery = {

  driversByPhoneNumber: (phoneNumber, eventCallback) => (
    new Promise((resolve, reject) => {
      db.collection('drivers')
      .where('phone', '==', phoneNumber)
      .where('active', '==', true)
      .onSnapshot((snapshot) => {
        let data = toArrayWithIds(snapshot);
        if (eventCallback && typeof eventCallback === 'function') {
          eventCallback(data);
        } else {
          resolve(data);
        }
      }, (error) => {
        reject(error);
      });
    })
  ),

  fetchAll: (phoneNumber, eventCallback) => (
    new Promise((resolve, reject) => {
      db.collection('deliveries')
      .where('d.driver.phone', '==', phoneNumber)
      .where('d.status', '==', 'ready')
      .onSnapshot((snapshot) => {
        let data = formatDeliveryData(toArrayWithIds(snapshot));
        if (eventCallback && typeof eventCallback === 'function') {
          eventCallback(data);
        } else {
          resolve(data);
        }
      }, reject);
    })
  ),

  fetch: (id, eventCallback) => (
    new Promise((resolve, reject) => {
      db.collection('deliveries')
      .doc(id)
      .onSnapshot((snapshot) => {
        if (eventCallback && typeof eventCallback === 'function') {
          eventCallback(toObjectWithId(snapshot));
        } else {
          resolve(toObjectWithId(snapshot));
        }
      }, reject);
    })
  ),

  fetchCompaniesByIds: (ids) => {
    if (!ids) return;
    const promises = [];
    ids.forEach((id) => promises.push(db.collection('companiesPublicData').doc(id).get()));
    return new Promise((resolve, reject) => {
      new Promise.all(promises)
      .then((values) => {
        resolve(toArrayWithIds({ docs: values }))
      })
      .catch(reject);
    });
  },

  fetchRecords: (deliveryId, driverId, eventCallback) => (
    new Promise((resolve, reject) => {
      db.collection('deliveries')
      .doc(deliveryId)
      .collection('records')
      .where('createdBy.id', '==', driverId)
      .orderBy('createdAt', 'desc')
      .onSnapshot((snapshot) => {
        const data = toArrayWithIds(snapshot).filter((item) => item.attachment);
        if (eventCallback && typeof eventCallback === 'function') {
          eventCallback(data);
        } else {
          resolve(data);
        }
      }, reject);
    })
  ),

  uploadFile: (imageData, progressCallback) => (
    new Promise((resolve, reject) => {
      const ref = storage().ref(imageData.fileRef);
      const path = imageData.filePath;
      const task = ref.putFile(path, { cacheControl: 'no-store' })
      .on(
        storage.TaskEvent.STATE_CHANGED,
        (snapshot) => {
          if (progressCallback) progressCallback(snapshot.bytesTransferred, snapshot.totalBytes);
          if (snapshot.state === storage.TaskState.SUCCESS) snapshot.ref.getDownloadURL().then(resolve);
          if (snapshot.state === snapshot.state.CANCELLED) reject('Operação cancelada, tente novamente.');
          if (snapshot.state === snapshot.state.ERROR) reject('Ocorreu um erro, tente novamente.');
        }, (error) => {
          task();
          reject(error);
        },
      );
    })
  ),

  createRecord: (body) => (
    new Promise((resolve, reject) => {
      db.collection('deliveries')
      .doc(body.deliveryId)
      .collection('records')
      .add(body)
      .then(resolve)
      .catch(reject);
    })
  ),

  update: (id, body) => (
    new Promise((resolve, reject) => {
      db.collection('deliveries')
      .doc(id)
      .set({
        d: {
          ...body,
          updatedAt: moment(moment()).toDate(),
        }
      }, { merge: true })
      .then(resolve)
      .catch(reject);
    })
  ),

  takerLabel: (value) => {
    let taker = {};
    switch (value) {
      case 0:
        taker.who = 'Remetente';
        taker.label = 'CIF';
        break
      case '0':
        taker.who = 'Remetente';
        taker.label = 'CIF';
        break
      case 1:
        taker.who = 'Expedidor';
        taker.label = 'Embarcadora';
        break
      case '1':
        taker.who = 'Expedidor';
        taker.label = 'Embarcadora';
        break
      case 2:
        taker.who = 'Recebedor';
        taker.label = 'Cliente';
        break
      case '2':
        taker.who = 'Recebedor';
        taker.label = 'Cliente';
        break
      case 3:
        taker.who = 'Destinatário';
        taker.label = 'FOB';
        break
      case '3':
        taker.who = 'Destinatário';
        taker.label = 'FOB';
        break
      default:
        taker.who = '';
        taker.label = '';
        break
    }
    return taker;
  }

};

export default delivery;
