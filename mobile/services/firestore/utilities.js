export const toArrayWithIds = (snapshot) => {
  return [...snapshot.docs.map((doc) => {
    let data = doc.data();
    if (!data) return false;
    if (data.hasOwnProperty('d')) {
      data = data.d;
    }
    return { ...data, id: doc.id };
  })];
};

export const toObjectWithId = (snapshot) => {
  let data = snapshot.data();
  if (!data) return false;
  if (data.hasOwnProperty('d')) {
    data = data.d;
  }
  return {
    ...data,
    id: snapshot.id
  };
};

export const formatDeliveryData = (data) => {
  return data.map((item) => {
    item.estimation = item.estimation ? item.estimation.toDate() : null;
    item.releasedAt = item.releasedAt ? item.releasedAt.toDate() : null;
    item.finishedAt = item.finishedAt ? item.finishedAt.toDate() : null;
    return ({
      ...item,
      latitude: item.destination.coordinates.lat,
      longitude: item.destination.coordinates.lng
    })
  });
};
