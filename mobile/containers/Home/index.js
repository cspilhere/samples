import React from 'react';

import { connect } from 'react-redux';
import * as actions from './actions';

import { GEOFENCES_RADIUS } from 'core/constants';

import {
  WrapperWithGeolocation,
  Container,
  TopBar,
  BottomDrawer,
  CardsList,
  CompanyTag,
  NearestDeliveryCard
} from './components';

import { Icon, Button } from 'components';

import Map from 'components/modules/Map';

import sortByDistance from 'sort-by-distance';

class Home extends React.PureComponent {

  state = {}

  componentDidMount() {
    const { currentUser } = this.props.screenProps;
    if (currentUser.phoneNumber) {
      this.props.fetchDeliveries(currentUser.phoneNumber);
    }
  }

  render() {

    const { loading, deliveries, markers, geofences } = this.props.reducer;
    const { currentCompany, currentUser } = this.props.screenProps;

    console.log(loading, deliveries, markers, geofences);

    const company = currentCompany;
    const user = currentUser;
    const driver = deliveries && deliveries[0] && deliveries[0].driver;
    const hasCompany = company && company.name;

    const extras = hasCompany ? { driver, companyId: company.id } : {};


    console.log('Home!!!!!!');

    return (
      <Container>
        <WrapperWithGeolocation
          extras={extras}
          render={({ currentPosition, enabled, startTracking, stopTracking }) => {

            let sortedData = [];

            let directionCoordinates = null;

            if (currentPosition) {
              sortedData = sortByDistance(currentPosition, deliveries, { yName: 'latitude', xName: 'longitude' });
            }

            if (sortedData && sortedData[0] && sortedData[0].destination.coordinates) {
              directionCoordinates = {
                latitude: sortedData[0].destination.coordinates.lat,
                longitude: sortedData[0].destination.coordinates.lng
              };
            }

            return (
              <>

                <TopBar
                  left={(
                    <Button align="left" style={{ paddingLeft: 15 }} onPress={() => this.props.navigation.navigate('General')}>
                      <Icon name="faBars" color="#FFF" size={16} />
                    </Button>
                  )}
                  right={(
                    <Button
                      align="right"
                      style={{ paddingRight: 15 }}
                      onPress={() => this.toggleTrackingState(enabled, startTracking, stopTracking)}>
                      <Icon name="faLocationArrow" color={enabled ? '#3dcc91' : '#1C2541'} family="fas" size={16} />
                    </Button>
                  )}
                />

                {currentPosition && enabled && (
                  <NearestDeliveryCard
                    data={sortedData[0]}
                    origin={currentPosition}
                    destination={directionCoordinates}
                    onPress={(data) => this.selectDelivery(data, currentPosition)}
                  />
                )}

                {hasCompany && <CompanyTag color={company.color}>Transportadora: {company.name}</CompanyTag>}

                <BottomDrawer>
                  <CardsList
                    data={sortedData}
                    onPress={(data) => this.selectDelivery(data, currentPosition)}
                  />
                </BottomDrawer>

                <Map
                  useGeofencesWithMarkers
                  geofencesRadius={GEOFENCES_RADIUS}
                  markers={markers}
                  showDeviceMarker={enabled}
                  currentPosition={currentPosition}
                />

              </>
            );
          }}
        />
      </Container>
    );
  }

  selectDelivery(data, location) {
    this.props.navigation.navigate('DeliveryDetails', { deliveryId: data.id, location });
  }

  toggleTrackingState = (enabled, startTracking, stopTracking) => {
    if (!enabled) {
      startTracking();
    } else {
      stopTracking();
    }
  }

};

export default connect((store) => ({
  reducer: store.deliveries
}), { ...actions })(Home);
