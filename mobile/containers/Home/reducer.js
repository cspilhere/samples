import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
  key: 'deliveries',
  storage,
  blacklist: ['loading', 'error']
};

const INITIAL_STATE = {
  deliveries: [],
  markers: [],
  geofences: [],
  loading: null,
  error: false,
};

export default persistReducer(persistConfig, (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {

    case 'FETCH_DELIVERIES':
      return state = {
        ...state,
        loading: true
      };

    case 'FETCH_DELIVERIES_SUCCESS':
      return state = {
        ...state,
        ...action.payload,
        loading: false,
        error: false
      };

    case 'FETCH_DELIVERIES_ERROR':
      return state = {
        ...state,
        loading: false,
        error: true
      };

    default:
      return state;
  }
});
