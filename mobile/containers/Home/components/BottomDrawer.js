import React from 'react';

import styled from 'styled-components/native';

import BlueBar from 'statics/media/blue-bar.svg';

import { ScrollView } from 'react-native-gesture-handler';

import BottomSheet from 'reanimated-bottom-sheet';

import utils from 'components/utils';
import colors from 'components/colors';

const Content = styled.View`
  flex: 1;
  border-radius: 3px;
  background-color: #fff;
  width: ${utils.deviceWidth - 20};
  height: 372px;
  position: absolute;
  z-index: 100;
  left: 10px;
  elevation: 4;
  margin-top: 6px;
`;

const Handler = styled.View.attrs({
  children: <BlueBar width={48} height={6} />
})`
  width: ${utils.deviceWidth - 20};
  height: 28px;
  border-radius: 3px;
  background-color: #FFF;
  justify-content: center;
  align-items: center;
  margin-left: 10px;
  elevation: 4;
`;




// const MyBottomSheet = (props: Props) => {
//   const { onClose } = props
//   const bottomSheetRef = useRef<BottomSheet | null>(null)
//   const callbackNode = useRef(new Animated.Value(1))

//   Animated.useCode(
//     Animated.onChange(
//       callbackNode.current,
//       Animated.block([
//         Animated.cond(
//           Animated.greaterOrEq(callbackNode.current, 1),
//           Animated.call([], () => {
//             onClose && onClose()
//           })
//         ),
//       ])
//     ),
//     [onClose]
//   )

//   return (
//     <React.Fragment>
//       <BottomSheet
//         ref={bottomSheetRef}
//         callbackNode={callbackNode.current}
//         initialSnap={0}
//         snapPoints={[0, MODAL_HEIGHT]}
//         renderHeader={...}
//         renderContent={...}
//       />
//     </React.Fragment>
//   )
// }



const BottomDrawer = ({
  children
}) => {
  return (
    <BottomSheet
      snapPoints={[400, 400, 34]}
      renderContent={() => {
        return (
          <Content>
            <ScrollView bounces={false}>
              {children}
            </ScrollView>
          </Content>
        );
      }}
      renderHeader={() => <Handler />}
      initialSnap={2}
    />
  );
};

BottomDrawer.propTypes = {};

export default BottomDrawer;
