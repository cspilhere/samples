import React from 'react';
import PropTypes from 'prop-types';

import GeolocationWrapper from 'components/modules/GeolocationWrapper';

const WrapperWithGeolocation = (props) => props.render(props);

WrapperWithGeolocation.propTypes = {};

export default GeolocationWrapper(WrapperWithGeolocation);
