import React from 'react';
import styled from 'styled-components/native';

import utils from 'components/utils';

import { Text } from 'components';

export const Container = styled.View`
  flex: 1;
`;

const CompanyTagContainer = styled.View`
  position: absolute;
  z-index: 10;
  bottom: 40px;
  left: 0;
  width: ${utils.deviceWidth};
  justify-content: center;
  align-items: center;
`;

export const CompanyTag = ({ children, color }) => (
  <CompanyTagContainer>
    <Text color="#FFF" tagMode tagColor={color}>{children}</Text>
  </CompanyTagContainer>
);

export { default as WrapperWithGeolocation } from './WrapperWithGeolocation';
export { default as TopBar } from './TopBar';
export { default as BottomDrawer } from './BottomDrawer';
export { default as CardsList } from './CardsList';
export { NearestDeliveryCard } from './CardsList';
