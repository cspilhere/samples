import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components/native';

import utils from 'components/utils';

import format from 'services/format';

import { Box, Text, Icon } from 'components';

import DeliveryCard from 'components/custom/DeliveryCard';

import { GOOGLE_API_KEY } from 'core/constants';

import getDirections from 'services/directions';

const Wrapper = styled.View`
  padding: 10px;
`;

const Container = styled.View`
  padding: 10px;
  z-index: 100;
`;

const DirectionsWrapper = styled.View`
  flex-direction: row;
  align-items: center;
`;

const CardsList = ({ data, onPress }) => {
  const cards = data.map((item) => {
    return (
      <Container key={item.id}>
        <DeliveryCard
          onPress={() => onPress(item)}
          truncate
          smallTag
          data={item}
        />
      </Container>
    );
  });
  return (
    <Wrapper>{cards}</Wrapper>
  );
};

CardsList.defaultProps = {
  onPress: () => {}
};

CardsList.propTypes = {};

export const NearestDeliveryCard = ({ data, onPress, origin, destination }) => {

  if (!data) return null;

  const [directions, setDirections] = useState(null);

  useEffect(() => {

    getDirections({
      origin,
      destination,
      apikey: GOOGLE_API_KEY,
      onReady: (result) => {
        // console.log(result);
        setDirections(result);
      },
    });

    return () => {
      setDirections(null);
    }
  }, [origin, destination]);

  let duration = null;
  let distance = null;

  let isLessThanOneMinute = false;

  if (directions) {

    duration = format.duration(directions.duration, 'minutes');

    let hours = duration.hours();
    hours = hours < 10 ? '0' + hours : hours;

    let minutes = duration.minutes();
    isLessThanOneMinute = minutes < 1;
    minutes = minutes < 10 ? '0' + minutes : minutes;

    duration = hours + ':' + minutes;

    distance = directions.distance * 1000;
    distance = distance > 1000 ? distance + 'Km' : distance + 'm';

  }

  return (
    <Box
      elevated
      style={{
        position: 'absolute',
        zIndex: 10,
        left: 10,
        top: 60,
        width: utils.deviceWidth - 20
      }}>
      <DeliveryCard
        onPress={() => onPress(data)}
        truncate
        smallTag
        lite
        data={data}
        headerRight={distance && duration && (
          <DirectionsWrapper>
            <Icon name="faRoad" size={14} />
            <Text bold style={{ marginLeft: 4, marginRight: 10 }}>
              {distance}
            </Text>
            <Icon name="faStopwatch" size={14} />
            <Text bold style={{ marginLeft: 4 }}>
              {isLessThanOneMinute ? 'menos de 1 minuto' : duration}
            </Text>
          </DirectionsWrapper>
        )}
      />
    </Box>
  );
};

NearestDeliveryCard.defaultProps = {
  onPress: () => {}
};

export default CardsList;
