import React from 'react';
import PropTypes from 'prop-types';

import { Dimensions } from 'react-native';

import styled from 'styled-components/native';
import colors from 'components/colors';
import Logo from 'statics/media/levalog-logo.svg';

const { width, height } = Dimensions.get('window');

const TopBarContainer = styled.View`
  position: absolute;
  z-index: 1;
  top: 10px;
  left: 10px;
  width: ${width - 20};
  height: 38px;
  background-color: ${colors.primary};
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  border-radius: 3px;
  elevation: 4;
`;

const TopBarLeft = styled.View`
  width: 30px;
  border-radius: 4px;
  justify-content: center;
  align-items: flex-start;
`;

const TopBarRight = styled.View`
  width: 30px;
  border-radius: 4px;
  justify-content: center;
  align-items: flex-end;
`;

const TopBar = (props) => {
  return (
    <TopBarContainer>
      <TopBarLeft>{props.left}</TopBarLeft>
      <Logo width={100} height={20} />
      <TopBarRight>{props.right}</TopBarRight>
    </TopBarContainer>
  );
};

TopBar.propTypes = {
  left: PropTypes.element,
  right: PropTypes.element
};

export default TopBar;
