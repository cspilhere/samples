export const fetchDeliveries = (phoneNumber) => {
  return {
    type: 'FETCH_DELIVERIES',
    phoneNumber,
    meta: {
      retry: true
    }
  };
};
