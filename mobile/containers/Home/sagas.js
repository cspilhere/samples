import { eventChannel } from 'redux-saga';
import { takeEvery, put, take, call, all } from 'redux-saga/effects';

import BackgroundGeolocation from 'react-native-background-geolocation';

import { deliveries } from 'services/firestore';

import { GEOFENCES_RADIUS } from 'core/constants';

function* fetchDeliveries(action) {
  console.log('fetchDeliveries');
  const createChannel = () => eventChannel((emit) => {
    deliveries.fetchAll(action.phoneNumber, emit).catch((error) => emit(new Error(error)));
    return () => {};
  });
  const channel = yield call(createChannel);
  while (true) {
    try {
      const deliveries = yield take(channel);
      const markers = [];
      const geofences = [];
      deliveries.forEach((item) => {
        if (
          item.destination &&
          item.destination.coordinates &&
          item.destination.coordinates.latitude &&
          item.destination.coordinates.longitude
        ) {
          markers.push({
            coordinates: {
              latitude: item.destination.coordinates.latitude,
              longitude: item.destination.coordinates.longitude
            },
            label: item.number,
            identifier: item.id,
            data: item
          });
          geofences.push({
            identifier: item.id,
            radius: GEOFENCES_RADIUS,
            latitude: item.destination.coordinates.latitude,
            longitude: item.destination.coordinates.longitude,
            notifyOnEntry: true,
            notifyOnExit: true
          });
        }
      });
      if (geofences.length > 0) BackgroundGeolocation.addGeofences(geofences);
      yield put({ type: 'FETCH_DELIVERIES_SUCCESS', payload: { deliveries, markers, geofences } });
    } catch (error) {
      console.log(error);
      yield put({ type: 'FETCH_DELIVERIES_ERROR' });
    };
  };
};

export default function* root() {
  yield all([
    takeEvery('FETCH_DELIVERIES', fetchDeliveries),
  ]);
};
