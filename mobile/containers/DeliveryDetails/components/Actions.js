import React from 'react';
import PropTypes from 'prop-types';

import ImagePicker from 'react-native-image-picker';

import styled from 'styled-components/native';

import { Button, Text, Grid, Col  } from 'components';

import colors from 'components/colors';

const Container = styled.View`
  flex-direction: row;
  padding-top: 5px;
`;

const imagePickerOptions = {
  title: 'Selecione uma imagem',
  takePhotoButtonTitle: 'Usar câmera',
  chooseFromLibraryButtonTitle: 'Buscar na galeria',
  cancelButtonTitle: 'Cancelar',
  mediaType: 'photo',
  storageOptions: {
    skipBackup: true,
    waitUntilSaved: true,
    path: 'Levalog Imagens'
  }
};

const callImagePicker = (extra) => {
  return new Promise((resolve, reject) => {
    ImagePicker.showImagePicker(imagePickerOptions, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
        reject('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        reject(response.error);
      } else {
        resolve({ ...response, ...extra });
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
      }
    });
  });
};

const Actions = ({ onPress, uploading }) => {
  return (
    <Container>
      <Grid>
        <Col>
          <Button disabled={uploading} primary onPress={() => onPress(callImagePicker({ type: 'gathering' }))}>
            <Text size={14} color="#FFF">Coleta</Text>
          </Button>
        </Col>
        <Col style={{ width: 4 }} />
        <Col>
          <Button disabled={uploading} danger onPress={() => onPress(callImagePicker({ type: 'damage' }))}>
            <Text size={14} color="#FFF">Avaria</Text>
          </Button>
        </Col>
        <Col style={{ width: 4 }} />
        <Col>
          <Button disabled={uploading} success onPress={() => onPress(callImagePicker({ type: 'receipt' }))}>
            <Text size={14} color="#FFF">Comprovante</Text>
          </Button>
        </Col>
      </Grid>
    </Container>
  );
};

Actions.propTypes = {};

export default Actions;
