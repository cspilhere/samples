import { eventChannel } from 'redux-saga';
import { takeEvery, takeLatest, select, put, take, call, all } from 'redux-saga/effects';

import { deliveries } from 'services/firestore';

import moment from 'moment';

function* selectDelivery({ deliveryId }) {
  console.log('selectDelivery');
  try {
    const deliveries = yield select((data) => data.deliveries.deliveries);
    const delivery = deliveries.filter((item) => item.id === deliveryId);
    if (delivery.length < 1) yield put({ type: 'SELECT_DELIVERY_EMPTY' });
    if (delivery.length > 0) yield put({ type: 'SELECT_DELIVERY_SUCCESS', payload: delivery[0] });
  } catch (error) {
    console.log(error);
    yield put({ type: 'SELECT_DELIVERY_ERROR', payload: error });
  };
};

function* fetchRecords({ deliveryId, driverId }) {
  console.log('fetchRecords');
  const createChannel = () => eventChannel((emit) => {
    deliveries.fetchRecords(deliveryId, driverId, emit).catch((error) => emit(new Error(error)));
    return () => {};
  });
  const channel = yield call(createChannel);
  while (true) {
    try {
      const response = yield take(channel);
      yield put({ type: 'FETCH_RECORDS_SUCCESS', payload: response });
    } catch (error) {
      console.log(error);
      yield put({ type: 'FETCH_RECORDS_ERROR', payload: error });
    };
  };
};

function* createRecord({ imageData, user, progressCallback }) {
  try {
    const delivery = yield select(data => data.delivery.delivery);
    const recordData = {
      automatic: false,
      clientId: delivery.client.id,
      companyId: delivery.companyId,
      deliveryId: delivery.id,
      description: 'Imagem enviada pelo APP do Motorista.',
      linkedTo: 'CTe: ' + delivery.billOfLading,
      shipperId: delivery.shipper.id,
      type: imageData.type,
      location: imageData.location,
      createdBy: {
        id: user.uid,
        displayName: user.displayName,
        email: user.email,
        phoneNumber: user.phoneNumber
      },
      createdAt: moment().toDate(),
    };
    const attachmentUrl = yield call(deliveries.uploadFile, imageData, progressCallback);
    recordData.attachment = { url: attachmentUrl, type: 'image/jpeg' };
    const response = yield call(deliveries.createRecord, recordData);
    yield put({ type: 'CREATE_RECORD_SUCCESS', payload: response });
  } catch (error) {
    yield put({ type: 'CREATE_RECORD_ERROR', payload: error });
  };
};

export default function* root() {
  yield all([
    takeEvery('FETCH_DELIVERIES_SUCCESS', selectDelivery),
    takeLatest('SELECT_DELIVERY', selectDelivery),
    takeLatest('FETCH_RECORDS', fetchRecords),
    takeLatest('CREATE_RECORD', createRecord)
  ]);
};
