import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
  key: 'delivery',
  storage,
  blacklist: ['loading', 'loadingRecords', 'error']
};

const INITIAL_STATE = {
  delivery: null,
  records: [],
  loading: true,
  loadingRecords: null,
  uploading: null,
  error: null
};

export default persistReducer(persistConfig, (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {

    /**
     * Select delivery
     */

    case 'SELECT_DELIVERY':
      return state = {
        ...state,
        loading: true,
        error: null
      };

    case 'SELECT_DELIVERY_EMPTY':
      return state = {
        ...state,
        loading: false,
        error: {
          code: 'no-content',
          message: 'Delivery not found'
        }
      };

    case 'SELECT_DELIVERY_SUCCESS':
      return state = {
        ...state,
        loading: false,
        error: null,
        delivery: action.payload
      };

    case 'SELECT_DELIVERY_ERROR':
      return state = {
        ...state,
        loading: false,
        error: true
      };

    /**
     * Fetch records
     */

    case 'FETCH_RECORDS':
      return state = {
        ...state,
        loadingRecords: true
      };

    case 'FETCH_RECORDS_SUCCESS':
      return state = {
        ...state,
        loadingRecords: false,
        records: action.payload
      };

    case 'FETCH_RECORDS_ERROR':
      return state = {
        ...state,
        loadingRecords: false,
        error: true
      };

    /**
     * Create record
     */

    case 'CREATE_RECORD':
      return state = {
        ...state,
        uploading: true
      };

    case 'CREATE_RECORD_SUCCESS':
      return state = {
        ...state,
        uploading: false
      };

    case 'CREATE_RECORD_ERROR':
      return state = {
        ...state,
        uploading: false,
        error: action.payload
      };

    /**
     * Reset delivery state
     */

    case 'RESET_DELIVERY_STATE':
      return state = INITIAL_STATE;

    default:
      return state;
  }
});
