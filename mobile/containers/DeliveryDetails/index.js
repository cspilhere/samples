import React from 'react';

import { connect } from 'react-redux';
import * as actions from './actions';

import colors from 'components/colors';

import { Text, AppViewWrapper, AppHeader, AppContent, toast } from 'components';

import { Actions } from './components';

import DeliveryCard from 'components/custom/DeliveryCard';
import RecordsList from 'components/custom/RecordsList';
import OperationIndicator from 'components/custom/OperationIndicator';

import { uuid } from 'services/utils';

import Modal from 'react-native-modal';
import ImageViewer from 'react-native-image-zoom-viewer';

class Delivery extends React.Component {

  toast = null

  componentDidMount() {
    const { params } = this.props.navigation.state;
    const { currentUser } = this.props.screenProps;
    this.props.selectDelivery(params.deliveryId);
    this.props.fetchRecords(params.deliveryId, currentUser.uid);
  }

  getSnapshotBeforeUpdate() {
    return null;
  }

  componentDidUpdate() {
    if (this.props.reducer.error && this.props.reducer.error.code == 'no-content') {
      this.props.navigation.navigate('Home');
    }
  }

  componentWillUnmount() {
    this.toast = null;
  }

  render() {

    const { delivery, records, loading, loadingRecords, uploading } = this.props.reducer;

    console.log('Delivery', loading, uploading);

    // const images = [...data].map((image) => image.attachmentUrl ? ({ url: image.attachmentUrl, freeHeight: true }) : false);
    if (loading || !delivery) return null;

    return (
      <AppViewWrapper>

        <AppHeader
          align="left"
          backgroundColor={colors.primaryDark}
          onPressBackButton={this.props.navigation.goBack}>
          <Text size={16} bold color="#FFF">Detalhes da entrega</Text>
        </AppHeader>

        <AppContent>

          <DeliveryCard data={delivery} />

          {/* <Modal
            isVisible={modalIsVisible}
            onBackButtonPress={() => toggleModal(false)}
            onBackdropPress={() => toggleModal(false)}>
              <ImageViewer
                enableSwipeDown
                index={imageIndex}
                saveToLocalByLongPress={false}
                backgroundColor="transparent"
                onChange={selectImageIndex}
                onCancel={() => toggleModal(false)}
                onClick={() => toggleModal(false)}
                imageUrls={images}
              />
          </Modal> */}

          <RecordsList data={records} alreadyFetched={loadingRecords !== null} loading={loading} />

          <OperationIndicator
            visible={uploading}
            description={`Enviando imagem, aguarde...`}
          />

          <Actions
            uploading={uploading}
            onPress={this.uploadFile}
          />

        </AppContent>

      </AppViewWrapper>
    );
  }

  uploadFile = (promise) => {
    promise.then((response) => {

      const { delivery } = this.props.reducer;
      const { params } = this.props.navigation.state;
      const { currentUser } = this.props.screenProps;

      const source = { uri: response.uri };
      const fileUri = response.uri;
      const type = response.type;
      const ext = fileUri.split('.').pop();
      const fileRef = `${delivery.companyId}/${delivery.id}/${uuid()}.${ext}`;

      this.props.createRecord({
        type,
        fileUri,
        filePath: response.path,
        fileName: response.fileName,
        fileRef,
        location: params.location,
      }, currentUser, (loaded, total) => {
        // console.log(loaded, total);
      });

    })
    .catch((error) => {
      console.log(error);
    });
  }

};

export default connect((store) => ({
  reducer: store.delivery
}), { ...actions })(Delivery);
