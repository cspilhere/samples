export const selectDelivery = (deliveryId) => {
  return {
    type: 'SELECT_DELIVERY',
    deliveryId
  };
};

export const fetchRecords = (deliveryId, driverId) => {
  return {
    type: 'FETCH_RECORDS',
    deliveryId,
    driverId,
    meta: {
      retry: true
    }
  };
};

export const createRecord = (imageData, user, progressCallback) => {
  return {
    type: 'CREATE_RECORD',
    imageData,
    user,
    progressCallback,
    meta: {
      retry: true
    }
  };
};

export const resetState = (payload) => {
  return {
    type: 'RESET_DELIVERY_STATE'
  };
};
