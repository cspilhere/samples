import React from 'react';
import styled from 'styled-components/native';

import colors from 'components/colors';

import Logo from 'statics/media/levalog-logo.svg';

import LinearGradient from 'react-native-linear-gradient';

import {
  Grid,
  Text,
  Col,
  Input,
  Icon
} from 'components';

export const Container = styled(LinearGradient).attrs({
  start: { x: 0, y: 0 },
  end: { x: 0, y: 1 },
  colors: [colors.primary, colors.primaryDark]
})`
  background-color: ${colors.primaryDark};
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const LogoContainer = styled.View`
  width: 100%;
  justify-content: center;
  align-items: center;
  flex: 1;
`;

export const LevaLogLogo = () => <LogoContainer><Logo width={200} height={35} /></LogoContainer>;

export const FormBox = styled.View`
  width: 100%;
  justify-content: flex-end;
  align-items: center;
  flex: 1;
  z-index: 1;
`;

export const KeyboardAvoidedArea = styled.KeyboardAvoidingView`
  width: 100%;
  height: 110px;
  justify-content: center;
  align-items: center;
  padding-top: 17px;
  padding-bottom: 22px;
  background-color: ${colors.primary};
`;

export const FormFieldDescription = styled.Text`
  width: 100%;
  text-align: center;
  margin-bottom: 12px;
  font-size: 15px;
  color: #FFF;
`;

const PhoneInput = styled(Input)`
  width: 160px;
  height: 36px;
  font-size: 18px;
  margin-bottom: 12px;
  padding-left: 13px;
  padding-bottom: 6px;
  background-color: #FFF;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
`;

const Button = styled.TouchableOpacity`
  height: 36px;
  padding-left: 10px;
  padding-right: 10px;
  justify-content: center;
  align-items: center;
  background-color: ${({ disabled }) => !disabled ? colors.secondary : colors.gray3};
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
`;

export const FormField = ({ onChangeText, values, onPress, isLoading }) => {

  return (
    <Grid>
      <Col style={{ width: 160, height: 37 }}>
        <PhoneInput
          onChangeText={onChangeText}
          keyboardType="phone-pad"
          placeholder="DDD 000 000 000"
          mask={'[00] [0] [0000] [0000]'}
          value={values.phone}
        />
      </Col>
      <Col style={{ width: 60 }}>
        <Button onPress={!isLoading ? onPress : null} disabled={isLoading}>
          <Text color="#FFF" bold size={14}>Entrar</Text>
        </Button>
      </Col>
    </Grid>
  );
};
