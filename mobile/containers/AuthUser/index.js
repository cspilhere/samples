import React from 'react';

import { AuthContext } from 'containers/AuthManager';
import NetInfo from '@react-native-community/netinfo';

import { auth } from 'services/firestore';

import {
  StatusBar,
  ActivityIndicator,
} from 'react-native';

import {
  Form,
  BlockScreenContainer,
  blockScreen,
  ToastContainer,
  toast,
} from 'components';

import {
  Container,
  LevaLogLogo,
  FormBox,
  KeyboardAvoidedArea,
  FormFieldDescription,
  FormField
} from './components';

import colors from 'components/colors';

import { validator } from 'services/masksAndValidators';

export default class Auth extends React.Component {

  static contextType = AuthContext;

  static navigationOptions = {
    header: null
  }

  toastFromSubmit = null
  toastValidation = null

  state = {
    isLoading: false,
    isSuccess: false,
    message: '',
    phoneNumber: '',
    isConnected: true
  }

  componentDidMount() {
    this.netInfoStatus = NetInfo.addEventListener((state) => {
      this.setState({
        isConnected: state.isConnected
      });
    });
  }

  componentWillUnmount() {
    this.netInfoStatus();
    this.toastFromSubmit = null;
    this.toastValidation = null;
  }

  render() {

    const { isLoading, isConnected } = this.state;

    return (
      <>
        <ToastContainer />

        <Container>

          <StatusBar backgroundColor={colors.primary} />

          <BlockScreenContainer />

          <FormBox>

            <LevaLogLogo />

            <Form
              initialValues={{ phone: '' }}
              validationSchema={validator.fields(['phone'])}
              onSubmit={({ phone }) => {

                if (this.toastValidation) this.toastValidation.close(null, true);

                if (this.toastFromSubmit) this.toastFromSubmit.close(null, true);

                const working = blockScreen({
                  content: <ActivityIndicator size="large" color={'#1C99FE'} />
                });

                auth.signIn('+55' + phone)
                .then(() => {
                  working.close();
                  if (this.toastFromSubmit) this.toastFromSubmit.close(null, true);
                  this.props.navigation.navigate('App');
                })
                .catch((error) => {
                  console.log(error);
                  if (this.toastFromSubmit) this.toastFromSubmit.close(null, true);
                  working.close();
                  this.toastFromSubmit = toast({
                    content: isConnected ? (
                      'Ocorreu um erro ao tentar entrar no sistema, tente novamente.'
                    ) : (
                      'Você está sem conexão e por isso não será possível entrar no sistema.'
                    ),
                    state: 'danger',
                    timer: 5000,
                    onClose: () => this.toastFromSubmit = null
                  });
                });

              }}>
              {({ values, errors, handleChange, handleSubmit }) => (
                <KeyboardAvoidedArea>

                  <FormFieldDescription>Qual é o número do seu telefone?</FormFieldDescription>

                  <FormField
                    onChangeText={(value) => handleChange('phone')(value)}
                    values={values}
                    onPress={() => {
                      if (errors.phone || !values.phone) {
                        this.toastValidation = toast({
                          content: 'Informe um número de telefone válido.',
                          state: 'warning',
                          timer: 5000,
                          onClose: () => this.toastValidation = null
                        });
                      } else {
                        handleSubmit();
                      }
                    }}
                    isLoading={isLoading}
                  />

                </KeyboardAvoidedArea>
              )}
            </Form>

          </FormBox>

        </Container>
      </>
    );
  }

};
