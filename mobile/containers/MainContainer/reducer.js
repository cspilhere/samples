import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
  key: 'main',
  storage,
  blacklist: ['loading', 'error']
};

const INITIAL_STATE = {
  driverData: [],
  loading: null,
  error: false,
  currentCompany: null
};

export default persistReducer(persistConfig, (state = INITIAL_STATE, action = {}) => {

  switch (action.type) {

    case 'GET_DRIVER_DATA':
      return state = {
        ...state,
        loading: true
      };

    case 'GET_DRIVER_DATA_SUCCESS':
      return state = {
        ...state,
        driverData: action.payload,
        loading: false,
        error: false
      };

    case 'GET_DRIVER_DATA_EMPTY':
      return state = {
        ...state,
        loading: false,
        error: {
          code: 'no-content',
          message: 'Driver data not found'
        }
      };

    case 'GET_DRIVER_DATA_ERROR':
      return state = {
        ...state,
        loading: false,
        error: true
      };

    case 'SELECT_COMPANY':
      return state = {
        ...state,
        currentCompany: action.payload
      };

    default:
      return state;
  }
});
