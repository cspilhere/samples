import { eventChannel } from 'redux-saga';
import { put, take, call, all, select, takeLatest } from 'redux-saga/effects';

import { deliveries } from 'services/firestore';

function* getDriverData(action) {
  console.log('getDriverData');
  const createDriversChanel = () => eventChannel((emit) => {
    deliveries.driversByPhoneNumber(action.phoneNumber, emit).catch((error) => emit(new Error(error)));
    return () => {};
  });
  const driversChanel = yield call(createDriversChanel);
  while (true) {
    try {
      const drivers = yield take(driversChanel);
      if (drivers.length < 1) return yield put({ type: 'GET_DRIVER_DATA_EMPTY' });
      const companies = yield call(deliveries.fetchCompaniesByIds, [...drivers].map((driver) => driver.companyId));
      const enhancedData = [...drivers].map((driver) => {
        driver.company = companies.filter((company) => company.id === driver.companyId)[0];
        return driver;
      });
      const company = yield select((data) => data.main.currentCompany);
      yield all([
        put({ type: 'GET_DRIVER_DATA_SUCCESS', payload: enhancedData }),
        !company && enhancedData && put({ type: 'SELECT_COMPANY', payload: enhancedData[0].company })
      ]);
    } catch (error) {
      yield put({ type: 'GET_DRIVER_DATA_ERROR' });
      console.log(error);
    };
  };
};

export default function* root() {
  yield all([
    takeLatest('GET_DRIVER_DATA', getDriverData)
  ]);
};
