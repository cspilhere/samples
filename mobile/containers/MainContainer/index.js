import React from 'react';

import NetInfo from '@react-native-community/netinfo';
import BackgroundGeolocation from 'react-native-background-geolocation';

import { connect } from 'react-redux';
import * as actions from './actions';

import {
  StatusBar,
  View
} from 'react-native';

import Navigator from './Navigator';

import colors from 'components/colors';

import {
  ToastContainer
} from 'components';

import { AuthContext } from 'containers/AuthManager';

import AsyncStorage from '@react-native-community/async-storage';
const persistenceKey = "persistenceKey"

const persistNavigationState = async (navState) => {
  try {
    await AsyncStorage.setItem(persistenceKey, JSON.stringify(navState))
  } catch(err) {}
}

const loadNavigationState = async () => {
  const jsonString = await AsyncStorage.getItem(persistenceKey)
  return JSON.parse(jsonString)
}

const persistenceKeyConfig = __DEV__ ? {
  persistNavigationState: persistNavigationState,
  loadNavigationState: loadNavigationState
} : {};

class MainContainer extends React.Component {

  static contextType = AuthContext;

  componentDidMount() {
    this.context.onUserAuthChanged(({ isSignedIn, currentUser }) => {
      if (isSignedIn && currentUser && currentUser.phoneNumber) this.props.getUserByPhoneNumber(currentUser.phoneNumber);
    });
    this.netInfoStatus = NetInfo.addEventListener((state) => {
      console.log('Connection type', state.type);
      console.log('Is connected?', state.isConnected);
    });
  }

  componentWillUnmount() {
    this.netInfoStatus();
  }

  render() {

    const {
      loading,
      driverData,
      currentCompany,
    } = this.props.reducer;

    const network = this.props.network;

    const { currentUser, clearState } = this.context;

    if (loading === null || loading === true) return null;

    console.log('MainContainer', currentUser, network);

    return (
      <View style={{ flex: 1 }}>

        <StatusBar backgroundColor={colors.primary} />

        <ToastContainer />

        <Navigator
          uriPrefix="/levalog"
          {...persistenceKeyConfig}
          screenProps={{
            driverData,
            currentCompany,
            currentUser,
            logout: () => {
              clearState();
              BackgroundGeolocation.stop();
              this.props.logout();
            }
          }}
        />
      </View>
    );
  }

};

export default connect((store) => ({
  reducer: store.main,
  network: store.network
}), { ...actions })(MainContainer);
