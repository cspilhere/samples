import { persistor } from 'core/store'

import { auth } from 'services/firestore';

export const getUserByPhoneNumber = (phoneNumber) => {
  return {
    type: 'GET_DRIVER_DATA',
    phoneNumber,
    meta: {
      retry: true
    }
  };
};

export const logout = () => {
  persistor.purge()
  .then(() => {
    auth.signOut();
  });
  return { type: 'RESET' };
};
