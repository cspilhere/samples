import { createStackNavigator, createAppContainer } from 'react-navigation';

import ScreenFocusWrapper from 'components/modules/ScreenFocusWrapper';

import Home from 'containers/Home';
import General from 'containers/General';
import DeliveryDetails from 'containers/DeliveryDetails';

const Stack = createStackNavigator({
  Home: __DEV__ ? ScreenFocusWrapper(Home) : Home,
  General: ScreenFocusWrapper(General),
  DeliveryDetails: ScreenFocusWrapper(DeliveryDetails)
}, {
  initialRouteName: 'Home',
  headerTransitionPreset: 'fade-in-place',
  defaultNavigationOptions: {
    header: null
  }
});

export default createAppContainer(Stack);
