import React from 'react';

import { View, ActivityIndicator } from 'react-native';

import { firebase } from '@react-native-firebase/auth';

export const AuthContext = React.createContext({});

class AuthManager extends React.PureComponent {

  state = {
    currentUser: null,
    isWaitingForUserData: true
  }

  onUserAuthChanged = (callback) => (
    firebase.auth().onAuthStateChanged((currentUser) => {
      if (currentUser) {
        this.setState({
          currentUser: currentUser.toJSON(),
          isWaitingForUserData: false
        }, () => {
          if (callback && typeof callback === 'function') callback({ isSignedIn: !!currentUser, currentUser: currentUser.toJSON() });
        });
      } else {
        if (!currentUser) {
          console.log('User are logged out');
          if (callback && typeof callback === 'function') callback({ isSignedIn: false, currentUser: null });
        }
      }
    })
  )

  clearState = () => {
    this.setState({
      currentUser: null,
      isWaitingForUserData: true
    });
  }

  render() {
    return (
      <AuthContext.Provider
        value={{
          onUserAuthChanged: this.onUserAuthChanged,
          clearState: this.clearState,
          userIsReady: !this.state.isWaitingForUserData && this.state.currentUser ? true : false,
          ...this.state
        }}>
        {this.props.children}
      </AuthContext.Provider>
    );
  }

};

export class Redirect extends React.Component {
  static contextType = AuthContext;
  componentDidMount() {
    this.context.onUserAuthChanged(({ isSignedIn }) => {
      if (isSignedIn) {
        this.props.navigation.navigate('App');
      } else {
        this.props.navigation.navigate('Login');
      }
    });
  }

  render() {
    return (
      <View
        style={{
          width: '100%',
          height: '100%',
          position: 'absolute',
          top: 0,
          left: 0,
          zIndex: 999999,
          justifyContent: 'center',
          alignItems: 'center'
        }}>
        <ActivityIndicator size="large" color={'#FFFFFF'} />
      </View>
    );
  }

};

export default AuthManager;
