import React from 'react';

import NetInfo from "@react-native-community/netinfo";

import {
  Header,
  Content,
  Credits
} from './components';

import { ActivityIndicator } from 'react-native';

import DisplayUser from './components/DisplayUser';
import ConnectivityStatus from './components/ConnectivityStatus';

import colors from 'components/colors';

import {
  BlockScreenContainer,
  blockScreen,
  Icon,
  Text,
  Button,
  AppViewWrapper,
  AppHeader,
  AppContent
} from 'components';

import { auth } from 'services/firestore';

class CompanySelector extends React.Component {

  unsubscribe = null

  state = {
    isConnected: null
  }

  componentDidMount() {
    this.unsubscribe = NetInfo.addEventListener(({ isConnected, isInternetReachable }) => {
      this.setState({ isConnected: isConnected && isInternetReachable });
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  render() {

    const { currentCompany, currentUser, companies, allDeliveries, logout } = this.props.screenProps;
    const { isConnected } = this.state;

    const company = currentCompany;
    const user = currentUser;

    console.log('General', currentCompany, currentUser, companies, allDeliveries);

    return (
      <AppViewWrapper>

        <AppHeader
          align="left"
          backgroundColor={colors.primaryDark}
          onPressBackButton={this.props.navigation.goBack}>
          <Text size={16} bold color="#FFF">Configurações</Text>
        </AppHeader>

        <AppContent>

          <BlockScreenContainer />

          <Header>
            <DisplayUser currentUser={currentUser} />
          </Header>

          <ConnectivityStatus isConnected={isConnected} />

          <Content>
            <Button
              danger
              onPress={logout}
              render={({ labelColorSuggestion }) => (
                <>
                  <Text
                    style={{ marginRight: 6 }}
                    color={labelColorSuggestion}
                    size={14}>
                    Sair do Levalog
                  </Text>
                  <Icon color={labelColorSuggestion} name="faSignOut" />
                </>
              )}
            />
            {/*
              Tracking ligado/desligado - total de entregas para fazer -
              empresa selecionada e empresas disponiveis
              stats (
                se tem algo na fila esperando para ser sincronizado
                Totalizadores de entregas,
                estatisticas básicas,
                data de inicio da viagem,
                dias para terminar,
                etc
              )
            */}
          </Content>

          <Credits />

        </AppContent>
      </AppViewWrapper>
    );
  }

};

export default CompanySelector;
