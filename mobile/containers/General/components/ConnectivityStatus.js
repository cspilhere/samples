import React from 'react';
import PropTypes from 'prop-types';

import {
  ConnectivityWrapper,
  ConnectivityDescription,
  ConnectivityIcon
} from './';

const ConnectivityStatus = ({ isConnected }) => {
  return (
    <ConnectivityWrapper>
      <ConnectivityDescription isConnected={isConnected}>
        {isConnected ? 'Você está conectado' : 'Sem conexão'}
      </ConnectivityDescription>
      <ConnectivityIcon isConnected={isConnected} />
    </ConnectivityWrapper>
  );
};

ConnectivityStatus.propTypes = {

};

export default ConnectivityStatus;
