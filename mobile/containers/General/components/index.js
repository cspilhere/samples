import React from 'react';
import DeviceInfo from 'react-native-device-info';

import styled from 'styled-components/native';

import { Icon, Text } from 'components';

import colors from 'components/colors';

export const Container = styled.View`
  flex: 1;
  background-color: ${colors.lightGray};
  padding: 20px 15px;
`;

export const Header = styled.View``;

export const Content = styled.View`
  flex: 1;
  justify-content: flex-end;
  padding-top: 15px;
`;

export const Credits = styled.Text.attrs({
  children: `© Levalog 2019 - Driver APP V${DeviceInfo.getVersion()}`
})`
  color: ${colors.gray3};
  text-align: center;
  padding-top: 20px;
  font-size: 11px;
  font-style: italic;
`;

export const DisplayUserWrapper = styled.View`
  justify-content: center;
  align-items: center;
`;

export const UserAvatar = styled.Image``;

export const UserAvatarPlaceholder = styled.View.attrs({
  children: <Icon name="faSteeringWheel" size={48} color={colors.gray3} />
})`
  width: 80px;
  height: 80px;
  justify-content: center;
  align-items: center;
  border-radius: 100px;
  background-color: ${colors.lightGray1};
`;

export const UserDisplayName = styled.Text`
  color: ${colors.darkGray2};
  font-weight: bold;
  text-align: center;
  padding: 10px;
  font-size: 14px;
`;

export const ConnectivityWrapper = styled.View`
  justify-content: center;
  align-items: center;
  flex-direction: row;
`;

export const ConnectivityDescription = styled(Text)`
  color: ${({ isConnected }) => isConnected ? colors.successDark : colors.gray3};
  margin-right: 6px;
`;

export const ConnectivityIcon = styled(Icon).attrs(({ isConnected }) => ({
  size: 18,
  color: isConnected ? colors.successDark : colors.gray3,
  name: isConnected ? 'faWifi' : 'faWifiSlash'
}))``;
