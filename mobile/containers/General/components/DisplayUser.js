import React from 'react';
import PropTypes from 'prop-types';

import {
  DisplayUserWrapper,
  UserAvatarPlaceholder,
  UserDisplayName
} from './';

const DisplayUser = ({ currentUser }) => {
  return (
    <DisplayUserWrapper>
      <UserAvatarPlaceholder />
      <UserDisplayName>{currentUser.phoneNumber || currentUser.email}</UserDisplayName>
    </DisplayUserWrapper>
  );
};

DisplayUser.propTypes = {

};

export default DisplayUser;
