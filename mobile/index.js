import React from 'react';
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from 'core/store';

import ScreenFocusWrapper from 'components/modules/ScreenFocusWrapper';

import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation';

import { ReduxNetworkProvider } from 'react-native-offline';

import AuthManager, { Redirect } from 'containers/AuthManager';
import MainContainer from 'containers/MainContainer';
import Login from 'containers/AuthUser';

const AppStack = createStackNavigator({
  App: ScreenFocusWrapper(MainContainer)
}, {
  initialRouteName: 'App',
  headerMode: 'none'
});

const AuthStack = createStackNavigator({
  Login: Login
}, {
  initialRouteName: 'Login',
});

const RootNavigator = createAppContainer(createSwitchNavigator({
  App: AppStack,
  Login: AuthStack,
  Redirect: Redirect
}, {
  initialRouteName: 'Redirect'
}));

export default () => (
  <AuthManager>
    <Provider store={store}>
      <ReduxNetworkProvider>
        <PersistGate loading={null} persistor={persistor}>
          <RootNavigator />
        </PersistGate>
      </ReduxNetworkProvider>
    </Provider>
  </AuthManager>
);
