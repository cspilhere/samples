import { combineReducers } from 'redux';

import { reducer as network } from 'react-native-offline';

import main from 'containers/MainContainer/reducer';
import deliveries from 'containers/Home/reducer';
import delivery from 'containers/DeliveryDetails/reducer';

const reducers = combineReducers({
  main,
  deliveries,
  delivery,
  network
});

export default (state, action) => {
  if (action.type === 'RESET') state = undefined;
  return reducers(state, action);
};
