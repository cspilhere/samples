import { applyMiddleware, createStore } from 'redux';

import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';

import createSagaMiddleware from 'redux-saga';

import reducers from './reducers';
import sagas from './sagas';

import { createNetworkMiddleware } from 'react-native-offline';

const networkMiddleware = createNetworkMiddleware();

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['network', 'main', 'deliveries', 'delivery'],
  stateReconciler: autoMergeLevel2,
};

const persistedReducer = persistReducer(persistConfig, reducers);

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(
  persistedReducer,
  applyMiddleware(networkMiddleware, sagaMiddleware)
);

sagaMiddleware.run(sagas);

export const persistor = persistStore(store);
