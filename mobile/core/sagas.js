import { networkSaga } from 'react-native-offline';

import { all, fork } from 'redux-saga/effects';

import main from 'containers/MainContainer/sagas';
import home from 'containers/Home/sagas';
import delivery from 'containers/DeliveryDetails/sagas';

export default function* root() {
  yield all([
    fork(main),
    fork(home),
    fork(delivery),
    fork(networkSaga, { pingInterval: 20000 })
  ])
};
