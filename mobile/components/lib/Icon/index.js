import React from 'react';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

import { fas } from '@fortawesome/pro-solid-svg-icons';
import { far } from '@fortawesome/pro-regular-svg-icons';
import { fal } from '@fortawesome/pro-light-svg-icons';

const icons = { fas, far, fal };

const Icon = ({
  family,
  name,
  color,
  size
}) => {

  if (!icons[family].hasOwnProperty(name)) return null;

  const iconName = icons[family][name];

  return (
    <FontAwesomeIcon
      icon={iconName}
      color={color}
      size={size}
    />
  );
};

Icon.defaultProps = {
  family: 'far'
};

Icon.propTypes = {
  name: PropTypes.string.isRequired
};

export default Icon;
