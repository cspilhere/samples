import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components/native';

import colors from 'components/colors';

export const Text = styled.Text.attrs(({ truncate, truncateMode }) => ({
  numberOfLines: truncate ? 1 : null,
  ellipsizeMode: truncateMode && (truncateMode || 'tail')
}))`
  font-size: ${({ size }) => size || 12}px;
  color: ${({ color }) => color || colors.darkGray2};
  font-weight: ${({ bold }) => bold ? 'bold' : 'normal'};
  ${({ tagMode, tagColor }) => tagMode && `
    background-color: ${tagColor || colors.secondary};
    padding: 1px 8px 2px;
    border-radius: 100px;
  `}
`;

export const Bold = styled.Text`font-weight: bold;`;
