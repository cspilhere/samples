import React from 'react';
import PropTypes from 'prop-types';

import TextInputMask from 'react-native-text-input-mask';

const Input = (props) => {
  // onChangeText will return formatted and extracted values
  return (
    <TextInputMask
      {...props}
      refInput={props.ref}
      mask={props.mask}
      style={props.style}
    />
  );
};

Input.propTypes = {
  mask: PropTypes.string
};

export default Input;
