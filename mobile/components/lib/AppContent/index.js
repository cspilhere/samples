import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components/native';

const Container = styled.View`
  flex: 1;
  padding: 20px 15px;
`;

const AppContent = ({ children }) => {
  return (
    <Container>
      {children}
    </Container>
  );
};

AppContent.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ]).isRequired
};

export default AppContent;
