import React from 'react';
import PropTypes from 'prop-types';

export let blockScreen = null;

import utils from 'components/utils';

const defaultState = {
  title: null,
  description: null,
  onConfirm: null,
  onCancel: null,
  enableConfirmActionAfter: null,
  isOpen: false,
  seconds: null,
  isConfirmActionEnabled: true
};

import {
  StatusBar,
  KeyboardAvoidingView,
  View,
  StyleSheet,
  Alert,
  NativeModules,
  ActivityIndicator,
  Text,
  Image,
  TextInput
} from 'react-native';

class BlockScreenContainer extends React.PureComponent {

  constructor() {
    super();
    this.state = {
      ...defaultState
    };
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidMount() {
    this._isMounted = true;
    blockScreen = this.setUp;
  }

  render() {

    const {
      content,
      isOpen,
      enableConfirmActionAfter,
      seconds,
      isConfirmActionEnabled
    } = this.state;

    const time = seconds;

    if (!isOpen) return null;

    return (
      <View style={{ flex: 1, position: 'absolute', top: 0, left: 0, zIndex: 9999999, width: utils.deviceWidth, height: utils.deviceHeight, ...StyleSheet.absoluteFill }}>

        <View
          style={{
            position: 'absolute',
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
            top: 0,
            left: 0,
            zIndex: 100,
            width: utils.deviceWidth,
            height: utils.deviceHeight,
            ...StyleSheet.absoluteFill
          }}>
          {content}
        </View>

        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, .5)',
            position: 'absolute',
            top: 0,
            left: 0,
            zIndex: 10,
            width: utils.deviceWidth,
            height: utils.deviceHeight,
            flex: 1,
            ...StyleSheet.absoluteFill
          }}
        />

      </View>
    );
  }

  timer = (time) => {
    time = time / 1000;
    const { seconds } = this.state;
    if (seconds <= time && seconds >= 0) {
      if (this._isMounted) {
        this.setState((state) => ({ seconds: state.seconds - 1, isConfirmActionEnabled: seconds < 2 }));
      }
    } else {
      clearInterval(this.interval);
    }
  }

  setUp = ({ content, onConfirm, onCancel, enableConfirmActionAfter }) => {
    this.setState({
      content,
      onConfirm,
      onCancel,
      enableConfirmActionAfter,
      // For internal usage
      seconds: enableConfirmActionAfter / 1000,
      isConfirmActionEnabled: !(enableConfirmActionAfter && enableConfirmActionAfter > 0),
      isOpen: true
    });

    if (enableConfirmActionAfter) {
      this.interval = setInterval(() => this.timer(enableConfirmActionAfter), 1000);
    }

    return {
      close: this.close
    };
  }

  confirm = () => {
    if (this.state.onConfirm) this.state.onConfirm();
  }

  close = () => {
    if (this._isMounted) {
      this.setState(defaultState, () => {
        clearInterval(this.interval);
        if (typeof callback === 'function') callback();
        if (this.state.onClose) this.state.onClose();
      });
    }
  }

};

export default BlockScreenContainer;
