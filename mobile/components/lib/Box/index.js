import React from 'react';
import PropTypes from 'prop-types';

import styled, { css } from 'styled-components/native';

const Box = styled.View`
  background-color: #FFF;
  padding: 10px;
  border-radius: 3px;
  ${({ elevated }) => elevated && css`elevation: 4;`}
`;

Box.propTypes = {};

export default Box;
