import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components/native';

const Container = styled.View`
  flex: 1;
`;

const AppViewWrapper = ({ children }) => {
  return (
    <Container>
      {children}
    </Container>
  );
};

AppViewWrapper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ]).isRequired
};

export default AppViewWrapper;
