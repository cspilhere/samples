import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components/native';

import colors from 'components/colors';

export const ListContainer = styled.View`
  border-bottom: 1px solid #CCC;
`;

export const ListHeader = styled.View`

`;

export const ListItem = styled.View`
  border-top: 1px solid #CCC;
`;
