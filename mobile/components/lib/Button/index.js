import React from 'react';

import styled, { css } from 'styled-components/native';

import invertColor from 'invert-color';

import { Keyboard } from 'react-native';

import { TouchableOpacity } from 'react-native-gesture-handler';

import colors from 'components/colors';

const Wrapper = styled(TouchableOpacity).attrs({
  activeOpacity: .5
})`${({

  vOffset,
  hOffset,
  align,

  themeColor,

  /**
   * States/Themes
   */
  primary,
  secondary,
  success,
  warning,
  danger,
  disabled

}) => css`
  min-width: 40px;
  min-height: 40px;
  margin-top: ${vOffset || 0}px;
  margin-bottom: ${vOffset || 0}px;
  margin-left: ${hOffset || 0}px;
  margin-right: ${hOffset || 0}px;
  justify-content: center;
  align-items: ${align};
  border-radius: 4px;
  ${themeColor && css`background-color: ${themeColor};`}
`}`;

const Content = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const Label = styled.Text``;

const Button = (props) => {

  const {
    onPress,
    children,
    style,
    render,
    labelColorSuggestionThreshold,
    ...rest
  } = props;

  switch (rest.align) {
    case 'left':
      rest.align = 'flex-start';
      break;
    case 'right':
      rest.align = 'flex-end';
      break;
    default:
      rest.align = 'center';
      break;
  }

  let themeColor = null;

  if (rest.primary) themeColor = colors.primary;
  if (rest.secondary) themeColor = colors.secondary;
  if (rest.success) themeColor = colors.success;
  if (rest.warning) themeColor = colors.warning;
  if (rest.danger) themeColor = colors.danger;
  if (rest.disabled) themeColor = colors.disabled;

  const invertColorConfig = {
    black: '#000',
    white: '#FFF',
    threshold: labelColorSuggestionThreshold || .4
  };

  return (
    <Wrapper
      {...rest}
      themeColor={themeColor}
      style={style}
      onPress={!rest.disabled ? (() => {
        // Keyboard.dismiss();
        onPress();
      }) : null}>
      <Content>
        {typeof children === 'string' ? (
          <Label>
            {children}
          </Label>
        ) : render ? render({
          ...rest,
          labelColorSuggestion: invertColor(themeColor, invertColorConfig)
        }) : children}
      </Content>
    </Wrapper>
  );

};

Button.defaultProps = {
  onPress: () => {}
};

export default Button;
