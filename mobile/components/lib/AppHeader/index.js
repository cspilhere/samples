import React from 'react';
import PropTypes from 'prop-types';
import invertColor from 'invert-color';
import styled from 'styled-components/native';

import { Button, Icon } from 'components';

const Container = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 15px;
`;

const Left = styled.View`
  min-width: 10%;
`;

const Center = styled.View`
  flex: 1;
  justify-content: center;
  align-items: ${({ align }) => align};
`;

const Right = styled.View`
  min-width: 10%;
`;

const AppHeader = ({ backgroundColor, left, onPressBackButton, right, align, children }) => {

  switch (align) {
    case 'left':
      align = 'flex-start';
      break;
    case 'right':
      align = 'flex-end';
      break;
    default:
      align = 'center';
      break;
  }

  return (
    <Container style={{ backgroundColor }}>
      <Left>
        {!left ? (
          <Button align="left" vOffset={-10} onPress={onPressBackButton}>
            <Icon
              name="faArrowLeft"
              color={invertColor(backgroundColor || '#FFFFFF', true)}
            />
          </Button>
        ) : left}
      </Left>
      <Center align={align}>{children}</Center>
      <Right>{right}</Right>
    </Container>
  );
};

AppHeader.propTypes = {
  left: PropTypes.element,
  right: PropTypes.element,
  children: PropTypes.element,
  align: PropTypes.string,
  onPressBackButton: PropTypes.func
};

export default AppHeader;
