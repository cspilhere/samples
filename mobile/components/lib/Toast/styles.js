import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  base: {
    width: 300,
    height: 54,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 14,
    paddingRight: 14,
    backgroundColor: '#FFF',
    borderRadius: 3,
    // borderWidth: 1,
    borderColor: '#F2F2F2',

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  success: {
    backgroundColor: '#3dcc91',
    borderColor: '#0a6640'
  },
  warning: {
    backgroundColor: '#ffc940',
    borderColor: '#a67908'
  },
  danger: {
    backgroundColor: '#ff7373',
    borderColor: '#a82a2a'
  }
});
