import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { PanGestureHandler, TapGestureHandler, State } from 'react-native-gesture-handler';

import { Animated, Text, View } from 'react-native';

const Swipeable = ({ children, state, onOffsetLimit = () => {} }) => {

  const [translateX, setXTranslation] = useState(new Animated.Value(0));

  const lastOffset = { x: 0, y: 0 };

  const gestureEventHandler = Animated.event([{
    nativeEvent: {
      translationX: translateX
    },
  }], { useNativeDriver: true });

  const stateChangeHandler = (event) => {
    if (event.nativeEvent.oldState === State.ACTIVE) {
      if (event.nativeEvent.translationX > 0 || event.nativeEvent.translationX < -0) {
        onOffsetLimit();
      } else {
        lastOffset.x += event.nativeEvent.translationX;
        translateX.setOffset(lastOffset.x);
        translateX.setValue(0);
      }
    }
  };

  return  (
    <PanGestureHandler
      onGestureEvent={gestureEventHandler}
      onHandlerStateChange={stateChangeHandler}>
      <Animated.View
        style={{
          ...styles.base,
          ...styles[state],
          translateX: translateX
        }}>
        {children}
      </Animated.View>
    </PanGestureHandler>
  );
};

import styles from './styles';

export let toast = null;

const defaultState = {
  content: null,
  state: null,
  onClose: null,
  isOpen: false,
  seconds: null,
  fade: new Animated.Value(0),
  translateY: new Animated.Value(-100)
};

class ToastContainer extends React.Component {

  state = defaultState

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidMount() {
    this._isMounted = true;
    toast = this.setUp;
  }

  render() {

    const {
      content,
      state,

      isOpen,
      seconds,

      translateY,
      fade

    } = this.state;

    if (!isOpen) return null;

    return (
      <View style={{ flex: 1, width: '100%', position: 'absolute', zIndex: 100, alignItems: 'center' }}>
        <TapGestureHandler
          onHandlerStateChange={(event) => {
            if (event.nativeEvent.oldState === State.ACTIVE) {
              this.reverseAnimation(this.close);
            }
          }}>
          <Animated.View
            style={{
              position: 'absolute',
              top: 20,
              zIndex: 9999999,
              translateY: translateY,
              opacity: fade,
            }}>
            <Swipeable
              state={state}
              onOffsetLimit={() => this.reverseAnimation(this.close)}>
              <Text style={{ color: '#FFF', fontSize: 14 }}>{content}</Text>
            </Swipeable>
          </Animated.View>
        </TapGestureHandler>
      </View>
    );
  }

  timer = (time) => {

    if (!this.state.isOpen) {
      clearInterval(this.interval);
      return;
    }

    time = time / 1000;
    const { seconds } = this.state;
    if (seconds <= time && seconds >= 0) {
      if (this._isMounted) {
        this.setState((state) => ({ seconds: state.seconds - 1 }));
      }
    } else {
      this.reverseAnimation(this.close);
      clearInterval(this.interval);
    }

  }

  setUp = ({ content, state, onClose, timer }) => {


    if (this.state.isOpen) {
      this.reverseAnimation(() => {
        this.close(() => {
          this.setUp({ content, state, onClose, timer });
        });
      });
      return;
    }

    this.setState({
      content,
      state,
      onClose,
      timer,
      // For internal usage
      seconds: timer / 1000,
      isOpen: true,

      fade: new Animated.Value(0),
      translateY: new Animated.Value(-100)

    }, () => {
      Animated.parallel([
        Animated.spring(this.state.translateY, {
          toValue: 0,
          duration: 300
        }),
        Animated.timing(this.state.fade, {
          toValue: 1,
          duration: 300
        })
      ], { useNativeDriver: true }).start();
    });


    if (timer) {
      this.interval = setInterval(() => this.timer(timer), 1000);
    }

    return {
      close: (callback, immediately) => {
        if (immediately) {
          this.close(callback);
          return;
        }
        this.reverseAnimation(() => this.close(callback));
      }
    };

  }

  close = (callback) => {
    if (this._isMounted) {
      this.setState(defaultState, () => {
        clearInterval(this.interval);
        if (typeof callback === 'function') callback();
        if (this.state.onClose) this.state.onClose();
      });
    }
  }

  reverseAnimation = (afterFinish) => {
    Animated.parallel([
      Animated.timing(this.state.translateY, {
        toValue: -100,
        duration: 0,
        delay: 200
      }),
      Animated.timing(this.state.fade, {
        toValue: 0,
        duration: 200
      })
    ], { useNativeDriver: true }).start(afterFinish);
  }

};

export default ToastContainer;
