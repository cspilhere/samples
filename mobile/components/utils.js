import { Dimensions, Platform, PixelRatio } from 'react-native';

const { width, height } = Dimensions.get('window');

export default {
  deviceWidth: width,
  deviceHeight: height,
  getPixelSize: (pixels) => Platform.select({
    ios: pixels,
    android: PixelRatio.getPixelSizeForLayoutSize(pixels)
  })
};
