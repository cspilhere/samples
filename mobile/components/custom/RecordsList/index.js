import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components/native';

import utils from 'components/utils';
import colors from 'components/colors';

import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

import { Text, Icon } from 'components';

const ScrollContainer = styled.ScrollView`
  flex: 1;
  margin: 15px 0;
`;

const Container = styled.View`
  flex: 1;
`;

const ThumbsWrapper = styled.View`
  flex: 1;
  width: 100%;
  flex-direction: row;
  flex-wrap: wrap;
  margin-top: -2px;
`;

const Thumb = styled.View`
  margin: 2px;
`;

const ThumbTitle = styled.View`
  position: absolute;
  z-index: 10;
  bottom: 5px;
  right: 5px;
`;

const Image = styled.Image`
  background-color: ${colors.primaryDark};
  border-radius: 3px;
  width: ${(utils.deviceWidth / 3) - 14}px;
  height: ${(utils.deviceWidth / 3) - 14}px;
`;

const EmptyPlaceholder = styled.View`
  flex: 1;
  width: 100%;
  justify-content: center;
  align-items: center;
`;

const RecordsList = ({ data, alreadyFetched, loading, onPress }) => {

  const hasData = data && data.length > 0;

  const records = data.map((item, index) => {

    let type = {};

    switch (item.type) {
      case 'damage':
        type.label = 'Avaria';
        type.color = '#ff7373';
        break
      case 'gathering':
        type.label = 'Coleta';
        type.color = '#1A497B';
        break
      case 'receipt':
        type.label = 'Comprovante';
        type.color = '#669eff';
        break
    }

    return (
      <Thumb key={item.id}>
        <TouchableWithoutFeedback onPress={onPress}>
          <>
            <ThumbTitle>
              <Text color="white" tagMode tagColor={type.color}>
                {type.label}
              </Text>
            </ThumbTitle>
            <Image
              progressiveRenderingEnabled={true}
              source={{ uri: item.attachment.url }}
              resizeMode="cover"
            />
          </>
        </TouchableWithoutFeedback>
      </Thumb>
    );
  });

  return records.length > 0 ? (
    <ScrollContainer>
      <ThumbsWrapper>{records}</ThumbsWrapper>
    </ScrollContainer>
  ) : (
    <Container>
      <EmptyPlaceholder>
        <Icon name="faImages" size={74} color={colors.lightGray1} />
        {alreadyFetched && !hasData && <Text>Nenhum registro encontrado</Text>}
        {alreadyFetched && loading && <Text>Carregando...</Text>}
      </EmptyPlaceholder>
    </Container>
  );

};

RecordsList.propTypes = {};

export default RecordsList;
