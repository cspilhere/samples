import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components/native';

import moment from 'moment';

import colors from 'components/colors';

import { Icon } from 'components';

const Wrapper = styled.View`
  width: 100%;
  margin-top: 5px;
`;

const DescriptionWrapper = styled.View`
  width: 100%;
  align-items: center;
  flex-direction: row;
  padding: 2px;
`;

const Description = styled.Text`
  font-size: 13px;
  font-weight: bold;
  flex: 1;
  margin-left: 2px;
  color: ${({ color }) => color || colors.darkGray1};
`;

const FormatedDate = styled.Text`
  font-size: 13px;
  color: ${({ color }) => color || colors.darkGray1};
`;

const Bar = styled.View`
  width: 100%;
  height: 6px;
  margin-top: 2px;
  border-radius: 100px;
  background-color: ${({ color }) => color || '#666'};
`;

const Progress = styled.View`
  width: ${({ value }) => value || 0}%;
  height: 6px;
  border-radius: 100px;
  background-color: ${({ color }) => color || '#F2F2F2'};
`;

const Estimation = (props) => {

  const { data, releasedAt, isFinished } = props;

  if (!data) return null;

  let estimation = data;

  if (typeof data == 'number') estimation = moment.unix(data);

  const today = moment().startOf('day');
  const future = moment(estimation).endOf('day');
  const diff = future.diff(today, 'days');
  const releasedDate = moment(releasedAt).startOf('day');

  const timelineDiff = future.diff(releasedDate, 'days');

  const timelineValue = (diff * 100) / timelineDiff;

  let message = `${diff} dias para concluir a entrega`;

  const isLate = diff < 0;

  if (diff < 0) {
    message = `A entrega está atrasada`;
  }

  if (isFinished) {
    message = `A entrega está concluída`;
  }

  return (
    <Wrapper>
      <DescriptionWrapper>
        <Icon name="faClock" size={14} color={isLate ? colors.dangerDark : null} />
        <Description color={isLate && colors.dangerDark}>{message}</Description>
        <FormatedDate color={isLate && colors.dangerDark}>
          {moment(estimation).format('DD/MM/YYYY HH:mm')}
        </FormatedDate>
      </DescriptionWrapper>
      {releasedAt && (
        <Bar color={colors.gray3}>
          <Progress value={timelineValue}  color={colors.lightGray1}/>
        </Bar>
      )}
    </Wrapper>
  );
};

Estimation.propTypes = {};

export default Estimation;
