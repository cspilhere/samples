import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components/native';

import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

import { Box, Text, Bold } from 'components';

import Estimation from './components/Estimation';

const Container = styled.View`
  padding-top: 2px;
`;

const Identifier = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const Description = styled.View`
  padding: 5px 2px 0;
`;

const DeliveryCard = ({ data, onPress, truncate, smallTag, lite, headerRight }) => {

  const {
    number,
    client,
    shipper,
    destination,
    releasedAt,
    finishedAt,
    status,
    estimation
  } = data;

  const isFinished = finishedAt && status == 'finished';

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <Container>

        <Identifier>
          <Text color="white" size={smallTag ? 12 : 14} tagMode>CTe: {number}</Text>
          {headerRight}
        </Identifier>

        <Description>
          <Text size={16} bold truncate={truncate}>{client.name}</Text>
          {!lite && (
            <>
              <Text size={14} truncate={truncate}><Bold>Destino:</Bold> {destination.address}</Text>
              <Text size={14} truncate={truncate}><Bold>Embarcador:</Bold> {shipper.name}</Text>
            </>
          )}
        </Description>

        {estimation && <Estimation isFinished={isFinished} releasedAt={releasedAt} data={estimation} />}

      </Container>
    </TouchableWithoutFeedback>
  );
};

DeliveryCard.propTypes = {

};

export default DeliveryCard;
