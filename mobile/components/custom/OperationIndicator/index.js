import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components/native';

import { ActivityIndicator } from 'react-native';

import utils from 'components/utils';

import { Box, Text, Bold } from 'components';

const Container = styled.View`
  width: ${utils.deviceWidth - 20};
  border-radius: 3px;
  height: 50px;
  padding: 10px;
  position: absolute;
  z-index: 100;
  bottom: 60px;
  right: 0;
  background-color: #FFF;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 15px;
  margin: 10px;
  elevation: 4;
`;

const Description = styled.View`
`;

const OperationIndicator = ({ visible, description }) => {

  if (!visible) return null;

  return (
    <Container>

      <Description>
        <Text>{description}</Text>
      </Description>
      <ActivityIndicator size="small" />

    </Container>
  );
};

OperationIndicator.propTypes = {

};

export default OperationIndicator;
