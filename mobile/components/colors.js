export default {
  primary: '#1A497B',
  secondary: '#1C99FE',
  primaryDark: '#1C2541',
  secondaryDark: '',

  success: '#3dcc91',
  successDark: '#0a6640',
  warning: '#ffc940',
  warningDark: '#a67908',
  danger: '#ff7373',
  dangerDark: '#a82a2a',
  disabled: '#8a9ba8',

  white: '#ffffff',

  darkGray1: '#202b33',
  darkGray2: '#293742',

  gray3: '#8a9ba8',

  lightGray: '#FAFAFA',

  lightGray1: '#ced9e0',
}

export const convertHexToRGB = (hex, opacity) => {
  hex = hex.replace('#','');
  let r = parseInt(hex.substring(0, 2), 16);
  let g = parseInt(hex.substring(2, 4), 16);
  let b = parseInt(hex.substring(4, 6), 16);
  let result = `rgba(${r}, ${g}, ${b}, ${opacity})`;
  if (!opacity) result = `rgb(${r}, ${g}, ${b})`;
  return result;
};
