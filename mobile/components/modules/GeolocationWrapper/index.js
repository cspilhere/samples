import React from 'react';

import BackgroundGeolocation from 'react-native-background-geolocation';

import BackgroundGeolocationFirebase from 'react-native-background-geolocation-firebase';
import DeviceInfo from 'react-native-device-info';

import hoistNonReactStatics from 'hoist-non-react-statics';

import moment from 'moment';

export default function GeolocationContainerWrapper(Component) {

  class GeolocationContainer extends React.Component {

    state = {
      enabled: false,
      currentPosition: {}
    }

    _isMounted = false;

    componentDidMount() {

      this._isMounted = true;

      /**
       * Firebase plugin settings
       */
      BackgroundGeolocationFirebase.configure({
        locationsCollection: 'locations',
        geofencesCollection: 'geofences'
      });

      /**
       * Setup listeners
       */
      BackgroundGeolocation.onLocation(this.onLocation, this.onError);
      BackgroundGeolocation.onEnabledChange(this.onEnabledChange);
      BackgroundGeolocation.onGeofence(this.onGeofence);

      /**
       * Setup Geolocation plugin
       */
      BackgroundGeolocation.ready({
        reset: false,
        desiredAccuracy: BackgroundGeolocation.DESIRED_ACCURACY_HIGH,
        debug: false,
        startOnBoot: true,

        stopOnTerminate: false,
        enableHeadless: true, // https://github.com/transistorsoft/react-native-background-geolocation/wiki/Android-Headless-Mode

        autoSync: true,
        autoSyncThreshold: 5, // The minimum number of persisted records the plugin must accumulate before triggering an autoSync action.
        // batchSync: true,

        foregroundService: true,
        logLevel: BackgroundGeolocation.LOG_LEVEL_OFF,

        preventSuspend: true, // iOS only
        heartbeatInterval: 60,
        maxDaysToPersist: 14,

        geofenceModeHighAccuracy: true,
        geofenceInitialTriggerEntry: true,

        notification: {
          channelName: 'Levalog location tracker',
          title: 'Viagem em andamento',
          text: 'Sua posição está sendo emitida em tempo real',
          // smallIcon: 'mipmap/my_small_icon',
          // largeIcon: 'mipmap/my_large_icon'
        },

        params: {
          device: {
            uuid: (DeviceInfo.getModel() + '-' + DeviceInfo.getSystemVersion()).replace(/[\s\.,]/g, '-'),
            model: DeviceInfo.getModel(),
            platform: DeviceInfo.getSystemName(),
            manufacturer: DeviceInfo.getManufacturer(),
            version: DeviceInfo.getSystemVersion()
          },
        },

        enableTimestampMeta: true,

        extras: {
          ...this.props.extras
        }

      }, (state) => {
        // console.log('[BackgroundGeolocation is configured and ready] - ', state.enabled);
        this.geolocationStartSuccess(state);
      });
    }

    componentWillUnmount() {
      this._isMounted = false;
      BackgroundGeolocation.removeListeners();
    }

    render() {
      if (!this._isMounted) return null;
      return (
        <Component
          {...this.props}
          {...this.state}
          stopTracking={this.stopTracking}
          startTracking={this.startTracking}
        />
      );
    }

    /**
     */
    onLocation = (location) => {
      // console.log('[location] - ', location);
      if (!location.sample) {
        if (this._isMounted) this.setState({ currentPosition: location.coords });
      }
    }

    /**
     */
    onError = (error) => {
      // console.warn('[location] ERROR - ', error);
    }

    onGeofence = (geofence) => {
      // console.log('[geofence] ', geofence.identifier, geofence.action);
    }

    /**
     */
    onEnabledChange = (enabled) => {
      // console.log('[enabledchange] - ', enabled);
      if (this._isMounted) this.setState({ enabled });
    }

    /**
     * Stop Geolocation plugin
     */
    stopTracking = () => {
      BackgroundGeolocation.stop();
    }

    /**
     * Start Geolocation plugin
     */
    startTracking = () => {
      BackgroundGeolocation.start((state) => {
        this.geolocationStartSuccess(state);

        BackgroundGeolocation.getCurrentPosition({
          params: {
            device: {
              uuid: (DeviceInfo.getModel() + '-' + DeviceInfo.getSystemVersion()).replace(/[\s\.,]/g, '-'),
              model: DeviceInfo.getModel(),
              platform: DeviceInfo.getSystemName(),
              manufacturer: DeviceInfo.getManufacturer(),
              version: DeviceInfo.getSystemVersion()
            },
          },

          enableTimestampMeta: true,

          extras: {
            ...this.props.extras
          }
        });

      });
    }

    /**
     * Execute after plugin are ready
     */
    geolocationStartSuccess = (state) => {
      BackgroundGeolocation.getCurrentPosition({ persist: true, samples: 1 }, (location) => {
        if (this._isMounted) this.setState({ currentPosition: location.coords, enabled: state.enabled });
      }, (error) => {
        // console.warn('[getCurrentPosition error] - ', error);
      });
    }

  };

  GeolocationContainer.displayName = `GeolocationContainer(${getDisplayName(Component)})`;

  GeolocationContainer.defaultProps = {
    onTrackingStateChange: () => {},
    onUpdateLocation: () => {},
  };

  hoistNonReactStatics(GeolocationContainer, Component);

  return GeolocationContainer;

};

function getDisplayName(Component) {
  return Component.displayName || Component.name || 'Component';
};
