import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components/native';

import moment from 'moment';

import colors from 'components/colors';

const Wrapper = styled.View`
  width: 100%;
`;

const DescriptionWrapper = styled.View`
  width: 100%;
  justify-content: space-between;
  flex-direction: row;
  padding: 2px;
`;

const Description = styled.Text`
  font-size: 13px;
  font-weight: bold;
  color: ${colors.darkGray1};
`;

const FormatedDate = styled.Text`
  font-size: 13px;
  color: ${colors.darkGray1};
`;

const Bar = styled.View`
  width: 100%;
  padding: 1px;
  height: 8px;
  border-radius: 100px;
  background-color: ${({ color }) => color || '#666'};
`;

const Progress = styled.View`
  width: ${({ value }) => value || 0}%;
  height: 6px;
  border-radius: 100px;
  background-color: ${({ color }) => color || '#F2F2F2'};
`;

const EstimationTimeline = (props) => {

  const { data } = props;

  if (!data) return null;

  let estimation = data;

  if (typeof data == 'number') estimation = moment.unix(data);

  // const isFinished = item.status === 'finished';

  const today = moment().startOf('day');
  const future = moment(estimation).endOf('day');
  const diff = future.diff(today, 'days');

  // console.log(data, estimation, diff);

  return (
    <Wrapper>
      <DescriptionWrapper>
        <Description>{diff} dias para concluir a entrega</Description>
        <FormatedDate>{moment(estimation).format('DD/MM/YYYY HH:mm')}</FormatedDate>
      </DescriptionWrapper>
      <Bar color={colors.success}>
        <Progress value={100}  color={colors.successDark}/>
      </Bar>
    </Wrapper>
  );
};

EstimationTimeline.propTypes = {};

export default EstimationTimeline;
