import React from 'react';

import { withNavigationFocus } from 'react-navigation';

import hoistNonReactStatics from 'hoist-non-react-statics';

export default function ScreenFocusContainer(Component) {

  const ScreenFocusWrapper = (props) => {
    if (!props.isFocused) return null;
    return <Component {...props} />;
  };

  ScreenFocusWrapper.displayName = `ScreenFocusWrapper(${getDisplayName(Component)})`;

  hoistNonReactStatics(ScreenFocusWrapper, Component);

  return withNavigationFocus(ScreenFocusWrapper);

};

function getDisplayName(Component) {
  return Component.displayName || Component.name || 'Component';
};
