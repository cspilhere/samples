import React, { useState, useEffect, useRef } from 'react';

import {
  View,
  StyleSheet
} from 'react-native';

import MapView, { Marker, Circle, PROVIDER_GOOGLE } from 'react-native-maps';

import { Container, MapContainer, MapChildContent } from './components';

import { Icon, Text } from 'components';

import { TouchableOpacity } from 'react-native-gesture-handler';

const RoundedButton = (props) => {
  const { size, iconName, onPress, secondary } = props;
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        width: size,
        height: size,
        backgroundColor: '#1A497B',
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
        elevation: 4
      }}>
      <Icon name={iconName} color={'#FFF'} size={secondary ? 20 : 30} />
    </TouchableOpacity>
  );
};

import BoxPin from 'statics/media/map-box-pin.svg';
import TruckPin from 'statics/media/map-truck-pin.svg';

import { initialCamera, mapPadding, generateMarkersIds } from './components/settings';

const Map = (props) => {

  const [isFollowing, setFollowingState] = useState(true);

  const {
    onPressMarker,
    markers,
    useGeofencesWithMarkers,
    geofencesRadius,
    currentPosition,
    onChangeFollowState,
    showDeviceMarker
  } = props;

  const mapRef = useRef();
  const mapRefReady = mapRef.current;

  const hasCurrentPosition = currentPosition && currentPosition.latitude;

  let camera = initialCamera;

  if (hasCurrentPosition) {
    camera = {
      ...initialCamera,
      center: { latitude: currentPosition.latitude, longitude: currentPosition.longitude }
    }
  }

  const changeFollowState = (value) => {
    setFollowingState(value);
    onChangeFollowState(value);
  };

  return (
    <Container>

      <MapContainer>
        <MapView
          ref={mapRef}
          provider={PROVIDER_GOOGLE}
          onPanDrag={() => changeFollowState(false)}
          initialCamera={initialCamera}
          camera={isFollowing ? camera : null}
          paddingAdjustmentBehavior={'always'}
          mapPadding={mapPadding}
          style={{ ...StyleSheet.absoluteFill }}>

          {hasCurrentPosition && showDeviceMarker && (
            <Marker identifier={`MainMarker`} coordinate={currentPosition} anchor={{ x: 0.5, y: 0.5 }}>
              <View style={{ elevation: 4 }}>
                <TruckPin width={26} height={26} />
              </View>
            </Marker>
          )}

          {hasCurrentPosition && markers.map((marker, index) => {
            let coordinates = marker.coordinates;
            if (!coordinates) return null;
            return (
              <Marker
                key={marker.identifier}
                identifier={`Marker${index}`}
                coordinate={{ latitude: coordinates.latitude * 1, longitude: coordinates.longitude * 1 }}
                onPress={onPressMarker}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Text color="white" tagMode tagColor="black">{marker.label}</Text>
                  <BoxPin width={34.532} height={40} />
                </View>
              </Marker>
            );
          })}

          {hasCurrentPosition && useGeofencesWithMarkers && markers.map((marker) => {
            let coordinates = marker.coordinates;
            if (!coordinates) return null;
            return (
              <Circle
                key={marker.identifier}
                radius={geofencesRadius}
                center={{ latitude: coordinates.latitude * 1, longitude: coordinates.longitude * 1 }}
                strokeWidth={1}
                strokeColor={'rgba(17, 183, 0, .5)'}
                fillColor={'rgba(17, 183, 0, .2)'}
              />
            );
          })}

        </MapView>
      </MapContainer>

      <MapChildContent>
        <RoundedButton
          size={48}
          iconName="faExpand"
          onPress={() => {
            changeFollowState(false);
            mapRefReady.fitToSuppliedMarkers(
              ['MainMarker', ...generateMarkersIds(markers.length)],
              { edgePadding: { ...mapPadding, top: 500, bottom: 200 } }
            );
          }}
          secondary
        />
        <RoundedButton
          size={48}
          iconName="faLocation"
          onPress={() => {
            changeFollowState(true);
            mapRefReady.animateCamera(camera);
          }}
          secondary
        />
      </MapChildContent>

    </Container>
  );

};

Map.defaultProps = {
  markers: [],
  renderChild: () => null,
  getNearestMarkerInfo: () => {},
  onChangeFollowState: () => {}
};

export default Map;
