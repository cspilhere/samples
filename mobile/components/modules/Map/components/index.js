import React from 'react';
import styled from 'styled-components/native';

import colors from 'components/colors';

import {
  StyleSheet
} from 'react-native';

export const Container = styled.View`
  flex: 1;
`;

export const MapContainer = styled.View`
  ${StyleSheet.absoluteFill}
  flex: 1;
`;

export const MapChildContent = styled.View`
  position: absolute;
  z-index: 10;
  bottom: 64px;
  right: 20px;
`;
