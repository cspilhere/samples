import { Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 0;
const LONGITUDE = 0;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export const initialCamera = {
  center: { latitude: 0, longitude: 0 },
  pitch: 0,
  heading: 0,
  altitude: 0,
  zoom: 16
};

export const initialRegion = {
  latitude: LATITUDE,
  longitude: LONGITUDE,
  latitudeDelta: LATITUDE_DELTA,
  longitudeDelta: LONGITUDE_DELTA,
};

export const mapPadding = {
  top: 200,
  bottom: 100,
  left: 100,
  right: 100
};

export const generateMarkersIds = (length) => {
  const markersIds = [];
  for (let index = 0; index < length; index++) {
    markersIds.push('Marker' + index);
  }
  return markersIds;
};
