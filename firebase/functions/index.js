const functions = require('firebase-functions');
const admin = require('firebase-admin');
const config = functions.config();

admin.initializeApp(config.firebase);

exports.companies = require('./basics/companies');

exports.deliveries = require('./basics/deliveries');

exports.records = require('./basics/records');

exports.drivers = require('./basics/drivers');

exports.clients = require('./basics/clients');
exports.shippers = require('./basics/shippers');

exports.locations = require('./basics/locations');
