const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();

const valueIsEqual = (snapshot, field) => {
  const prevData = snapshot.before.data();
  const newData = snapshot.after.data();
  return JSON.stringify(prevData[field]) === JSON.stringify(newData[field]);
};

const onCreateShipper = functions.firestore.document('shippers/{shipperId}')
.onCreate((snapshot) => {

  const data = snapshot.data();
  const objectId = snapshot.id;

  const companiesIndexes = db.collection('companiesIndexes').doc(data.companyId);

  return db.runTransaction((transaction) => {
    return transaction.get(companiesIndexes).then((doc) => {
      try {
        const fieldToUpdate = { shippers: { [data.document]: data.name } };
        return transaction.set(companiesIndexes, fieldToUpdate, { merge: true });
      } catch (error) {
        return Promise.reject(error);
      }
    });
  }).then(() => {
    return console.log('companiesIndexes update success');
  }).catch((error) => {
    return console.error('companiesIndexes update error: ', error);
  });

});

const onUpdateShipper = functions.firestore.document('shippers/{shipperId}')
.onUpdate((snapshot) => {

  const prevData = snapshot.before.data();
  const data = snapshot.after.data();
  const objectId = snapshot.after.id;

  const companiesIndexes = db.collection('companiesIndexes').doc(data.companyId);

  if (!valueIsEqual(snapshot, 'name')) {

    return db.runTransaction((transaction) => {
      return transaction.get(companiesIndexes).then((doc) => {
        try {
          const fieldToUpdate = { shippers: { [data.document]: data.name } };
          return transaction.set(companiesIndexes, fieldToUpdate, { merge: true });
        } catch (error) {
          return Promise.reject(error);
        }
      });
    }).then(() => {
      return console.log('companiesIndexes update success');
    }).catch((error) => {
      return console.error('companiesIndexes update error: ', error);
    });

  }

  return 'ok';

});

const onDeleteShipper = functions.firestore.document('shippers/{shipperId}')
.onDelete((snapshot) => {

  const data = snapshot.data();
  const objectId = snapshot.id;

  const companiesIndexes = db.collection('companiesIndexes').doc(data.companyId);

  if (!data.deliveries || data.deliveries < 1) {

    return db.runTransaction((transaction) => {
      return transaction.get(companiesIndexes).then((doc) => {
        try {
          const fieldToUpdate = { shippers: { [data.document]: admin.firestore.FieldValue.delete() } };
          return transaction.set(companiesIndexes, fieldToUpdate, { merge: true });
        } catch (error) {
          return Promise.reject(error);
        }
      });
    }).then(() => {
      return console.log('companiesIndexes update success');
    }).catch((error) => {
      return console.error('companiesIndexes update error: ', error);
    });

  }

  return 'ok';

});

module.exports = {
  onCreateShipper: onCreateShipper,
  onUpdateShipper: onUpdateShipper,
  onDeleteShipper: onDeleteShipper
};
