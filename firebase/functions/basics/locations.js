const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();

const onCreateLocation = functions.firestore.document('locations/{locationId}')
.onCreate((snapshot, context) => {
  const data = snapshot.data();
  const objectId = snapshot.id;
  const location = data.location;
  data.location.extras.createdAt = admin.firestore.FieldValue.serverTimestamp();
  return snapshot.ref.set(data);
});

const onCreateGeofence = functions.firestore.document('geofences/{geofenceId}')
.onCreate((snapshot, context) => {
  const data = snapshot.data();
  const objectId = snapshot.id;
  const location = data.location;
  data.location.extras.createdAt = admin.firestore.FieldValue.serverTimestamp();
  return snapshot.ref.set(data);
});

module.exports = {
  onCreateLocation: onCreateLocation,
  onCreateGeofence: onCreateGeofence
};
