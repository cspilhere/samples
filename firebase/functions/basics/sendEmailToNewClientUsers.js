const functions = require('firebase-functions');

const admin = require('firebase-admin');

const config = functions.config();

const helpers = require('./_helpers');

const SENDGRID_API_KEY = config.sendgrid.key;

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(SENDGRID_API_KEY);

const sendEmailToNewClientUsers = functions.firestore
.document('clients/{clientId}/users/{userId}')
.onCreate((snapshot, context) => {

  const clientId = context.params.clientId;
  const user = snapshot.data();
  const db = admin.firestore();

  helpers.saveIdsOnUserDocument(db, user, clientId, 'clientIds');

  return db.collection('clients').doc(clientId)
  .get()
  .then((clientSnapshot) => {
    const msg = {
      from: {
        name: clientSnapshot.data().name + ' - by PinMyCargo',
        email: 'hello@pinmycargo.com'
      },
      template_id: 'd-62516801215344f18331e2c03e02c0c1',
      personalizations: [{
        to: [{
          email: user.email
        }],
        dynamic_template_data: {
          name: user.name,
          clientName: clientSnapshot.data().name
        },
        subject: 'Você foi convidado!'
      }]
    };
    return sgMail.send(msg);
  })
  .then(() => console.log('Email sent to the users!'))
  .catch((error) => console.log(error));

});

module.exports = sendEmailToNewClientUsers;
