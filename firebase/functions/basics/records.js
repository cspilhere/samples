const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();

const onCreateRecord = functions.firestore.document('deliveries/{deliveryId}/records/{recordId}')
.onCreate((snapshot) => {

  const data = snapshot.data();
  const objectId = snapshot.id;
  const deliveryRef = db.collection('deliveries').doc(data.deliveryId);

  const field = {
    d: {
      recordsList: {
        [objectId]: data
      },
      records: admin.firestore.FieldValue.increment(1),
      recordsByType: {
        [data.type]: admin.firestore.FieldValue.increment(1)
      }
    }
  };

  return deliveryRef.set(field, { merge: true });

});

const onDeleteRecord = functions.firestore.document('deliveries/{deliveryId}/records/{recordId}')
.onDelete((snapshot) => {

  const data = snapshot.data();
  const objectId = snapshot.id;
  const deliveryRef = db.collection('deliveries').doc(data.deliveryId);

  const field = {
    d: {
      recordsList: {
        [objectId]: admin.firestore.FieldValue.delete()
      },
      records: admin.firestore.FieldValue.increment(-1),
      recordsByType: {
        [data.type]: admin.firestore.FieldValue.increment(-1)
      }
    }
  };

  return deliveryRef.get()
  .then((doc) => {
    if (doc.exists) {
      return deliveryRef.set(field, { merge: true });
    }
    return 'ok';
  });

});

module.exports = {
  onCreateRecord: onCreateRecord,
  onDeleteRecord: onDeleteRecord
};
