const functions = require('firebase-functions');
const admin = require('firebase-admin');
const config = functions.config();
const db = admin.firestore();

function saveIdsOnUserDocument(db, user, id, fieldName) {
  const getUserCollection = db.collection('users').doc(user.email);
  getUserCollection.get()
  .then((doc) => {
    const array = { [fieldName]: admin.firestore.FieldValue.arrayUnion(id) };
    if (!doc.exists) {
      return doc.ref.set({}).then(() => doc.ref.update(array));
    }
    doc.ref.update(array);
    return;
  })
  .catch(() => {
    console.log('Error on users when trying save the id on ' + fieldName + ' field.');
  });
}

function saveDriverIdOnCompanyDocument(db, driverId, companyId) {
  const getCompany = db.collection('companies').doc(companyId);
  getCompany.get()
  .then((doc) => {
    const array = { drivers: admin.firestore.FieldValue.arrayUnion(driverId) };
    doc.ref.update(array);
    return;
  })
  .catch(() => {
    console.log('Error when trying save the driver id on company: ' + companyId);
  });
}

module.exports = {
  saveIdsOnUserDocument: saveIdsOnUserDocument,
  saveDriverIdOnCompanyDocument: saveDriverIdOnCompanyDocument
};
