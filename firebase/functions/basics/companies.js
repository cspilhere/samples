const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();

const valueIsEqual = (snapshot, field) => {
  const prevData = snapshot.before.data();
  const newData = snapshot.after.data();
  return JSON.stringify(prevData[field]) === JSON.stringify(newData[field]);
};

const onCreateCompany = functions.firestore.document('companies/{companyId}')
.onCreate((snapshot) => {

  const data = snapshot.data();
  const objectId = snapshot.id;

  const defaultSettings = {
    labels: ['Pago', 'Pendente', 'Faturado'],
    tags: []
  };

  // When create a new company, set basic data
  return db.collection('companies').doc(objectId).update({
    logo: null,
    color: null,
    settings: {},
    labels: defaultSettings.labels,
    tags: defaultSettings.tags,
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
    account: {
      plan: 'trial', // trial, basic, intermediary, advanced, enterprise
      expired: false,
      overdue: false
    },
  })
  .then(() => {

    db.collection('companiesLog').doc(objectId).set({});

    // Create new doc on indexes collection
    db.collection('companiesIndexes').doc(objectId).set({
      clients: {},
      shippers: {},
      drivers: {},
      deliveries: 0,
    });

    // Save data from customization
    return db.collection('companiesPublicData').doc(objectId).set({
      name: data.name,
      logo: null,
      color: null,
      settings: {},
    });

  });

});

const onUpdateCompany = functions.firestore.document('companies/{companyId}')
.onUpdate((snapshot) => {

  const data = snapshot.after.data();
  const objectId = snapshot.after.id;

  const companiesPublicData = db.collection('companiesPublicData').doc(objectId);

  return db.runTransaction((transaction) => {
    return transaction.get(companiesPublicData).then((doc) => {
      try {
        if (!doc.exists) {
          throw 'Document not found';
        } else {
          if (!valueIsEqual(snapshot, 'name')) transaction.set(companiesPublicData, { name: data.name }, { merge: true });
          if (!valueIsEqual(snapshot, 'logo')) transaction.set(companiesPublicData, { logo: data.logo }, { merge: true });
          if (!valueIsEqual(snapshot, 'color')) transaction.set(companiesPublicData, { color: data.color }, { merge: true });
          if (!valueIsEqual(snapshot, 'settings')) transaction.set(companiesPublicData, { settings: data.settings }, { merge: true });
          return Promise.resolve();
        }
      } catch (error) {
        return Promise.reject(error);
      }
    });
  }).then(() => {
    return console.log('companiesPublicData update success');
  }).catch((error) => {
    return console.error('companiesPublicData update error: ', error);
  });

});

module.exports = {
  onCreateCompany: onCreateCompany,
  onUpdateCompany: onUpdateCompany
};
