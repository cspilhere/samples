const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();

const indexToRemove = (key, name) => ({ [key]: { [name]: admin.firestore.FieldValue.delete() } });

const onCreateDelivery = functions.firestore.document('deliveries/{deliveryId}')
.onCreate((snapshot) => {

  const data = snapshot.data().d;

  const companiesIndexes = db.collection('companiesIndexes').doc(data.companyId);

  const clientRef = db.collection('clients').doc(data.client.id);
  const shipperRef = db.collection('shippers').doc(data.shipper.id);
  let driverRef = null;

  if (data.driver && data.driver.id) driverRef = db.collection('drivers').doc(data.driver.id);

  const fieldToIncrement = { deliveries: admin.firestore.FieldValue.increment(1) };

  return db.runTransaction((transaction) => {
    try {
      return Promise.all([
        transaction.set(companiesIndexes, fieldToIncrement, { merge: true }),
        transaction.set(clientRef, fieldToIncrement, { merge: true }),
        transaction.set(shipperRef, fieldToIncrement, { merge: true }),
        driverRef && transaction.set(driverRef, fieldToIncrement, { merge: true })
      ]);
    } catch (error) {
      return Promise.reject(error);
    }
  }).then(() => {
    return console.log('deliveries field index update success');
  }).catch((error) => {
    return console.error('deliveries field index update error: ', error);
  });

});

const onUpdateDelivery = functions.firestore.document('deliveries/{deliveryId}')
.onUpdate((snapshot) => {

  const prevData = snapshot.before.data().d;
  const data = snapshot.after.data().d;
  const objectId = snapshot.after.id;

  const companiesIndexes = db.collection('companiesIndexes').doc(data.companyId);

  let oldDriverRef = null;
  let driverRef = null;

  if (prevData.driver) oldDriverRef = db.collection('drivers').doc(prevData.driver.id);
  if (data.driver) driverRef = db.collection('drivers').doc(data.driver.id);

  const fieldToIncrement = { deliveries: admin.firestore.FieldValue.increment(1) };
  const fieldToDecrement = { deliveries: admin.firestore.FieldValue.increment(-1) };

  return db.runTransaction((transaction) => {
    try {
      return Promise.all([

        // Add driver for the first time
        (!oldDriverRef && driverRef) && transaction.update(driverRef, fieldToIncrement),

        // Change drivers
        (oldDriverRef && prevData.driver.id !== data.driver.id) && transaction.update(driverRef, fieldToIncrement),

        // If old driver have no delivery or does not exist we remove it from indexes
        (oldDriverRef && prevData.driver.id !== data.driver.id) && oldDriverRef.get().then((doc) => {
          if (doc.exists) transaction.update(oldDriverRef, fieldToDecrement);
          if (!doc.exists) transaction.set(companiesIndexes, indexToRemove('drivers', prevData.driver.document), { merge: true });
          return Promise.resolve();
        }).catch(() => Promise.reject)

      ]);
    } catch (error) {
      return Promise.reject(error);
    }
  }).then(() => {
    return console.log('deliveries field index update success');
  }).catch((error) => {
    return console.error('deliveries field index update error: ', error);
  });

});

const onDeleteDelivery = functions.firestore.document('deliveries/{deliveryId}')
.onDelete((snapshot) => {

  const data = snapshot.data().d;
  const objectId = snapshot.id;

  const recordsCollection = db.collection('deliveries').doc(objectId).collection('records');

  const companiesIndexes = db.collection('companiesIndexes').doc(data.companyId);

  const clientRef = db.collection('clients').doc(data.client.id);
  const shipperRef = db.collection('shippers').doc(data.shipper.id);
  let driverRef = null;

  if (data.driver && data.driver.id) driverRef = db.collection('drivers').doc(data.driver.id);

  const fieldToDecrement = { deliveries: admin.firestore.FieldValue.increment(-1) };

  return db.runTransaction((transaction) => {
    try {
      return Promise.all([

        // Remove all records from delivery
        (data.records && data.records > 0) && recordsCollection.get().then((recordsSnapshot) => {
          if (recordsSnapshot.size > 0) recordsSnapshot.docs.forEach(({ ref }) => transaction.delete(ref));
          return Promise.resolve();
        }).catch(() => Promise.reject),

        // Decrement all counters
        transaction.update(companiesIndexes, fieldToDecrement),

        clientRef.get().then((doc) => {
          if (doc.exists) transaction.update(clientRef, fieldToDecrement);
          if (!doc.exists) transaction.set(companiesIndexes, indexToRemove('clients', data.client.document), { merge: true });
          return Promise.resolve();
        }).catch(() => Promise.reject),

        shipperRef.get().then((doc) => {
          if (doc.exists) transaction.update(shipperRef, fieldToDecrement);
          if (!doc.exists) transaction.set(companiesIndexes, indexToRemove('shippers', data.shipper.document), { merge: true });
          return Promise.resolve();
        }).catch(() => Promise.reject),

        driverRef && driverRef.get().then((doc) => {
          if (doc.exists) transaction.update(driverRef, fieldToDecrement);
          if (!doc.exists) transaction.set(companiesIndexes, indexToRemove('drivers', data.driver.document), { merge: true });
          return Promise.resolve();
        }).catch(() => Promise.reject)

      ]);
    } catch (error) {
      return Promise.reject(error);
    }
  }).then(() => {
    return console.log('deliveries remove success');
  }).catch((error) => {
    return console.error('deliveries remove error: ', error);
  });

});

module.exports = {
  onCreateDelivery: onCreateDelivery,
  onUpdateDelivery: onUpdateDelivery,
  onDeleteDelivery: onDeleteDelivery
};
