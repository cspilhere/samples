const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();

/**
 * SendPulse config
 */
const sendpulse = require('sendpulse-api');
const API_USER_ID = '0978aac84a2c09255256a5a77569a1d4';
const API_SECRET = 'f18cd36dbe389b1fe2a8efd210c22d71';
const TOKEN_STORAGE = '/tmp/';
sendpulse.init(API_USER_ID, API_SECRET,TOKEN_STORAGE, () => {});

const onCreateDriver = functions.firestore.document('drivers/{driverId}')
.onCreate((snapshot) => {

  const data = snapshot.data();
  const objectId = snapshot.id;

  const companyIndexesRef = db.collection('companiesIndexes').doc(data.companyId);

  const indexToUpdate = { drivers: { [data.document]: data.name } };

  return companyIndexesRef.set(indexToUpdate, { merge: true }).then(() => {
    return sendpulse.smsSend((data) => console.log('sendpulse: ', data),
    'Levalog',
    [data.phone],
    `Olá ${
      data.name
    }, você foi convidado para utilizar o Levalog Motorista. Se você ainda não tem o APP instalado clique aqui: LINK PLAY STORE.`);
  });

});

const onUpdateDriver = functions.firestore.document('drivers/{driverId}')
.onUpdate((snapshot) => {

  const prevData = snapshot.before.data();
  const data = snapshot.after.data();
  const objectId = snapshot.after.id;

  const companyIndexesRef = db.collection('companiesIndexes').doc(data.companyId);

  if (prevData.name !== data.name || (!prevData.name && data.name)) {
    const indexToUpdate = { drivers: { [data.document]: data.name } };
    return companyIndexesRef.set(indexToUpdate, { merge: true });
  }

  return 'ok';

});

const onDeleteDriver = functions.firestore.document('drivers/{driverId}')
.onDelete((snapshot) => {

  const data = snapshot.data();
  const objectId = snapshot.id;
  const companyIndexesRef = db.collection('companiesIndexes').doc(data.companyId);

  if (!data.deliveries) {
    const field = { drivers: { [data.document]: admin.firestore.FieldValue.delete() } };
    return companyIndexesRef.set(field, { merge: true });
  }

  return 'ok';

});

module.exports = {
  onCreateDriver: onCreateDriver,
  onUpdateDriver: onUpdateDriver,
  onDeleteDriver: onDeleteDriver
};
